//=============================================================================
// POST_AcqMode.h
//=============================================================================
// abstraction.......POST acquisition mode abstraction
// class.............POST_AcqMode
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _POST_ACQ_MODE_H_
#define _POST_ACQ_MODE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "AcqMode.h"

namespace aicontroller
{

// ============================================================================
// class: POST_AcqMode
// This class provides specific behavior for POST trigger mode.
// ============================================================================
class POST_AcqMode : public AcqMode
{
public:

  //- constructor
  POST_AcqMode(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~POST_AcqMode();
  
  //---------------------------------------------------------------------------
  //--- Local methods
  //---------------------------------------------------------------------------

  //- Stops current acquisition
  void stopAcquisition ()
    throw (Tango::DevFailed);

  //- Aborts current acquisition
  void abortAcquisition ()
    throw (Tango::DevFailed);

  //- Sets new ReTrig counter
  virtual void setReTrigCnt(yat::uint32 cnt)
    throw (Tango::DevFailed);

  //- Gets current ReTrig counter
  virtual yat::uint32 getReTrigCnt()
  {
    return (this->m_reTrigCnt);
  }

  //- Sets new number of data buffers
  virtual void setDataBufferNb(yat::uint32 nb)
    throw (Tango::DevFailed);

  //- Gets current number of data buffers
  virtual yat::uint32 getDataBufferNb()
  {
    return (this->m_dataBufferNb);
  }

  //- Sets new buffer depth
  virtual void setBufferDepth(yat::uint32 nb)
    throw (Tango::DevFailed);

  //- Gets current buffer depth
  virtual yat::uint32 getBufferDepth()
  {
    return (this->m_bufferDepth);
  }

  //- Sets new data mask (time slot format)
  virtual void setDataMask (std::string mask)
    throw (Tango::DevFailed);

  //- read callback function for number of data buffers
  void read_callback_data_buffer_nb(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for number of data buffers
  void write_callback_data_buffer_nb(yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- read callback function for buffer depth
  void read_callback_buffer_depth(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for buffer depth
  void write_callback_buffer_depth(yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- read callback function for data mask
  void read_callback_data_mask(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for data mask
  void write_callback_data_mask(yat4tango::DynamicAttributeWriteCallbackData& cbd);

protected:
  
  //- Extracts specific acquisition definition from raw config according
  //- to acquisition mode
  //- Sets m_acqDefinition
  virtual void extractSpecificAcqDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Extracts specific data treatment definition from raw config
  virtual void extractSpecificDataTrtDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Sets specific configuration depending on acquisition mode
  virtual void specificAIconfigMapper_i()
      throw (Tango::DevFailed);

  //- Creates specific dynamic attributes according to acquisition mode
  virtual void define_specific_attributes_i(std::vector<yat4tango::DynamicAttributeInfo>& dynAttrList);

  //- Data processing user hook.
  virtual void handle_input_i(asl::AIRawData* raw_data)
    throw (Tango::DevFailed);

  //- Data lost user hook.
  void handle_data_lost_i()
    throw (Tango::DevFailed);

  //- Timeout user hook.
  void handle_timeout_i()
    throw (Tango::DevFailed);

  //- DAQ end user hook.
  void handle_daq_end_i()
    throw (Tango::DevFailed);

  //- Initializes specific attributes
  virtual void initSpecificAttributes_i()
    throw (Tango::DevFailed);

  //- Retrig counter
  yat::uint32 m_reTrigCnt;

  //- Number of data buffers (= nb of external triggers)
  yat::uint32 m_dataBufferNb;

  //- Buffer depth (= in SAFE mode, intermediate buffers will be received every 
  //- 'buffer depth' external triggers). If set to 0, no intermediate buffers,
  //- but only final one.
  yat::uint32 m_bufferDepth;

};

} // namespace aicontroller

#endif // _POST_ACQ_MODE_H_
