//=============================================================================
// BoardType.h
//=============================================================================
// abstraction.......Board type abstraction
// class.............BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _BOARD_TYPE_H_
#define _BOARD_TYPE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <asl/ContinuousAIConfig.h>
#include <yat4tango/ExceptionHelper.h>
#include "TypesAndConsts.h"

namespace aicontroller
{

// ============================================================================
// class: ADSAI2005_BoardType
// ============================================================================
class BoardType
{
public:

  //- Acquisition parameters to check
  typedef struct acqParamCheck
  {
    //- members --------------------
    //- Sampling rate in Hz
    double samplingRate;

    //- Number of active channels
    yat::uint16 activeChanNb;

    //- default constructor -----------------------
    acqParamCheck ()
      : samplingRate(0.0),
        activeChanNb(0)
    {
    }

    //- copy constructor ------------------
    acqParamCheck (const acqParamCheck& src)
    {
      *this = src;
    }

    //- operator= ------------------
    const acqParamCheck & operator= (const acqParamCheck& src)
    {
        if (this == & src) 
          return *this;

        this->samplingRate = src.samplingRate;
        this->activeChanNb = src.activeChanNb;

        return *this;
    }
  } acqParamCheck;

  //- constructor
  BoardType();

  //- destructor
  virtual ~BoardType();
  
  //- board model name as string 
  virtual std::string board_model_name () const = 0;

  //- board model as reference 
  virtual unsigned short board_model_id () const = 0;

  //- checks if acquisition parameters map with board type
  virtual void checkParameters (acqParamCheck acq_param) const
    throw (Tango::DevFailed);

  //- gets maximum possible frequency
  double getMaxFrequency()
  {
    return m_maxSamplingRate;
  }

  //- tells if board is multiplexed
  bool isBoardMultiplexed()
  {
    return m_isMultiplexed;
  }

protected:
  
  //- is board multiplexed
  bool m_isMultiplexed;

  //- board max sampling rate 
  double  m_maxSamplingRate;

  //- board min sampling rate
  double m_minSamplingRate;

  //- board max number of channels
  double m_maxChannelsNb;
};

} // namespace aicontroller

#endif // _BOARD_TYPE_H_
