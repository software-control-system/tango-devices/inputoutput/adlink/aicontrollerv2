//=============================================================================
// ADMAI2204_BoardType.h
//=============================================================================
// abstraction.......ADLINK MAI 2204 Board type
// class.............ADMAI2204_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _ADMAI2204_BOARD_H_
#define _ADMAI2204_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BoardType.h"

namespace aicontroller
{


// ============================================================================
// class: ADMAI2204_BoardType
// ============================================================================
class ADMAI2204_BoardType : public BoardType
{
public:
  //- constructor
  ADMAI2204_BoardType ();

  //- destructor
  virtual ~ADMAI2204_BoardType ();
  
  //- board model name as string 
  virtual std::string board_model_name () const
  {
    return kBOARD_TYPE_ADMAI2204_STR;
  }

  //- board model as reference 
  virtual unsigned short board_model_id () const 
  {
    return adl::DAQ2204;
  }
};

} // namespace aicontroller

#endif // _ADMAI2204_BOARD_H_
