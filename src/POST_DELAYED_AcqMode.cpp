//=============================================================================
// POST_DELAYED_AcqMode.cpp
//=============================================================================
// abstraction.......POST DELAYED acquisition mode implementation
// class.............POST_DELAYED_AcqMode
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "POST_DELAYED_AcqMode.h"

namespace aicontroller
{

//-----------------------------------------------------------------------------
//- Check nexus manager macro:
#define CHECK_NX_MANAGER \
    if (! this->m_nexusManager) \
    { \
      this->m_errorMsg = "Internal error."; \
      this->m_errorOccurred = true; \
      THROW_DEVFAILED(_CPTC("INTERNAL_ERROR"), \
                      _CPTC("request aborted - the Nexus manager isn't properly initialized"), \
                      _CPTC("POST_DELAYED_AcqMode::check_nx_manager")); \
    }
//-----------------------------------------------------------------------------

// ======================================================================
// POST_DELAYED_AcqMode::POST_DELAYED_AcqMode
// ======================================================================
POST_DELAYED_AcqMode::POST_DELAYED_AcqMode(Tango::DeviceImpl * host_device)
:AcqMode(host_device),
 m_reTrigCnt(0),
 m_dataBufferNb(0),
 m_bufferDepth(0)
{
}

// ======================================================================
// POST_DELAYED_AcqMode::~POST_DELAYED_AcqMode
// ======================================================================
POST_DELAYED_AcqMode::~POST_DELAYED_AcqMode()
{
}

// ======================================================================
// POST_DELAYED_AcqMode::extractSpecificAcqDefinition_i
// ======================================================================
void POST_DELAYED_AcqMode::extractSpecificAcqDefinition_i(RawAcquitisionConfig raw_acq_config)
  throw (Tango::DevFailed)
{
  ConfigurationParser config_parser(this->m_hostDev);

  try
  {
    // extract external trigger informations
    this->m_acqDefinition.triggerConfig.externalTrigType = 
      config_parser.extractExtTrigType(raw_acq_config.configKeyList);

    this->m_acqDefinition.triggerConfig.externalTrigEdge = 
      config_parser.extractExtTrigEdge(raw_acq_config.configKeyList);

    if (this->m_acqDefinition.triggerConfig.externalTrigType == EXT_TRG_ANALOG)
    {
      this->m_acqDefinition.triggerConfig.externalATrigInfos = 
        config_parser.extractExtATrigInfo(raw_acq_config.configKeyList);
    }

    // extract delay informations
    this->m_acqDefinition.triggerConfig.postDelay = 
      config_parser.extractPostDelay(raw_acq_config.configKeyList, TRG_POST_DELAYED);

    // extract finite mode
    this->m_bufferConfig.postFiniteMode = 
      config_parser.extractPostFiniteMode(raw_acq_config.configKeyList);
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "POST_DELAYED_AcqMode::extractSpecificAcqDefinition_i -> caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to analyse acquisition configuration (caught DevFailed)!"),
                      _CPTC("POST_DELAYED_AcqMode::extractSpecificAcqDefinition_i"));
  }
  catch (...)
  {
    ERROR_STREAM << "POST_DELAYED_AcqMode::extractSpecificAcqDefinition_i -> caught [...] Exception" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to analyse acquisition configuration (caught [...])!"),
                    _CPTC("POST_DELAYED_AcqMode::extractSpecificAcqDefinition_i"));
  }
}

// ======================================================================
// POST_DELAYED_AcqMode::setReTrigCnt
// ======================================================================
void POST_DELAYED_AcqMode::setReTrigCnt(yat::uint32 cnt)
  throw (Tango::DevFailed)
{
  //- set new retrig counter
  yat::uint32 new_cnt;

  if (this->m_bufferConfig.postFiniteMode == TRG_POST_FINITE_MODE_SAFE)
  {
    // in safe mode, take cnt into account:
    // � if cnt = 0 => infinite acquisition
    // � if cnt != 0 => finite acquisition (retrigger mechanism enabled)
    new_cnt = cnt;
  }
  else
  {
    // => in best effort mode, no retrigger mechanism => force to 0
    new_cnt = 0;
  }
  this->m_daqConfig.enable_retrigger(new_cnt);
  
  // call ContinuousAI::configure()
  try
  {
    asl::ContinuousAI::configure(this->m_daqConfig);
  }
  catch (asl::DAQException& daqe)
  {
    this->m_errorMsg = "DAQ configuration failed.";
    this->m_errorOccurred = true;

    Tango::DevFailed df = this->daqToTangoException(daqe);
    ERROR_STREAM << "POST_DELAYED_AcqMode::setReTrigCnt DevFAILED :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to configure DAQ board (caught DevFailed)!"),
                      _CPTC("POST_DELAYED_AcqMode::setReTrigCnt"));
  }
  catch (...)
  {
    this->m_errorMsg = "DAQ configuration failed.";
    this->m_errorOccurred = true;

    ERROR_STREAM << "POST_DELAYED_AcqMode::setSamplingRate caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to configure DAQ board (caught [...])!"),
                    _CPTC("POST_DELAYED_AcqMode::setReTrigCnt"));
  }
    
  //- set local value
  this->m_reTrigCnt = new_cnt;
}

// ======================================================================
// POST_DELAYED_AcqMode::setDataBufferNb
// ======================================================================
void POST_DELAYED_AcqMode::setDataBufferNb (yat::uint32 nb)
  throw (Tango::DevFailed)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  //- set retrigger counter
  this->setReTrigCnt(nb);

  //- set local value
  this->m_dataBufferNb = nb;
}

// ======================================================================
// POST_DELAYED_AcqMode::setBufferDepth
// ======================================================================
void POST_DELAYED_AcqMode::setBufferDepth (yat::uint32 nb)
  throw (Tango::DevFailed)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  // use buffer depth only in SAFE mode
  if (this->m_bufferConfig.postFiniteMode == TRG_POST_FINITE_MODE_SAFE)
  {
    if (nb)
    {
      // buffer depth is not null, enable intermediate buffer sending
      this->m_daqConfig.enable_intermediate_buffers (nb);

      // call ContinuousAI::configure()
      try
      {
        asl::ContinuousAI::configure(this->m_daqConfig);
      }
      catch (asl::DAQException& daqe)
      {
        this->m_errorMsg = "Set intermediate buffer depth failed.";
        this->m_errorOccurred = true;
        Tango::DevFailed df = this->daqToTangoException(daqe);
        ERROR_STREAM << "POST_DELAYED_AcqMode::setBufferDepth DevFAILED :\n" << df << std::endl;
        RETHROW_DEVFAILED(df,
                          _CPTC("CONFIGURATION_ERROR"),
                          _CPTC("Failed to configure DAQ board (caught DevFailed)!"),
                          _CPTC("POST_DELAYED_AcqMode::setBufferDepth"));
      }
      catch (...)
      {
        this->m_errorMsg = "Set intermediate buffer depth failed.";
        this->m_errorOccurred = true;
        ERROR_STREAM << "POST_DELAYED_AcqMode::setBufferDepth caught [...]" << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                        _CPTC("Failed to configure DAQ board (caught [...])!"),
                        _CPTC("POST_DELAYED_AcqMode::setBufferDepth"));
      }
    }
    else
    {
      // buffer depth is null, disable intermediate buffer sending
      this->m_daqConfig.disable_intermediate_buffers ();

      // call ContinuousAI::configure()
      try
      {
        asl::ContinuousAI::configure(this->m_daqConfig);
      }
      catch (asl::DAQException& daqe)
      {
        this->m_errorMsg = "Disable intermediate buffer sending failed.";
        this->m_errorOccurred = true;
        Tango::DevFailed df = this->daqToTangoException(daqe);
        ERROR_STREAM << "POST_DELAYED_AcqMode::setBufferDepth DevFAILED :\n" << df << std::endl;
        RETHROW_DEVFAILED(df,
                          _CPTC("CONFIGURATION_ERROR"),
                          _CPTC("Failed to configure DAQ board (caught DevFailed)!"),
                          _CPTC("POST_DELAYED_AcqMode::setBufferDepth"));
      }
      catch (...)
      {
        this->m_errorMsg = "Disable intermediate buffer sending failed.";
        this->m_errorOccurred = true;
        ERROR_STREAM << "POST_DELAYED_AcqMode::setBufferDepth caught [...]" << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                        _CPTC("Failed to configure DAQ board (caught [...])!"),
                        _CPTC("POST_DELAYED_AcqMode::setBufferDepth"));
      }
    }

    //- set local value
    this->m_bufferDepth = nb;
  }
  else
  {
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Buffer depth not available in this mode! Use POST finite SAFE mode instead."),
                    _CPTC("POST_DELAYED_AcqMode::setBufferDepth"));
  }
}

// ======================================================================
// POST_DELAYED_AcqMode::specificAIconfigMapper_i
// ======================================================================
void POST_DELAYED_AcqMode::specificAIconfigMapper_i()
  throw (Tango::DevFailed)
{
  //- POST DELAYED trigger mode specific configuration:

  //- external trigger type:
  switch (this->m_acqDefinition.triggerConfig.externalTrigType)
  {
    case EXT_TRG_ANALOG:
      this->m_daqConfig.set_trigger_source(adl::external_analog);
      break;
    case EXT_TRG_DIGITAL:
      this->m_daqConfig.set_trigger_source(adl::external_digital);
      break;
    default:
      this->m_errorMsg = "DAQ configuration failed.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "DAQ configuration error - TRIGGER TYPE must be defined for this trigger mode" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      _CPTC("DAQ configuration error - see logs for details"), 
                      _CPTC("POST_DELAYED_AcqMode::specificAIconfigMapper_i"));
      break;
  }

  //- external trigger edge:
  switch (this->m_acqDefinition.triggerConfig.externalTrigEdge)
  {
    case EXT_ATRIG_EDGE_FALLING:
      this->m_daqConfig.set_trigger_polarity(adl::ai_falling_edge);
      break;
    case EXT_ATRIG_EDGE_RISING:
      this->m_daqConfig.set_trigger_polarity(adl::ai_rising_edge);
      break;
    default:
      this->m_errorMsg = "DAQ configuration failed.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "DAQ configuration error - TRIGGER EDGE must be defined for this trigger mode" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      _CPTC("DAQ configuration error - see logs for details"), 
                      _CPTC("POST_DELAYED_AcqMode::specificAIconfigMapper_i"));
      break;
  }

  //- delay unit:
  switch (this->m_acqDefinition.triggerConfig.postDelay.unit)
  {
    case TRG_DELAY_UNIT_TICKS:
      this->m_daqConfig.set_delay_unit(adl::clock_ticks);
      break;
    case TRG_DELAY_UNIT_SAMPLES:
      this->m_daqConfig.set_delay_unit(adl::samples);
      break;
    default:
      this->m_errorMsg = "DAQ configuration failed.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "DAQ configuration error - DELAY UNIT must be defined for this trigger mode" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      _CPTC("DAQ configuration error - see logs for details"), 
                      _CPTC("POST_DELAYED_AcqMode::specificAIconfigMapper_i"));
      break;
  }

  //- delay value:
  this->m_daqConfig.set_middle_or_delay_scans(this->m_acqDefinition.triggerConfig.postDelay.value);

  //- if analog trigger chosen
  if (this->m_acqDefinition.triggerConfig.externalTrigType == EXT_TRG_ANALOG)
  {
    //- analog trigger source:
    switch (this->m_acqDefinition.triggerConfig.externalATrigInfos.src)
    {
      case EXT_ATRG_SRC_EXT:
        this->m_daqConfig.set_analog_trigger_source(adl::analog_trigger_ext);
        break;
      case EXT_ATRG_CHAN0:
        this->m_daqConfig.set_analog_trigger_source(adl::analog_trigger_chan0);
        break;
      case EXT_ATRG_CHAN1:
        this->m_daqConfig.set_analog_trigger_source(adl::analog_trigger_chan1);
        break;
      case EXT_ATRG_CHAN2:
        this->m_daqConfig.set_analog_trigger_source(adl::analog_trigger_chan2);
        break;
      case EXT_ATRG_CHAN3:
        this->m_daqConfig.set_analog_trigger_source(adl::analog_trigger_chan3);
        break;
      default:
        this->m_errorMsg = "DAQ configuration failed.";
        this->m_errorOccurred = true;

        ERROR_STREAM << "DAQ configuration error - ANALOG TRIGGER SRC must be defined for this trigger mode" << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        _CPTC("DAQ configuration error - see logs for details"), 
                        _CPTC("POST_DELAYED_AcqMode::specificAIconfigMapper_i"));
        break;
    }

    //- analog trigger condition type:
    switch (this->m_acqDefinition.triggerConfig.externalATrigInfos.conditionType)
    {
      case EXT_ATRG_COND_BELOW:
        this->m_daqConfig.set_analog_trigger_condition(adl::below_low_level);
        break;
      case EXT_ATRG_COND_ABOVE:
        this->m_daqConfig.set_analog_trigger_condition(adl::above_high_level);
        break;
      case EXT_ATRG_COND_REGION:
        this->m_daqConfig.set_analog_trigger_condition(adl::inside_region);
        break;
      case EXT_ATRG_COND_HYST:
        this->m_daqConfig.set_analog_trigger_condition(adl::high_hysteresis);
        break;
      default:
        this->m_errorMsg = "DAQ configuration failed.";
        this->m_errorOccurred = true;

        ERROR_STREAM << "DAQ configuration error - ANALOG TRIGGER CONDITION must be defined for this trigger mode" << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        _CPTC("DAQ configuration error - see logs for details"), 
                        _CPTC("POST_DELAYED_AcqMode::specificAIconfigMapper_i"));
        break;
    }

    //- analog trigger condition values:
    unsigned short level = 0;
    level = (unsigned short) (this->m_acqDefinition.triggerConfig.externalATrigInfos.lowLevelCond * 12.8  + 128);

    if (level > 255)
      level = 255;

    INFO_STREAM << "LOW level checked at " << level << " bit(s)" << std::endl;
    //- the voltage is converted in digital value (8 bits of resolution on EXTATRIG with +/-10 V (256/20 = 0.078125))
    this->m_daqConfig.set_analog_low_level_condition(level);

    level = (unsigned short)(this->m_acqDefinition.triggerConfig.externalATrigInfos.highLevelCond * 12.8 ) + 128;

    if (level > 255)
      level = 255;

    INFO_STREAM << "HIGH level at " << level << " bit(s)" << std::endl;
    this->m_daqConfig.set_analog_high_level_condition(level);
  }
} 

// ======================================================================
// POST_DELAYED_AcqMode::handle_input_i
// ======================================================================
void POST_DELAYED_AcqMode::handle_input_i(asl::AIRawData* raw_data)
  throw (Tango::DevFailed)
{
  yat::AutoMutex<> guard(this->m_dataLock);

  //- In POST DELAYED trigger mode, the acquisition can be:
  //  � infinite (acquisition must be stopped by user <=> dataBufferNb = 0, reTrigCnt = 0)
  //  � or finite & safe (acquisition is stopped by driver once the reTrigCnt number of ext 
  //    triggers are received <=> reTrigCnt = dataBufferNb != 0, bufferDepth = 0)
  //  � or finite & safe & intermediate buffers enabled (= idem above + intermediate
  //    buffers received each buffer depth triigers <=> reTrigCnt = dataBufferNb != 0, bufferDepth != 0)
  //  � of finite & best effort (acquisition should be internally stopped when the nb of 
  //    received data buffers equals dataBufferNb <=> dataBufferNb != 0, reTrigCnt = 0)

  //- In infinite mode OR finite & best effort mode, 
  //  1 trigger => 1 handle_input() received => 1 data buffer
  size_t nb_loop_to_do = 1;
  size_t raw_data_size = raw_data->depth();
  size_t channel_data_size = this->m_daqConfig.get_buffer_depth();

  //- test if acquisition has been internally stopped (in finite & best effort mode)
  //- but stop not yet processed by driver
  if ((this->m_dataBufferNb != 0) &&
      (0 == this->m_reTrigCnt) &&
      (this->m_stopDoneFromHandleInput))
  {
    // do nothing
    return;
  }

  // In best effort mode, get timestamp (if requested)
  bool l_timestamped = false;
  if ((this->m_bufferConfig.postFiniteMode == TRG_POST_FINITE_MODE_BEST_EFFORT) &&
      (m_dataTrt.timestamped))
  {
    l_timestamped = true;
    this->stampBuffer_s();
  }

  //- In finite & safe mode:
  if (this->m_reTrigCnt != kINFINITE_ACQ)
  {
    if (this->m_bufferDepth == 0)
    {
      // handle_input() is called when all triggers are received.
      nb_loop_to_do = this->m_reTrigCnt;
      raw_data_size /= this->m_reTrigCnt;
    }
    else
    {
      // with intermediate buffers, handle_input() is called when buffer depth 
      // triggers are received.
      // The valid data is not contained at index 0 of the received raw data, 
      // but at trigger index * raw_data_size / bufferDepth !!
      nb_loop_to_do = this->m_bufferDepth;
      raw_data_size = channel_data_size * this->m_daqConfig.num_active_channels();
      nb_loop_to_do = min(this->m_bufferDepth, this->m_dataBufferNb - this->m_acqData.dataCounter);
    }
  }

  // loop on number of triggers received
  for (size_t loop_num = 0; loop_num < nb_loop_to_do; loop_num++) 
  {
    //- initialize local variables
    double channel_average = 0.;
    double scaled_average = 0.;
    double min_data = 0.;
    double max_data = 0.;
    double rms = 0.;
    double pk = 0.;

    const asl::ActiveAIChannels& ac = this->m_daqConfig.get_active_channels();
    unsigned int nbac = this->m_daqConfig.num_active_channels();
    unsigned int nca = 0; // active channel id (for data storage)

    if (nbac == 0)
    {
      ERROR_STREAM << "Number of active channels is null!" << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                      _CPTC("Error while trying to get input data - see logs for details"), 
                      _CPTC("POST_DELAYED_AcqMode::handle_input_i"));
    }

    //- loop on active channels
    for (unsigned int chan_idx = 0;  chan_idx < ac.size();  chan_idx++) 
    {
      channel_average = 0.;
      scaled_average = 0.;
      nca = ac[chan_idx].id;

      //- get local data associated to current channel
      if (this->m_acqData.rawData.count(nca) == 0)
      {
        // bad id ==> fatal error
        this->m_errorMsg = "Internal error. Bad channel id for raw data.";
        this->m_errorOccurred = true;
        ERROR_STREAM << "Bad channel id for local raw data:" << nca << std::endl;
        THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                        _CPTC("Error while trying to get input data - see logs for details"), 
                        _CPTC("POST_DELAYED_AcqMode::handle_input_i"));
      }

      RawData_t & bRaw = this->m_acqData.rawData[nca];

      // add offset for SAFE & intermediate buffer mode
      yat::uint32 index_offset = 0;
      if ((this->m_reTrigCnt != kINFINITE_ACQ) && (this->m_bufferDepth != 0))
        index_offset = this->m_acqData.dataCounter;
		
      //- 1st loop on raw data to get scaled data & average value
      for (size_t i = 0, j=0; 
           i < raw_data_size; 
           i += nbac, j++)
      {
        //- store raw data
        size_t data_idx = (i + chan_idx) + raw_data_size * (loop_num + index_offset);
        bRaw[j] = (*raw_data)[data_idx];
        
        //- store scaled data (if required)
        if (this->m_dataTrt.isDataScaled)
        {
          //- get & set local data associated to current channel
          if (this->m_dynAcqData.scaledData.count(nca) == 0)
          {
            // bad id ==> fatal error
            this->m_errorMsg = "Internal error. Bad channel id for scaled data.";
            this->m_errorOccurred = true;

            ERROR_STREAM << "Bad channel id for local scaled data:" << nca << std::endl;
            THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                            _CPTC("Error while trying to get input data - see logs for details"), 
                            _CPTC("POST_DELAYED_AcqMode::handle_input_i"));
          }

          ScaledData_t & bScaled = this->m_dynAcqData.scaledData[nca];
          bScaled[j] = (*this->m_scaledData)[data_idx];

          //- store user data (if required)
          if (ac[chan_idx].user_data_enabled)
          {
            //- get & set local data associated to current channel
            if (this->m_dynAcqData.userData.count(nca) == 0)
            {
              this->m_errorMsg = "Internal error. Bad channel id for user data.";
              this->m_errorOccurred = true;

              // bad id ==> fatal error
              ERROR_STREAM << "Bad channel id for local user data:" << nca << std::endl;
              THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                              _CPTC("Error while trying to get input data - see logs for details"), 
                              _CPTC("POST_DELAYED_AcqMode::handle_input_i"));
            }

            UserData_t & bUData = this->m_dynAcqData.userData[nca];
            bUData[j] = (*this->m_userData)[data_idx];

            //- compute statistics upon user data if enabled
            channel_average += bUData[j];
            //- except average value: computed whatever enabled mode
            scaled_average += bScaled[j];

            if (j == 0)
            {
              min_data = bUData[j];
              max_data = bUData[j];
            }
            else
            {
              min_data = (bUData[j] < min_data) ? bUData[j] : min_data;
              max_data = (bUData[j] > max_data) ? bUData[j] : max_data;
            }
          }
          else
          {
            //- compute statistics upon scaled data if user data disabled	  		  
            channel_average += bScaled[j];

            if (j == 0)
            {
              min_data = bScaled[j];
              max_data = bScaled[j];
            }
            else
            {
              min_data = (bScaled[j] < min_data) ? bScaled[j] : min_data;
              max_data = (bScaled[j] > max_data) ? bScaled[j] : max_data;
            }
          }
        }
      } //- end of 1st loop on raw data


      //- compute & store items from scaled data (if required)
      if (this->m_dataTrt.isDataScaled)
      {
        if (this->m_dataTrt.hasStatistics)
        {
          //- compute and store average values (1 value per handle_input() received)
          channel_average /= channel_data_size;
          scaled_average /= channel_data_size;

          //- get & set local data associated to current channel
          if (this->m_dynAcqData.averageData.count(nca) == 0)
          {
            // bad id ==> fatal error
            this->m_errorMsg = "Internal error. Bad channel id for average data.";
            this->m_errorOccurred = true;

            ERROR_STREAM << "Bad channel id for local average data:" << nca << std::endl;
            THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                            _CPTC("Error while trying to get input data - see logs for details"), 
                            _CPTC("POST_DELAYED_AcqMode::handle_input_i"));
          }

          this->m_dynAcqData.averageData[nca] = channel_average;
          
          // scaled average value only available if user data enabled
          if (ac[chan_idx].user_data_enabled)
          {
            this->m_dynAcqData.scaledAvData[nca] = scaled_average;
          }

          //- compute and store rms & peak-to-peak values
          if (ac[chan_idx].user_data_enabled)
          {
            rms = rmsCalculation(this->m_dynAcqData.userData[nca], channel_average);
          }
          else
          {
            rms = rmsCalculation(this->m_dynAcqData.scaledData[nca], channel_average);
          }
          pk = max_data - min_data;

          //- get & set local data associated to current channel
          if ((this->m_dynAcqData.rms.count(nca) == 0) ||
              (this->m_dynAcqData.peakToPeak.count(nca) == 0))
          {
            // bad id ==> fatal error
            this->m_errorMsg = "Internal error. Bad channel id for statistic data.";
            this->m_errorOccurred = true;

            ERROR_STREAM << "Bad channel id for local statistic data:" << nca << std::endl;
            THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                            _CPTC("Error while trying to get input data - see logs for details"), 
                            _CPTC("POST_DELAYED_AcqMode::handle_input_i"));
          }

          this->m_dynAcqData.rms[nca] = rms;
          this->m_dynAcqData.peakToPeak[nca] = pk;


          //- store average data history
        
          //- get & set local data associated to current channel
          if (this->m_dynAcqData.averageDataHistory.count(nca) == 0)
          {
            // bad id ==> fatal error
            this->m_errorMsg = "Internal error. Bad channel id for average history.";
            this->m_errorOccurred = true;

            ERROR_STREAM << "Bad channel id for local average history:" << nca << std::endl;
            THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                            _CPTC("Error while trying to get input data - see logs for details"), 
                            _CPTC("POST_DELAYED_AcqMode::handle_input_i"));
          }

          AverageHistorizedData_t * bHistorized = this->m_dynAcqData.averageDataHistory[nca];
          bHistorized->push(channel_average);
        }

        // check if Nexus & dataset enabled
        if (this->m_nxStorageStarted && this->m_acquisitionParam.datasetFlags[nca])
        {
          //- store the AVERAGE value in Nexus File (if required)
          if (this->m_nxStoreAVERAGE && this->m_dataTrt.hasStatistics)
          {
            CHECK_NX_MANAGER;
            
            try
            {
              this->m_nexusManager->pushNexusData(m_datasetNames[nca][kKEY_DATA_NX_DATA_TYPE_AV], 
                &channel_average);
            }
            catch (Tango::DevFailed & df)
            {
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input_i -> pushNexusData caught DevFailed: " << df << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
            catch (...)
            {
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_inpu_i -> pushNexusData caught [...] Exception" << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
          }

          //- store the PEAK to PEAK value in Nexus File (if required)
          if (this->m_nxStorePEAK && this->m_dataTrt.hasStatistics)
          {
            CHECK_NX_MANAGER;
            
            try
            {
              this->m_nexusManager->pushNexusData(m_datasetNames[nca][kKEY_DATA_NX_DATA_TYPE_PEAK], &pk);
            }
            catch (Tango::DevFailed & df)
            {
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input -> pushNexusData caught DevFailed: " << df << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
            catch (...)
            {
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input -> pushNexusData caught [...] Exception" << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
          }

          //- store the RMS value in Nexus File (if required)
          if (this->m_nxStoreRMS && this->m_dataTrt.hasStatistics)
          {
            CHECK_NX_MANAGER;
            
            try
            {
              this->m_nexusManager->pushNexusData(m_datasetNames[nca][kKEY_DATA_NX_DATA_TYPE_RMS], &rms);
            }
            catch (Tango::DevFailed & df)
            {
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input -> pushNexusData caught DevFailed: " << df << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
            catch (...)
            {     
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input -> pushNexusData caught [...] Exception" << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
          }

          //- store the SCALED spectrum in Nexus File (if required)
          if (this->m_nxStoreSCALED)
          {
            CHECK_NX_MANAGER;

            try
            {
              this->m_nexusManager->pushNexusData(m_datasetNames[nca][kKEY_DATA_NX_DATA_TYPE_SCALED], 
                this->m_dynAcqData.scaledData[nca].base());
            }
            catch (Tango::DevFailed & df)
            {
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input -> pushNexusData caught DevFailed: " << df << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
            catch (...)
            {
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input -> pushNexusData caught [...] Exception" << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
          }

          //- store the USER DATA spectrum in Nexus File (if required)
          if (this->m_nxStoreUSER_DATA && ac[chan_idx].user_data_enabled)
          {
            CHECK_NX_MANAGER;

            try
            {
              this->m_nexusManager->pushNexusData(m_datasetNames[nca][kKEY_DATA_NX_DATA_TYPE_USER], 
                this->m_dynAcqData.userData[nca].base());
            }
            catch (Tango::DevFailed & df)
            {
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input_i -> pushNexusData caught DevFailed: " << df << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
            catch (...)
            {
              this->m_errorMsg = "Nexus storage error.";
              this->m_errorOccurred = true;

              ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input_i -> pushNexusData caught [...] Exception" << std::endl;
              this->m_nexusManager->manageNexusAbort();
              return;
            }
          }
        }

      } //- end if scaled


      //- store the RAW spectrum in Nexus File (if required)
      if (this->m_nxStorageStarted && this->m_nxStoreRAW && this->m_acquisitionParam.datasetFlags[nca])
      {
        CHECK_NX_MANAGER;

        try
        {
          this->m_nexusManager->pushNexusData(m_datasetNames[nca][kKEY_DATA_NX_DATA_TYPE_RAW], 
            this->m_acqData.rawData[nca].base());
        }
        catch (Tango::DevFailed & df)
        {
          this->m_errorMsg = "Nexus storage error.";
          this->m_errorOccurred = true;

          ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input -> pushNexusData caught DevFailed: " << df << std::endl;
          this->m_nexusManager->manageNexusAbort();
          return;
        }
        catch (...)
        {
          this->m_errorMsg = "Nexus storage error.";
          this->m_errorOccurred = true;

          ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input -> pushNexusData caught [...] Exception" << std::endl;
          this->m_nexusManager->manageNexusAbort();
          return;
        }
      }

    } //- end loop on number of active channels
  
  }//- end loop on number of triggers received

  //- store the TIMESTAMP data in Nexus File (if required)
  if (l_timestamped && 
      this->m_nxStorageStarted && 
      m_nxStoreTIMESTAMP)
  {
    CHECK_NX_MANAGER;

    try
    {
      this->m_nexusManager->pushNexusData(kATTR_NAME_TAG_TIMESTAMP, &this->m_acqData.bufferTimestamp_s);
      this->m_nexusManager->pushNexusData(kATTR_NAME_TAG_RELATIVE_DATE, &this->m_acqData.relBuffTimestamp_s);
    }
    catch (Tango::DevFailed & df)
    {
      this->m_errorMsg = "Nexus storage error.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input_i -> pushNexusData caught DevFailed: " << df << std::endl;
      this->m_nexusManager->manageNexusAbort();
      return;
    }
    catch (...)
    {
      this->m_errorMsg = "Nexus storage error.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input_i -> pushNexusData caught [...] Exception" << std::endl;
      this->m_nexusManager->manageNexusAbort();
      return;
    }
  }

  //- increase data counter
  if (this->m_reTrigCnt != kINFINITE_ACQ)
  {
    if (this->m_bufferDepth == 0)
    {
      // finite trigger mode
      this->m_acqData.dataCounter += this->m_reTrigCnt;
    }
    else
    {
      // finite & intermediate buffer mode
      this->m_acqData.dataCounter += nb_loop_to_do;
    }
  }
  else
  {
    // infinite trigger mode
    this->m_acqData.dataCounter++;
  }

  //- test if acquisition should be stopped (in finite & best effort mode)
  if ((this->m_dataBufferNb != 0) &&
      (0 == this->m_reTrigCnt) &&
      (this->m_acqData.dataCounter >= this->m_dataBufferNb))
  {
    // stop acquisition
    try
    {
      AcqMode::stopAcquisition();
      this->m_stopDoneFromHandleInput = true;
      INFO_STREAM << "POST_DELAYED_AcqMode::handle_input_i -> internal stop cmd sent ..." << std::endl;
    }
    catch(Tango::DevFailed& df)
    {
      this->m_errorMsg = "Stop acquisition failed.";
      this->m_errorOccurred = true;
      ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input_i caught DevFailed :\n" << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("COMMAND_FAILED"),
                        _CPTC("Failed to stop the current acquisition (caught DevFailed)!"),
                        _CPTC("POST_DELAYED_AcqMode::handle_input_i"));
    }
    catch(...)
    {
      this->m_errorMsg = "Stop acquisition failed.";
      this->m_errorOccurred = true;
      ERROR_STREAM << "POST_DELAYED_AcqMode::handle_input_i caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Failed to stop the current acquisition (caught [...])!"),
                      _CPTC("POST_DELAYED_AcqMode::handle_input_i"));
    }
  }
}


// ======================================================================
// POST_DELAYED_AcqMode::handle_data_lost_i
// ======================================================================
void POST_DELAYED_AcqMode::handle_data_lost_i()
  throw (Tango::DevFailed)
{
  INFO_STREAM << "\ttry to change to a finite acquisition - set retrig counter attribute" << std::endl;
}

// ======================================================================
// POST_DELAYED_AcqMode::handle_timeout_i
// ======================================================================
void POST_DELAYED_AcqMode::handle_timeout_i()
  throw (Tango::DevFailed)
{  
	INFO_STREAM << "\treason: trigger signal is either off or disconnected" << std::endl; 
}

// ======================================================================
// POST_DELAYED_AcqMode::handle_daq_end_i
// ======================================================================
void POST_DELAYED_AcqMode::handle_daq_end_i()
  throw (Tango::DevFailed)
{
  //- get data history information (if required & if any)
  if ((this->m_dataTrt.isDataScaled) &&
      (this->m_dataTrt.isDataHistorized) &&
      (this->m_acqData.dataCounter != 0))
  {
    //- get data history for all channels & update data structure

    const asl::ActiveAIChannels& ac = this->m_daqConfig.get_active_channels();
    unsigned int chan_id = 0; // active channel id (for data storage)

    this->computeWholeDataHistory();
      
    //- store the DATA HISTORY spectrum in Nexus File (if required)
    if (this->m_nxStorageStarted && 
        this->m_nxStoreDATA_HIST)
    {
      CHECK_NX_MANAGER;

      //- loop on active channels
      for (unsigned int chan_idx = 0;  chan_idx < ac.size();  chan_idx++) 
      {
        chan_id = ac[chan_idx].id;

        // check if dataset enabled
        if (this->m_acquisitionParam.datasetFlags[chan_id])
        {
          try
          {
            this->m_nexusManager->pushNexusData(m_datasetNames[chan_id][kKEY_DATA_NX_DATA_TYPE_DATA_HIST], 
              this->m_dynAcqData.dataHistory[chan_id]);
          }
          catch (Tango::DevFailed & df)
          {
            this->m_errorMsg = "Nexus storage error.";
            this->m_errorOccurred = true;

            ERROR_STREAM << "POST_DELAYED_AcqMode::handle_daq_end_i -> pushNexusData caught DevFailed: " << df << std::endl;
            this->m_nexusManager->manageNexusAbort();
            return;
          }
          catch (...)
          {
            this->m_errorMsg = "Nexus storage error.";
            this->m_errorOccurred = true;

            ERROR_STREAM << "POST_DELAYED_AcqMode::handle_daq_end_i -> pushNexusData caught [...] Exception" << std::endl;
            this->m_nexusManager->manageNexusAbort();
            return;
          }
        }
      } // end of loop on active channels
    } 
  }
}

// ======================================================================
// POST_DELAYED_AcqMode::define_specific_attributes_i
// ======================================================================
void POST_DELAYED_AcqMode::define_specific_attributes_i(std::vector<yat4tango::DynamicAttributeInfo>& dynAttrList)
{
  //----->>>>>>>>>>  Add dynamic attributes for each active channel  <<<<<<<<<<-----
  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    std::string chanLabel = this->m_acqDefinition.activeChannels[chan].label;
    std::string chanDescription = this->m_acqDefinition.activeChannels[chan].description;

    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.hasUserData)
    {
      //-------------------
      //- user data gain:
      //-------------------
      yat4tango::DynamicAttributeInfo dai10;

      //- attribute definition:
      dai10.dev = this->m_hostDev;

      dai10.tai.name = aicontroller::kATTR_NAME_USER_DATA_GAIN + chanLabel;
      dai10.tai.label = dai10.tai.name; 
      dai10.tai.data_format = Tango::SCALAR;
      dai10.tai.data_type = Tango::DEV_DOUBLE;
      dai10.tai.disp_level = Tango::OPERATOR;
      dai10.tai.description = "user data gain - formula is: user data = ((scaled data - offset1)/gain) - offset2";

      //- attribute properties:
      dai10.tai.unit = "V per " + this->m_acqDefinition.activeChannels[chan].userDataDesc.unit;
      dai10.tai.standard_unit = "V per " + this->m_acqDefinition.activeChannels[chan].userDataDesc.unit;
      dai10.tai.display_unit = "V per " + this->m_acqDefinition.activeChannels[chan].userDataDesc.unit;
      dai10.tai.format = "%6.2e";

      dai10.cdb = true;

      switch(this->m_acqDefinition.activeChannels[chan].userDataDesc.gainType)
      {
        case USER_DATA_ATTR: // attribute => R/W
          dai10.tai.writable = Tango::READ_WRITE;

          //- read callback
          dai10.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                           &AcqMode::read_callback_user_data_gain);

          //- write callback
          dai10.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                            &AcqMode::write_callback_user_data_gain);

          //- add attribute description in list
          dynAttrList.push_back(dai10);
          break;

        case USER_DATA_PROXY: // proxy => read only attribute
          dai10.tai.writable = Tango::READ;

          //- read callback
          dai10.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                          &AcqMode::read_callback_user_data_gain);

          //- add attribute description in list
          dynAttrList.push_back(dai10);
          break;
        default:
          // do nothing
          break;
      }

      //---------------------
      //- user data offset1:
      //---------------------
      yat4tango::DynamicAttributeInfo dai11;

      //- attribute definition:
      dai11.dev = this->m_hostDev;

      dai11.tai.name = aicontroller::kATTR_NAME_USER_DATA_OFS1 + chanLabel;
      dai11.tai.label = dai11.tai.name; 
      dai11.tai.data_format = Tango::SCALAR;
      dai11.tai.data_type = Tango::DEV_DOUBLE;
      dai11.tai.disp_level = Tango::OPERATOR;
      dai11.tai.description = "user data offset1 - formula is: user data = ((scaled data - offset1)/gain) - offset2";

      //- attribute properties:
      dai11.tai.unit = "V";
      dai11.tai.standard_unit = "V";
      dai11.tai.display_unit = "V";
      dai11.tai.format = "%6.2e";

      dai11.cdb = true;

      switch(this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Type)
      {
        case USER_DATA_ATTR: // attribute => R/W
          dai11.tai.writable = Tango::READ_WRITE;

          //- read callback
          dai11.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                          &AcqMode::read_callback_user_data_offset1);

          //- write callback
          dai11.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                          &AcqMode::write_callback_user_data_offset1);

          //- add attribute description in list
          dynAttrList.push_back(dai11);
          break;

        case USER_DATA_PROXY: // proxy => read only attribute
          dai11.tai.writable = Tango::READ;

          //- read callback
          dai11.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                          &AcqMode::read_callback_user_data_offset1);

          //- add attribute description in list
          dynAttrList.push_back(dai11);
          break;
        default:
          // do nothing
          break;
      }

      //---------------------
      //- user data offset2:
      //---------------------
      yat4tango::DynamicAttributeInfo dai12;

      //- attribute definition:
      dai12.dev = this->m_hostDev;

      dai12.tai.name = aicontroller::kATTR_NAME_USER_DATA_OFS2 + chanLabel;
      dai12.tai.label = dai12.tai.name; 
      dai12.tai.data_format = Tango::SCALAR;
      dai12.tai.data_type = Tango::DEV_DOUBLE;
      dai12.tai.disp_level = Tango::OPERATOR;
      dai12.tai.description = "user data offset2 - formula is: user data = ((scaled data - offset1)/gain) - offset2";

      //- attribute properties:
      dai12.tai.unit = this->m_acqDefinition.activeChannels[chan].userDataDesc.unit;
      dai12.tai.standard_unit = this->m_acqDefinition.activeChannels[chan].userDataDesc.unit;
      dai12.tai.display_unit = this->m_acqDefinition.activeChannels[chan].userDataDesc.unit;
      dai12.tai.format = "%6.2e";

      dai12.cdb = true;

      switch(this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Type)
      {
        case USER_DATA_ATTR: // attribute => R/W
          dai12.tai.writable = Tango::READ_WRITE;

          //- read callback
          dai12.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                          &AcqMode::read_callback_user_data_offset2);

          //- write callback
          dai12.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                          &AcqMode::write_callback_user_data_offset2);

          //- add attribute description in list
          dynAttrList.push_back(dai12);
          break;

        case USER_DATA_PROXY: // proxy => read only attribute
          dai12.tai.writable = Tango::READ;

          //- read callback
          dai12.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                          &AcqMode::read_callback_user_data_offset2);

          //- add attribute description in list
          dynAttrList.push_back(dai12);
          break;
        default:
          // do nothing
          break;
      }
    }

    //-------------
    //- raw data:
    //-------------
    yat4tango::DynamicAttributeInfo dai;

    //- attribute definition:
    dai.dev = this->m_hostDev;

    dai.tai.name = chanLabel + aicontroller::kATTR_NAME_TAG_RAW;
    dai.tai.label = dai.tai.name; 
    dai.tai.data_format = Tango::SPECTRUM;
    dai.tai.data_type = Tango::DEV_USHORT;
    dai.tai.disp_level = Tango::OPERATOR;
    dai.tai.writable = Tango::READ;
    dai.tai.description = chanDescription;
	dai.tai.max_dim_x = LONG_MAX;

    //- attribute properties:
    dai.tai.unit = " ";
    dai.tai.standard_unit = " ";
    dai.tai.display_unit = " ";
    dai.tai.format = "%6d";

    dai.cdb = true;

    //- read callback
    dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                    &AcqMode::read_callback_raw_data);

    //- add attribute description in list
    dynAttrList.push_back(dai);
    

    if (this->m_dataTrt.isDataScaled)
    {
      //----------------
      //- scaled data:
      //----------------
      yat4tango::DynamicAttributeInfo dai1;

      //- attribute definition:
      dai1.dev = this->m_hostDev;

      dai1.tai.name = chanLabel;
      dai1.tai.label = dai1.tai.name; 
      dai1.tai.data_format = Tango::SPECTRUM;
      dai1.tai.data_type = Tango::DEV_DOUBLE;
      dai1.tai.disp_level = Tango::OPERATOR;
      dai1.tai.writable = Tango::READ;
      dai1.tai.description = chanDescription;
	  dai1.tai.max_dim_x = LONG_MAX;

      //- attribute properties:
      dai1.tai.unit = "V";
      dai1.tai.standard_unit = "V";
      dai1.tai.display_unit = "Volts";
      dai1.tai.format = "%6.2e";

      dai1.cdb = true;

      //- read callback
      dai1.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                      &AcqMode::read_callback_scaled_data);

      //- add attribute description in list
      dynAttrList.push_back(dai1);

	    if (this->m_dataTrt.hasStatistics)
	    {
        //----------------
        //- average data:
        //----------------
        yat4tango::DynamicAttributeInfo dai2;

        //- attribute definition:
        dai2.dev = this->m_hostDev;

        dai2.tai.name = aicontroller::kATTR_NAME_TAG_AVERAGE + chanLabel;
        dai2.tai.label = dai2.tai.name; 
        dai2.tai.data_format = Tango::SCALAR;
        dai2.tai.data_type = Tango::DEV_DOUBLE;
        dai2.tai.disp_level = Tango::OPERATOR;
        dai2.tai.writable = Tango::READ;
        dai2.tai.description = chanDescription;

        //- attribute properties:
        dai2.tai.unit = " ";
        dai2.tai.standard_unit = " ";
        dai2.tai.display_unit = " ";
        dai2.tai.format = "%6.2e";

        dai2.cdb = true;

        //- read callback
        dai2.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                        &AcqMode::read_callback_average_data);

        //- add attribute description in list
        dynAttrList.push_back(dai2);


        //-----------------------
        //- average data history:
        //-----------------------
        yat4tango::DynamicAttributeInfo dai5;

        //- attribute definition:
        dai5.dev = this->m_hostDev;

        dai5.tai.name = aicontroller::kATTR_NAME_TAG_AV_HIST + chanLabel;
        dai5.tai.label = dai5.tai.name; 
        dai5.tai.data_format = Tango::SPECTRUM;
        dai5.tai.data_type = Tango::DEV_DOUBLE;
        dai5.tai.disp_level = Tango::OPERATOR;
        dai5.tai.writable = Tango::READ;
        dai5.tai.description = chanDescription;
		dai5.tai.max_dim_x = LONG_MAX;

        //- attribute properties:
        dai5.tai.unit = " ";
        dai5.tai.standard_unit = " ";
        dai5.tai.display_unit = " ";
        dai5.tai.format = "%6.2e";

        dai5.cdb = true;

        //- read callback
        dai5.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                        &AcqMode::read_callback_average_history);

        //- add attribute description in list
        dynAttrList.push_back(dai5);


        //------------
        //- rms value:
        //------------
        yat4tango::DynamicAttributeInfo dai3;

        //- attribute definition:
        dai3.dev = this->m_hostDev;

        dai3.tai.name = aicontroller::kATTR_NAME_TAG_RMS + chanLabel;
        dai3.tai.label = dai3.tai.name; 
        dai3.tai.data_format = Tango::SCALAR;
        dai3.tai.data_type = Tango::DEV_DOUBLE;
        dai3.tai.disp_level = Tango::OPERATOR;
        dai3.tai.writable = Tango::READ;
        dai3.tai.description = chanDescription;

        //- attribute properties:
        dai3.tai.unit = " ";
        dai3.tai.standard_unit = " ";
        dai3.tai.display_unit = " ";
        dai3.tai.format = "%6.2e";

        dai3.cdb = true;

        //- read callback
        dai3.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                        &AcqMode::read_callback_rms);

        //- add attribute description in list
        dynAttrList.push_back(dai3);

        //---------------------
        //- peak-to-peak value:
        //---------------------
        yat4tango::DynamicAttributeInfo dai4;

        //- attribute definition:
        dai4.dev = this->m_hostDev;

        dai4.tai.name = aicontroller::kATTR_NAME_TAG_PEAK + chanLabel;
        dai4.tai.label = dai4.tai.name; 
        dai4.tai.data_format = Tango::SCALAR;
        dai4.tai.data_type = Tango::DEV_DOUBLE;
        dai4.tai.disp_level = Tango::OPERATOR;
        dai4.tai.writable = Tango::READ;
        dai4.tai.description = chanDescription;

        //- attribute properties:
        dai4.tai.unit = " ";
        dai4.tai.standard_unit = " ";
        dai4.tai.display_unit = " ";
        dai4.tai.format = "%6.2e";

        dai4.cdb = true;

        //- read callback
        dai4.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                        &AcqMode::read_callback_peak);

        //- add attribute description in list
        dynAttrList.push_back(dai4);
      }

      if (this->m_dataTrt.isDataHistorized)
      {
        //----------------------
        //- scaled data history:
        //----------------------
        yat4tango::DynamicAttributeInfo dai6;

        //- attribute definition:
        dai6.dev = this->m_hostDev;

        dai6.tai.name = aicontroller::kATTR_NAME_TAG_DATA_HIST + chanLabel;
        dai6.tai.label = dai6.tai.name; 
        dai6.tai.data_format = Tango::SPECTRUM;
        dai6.tai.data_type = Tango::DEV_DOUBLE;
        dai6.tai.disp_level = Tango::OPERATOR;
        dai6.tai.writable = Tango::READ;
        dai6.tai.description = chanDescription;
		dai6.tai.max_dim_x = LONG_MAX;

        //- attribute properties:
        dai6.tai.unit = "V";
        dai6.tai.standard_unit = "V";
        dai6.tai.display_unit = "V";
        dai6.tai.format = "%6.2e";

        dai6.cdb = true;

        //- read callback
        dai6.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                        &AcqMode::read_callback_data_history);

        //- add attribute description in list
        dynAttrList.push_back(dai6);
      }

      if (this->m_acqDefinition.activeChannels[chan].userDataDesc.hasUserData)
      {
        //----------------
        //- average data:
        //----------------
        yat4tango::DynamicAttributeInfo dai8;

        //- attribute definition:
        dai8.dev = this->m_hostDev;

        dai8.tai.name = aicontroller::kATTR_NAME_TAG_AVERAGE_SCD + chanLabel;
        dai8.tai.label = dai8.tai.name; 
        dai8.tai.data_format = Tango::SCALAR;
        dai8.tai.data_type = Tango::DEV_DOUBLE;
        dai8.tai.disp_level = Tango::EXPERT;
        dai8.tai.writable = Tango::READ;
        dai8.tai.description = chanDescription;

        //- attribute properties:
        dai8.tai.unit = " ";
        dai8.tai.standard_unit = " ";
        dai8.tai.display_unit = " ";
        dai8.tai.format = "%6.2e";

        dai8.cdb = true;

        //- read callback
        dai8.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                        &AcqMode::read_callback_scaled_average_data);

        //- add attribute description in list
        dynAttrList.push_back(dai8);

        //-------------------
        //- user data result:
        //-------------------
        yat4tango::DynamicAttributeInfo dai9;

        //- attribute definition:
        dai9.dev = this->m_hostDev;

        dai9.tai.name = aicontroller::kATTR_NAME_TAG_USER_DATA + chanLabel;
        dai9.tai.label = dai9.tai.name; 
        dai9.tai.data_format = Tango::SPECTRUM;
        dai9.tai.data_type = Tango::DEV_DOUBLE;
        dai9.tai.disp_level = Tango::OPERATOR;
        dai9.tai.writable = Tango::READ;
        dai9.tai.description = chanDescription;
		dai9.tai.max_dim_x = LONG_MAX;

        //- attribute properties:
        dai9.tai.unit = this->m_acqDefinition.activeChannels[chan].userDataDesc.unit;
        dai9.tai.standard_unit = this->m_acqDefinition.activeChannels[chan].userDataDesc.unit;
        dai9.tai.display_unit = this->m_acqDefinition.activeChannels[chan].userDataDesc.unit;
        dai9.tai.format = "%6.2f";

        dai9.cdb = true;

        //- read callback
        dai9.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                        &AcqMode::read_callback_user_data);

        //- add attribute description in list
        dynAttrList.push_back(dai9);
      }
	  
    } // end if scaled

  } // end of active channels parsing

  // add reTrig counter attribute
  yat4tango::DynamicAttributeInfo dai15;

  //- attribute definition:
  dai15.dev = this->m_hostDev;

  dai15.tai.name = "dataBufferNumber";
  dai15.tai.label = "number of data buffers"; 
  dai15.tai.data_format = Tango::SCALAR;
  dai15.tai.data_type = Tango::DEV_ULONG;
  dai15.tai.disp_level = Tango::OPERATOR;
  dai15.tai.writable = Tango::READ_WRITE;
  dai15.tai.description = "Number of data buffers (i.e. external triggers) to receive before stopping acquisition.<BR>\nIn 'safe' mode, the re-trigger mechanism is used.";

  //- attribute properties:
  dai15.tai.unit = " ";
  dai15.tai.standard_unit = " ";
  dai15.tai.display_unit = " ";
  dai15.tai.format = "%7d";

  dai15.cdb = true;

  //- read callback
  dai15.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                   &POST_DELAYED_AcqMode::read_callback_data_buffer_nb);

  //- write callback
  dai15.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                   &POST_DELAYED_AcqMode::write_callback_data_buffer_nb);

  //- add attribute description in list
  dynAttrList.push_back(dai15);

  
  // add intermediate buffer depth attribute
  yat4tango::DynamicAttributeInfo dai17;

  //- attribute definition:
  dai17.dev = this->m_hostDev;

  dai17.tai.name = "bufferDepth";
  dai17.tai.label = "intermediate buffer depth"; 
  dai17.tai.data_format = Tango::SCALAR;
  dai17.tai.data_type = Tango::DEV_ULONG;
  dai17.tai.disp_level = Tango::EXPERT;
  dai17.tai.writable = Tango::READ_WRITE;
  dai17.tai.description = "In finite retriggered mode (=SAFE), intermediate buffers will be received every 'buffer depth' triggers.<BR>\nIf NULL, no intermediate buffers received; only whole buffer at acquisition end.";

  //- attribute properties:
  dai17.tai.unit = " ";
  dai17.tai.standard_unit = " ";
  dai17.tai.display_unit = " ";
  dai17.tai.format = "%7d";

  dai17.cdb = true;

  //- read callback
  dai17.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                   &POST_DELAYED_AcqMode::read_callback_buffer_depth);

  //- write callback
  dai17.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                   &POST_DELAYED_AcqMode::write_callback_buffer_depth);

  //- add attribute description in list
  dynAttrList.push_back(dai17);


  // add data mask attribute (if required)
  if (this->m_dataTrt.hasMask)
  {
    yat4tango::DynamicAttributeInfo dai16;

    //- attribute definition:
    dai16.dev = this->m_hostDev;

    dai16.tai.name = "dataMask";
    dai16.tai.label = "data mask"; 
    dai16.tai.data_format = Tango::SCALAR;
    dai16.tai.data_type = Tango::DEV_STRING;
    dai16.tai.disp_level = Tango::OPERATOR;
    dai16.tai.writable = Tango::READ_WRITE;
    dai16.tai.description = "Data mask (list of time slots) to be applied on data buffer in ms: {t1;t2}:{t3;t4}:...";

    //- attribute properties:
    dai16.tai.unit = " ";
    dai16.tai.standard_unit = " ";
    dai16.tai.display_unit = " ";

    dai16.cdb = true;

    //- read callback
    dai16.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                     &POST_DELAYED_AcqMode::read_callback_data_mask);

    //- write callback
    dai16.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                     &POST_DELAYED_AcqMode::write_callback_data_mask);

    //- add attribute description in list
    dynAttrList.push_back(dai16);
  }

  // Add buffer timestamp histories (if required)
  if (this->m_dataTrt.timestamped)
  {
    // buffer timestamp history
    yat4tango::DynamicAttributeInfo dai19;

    //- attribute definition:
    dai19.dev = this->m_hostDev;

    dai19.tai.name = kATTR_NAME_TAG_TIMESTAMP;
    dai19.tai.label = kATTR_NAME_TAG_TIMESTAMP; 
    dai19.tai.data_format = Tango::SPECTRUM;
    dai19.tai.data_type = Tango::DEV_DOUBLE;
    dai19.tai.disp_level = Tango::EXPERT;
    dai19.tai.writable = Tango::READ;
    dai19.tai.description = "Timestamp history of the received buffers.";

    //- attribute properties:
    dai19.tai.unit = "s";
    dai19.tai.standard_unit = "s";
    dai19.tai.display_unit = "s";
    dai19.tai.format = "%14.3f";

    dai17.cdb = true;

    //- read callback
    dai19.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                     &AcqMode::read_callback_buffer_timestamps);

    //- add attribute description in list
    dynAttrList.push_back(dai19);


    // relative buffer datation history
    yat4tango::DynamicAttributeInfo dai18;

    //- attribute definition:
    dai18.dev = this->m_hostDev;

    dai18.tai.name = kATTR_NAME_TAG_RELATIVE_DATE;
    dai18.tai.label = kATTR_NAME_TAG_RELATIVE_DATE; 
    dai18.tai.data_format = Tango::SPECTRUM;
    dai18.tai.data_type = Tango::DEV_DOUBLE;
    dai18.tai.disp_level = Tango::EXPERT;
    dai18.tai.writable = Tango::READ;
    dai18.tai.description = "Relative time history of the received buffers.";

    //- attribute properties:
    dai18.tai.unit = "s";
    dai18.tai.standard_unit = "s";
    dai18.tai.display_unit = "s";
    dai18.tai.format = "%7.3f";

    dai18.cdb = true;

    //- read callback
    dai18.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                     &AcqMode::read_callback_relative_buffer_datation);

    //- add attribute description in list
    dynAttrList.push_back(dai18);

    // timestamp histories depth
    yat4tango::DynamicAttributeInfo dai20;

    //- attribute definition:
    dai20.dev = this->m_hostDev;

    dai20.tai.name = "timestampHistoriesDepth";
    dai20.tai.label = "timestampHistoriesDepth"; 
    dai20.tai.data_format = Tango::SCALAR;
    dai20.tai.data_type = Tango::DEV_ULONG;
    dai20.tai.disp_level = Tango::EXPERT;
    dai20.tai.writable = Tango::READ_WRITE;
    dai20.tai.description = "Timestamp histories depth, in number of points.";

    //- attribute properties:
    dai20.tai.unit = " ";
    dai20.tai.standard_unit = " ";
    dai20.tai.display_unit = " ";
    dai20.tai.format = "%5d";

    dai20.cdb = true;

    //- read callback
    dai20.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                     &AcqMode::read_callback_timestamp_histo_depth);

    //- write callback
    dai20.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<POST_DELAYED_AcqMode&>(*this), 
                                                                     &AcqMode::write_callback_timestamp_histo_depth);

    //- add attribute description in list
    dynAttrList.push_back(dai20);

  }

}

//+------------------------------------------------------------------
//
//  method:  POST_DELAYED_AcqMode::read_callback_data_buffer_nb
//
//  description:  read callback function for number of data buffers.
//
//+------------------------------------------------------------------
void POST_DELAYED_AcqMode::read_callback_data_buffer_nb(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for average history buffer depth: " 
  //          << attrName
  //          << std::endl;

  // set attribute value
	static Tango::DevULong __data_buffer_nb__;
  __data_buffer_nb__ = static_cast<Tango::DevULong>(POST_DELAYED_AcqMode::getDataBufferNb());
  cbd.tga->set_value(&__data_buffer_nb__);
}

//+----------------------------------------------------------------------------
//
// method : 		POST_DELAYED_AcqMode::write_callback_data_buffer_nb
// 
// description : 	write callback function for number of data buffers.
//
//-----------------------------------------------------------------------------
void POST_DELAYED_AcqMode::write_callback_data_buffer_nb(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = POST_DELAYED_AcqMode::getAcqState();
  if ((Tango::RUNNING == state) ||
      (Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("POST_DELAYED_AcqMode::write_callback_data_buffer_nb"));
  }

  Tango::DevULong nb;
  cbd.tga->get_write_value(nb);

  POST_DELAYED_AcqMode::setDataBufferNb(nb);

  this->store_value_as_property(nb, "__DataBufferNumber");
}

//+------------------------------------------------------------------
//
//  method:  POST_DELAYED_AcqMode::read_callback_buffer_depth
//
//  description:  read callback function for buffer depth.
//
//+------------------------------------------------------------------
void POST_DELAYED_AcqMode::read_callback_buffer_depth(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for average history buffer depth: " 
  //          << attrName
  //          << std::endl;

  // set attribute value
	static Tango::DevULong __buffer_depth__;
  __buffer_depth__ = static_cast<Tango::DevULong>(POST_DELAYED_AcqMode::getBufferDepth());
  cbd.tga->set_value(&__buffer_depth__);
}

//+----------------------------------------------------------------------------
//
// method : 		POST_DELAYED_AcqMode::write_callback_buffer_depth
// 
// description : 	write callback function for buffer depth.
//
//-----------------------------------------------------------------------------
void POST_DELAYED_AcqMode::write_callback_buffer_depth(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = POST_DELAYED_AcqMode::getAcqState();
  if ((Tango::RUNNING == state) ||
      (Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("POST_DELAYED_AcqMode::write_callback_buffer_depth"));
  }

  Tango::DevULong nb;
  cbd.tga->get_write_value(nb);

  POST_DELAYED_AcqMode::setBufferDepth(nb);

  this->store_value_as_property(nb, "__BufferDepth");
}
// fin FLYSCAN

// ======================================================================
// POST_DELAYED_AcqMode::initSpecificAttributes_i
// ======================================================================
void POST_DELAYED_AcqMode::initSpecificAttributes_i()
    throw (Tango::DevFailed)
{
  //- Call mother class function 1st
  AcqMode::initSpecificAttributes_i();

  //- Set the list of writable attributes memorized as Device property
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("__DataMask"));
	dev_prop.push_back(Tango::DbDatum("__DataBufferNumber"));
  dev_prop.push_back(Tango::DbDatum("__BufferDepth"));

	//-	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
    this->m_hostDev->get_db_device()->get_property(dev_prop);

  size_t idx = 0;

	//-	Try to extract saved property from database

  // Data mask:
  std::string	__DataMask;

  if (this->m_dataTrt.hasMask)
  {
	  if (dev_prop[idx].is_empty()==false)
	  {
		  dev_prop[idx++]  >>  __DataMask;
    }
    else
    {
      idx++;
      __DataMask = "";
    }

    this->setDataMask(__DataMask);
  }
  else
  {
    idx++;
  }

  // Number of data buffers:
  unsigned long __DataBufferNb = kINFINITE_ACQ; // default value
	if (dev_prop[idx].is_empty()==false)
	{
		dev_prop[idx++]  >>  __DataBufferNb;
	}
  else
  {
    idx++;
  }
  this->setDataBufferNb(__DataBufferNb);

  // buffer depth = number of triggers between each buffer retreival:
  unsigned long __BufferDepth = 0; // default value
  if (dev_prop[idx].is_empty()==false)
  {
    dev_prop[idx++]  >>  __BufferDepth;
  }
  // write memorized value only in SAFE mode
  if (this->m_bufferConfig.postFiniteMode == TRG_POST_FINITE_MODE_SAFE)
    this->setBufferDepth(__BufferDepth);
}

//+------------------------------------------------------------------
//
//  method:  POST_DELAYED_AcqMode::read_callback_data_mask
//
//  description:    read callback function for data mask.
//
//+------------------------------------------------------------------
void POST_DELAYED_AcqMode::read_callback_data_mask(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  // set attribute value
  static char * __data_mask_addr__;
	static std::string __data_mask__;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    __data_mask__ = POST_DELAYED_AcqMode::m_dataTrt.timeSlotMask;
    __data_mask_addr__ = const_cast<char*>(__data_mask__.c_str());      

    cbd.tga->set_value(&__data_mask_addr__);
  }
}

//+------------------------------------------------------------------
//
//  method:  POST_DELAYED_AcqMode::write_callback_data_mask
//
//  description:    write callback function for data mask.
//
//+------------------------------------------------------------------
void POST_DELAYED_AcqMode::write_callback_data_mask(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = POST_DELAYED_AcqMode::getAcqState();
  if ((Tango::RUNNING == state) ||
      (Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("POST_DELAYED_AcqMode::write_callback_data_mask"));
  }

  Tango::DevString	mask;
  cbd.tga->get_write_value(mask);

  POST_DELAYED_AcqMode::setDataMask(mask);
  this->store_value_as_property(mask, "__DataMask");
}

// ======================================================================
// POST_DELAYED_AcqMode::setDataMask
// ======================================================================
void POST_DELAYED_AcqMode::setDataMask (std::string mask)
  throw (Tango::DevFailed)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  //- analyze time stlots and set boolean mask
  this->setBoolDataMask(mask);

  //- set local value if everything is ok
  this->m_dataTrt.timeSlotMask = mask;
}

// ======================================================================
// POST_DELAYED_AcqMode::extractSpecificDataTrtDefinition_i
// ======================================================================
void POST_DELAYED_AcqMode::extractSpecificDataTrtDefinition_i(RawAcquitisionConfig raw_acq_config)
  throw (Tango::DevFailed) 
{
  ConfigurationParser config_parser(this->m_hostDev);

  // get data mask flag
  this->m_dataTrt.hasMask =
    config_parser.extractMaskComputed(raw_acq_config.configKeyList);

  // get buffer timestamp flag
  this->m_dataTrt.timestamped = 
    config_parser.extractBufferTimestamped(raw_acq_config.configKeyList);
  // If not BEST EFFORT mode, force flag to false
  if (this->m_bufferConfig.postFiniteMode != TRG_POST_FINITE_MODE_BEST_EFFORT)
    this->m_dataTrt.timestamped = false;
}

// ======================================================================
// POST_DELAYED_AcqMode::stopAcquisition
// ======================================================================
void POST_DELAYED_AcqMode::stopAcquisition()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "POST_DELAYED_AcqMode::stopAcquisition -->" << std::endl;

  // stop nexus (if previously started) in SAFE mode because no daq_end is received in this case!
  if (this->m_nxStorageStarted && 
      (this->m_bufferConfig.postFiniteMode == TRG_POST_FINITE_MODE_SAFE))
  {
    CHECK_NX_MANAGER;

    try
    {
      INFO_STREAM << "Finalize NEXUS generation..." << std::endl;
      this->m_nexusManager->finalizeNexusGeneration();

      this->m_nxStorageStarted = false;
    }
    catch (Tango::DevFailed & df)
	  {
      this->m_errorMsg = "Nexus storage end error.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "POST_DELAYED_AcqMode::stopAcquisition caught DevFailed : " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("DEVICE_ERROR"),
                        _CPTC("Failed to stop Nexus storage (caught DevFailed)!"),
                        _CPTC("POST_DELAYED_AcqMode::stopAcquisition"));
	  }    
  }

  // then call mother class
  AcqMode::stopAcquisition();
}

// ======================================================================
// POST_DELAYED_AcqMode::abortAcquisition
// ======================================================================
void POST_DELAYED_AcqMode::abortAcquisition()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "POST_DELAYED_AcqMode::abortAcquisition -->" << std::endl;

  // stop nexus (if previously started) in SAFE mode because no daq_end is received in this case!
  if (this->m_nxStorageStarted && 
      (this->m_bufferConfig.postFiniteMode == TRG_POST_FINITE_MODE_SAFE))
  {
    CHECK_NX_MANAGER;

    try
    {
      INFO_STREAM << "Finalize NEXUS generation..." << std::endl;
      this->m_nexusManager->finalizeNexusGeneration();

      this->m_nxStorageStarted = false;
    }
    catch (Tango::DevFailed & df)
	  {
      this->m_errorMsg = "Nexus storage end error.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "POST_AcqMode::abortAcquisition caught DevFailed : " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("DEVICE_ERROR"),
                        _CPTC("Failed to stop Nexus storage (caught DevFailed)!"),
                        _CPTC("POST_DELAYED_AcqMode::abortAcquisition"));
	  }    
  }

  // then call mother class
  AcqMode::abortAcquisition();
}

} // namespace aicontroller


