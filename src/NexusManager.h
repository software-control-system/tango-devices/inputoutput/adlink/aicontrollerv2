//=============================================================================
// NexusManager.h
//=============================================================================
// abstraction.......Nexus storage manager abstraction
// class.............NexusManager
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _NEXUS_MANAGER_H_
#define _NEXUS_MANAGER_H_

// ============================================================================
// USE_NX_DS_FINALIZER ?
// ============================================================================
#define USE_NX_DS_FINALIZER

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <nexuscpp/nexuscpp.h>
#include <yat4tango/ExceptionHelper.h>
#include <yat4tango/LogHelper.h>

namespace aicontroller
{

//- nexus data storage type
typedef enum
{
  NX_DATA_TYPE_UNDEF = 0x0,
  NX_DATA_TYPE_0D    = 0x1,
  NX_DATA_TYPE_1D    = 0x2,
  NX_DATA_TYPE_2D    = 0x3
} E_NxDataStorageType_t;

//- Item to store in Nexus file
typedef struct nxItem
{
  //- members --------------------
  //- Item name
  std::string name;

  //- Item storage type (dimension)
  E_NxDataStorageType_t storageType;

  //- Item 1D dimension
  yat::uint32 dim1;

  //- Item 2D dimension
  yat::uint32 dim2;

  //- default constructor -----------------------
  nxItem ()
    : name(""),
      storageType(NX_DATA_TYPE_UNDEF),
      dim1(0),
      dim2(0)
  {
  }

  //- copy constructor ------------------
  nxItem (const nxItem& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const nxItem & operator= (const nxItem& src)
  {
      if (this == & src) 
        return *this;

      this->name = src.name;
      this->storageType = src.storageType;
      this->dim1 = src.dim1;
      this->dim2 = src.dim2;

      return *this;
  }
} nxItem;

//- nxItemList_t: list of items to store in Nexus file
typedef std::vector<nxItem> nxItemList_t;
typedef nxItemList_t::iterator nxItemList_it_t;

// ============================================================================
// class: NexusManager
// ============================================================================
class NexusManager : public Tango::LogAdapter,
                            nxcpp::IExceptionHandler
{
public:

  //- constructor
  //- host_device: host device (for logs)
  NexusManager(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~NexusManager();
  
  //- sets nexus storage parameters
  //- nexus_file_path: path name for nexus files storage
  //- nexus_file_name: nexus file name
  //- acquisition_size: number of acquisitions (0 if infinite acquisition)
  //- nx_number_per_file: number of acquisition per nx file
  //- item_list: list of data types to store
  void initNexusAcquisition(std::string nexus_file_path, 
                            std::string nexus_file_name,                            
                            yat::uint32 acquisition_size,
                            yat::uint16 nx_number_per_file,
                            nxItemList_t item_list)
    throw (Tango::DevFailed);

  //- normally ends nexus storage
  void finalizeNexusGeneration()
    throw (Tango::DevFailed);

  //- aborts nexus storage
  void manageNexusAbort()
    throw (Tango::DevFailed);
  
  //- stores a T data in a nexus file
  template <class T> void pushNexusData(std::string item_name, T * data)
    throw (Tango::DevFailed)
  {
    if (m_pAcqWriter)
    {
      // check if specified item is in item list
      bool itemFound = false;
      for (nxItemList_it_t it = m_itemList.begin(); it != m_itemList.end(); ++it)
      {
        if ((*it).name == item_name)
        {
          itemFound = true;
          break;
        }
      }

      if (!itemFound)
      {
        ERROR_STREAM << "Bad item name:" << item_name << std::endl;
        this->manageNexusAbort();
        THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                        _CPTC("Bad item name, cannot store data in Nexus file"), 
                        _CPTC("NexusManager::pushNexusData"));
      }

      if (!data)
      {
        ERROR_STREAM << "Try to send null data to Nexus!" << std::endl;
        this->manageNexusAbort();
        THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                        _CPTC("Cannot store data in Nexus file: null data!"), 
                        _CPTC("NexusManager::pushNexusData"));
      }
	  
      try
      {
        this->m_pAcqWriter->PushData(item_name, data);
      }
      catch(nxcpp::NexusException &n)
      {
        ERROR_STREAM << "NexusManager::pushNexusData -> caught NEXUS Exception:" 
          << n.to_string()
          << std::endl;
        this->m_storageError = true;

        if (m_pAcqWriter)
          m_pAcqWriter->Abort();

        Tango::DevFailed df = this->nexusToTangoException(n);
        throw df;
      }
      catch(...)
      {
        ERROR_STREAM << "NexusManager::pushData -> caught [...] Exception" << std::endl;
        this->m_storageError = true;
        this->manageNexusAbort();
        THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                        _CPTC("Failed to store data in Nexus file (caught [...])!"), 
                        _CPTC("NexusManager::pushNexusData"));      
      }
    }
    else
    {
        ERROR_STREAM << "NexusManager::pushData -> nexus writer has null value!" << std::endl;
        THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                        _CPTC("Failed to store data in Nexus file (nexus writer has null value!)"), 
                        _CPTC("NexusManager::pushNexusData"));
    }
  }

  //- Gets Nexus storage state: if true, storage in progress
  Tango::DevState getNexusStorageState()
  {
    Tango::DevState l_nxSt = Tango::UNKNOWN;
    yat::AutoMutex<> guard(this->m_finalizeNxLock);
    if (this->m_finalizeDone)
    {
      // no storage in progress
      l_nxSt = Tango::STANDBY;
    }
    else
    {
      // storage in progress
      l_nxSt = Tango::RUNNING;
    }

    // check if storage error
    if (this->m_storageError)
    {
      l_nxSt = Tango::FAULT;
    }
    return l_nxSt;
  }

  // from nxcpp::IExceptionHandler
  void OnNexusException(const nxcpp::NexusException &e)
  {
    ERROR_STREAM << "NexusManager::OnNexusException -> caught NEXUS Exception:" 
      << e.to_string()
      << std::endl;
    this->m_pAcqWriter->Abort();
    this->m_storageError = true;
  }

protected:
  
  //- converts a NeXus exception to a tango exception.
  Tango::DevFailed nexusToTangoException(const nxcpp::NexusException &nxte);

  //- the Nexus buffer pointer
  nxcpp::DataStreamer* m_pAcqWriter;

#if defined (USE_NX_DS_FINALIZER)
  //- the Nexus buffer pointer
  nxcpp::NexusDataStreamerFinalizer m_NexusDataStreamerFinalizer;
#endif

  //- nexus file flag for end of storage
  bool m_finalizeDone;

  //- nexus finalization lock
  yat::Mutex m_finalizeNxLock;

  //- type of storage flag
  bool m_finiteStorage;

  //- list of items to store
  nxItemList_t m_itemList;

  // Storage error flag
  bool m_storageError;
  
};

} // namespace aicontroller

#endif // _NEXUS_MANAGER_H_
