//=============================================================================
// INTERNAL_POSTMORTEM_AcqMode.h
//=============================================================================
// abstraction.......INTERNAL POSTMORTEM acquisition mode abstraction
// class.............INTERNAL_POSTMORTEM_AcqMode
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _INTERNAL_POSTMORTEM_ACQ_MODE_H_
#define _INTERNAL_POSTMORTEM_ACQ_MODE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "AcqMode.h"

namespace aicontroller
{

// ============================================================================
// class: INTERNAL_POSTMORTEM_AcqMode
// This class provides specific behavior for INTERNAL POSTMORTEM trigger mode.
// This mode is an INTERNAL triggered mode with last data buffer recovery at stop
// command.
// ============================================================================
class INTERNAL_POSTMORTEM_AcqMode : public AcqMode
{
public:

  //- constructor
  INTERNAL_POSTMORTEM_AcqMode(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~INTERNAL_POSTMORTEM_AcqMode();
  
  //- read callback function for history last index
  void read_callback_history_last_index(yat4tango::DynamicAttributeReadCallbackData& cbd);

protected:

  //- Extracts specific acquisition definition from raw config according
  //- to acquisition mode
  //- Sets m_acqDefinition
  virtual void extractSpecificAcqDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Sets specific configuration depending on acquisition mode
  void specificAIconfigMapper_i()
    throw (Tango::DevFailed);

  //- Creates specific dynamic attributes according to acquisition mode
  virtual void define_specific_attributes_i(std::vector<yat4tango::DynamicAttributeInfo>& dynAttrList);

  //- Extracts specific data treatment definition from raw config
  virtual void extractSpecificDataTrtDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Data processing user hook.
  virtual void handle_input_i(asl::AIRawData* raw_data)
    throw (Tango::DevFailed);

  //- DAQ end user hook.
  void handle_daq_end_i()
    throw (Tango::DevFailed);

  //- Returns data history validity
  virtual bool getDataHistoryValidity();

private:
  // data history validity
  bool m_dataHistoryValidity;
};

} // namespace aicontroller

#endif // _INTERNAL_POSTMORTEM_ACQ_MODE_H_
