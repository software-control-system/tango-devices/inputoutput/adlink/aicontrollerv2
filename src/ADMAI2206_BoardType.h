//=============================================================================
// ADMAI2206_BoardType.h
//=============================================================================
// abstraction.......ADLINK MAI 2206 Board type
// class.............ADMAI2206_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _ADMAI2206_BOARD_H_
#define _ADMAI2206_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BoardType.h"

namespace aicontroller
{


// ============================================================================
// class: ADMAI2206_BoardType
// ============================================================================
class ADMAI2206_BoardType : public BoardType
{
public:
  //- constructor
  ADMAI2206_BoardType ();

  //- destructor
  virtual ~ADMAI2206_BoardType ();
  
  //- board model name as string 
  virtual std::string board_model_name () const
  {
    return kBOARD_TYPE_ADMAI2206_STR;
  }

  //- board model as reference 
  virtual unsigned short board_model_id () const 
  {
    return adl::DAQ2206;
  }
};

} // namespace aicontroller

#endif // _ADMAI2206_BOARD_H_
