//=============================================================================
// INTERNAL_AcqMode.h
//=============================================================================
// abstraction.......INTERNAL acquisition mode abstraction
// class.............INTERNAL_AcqMode
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _INTERNAL_ACQ_MODE_H_
#define _INTERNAL_ACQ_MODE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "AcqMode.h"

namespace aicontroller
{

// ============================================================================
// class: INTERNAL_AcqMode
// This class provides specific behavior for INTERNAL trigger mode.
// ============================================================================
class INTERNAL_AcqMode : public AcqMode
{
public:

  //- constructor
  INTERNAL_AcqMode(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~INTERNAL_AcqMode();
  
  //- Sets new number of data buffers
  virtual void setDataBufferNb(yat::uint32 nb)
  {
    // reinit error management
    this->m_errorMsg = "";
    this->m_errorOccurred = false;

    this->m_dataBufferNb = nb;
  }  

  //- Gets current number of data buffers
  virtual yat::uint32 getDataBufferNb()
  {
    return (this->m_dataBufferNb);
  }

  //- Extracts specific data treatment definition from raw config
  virtual void extractSpecificDataTrtDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- read callback function for number of data buffers
  void read_callback_data_buffer_nb(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for number of data buffers
  void write_callback_data_buffer_nb(yat4tango::DynamicAttributeWriteCallbackData& cbd);


protected:

  //- Extracts specific acquisition definition from raw config according
  //- to acquisition mode
  //- Sets m_acqDefinition
  virtual void extractSpecificAcqDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Sets specific configuration depending on acquisition mode
  void specificAIconfigMapper_i()
      throw (Tango::DevFailed);

  //- Creates specific dynamic attributes according to acquisition mode
  virtual void define_specific_attributes_i(std::vector<yat4tango::DynamicAttributeInfo>& dynAttrList);

  //- Initializes specific attributes
  virtual void initSpecificAttributes_i()
    throw (Tango::DevFailed);

  //- Data processing user hook.
  virtual void handle_input_i(asl::AIRawData* raw_data)
    throw (Tango::DevFailed);

  //- Timeout user hook.
  void handle_timeout_i()
      throw (Tango::DevFailed);

  //- DAQ end user hook.
  void handle_daq_end_i()
    throw (Tango::DevFailed);

  //- Number of data buffers (= nb of internal triggers)
  yat::uint32 m_dataBufferNb;
};

} // namespace aicontroller

#endif // _INTERNAL_ACQ_MODE_H_
