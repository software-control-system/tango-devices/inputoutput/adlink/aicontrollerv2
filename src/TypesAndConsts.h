//=============================================================================
// TypesAndConsts.h
//=============================================================================
// abstraction.......Basic types and Constants for AIControllerV2 Device
// class.............Basic structures
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _TYPES_AND_CONSTS_H_
#define _TYPES_AND_CONSTS_H_

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include <vector>
#include <map>
#include <iostream>
#include <yat/Portability.h>


namespace aicontroller
{
// ============================================================================
// UNIT TRANSFORMATION CONSTS
// ============================================================================
const double kSECONDS_TO_MILLISECONDS    = 1000;
const double kMILLISECONDS_TO_SECONDS    = 1 / kSECONDS_TO_MILLISECONDS;
const size_t kKILOBYTES_TO_BYTES         = 1024;

// ============================================================================
// MAX NUMBER OF CONFIGURATIONS
// ============================================================================
const size_t kMAX_NUMBER_OF_ACQ_CONFIG   = 20;

// ============================================================================
// TAGS FOR ATTRIBUTE NAMES
// ============================================================================
const std::string kATTR_NAME_TAG_AVERAGE      = "average";
const std::string kATTR_NAME_TAG_AVERAGE_SCD  = "scaledAv";
const std::string kATTR_NAME_TAG_RMS          = "rms";
const std::string kATTR_NAME_TAG_PEAK         = "peak";
const std::string kATTR_NAME_TAG_RAW          = "_Raw";
const std::string kATTR_NAME_TAG_AV_HIST      = "historized";
const std::string kATTR_NAME_TAG_DATA_HIST    = "dataHistorized";
const std::string kATTR_NAME_USER_DATA_GAIN   = "userDataGain";
const std::string kATTR_NAME_USER_DATA_OFS1   = "userDataOffset1";
const std::string kATTR_NAME_USER_DATA_OFS2   = "userDataOffset2";
const std::string kATTR_NAME_TAG_USER_DATA    = "userData";
const std::string kATTR_NAME_TAG_DATASET      = "enableDataset";
const std::string kATTR_NAME_TAG_TIMESTAMP    = "bufferAbsoluteTimestamps";
const std::string kATTR_NAME_TAG_RELATIVE_DATE  = "bufferRelativeTimestamps";

// ============================================================================
// Finite/Infinite acquisition consts
// ============================================================================
const yat::uint32 kINFINITE_ACQ = 0;

// ============================================================================
// Sampling rate consts
// ============================================================================
const double kDEFAULT_SAMPLING_RATE = 100000.0;
const double kDEFAULT_TIMEBASE_FREQ = 40000000.0;

// ============================================================================
// ADLINK BOARD - TYPEs
// ============================================================================
typedef enum
{
  UNDEFINED_BOARD_TYPE  = 0x0,
  ADLINK_SAI_2005       = 0x1,
  ADLINK_SAI_2006       = 0x2,
  ADLINK_SAI_2010       = 0x3,
  ADLINK_MAI_2204       = 0x4,
  ADLINK_MAI_2205       = 0x5,
  ADLINK_MAI_2206       = 0x6
} E_BoardTypes_t;

const std::string kBOARD_TYPE_ADSAI2005_STR = "ADLINK:SAI:2005";
const std::string kBOARD_TYPE_ADSAI2006_STR = "ADLINK:SAI:2006";
const std::string kBOARD_TYPE_ADSAI2010_STR = "ADLINK:SAI:2010";
const std::string kBOARD_TYPE_ADMAI2204_STR = "ADLINK:MAI:2204";
const std::string kBOARD_TYPE_ADMAI2205_STR = "ADLINK:MAI:2205";
const std::string kBOARD_TYPE_ADMAI2206_STR = "ADLINK:MAI:2206";

// ============================================================================
// ADLINK BOARD - CHANNEL GROUND REFs
// ============================================================================
typedef enum
{
  GRD_REF_UNDEFINED  = 0x0,
  GRD_REF_SINGLE     = 0x1,
  GRD_REF_DIFF       = 0x2
} E_ChannelGroundref_t;

// ============================================================================
// ADLINK BOARD - OVERRUN STRATEGIES
// ============================================================================
typedef enum
{
  OVRN_NOTIFY   = 0x1,
  OVRN_ABORT    = 0x2,
  OVRN_TRASH    = 0x3,
  OVRN_RESTART  = 0x4,
  OVRN_IGNORE   = 0x5
} E_OverrunStrategy_t;

// ============================================================================
// ADLINK BOARD - SAMPLING SOURCEs
// ============================================================================
typedef enum
{
  SRC_INTERNAL   = 0x1,
  SRC_EXTERNAL   = 0x2
} E_SamplingSource_t;

// ============================================================================
// ADLINK BOARD - TIMEBASE TYPEs
// ============================================================================
typedef enum
{
  TB_INTERNAL = 0x1,
  TB_EXTERNAL = 0x2,
  TB_SSI      = 0x3
} E_TimebaseType_t;

// ============================================================================
// ADLINK BOARD - TRIGGER MODEs
// ============================================================================
typedef enum
{
  TRG_UNDEFINED      = 0x0,
  TRG_INTERNAL       = 0x1,
  TRG_POST           = 0x2,
  TRG_PRE            = 0x3,
  TRG_MIDDLE         = 0x4,
  TRG_POST_DELAYED   = 0x5,
  TRG_INT_POSTMORTEM = 0x6
} E_TriggerMode_t;

// ============================================================================
// ADLINK BOARD - EXTERNAL TRIGGER TYPEs
// ============================================================================
typedef enum
{
  EXT_TRG_UNDEFINED   = 0x0,
  EXT_TRG_ANALOG      = 0x1, 
  EXT_TRG_DIGITAL     = 0x2 
} E_ExtTrigType_t;

// ============================================================================
// ADLINK BOARD - EXTERNAL ANALOG TRIGGER SOURCEs
// ============================================================================
typedef enum
{
  EXT_ATRG_SRC_UNDEFINED  = 0x0,
  EXT_ATRG_SRC_EXT        = 0x0, // external
  EXT_ATRG_CHAN0          = 0x1,
  EXT_ATRG_CHAN1          = 0x2,
  EXT_ATRG_CHAN2          = 0x3,
  EXT_ATRG_CHAN3          = 0x4
} E_ExtAtrigSrc_t;

// ============================================================================
// ADLINK BOARD - EXTERNAL ANALOG TRIGGER CONDITION TYPEs
// ============================================================================
typedef enum
{
  EXT_ATRG_COND_UNDEFINED  = 0x0,
  EXT_ATRG_COND_BELOW      = 0x1,
  EXT_ATRG_COND_ABOVE      = 0x2,
  EXT_ATRG_COND_REGION     = 0x3,
  EXT_ATRG_COND_HYST       = 0x4 // Hysteresis
} E_ExtAtrigCond_t;

// ============================================================================
// ADLINK BOARD - EXTERNAL ANALOG TRIGGER EDGE TYPEs
// ============================================================================
typedef enum
{
  EXT_ATRIG_EDGE_UNDEFINED  = 0x0,
  EXT_ATRIG_EDGE_FALLING    = 0x1,
  EXT_ATRIG_EDGE_RISING     = 0x2
} E_ExtAtrigEdge_t;

// ============================================================================
// ADLINK BOARD - DELAYED TRIGGER - DELAY UNITs
// ============================================================================
typedef enum
{
  TRG_DELAY_UNIT_UNDEFINED  = 0x0,
  TRG_DELAY_UNIT_TICKS      = 0x1,
  TRG_DELAY_UNIT_SAMPLES    = 0x2
} E_DelayedTrgUnit_t;

// ============================================================================
// ADLINK BOARD - POST TRIGGER - FINITE MODEs
// ============================================================================
typedef enum
{
  TRG_POST_FINITE_MODE_UNDEFINED    = 0x0,
  TRG_POST_FINITE_MODE_SAFE         = 0x1,
  TRG_POST_FINITE_MODE_BEST_EFFORT  = 0x2
} E_PostTrgFiniteMode_t;

// ============================================================================
// NEXUS STORAGE - DATA TYPEs
// ============================================================================
typedef enum
{
  NX_RAW_DATA       = 0x1,
  NX_SCALED_DATA    = 0x2,
  NX_AVERAGE_DATA   = 0x3,
  NX_RMS            = 0x4,
  NX_PEAK           = 0x5,
  NX_DATA_HIST      = 0x6,
  NX_USER_DATA      = 0x7,
  NX_TIMESTAMP      = 0x8
} E_Nexus_data_types_t;

//- NxDataTypes_t: list of Nexus data types
typedef std::vector<E_Nexus_data_types_t> NxDataTypes_t;
typedef NxDataTypes_t::iterator NxDataTypes_it_t;

// ============================================================================
// USER DATA - TYPEs
// ============================================================================
typedef enum
{
  USER_DATA_UNDEFINED   = 0x0,
  USER_DATA_ATTR        = 0x1,
  USER_DATA_PROXY       = 0x2
} E_UserDataType_t;

// ============================================================================
// ACQUISITION CONFIG - DATA TYPEs
// ============================================================================
//- List of <KEY, value> defining the acquisition configuration
typedef std::pair<const std::string, std::string> AcquisitionKey_pair_t;
typedef std::map<std::string, std::string> AcquisitionKeys_t;
typedef AcquisitionKeys_t::iterator AcquisitionKeys_it_t;


// ============================================================================
// DAQ board definition
// ============================================================================
typedef struct DaqBoard
{
  //- members --------------------
  // DAQ board type
  E_BoardTypes_t type;

  // DAQ board number on CPCI crate
  yat::uint16 id;

  //- default constructor -----------------------
  DaqBoard ()
    : type(UNDEFINED_BOARD_TYPE),
      id(0)
  {
  }

  //- destructor -----------------------
  ~DaqBoard ()
  {
  }

  //- copy constructor ------------------
  DaqBoard (const DaqBoard& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const DaqBoard & operator= (const DaqBoard& src)
  {
      if (this == & src) 
        return *this;

      this->type = src.type;
      this->id = src.id;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "DaqBoard::type........." 
              << this->type
              << std::endl; 
    std::cout << "DaqBoard::id........." 
              << this->id
              << std::endl; 
  }

} DaqBoard;

// ============================================================================
// User data device proxy
// ============================================================================
typedef struct UserDataProxy
{
  //- members --------------------
  // Device name
  std::string deviceName;

  // Attribute name
  std::string attributeName;

  // Attribute type (Tango::DEV_xxx)
  int attributeType;

  //- default constructor -----------------------
  UserDataProxy ()
    : deviceName(""),
      attributeName(""),
      attributeType(0)
  {
  }

  //- destructor -----------------------
  ~UserDataProxy ()
  {
  }

  //- copy constructor ------------------
  UserDataProxy (const UserDataProxy& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const UserDataProxy & operator= (const UserDataProxy& src)
  {
      if (this == & src) 
        return *this;

      this->deviceName = src.deviceName;
      this->attributeName = src.attributeName;
      this->attributeType = src.attributeType;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "UserDataProxy::deviceName........." 
              << this->deviceName
              << std::endl; 
    std::cout << "UserDataProxy::attributeName........." 
              << this->attributeName
              << std::endl; 
    std::cout << "UserDataProxy::attributeType........." 
              << this->attributeType
              << std::endl; 
  }

} UserDataProxy;

// ============================================================================
// User data parameters
// ============================================================================
typedef struct UserData
{
  //- members --------------------
  // Enabled?
  bool hasUserData;

  // unit
  std::string unit;

  // Gain type
  E_UserDataType_t gainType;

  // Gain device proxy
  UserDataProxy gainProxy;

  // Gain value
  double gain;

  // Offset1 type
  E_UserDataType_t offset1Type;

  // Offset1 device proxy
  UserDataProxy offset1Proxy;

  // Offset1 value
  double offset1;

  // Offset2 type
  E_UserDataType_t offset2Type;

  // offset2 device proxy
  UserDataProxy offset2Proxy;

  // Offset2 value
  double offset2;

  //- default constructor -----------------------
  UserData ()
    : hasUserData(false),
      unit(""),
      gainType(USER_DATA_UNDEFINED),
      gain(1.0),
      offset1Type(USER_DATA_UNDEFINED),
      offset1(0.0),
      offset2Type(USER_DATA_UNDEFINED),
      offset2(0.0)
  {
  }

  //- destructor -----------------------
  ~UserData ()
  {
  }

  //- copy constructor ------------------
  UserData (const UserData& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const UserData & operator= (const UserData& src)
  {
      if (this == & src) 
        return *this;

      this->hasUserData = src.hasUserData;
      this->unit = src.unit;
      this->gainType = src.gainType;
      this->gainProxy = src.gainProxy;
      this->gain = src.gain;
      this->offset1Type = src.offset1Type;
      this->offset1Proxy = src.offset1Proxy;
      this->offset1 = src.offset1;
      this->offset2Type = src.offset2Type;
      this->offset2Proxy = src.offset2Proxy;
      this->offset2 = src.offset2;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "UserData::hasUserData........." 
              << this->hasUserData
              << std::endl; 
    std::cout << "UserData::unit........." 
              << this->unit
              << std::endl; 
    std::cout << "UserData::gainType........." 
              << this->gainType
              << std::endl; 
    this->gainProxy.dump();

    std::cout << "UserData::gain........." 
              << this->gain
              << std::endl; 
    std::cout << "UserData::offset1Type........." 
              << this->offset1Type
              << std::endl; 
    this->offset1Proxy.dump();

    std::cout << "UserData::offset1........." 
              << this->offset1
              << std::endl; 
    std::cout << "UserData::offset2Type........." 
              << this->offset2Type
              << std::endl; 
    this->offset2Proxy.dump();

    std::cout << "UserData::offset2........." 
              << this->offset2
              << std::endl;
  }
} UserData;

// ============================================================================
// Acquisition channel definition
// ============================================================================
typedef struct Channel
{
  //- members --------------------
  // Channel label
  std::string label;

  // Channel number
  yat::uint16 number;

  // Channel range (unipolar or bipolar range)
  std::string range;

  // Channel ground reference (single or differential)
  E_ChannelGroundref_t groundRef;

  // Channel description
  std::string description;

  // User data description
  UserData userDataDesc;

  //- default constructor -----------------------
  Channel ()
    : label(""),
      number(0),
      range(""),
      groundRef(GRD_REF_UNDEFINED),
      description("")
  {
  }

  //- destructor -----------------------
  ~Channel ()
  {
  }

  //- copy constructor ------------------
  Channel (const Channel& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const Channel & operator= (const Channel& src)
  {
      if (this == & src) 
        return *this;

      this->label = src.label;
      this->number = src.number;
      this->range = src.range;
      this->groundRef = src.groundRef;
      this->description = src.description;
      this->userDataDesc = src.userDataDesc;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "Channel::label........." 
              << this->label
              << std::endl; 
    std::cout << "Channel::number........." 
              << this->number
              << std::endl; 
    std::cout << "Channel::range........." 
              << this->range
              << std::endl; 
    std::cout << "Channel::groundRef........." 
              << this->groundRef
              << std::endl; 
    std::cout << "Channel::description........." 
              << this->description
              << std::endl; 
    this->userDataDesc.dump();

  }

} Channel;


// ============================================================================
// Channel list types
// ============================================================================
typedef std::vector<Channel> Channels_t;
typedef Channels_t::iterator Channels_it_t;


// ============================================================================
// Sampling informations
// ============================================================================
typedef struct SamplingInfo
{
  //- members --------------------
  // Sampling source
  E_SamplingSource_t source;

  // Sampling rate in Hz
  double rate;

  //- default constructor -----------------------
  SamplingInfo ()
    : source(SRC_INTERNAL),
      rate(kDEFAULT_SAMPLING_RATE)
  {
  }

  //- destructor -----------------------
  ~SamplingInfo ()
  {
  }

  //- copy constructor ------------------
  SamplingInfo (const SamplingInfo& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const SamplingInfo & operator= (const SamplingInfo& src)
  {
      if (this == & src) 
        return *this;

      this->source = src.source;
      this->rate = src.rate;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "SamplingInfo::source........." 
              << this->source
              << std::endl; 
    std::cout << "SamplingInfo::rate........." 
              << this->rate
              << std::endl; 
  }
} SamplingInfo;

// ============================================================================
// Timebase informations
// ============================================================================
typedef struct TimebaseInfo
{
  //- members --------------------
  // Timebase type
  E_TimebaseType_t type;

  // Timebase value in Hz
  double clock_freq;

  //- default constructor -----------------------
  TimebaseInfo()
  : type(TB_INTERNAL),
    clock_freq(kDEFAULT_TIMEBASE_FREQ)
  {
  }

  //- destructor -----------------------
  ~TimebaseInfo()
  {
  }

  //- copy constructor ------------------
  TimebaseInfo(const TimebaseInfo& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const TimebaseInfo & operator= (const TimebaseInfo& src)
  {
    if (this == &src)
      return *this;

    this->type = src.type;
    this->clock_freq = src.clock_freq;

    return *this;
  }

  //- dump -----------------------
  void dump() const
  {
    std::cout << "TimebaseInfo::type........."
        << this->type
        << std::endl;
    std::cout << "TimebaseInfo::clock_freq........."
        << this->clock_freq
        << std::endl;
  }
} TimebaseInfo;


// ============================================================================
// Configuration of data buffer
// ============================================================================
typedef struct AcqBufferConfiguration
{
  //- members --------------------
  // Maximum memory size in KBytes
  yat::uint32 maxMemorySize;

  // Finite mode for POST or POST_DELAYED trigger mode
  E_PostTrgFiniteMode_t postFiniteMode;

  //- default constructor -----------------------
  AcqBufferConfiguration ()
    : maxMemorySize(1024),
      postFiniteMode(TRG_POST_FINITE_MODE_UNDEFINED)
  {
  }

  //- destructor -----------------------
  ~AcqBufferConfiguration ()
  {
  }

  //- copy constructor ------------------
  AcqBufferConfiguration (const AcqBufferConfiguration& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const AcqBufferConfiguration & operator= (const AcqBufferConfiguration& src)
  {
      if (this == & src) 
        return *this;

      this->maxMemorySize = src.maxMemorySize;
      this->postFiniteMode = src.postFiniteMode;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "AcqBufferConfiguration::maxMemorySize........." 
              << this->maxMemorySize
              << std::endl; 
    std::cout << "AcqBufferConfiguration::postFiniteMode........." 
              << this->postFiniteMode
              << std::endl; 
  }
} AcqBufferConfiguration;


// ============================================================================
// External analog trigger informations
// ============================================================================
typedef struct ExtAnalogTrigInfo
{
  //- members --------------------
  // Trigger source
  E_ExtAtrigSrc_t src;

  // Condition type
  E_ExtAtrigCond_t conditionType;

  // High level condition in V
  double highLevelCond;

  // Low level condition in V
  double lowLevelCond;

  //- default constructor -----------------------
  ExtAnalogTrigInfo ()
    : src(EXT_ATRG_SRC_UNDEFINED),
      conditionType(EXT_ATRG_COND_UNDEFINED),
      highLevelCond(0.0),
      lowLevelCond(0.0)
  {
  }

  //- destructor -----------------------
  ~ExtAnalogTrigInfo ()
  {
  }

  //- copy constructor ------------------
  ExtAnalogTrigInfo (const ExtAnalogTrigInfo& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const ExtAnalogTrigInfo & operator= (const ExtAnalogTrigInfo& src)
  {
      if (this == & src) 
        return *this;

      this->src = src.src;
      this->conditionType = src.conditionType;
      this->highLevelCond = src.highLevelCond;
      this->lowLevelCond = src.lowLevelCond;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "ExtAnalogTrigInfo::src........." 
              << this->src
              << std::endl; 
    std::cout << "ExtAnalogTrigInfo::conditionType........." 
              << this->conditionType
              << std::endl; 
    std::cout << "ExtAnalogTrigInfo::highLevelCond........." 
              << this->highLevelCond
              << std::endl; 
    std::cout << "ExtAnalogTrigInfo::lowLevelCond........." 
              << this->lowLevelCond
              << std::endl; 
  }
} ExtAnalogTrigInfo;


// ============================================================================
// Post delay unit & value for POST_DELAYED / MIDDLE trigger modes
// ============================================================================
typedef struct PostDelay
{
  //- members --------------------
  // Delay unit 
  E_DelayedTrgUnit_t unit;

  // Delay value
  double value;

  //- default constructor -----------------------
  PostDelay ()
    : unit(TRG_DELAY_UNIT_UNDEFINED),
      value(0.0)
  {
  }

  //- destructor -----------------------
  ~PostDelay ()
  {
  }

  //- copy constructor ------------------
  PostDelay (const PostDelay& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const PostDelay & operator= (const PostDelay& src)
  {
      if (this == & src) 
        return *this;

      this->unit = src.unit;
      this->value = src.value;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "PostDelay::unit........." 
              << this->unit
              << std::endl; 
    std::cout << "PostDelay::value........." 
              << this->value
              << std::endl; 
  }
} PostDelay;


// ============================================================================
// Trigger configuration
// ============================================================================
typedef struct TriggerConfiguration
{
  //- members --------------------
  // Overrun strategy
  E_OverrunStrategy_t overrunStrategy;

  // Sampling informations
  SamplingInfo samplingInfo;

  // Timebase informations
  TimebaseInfo timebaseInfo;

  // Data timeout in ms
  yat::uint32 dataTimeout;

  // Trigger mode
  E_TriggerMode_t triggerMode;

  // External trigger type (for external trigger types only)
  E_ExtTrigType_t externalTrigType;

  // External analog trigger infos
  ExtAnalogTrigInfo externalATrigInfos;

  // External analog trigger edge
  E_ExtAtrigEdge_t externalTrigEdge;

  // Delay unit & value for POST_DELAYED trigger mode
  PostDelay postDelay;

  // Delay unit & value for MIDDLE trigger mode
  PostDelay middleDelay;

  //- default constructor -----------------------
  TriggerConfiguration ()
    : overrunStrategy(OVRN_NOTIFY),
      dataTimeout(1000),
      triggerMode(TRG_UNDEFINED),
      externalTrigType(EXT_TRG_UNDEFINED),
      externalTrigEdge(EXT_ATRIG_EDGE_UNDEFINED)
  {
  }

  //- destructor -----------------------
  ~TriggerConfiguration ()
  {
  }

  //- copy constructor ------------------
  TriggerConfiguration (const TriggerConfiguration& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const TriggerConfiguration & operator= (const TriggerConfiguration& src)
  {
      if (this == & src) 
        return *this;

      this->overrunStrategy = src.overrunStrategy;
      this->samplingInfo = src.samplingInfo;
      this->timebaseInfo = src.timebaseInfo;
      this->dataTimeout = src.dataTimeout;
      this->triggerMode = src.triggerMode;
      this->externalTrigType = src.externalTrigType;
      this->externalATrigInfos = src.externalATrigInfos;
      this->externalTrigEdge = src.externalTrigEdge;
      this->postDelay = src.postDelay;
      this->middleDelay = src.middleDelay;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "TriggerConfiguration::overrunStrategy........." 
              << this->overrunStrategy
              << std::endl; 
    this->samplingInfo.dump();
    this->timebaseInfo.dump();
    std::cout << "TriggerConfiguration::dataTimeout........." 
              << this->dataTimeout
              << std::endl; 
    std::cout << "TriggerConfiguration::triggerMode........." 
              << this->triggerMode
              << std::endl; 
    std::cout << "TriggerConfiguration::externalTrigType........." 
              << this->externalTrigType
              << std::endl; 
    externalATrigInfos.dump();
    std::cout << "TriggerConfiguration::externalTrigEdge........." 
              << this->externalTrigEdge
              << std::endl; 
    std::cout << "TriggerConfiguration::postDelay........." 
              << std::endl; 
    this->postDelay.dump();
    std::cout << "TriggerConfiguration::middleDelay........." 
              << std::endl; 
    this->middleDelay.dump();
  }
} TriggerConfiguration;


// ============================================================================
// Acquisition definition
// These parameters define the acquisition (trigger mode, active channels, ...)
// ============================================================================
typedef struct AcquisitionDefinition
{
  //- members --------------------
  // Configuration identifier
  yat::uint16 configId;

  // Configuration name
  std::string configName;

  // List of active channels
  Channels_t activeChannels;

  // Trigger configuration
  TriggerConfiguration triggerConfig;

  //- default constructor -----------------------
  AcquisitionDefinition ()
    : configId(1),
      configName("")
  {
  }

  //- destructor -----------------------
  ~AcquisitionDefinition ()
  {
    activeChannels.clear();
  }

  //- copy constructor ------------------
  AcquisitionDefinition (const AcquisitionDefinition& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const AcquisitionDefinition & operator= (const AcquisitionDefinition& src)
  {
      if (this == & src) 
        return *this;

      this->configId = src.configId;
      this->configName = src.configName;
      this->activeChannels = src.activeChannels;
      this->triggerConfig = src.triggerConfig;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "AcquisitionDefinition::configId........." 
              << this->configId
              << std::endl; 
    std::cout << "AcquisitionDefinition::configName........." 
              << this->configName
              << std::endl; 
    std::cout << "AcquisitionDefinition::activeChannels........." 
              << std::endl; 
    for (size_t c = 0; c < activeChannels.size(); c++)
      activeChannels[c].dump();
    
    std::cout << "AcquisitionDefinition::triggerConfig........." 
              << std::endl;
    triggerConfig.dump();
  }
} AcquisitionDefinition;

// ============================================================================
// Raw acquisition configuration
// Acquisition configuration from Device property Config<i>
// ============================================================================
typedef struct RawAcquitisionConfig
{
  //- members --------------------
  // Configuration identifier
  yat::uint16 configId;

  // List of <key,value> config parameters
  AcquisitionKeys_t configKeyList;

  //- default constructor -----------------------
  RawAcquitisionConfig ()
    : configId(1)
  {
  }

  //- destructor -----------------------
  ~RawAcquitisionConfig ()
  {
    configKeyList.clear();
  }

  //- copy constructor ------------------
  RawAcquitisionConfig (const RawAcquitisionConfig& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const RawAcquitisionConfig & operator= (const RawAcquitisionConfig& src)
  {
      if (this == & src) 
        return *this;

      this->configId = src.configId;
      this->configKeyList = src.configKeyList;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "RawAcquitisionConfig::configId........." 
              << this->configId
              << std::endl; 
    std::cout << "RawAcquitisionConfig::configKeyList........." 
              << std::endl; 

    for (AcquisitionKeys_t::const_iterator it = this->configKeyList.begin(); it != this->configKeyList.end(); ++it)
    {
      std::cout << "{" << it->first << "," << it->second << "}" << std::endl;
    }
  }
} RawAcquitisionConfig;

// ============================================================================
// Acquisition data structure (contains all defined configurations)
// ============================================================================
typedef struct AIConfiguration
{
  //- members --------------------
  // DAQ board definition
  DaqBoard daqBoard;

  // List of acquisition definitions
  std::vector<RawAcquitisionConfig> acqDefinitions;

  // Acquisition buffer configuration
  AcqBufferConfiguration acqBufferConfig;

  // Flyscan spool
  std::string flyscanSpool;

  //- default constructor -----------------------
  AIConfiguration ()
    :flyscanSpool("unknown")
  {
    acqDefinitions.clear();
  }

  //- destructor -----------------------
  ~AIConfiguration ()
  {
    acqDefinitions.clear();
  }

  //- copy constructor ------------------
  AIConfiguration (const AIConfiguration& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const AIConfiguration & operator= (const AIConfiguration& src)
  {
      if (this == & src) 
        return *this;

      this->daqBoard = src.daqBoard;
      this->acqDefinitions = src.acqDefinitions;
      this->acqBufferConfig = src.acqBufferConfig;
      this->flyscanSpool = src.flyscanSpool;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "AIConfiguration::........." << std::endl; 
    this->daqBoard.dump();

    for (size_t c = 0; c < acqDefinitions.size(); c++)
      acqDefinitions[c].dump();

    this->acqBufferConfig.dump();

    std::cout << "Flyscan spool............"
              << this->flyscanSpool
              << std::endl; 
  }
} AIConfiguration;


} //- namespace aicontroller

#endif //- _TYPES_AND_CONSTS_H_

