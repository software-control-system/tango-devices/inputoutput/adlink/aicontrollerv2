//=============================================================================
// BoardTypeFactory.cpp
//=============================================================================
// abstraction.......Board type factory for AIControllerV2 Device
// class.............BoardTypeFactory
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BoardTypeFactory.h"
#include "ADSAI2005_BoardType.h"
#include "ADSAI2006_BoardType.h"
#include "ADSAI2010_BoardType.h"
#include "ADMAI2204_BoardType.h"
#include "ADMAI2205_BoardType.h"
#include "ADMAI2206_BoardType.h"

namespace aicontroller
{

// ======================================================================
// BoardTypeFactory::instanciate
// ======================================================================
BoardType * BoardTypeFactory::instanciate (E_BoardTypes_t board_type)
 throw (Tango::DevFailed)
{
  BoardType * board = 0;

  switch (board_type)
  {
    case ADLINK_SAI_2005:
      board = new ADSAI2005_BoardType();
      break;
      
    case ADLINK_SAI_2006:
      //- not supported by current version of ASL
      //board = new ADSAI2006_BoardType();
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("SAI 2006 board type not currently supported"),
                      _CPTC("BoardTypeFactory::instanciate"));
      break;

    case ADLINK_SAI_2010:
      board = new ADSAI2010_BoardType();
      break;

    case ADLINK_MAI_2204:
      board = new ADMAI2204_BoardType();
      break;
      
    case ADLINK_MAI_2205:
      board = new ADMAI2205_BoardType();
      break;

    case ADLINK_MAI_2206:
      //- not supported by current version of ASL
      //board = new ADMAI2206_BoardType();
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("SAI 2206 board type not currently supported"),
                      _CPTC("BoardTypeFactory::instanciate"));
      break;

    default:
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid board type specified [check device property]"),
                      _CPTC("BoardTypeFactory::instanciate"));
      break;
  }
 
  return board;
}

} // namespace aicontroller


