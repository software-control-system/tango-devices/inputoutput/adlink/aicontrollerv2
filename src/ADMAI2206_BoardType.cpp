//=============================================================================
// ADMAI2206_BoardType.cpp
//=============================================================================
// abstraction.......ADLINK MAI 2206 Board type
// class.............ADMAI2206_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ADMAI2206_BoardType.h"

namespace aicontroller
{

// ======================================================================
// ADMAI2206_BoardType::ADMAI2206_BoardType
// ======================================================================
ADMAI2206_BoardType::ADMAI2206_BoardType ()
 : BoardType()
{
  // set board characteristics
  m_isMultiplexed = true;

  //- TODO : default values (waiting for ASL modifications)
  /*
  m_maxSamplingRate = AD2206_MAX_SRATE;
  m_minSamplingRate = AD2206_MIN_SRATE;
  m_maxChannelsNb  = AD2206_NUM_CHANNELS;*/

  m_maxSamplingRate = AD2205_MAX_SRATE;
  m_minSamplingRate = AD2205_MIN_SRATE;
  m_maxChannelsNb  = AD2205_NUM_CHANNELS;
}

// ======================================================================
// ADMAI2206_BoardType::~ADMAI2206_BoardType
// ======================================================================
ADMAI2206_BoardType::~ADMAI2206_BoardType ()
{
}


} // namespace aicontroller


