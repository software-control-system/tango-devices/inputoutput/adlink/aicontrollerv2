//=============================================================================
// ADMAI2205_BoardType.h
//=============================================================================
// abstraction.......ADLINK MAI 2205 Board type
// class.............ADMAI2205_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _ADMAI2205_BOARD_H_
#define _ADMAI2205_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BoardType.h"

namespace aicontroller
{


// ============================================================================
// class: ADMAI2205_BoardType
// ============================================================================
class ADMAI2205_BoardType : public BoardType
{
public:
  //- constructor
  ADMAI2205_BoardType ();

  //- destructor
  virtual ~ADMAI2205_BoardType ();
  
  //- board model name as string 
  virtual std::string board_model_name () const
  {
    return kBOARD_TYPE_ADMAI2205_STR;
  } 

  //- board model as reference 
  virtual unsigned short board_model_id () const 
  {
    return adl::DAQ2205;
  }
};

} // namespace aicontroller

#endif // _ADMAI2205_BOARD_H_
