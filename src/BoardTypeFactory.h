//=============================================================================
// BoardTypeFactory.h
//=============================================================================
// abstraction.......Board type factory for AIControllerV2 Device
// class.............BoardTypeFactory
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _BOARD_TYPE_FACTORY_H_
#define _BOARD_TYPE_FACTORY_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/ExceptionHelper.h>
#include "BoardType.h"

namespace aicontroller
{

// ============================================================================
// class: BoardTypeFactory
// ============================================================================
class BoardTypeFactory
{
public: 
  //- instanciate a specialized Board Type
  static BoardType * instanciate (E_BoardTypes_t board_config)
    throw (Tango::DevFailed);

private:
  BoardTypeFactory ();
  ~BoardTypeFactory ();
};

} // namespace aicontroller

#endif // _BOARD_TYPE_FACTORY_H_
