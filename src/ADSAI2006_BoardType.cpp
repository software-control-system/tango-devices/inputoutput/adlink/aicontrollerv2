//=============================================================================
// ADSAI2006_BoardType.cpp
//=============================================================================
// abstraction.......ADLINK SAI 2006 Board type
// class.............ADSAI2006_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ADSAI2006_BoardType.h"

namespace aicontroller
{

// ======================================================================
// ADSAI2006_BoardType::ADSAI2006_BoardType
// ======================================================================
ADSAI2006_BoardType::ADSAI2006_BoardType ()
 : BoardType()
{
  // set board characteristics
  m_isMultiplexed = false;

  //- TODO : default values (waiting for ASL modifications)
  /*
  max_sampling_rate = AD2006_MAX_SRATE;
  min_sampling_rate = AD2006_MIN_SRATE;
  max_channels_num  = AD2006_NUM_CHANNELS; */

  m_maxSamplingRate = AD2005_MAX_SRATE;
  m_minSamplingRate = AD2005_MIN_SRATE;
  m_maxChannelsNb  = AD2005_NUM_CHANNELS;
}

// ======================================================================
// ADSAI2006_BoardType::~ADSAI2006_BoardType
// ======================================================================
ADSAI2006_BoardType::~ADSAI2006_BoardType ()
{
}


} // namespace aicontroller


