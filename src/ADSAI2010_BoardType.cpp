//=============================================================================
// ADSAI2010_BoardType.cpp
//=============================================================================
// abstraction.......ADLINK SAI 2010 Board type
// class.............ADSAI2010_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ADSAI2010_BoardType.h"

namespace aicontroller
{

// ======================================================================
// ADSAI2010_BoardType::ADSAI2010_BoardType
// ======================================================================
ADSAI2010_BoardType::ADSAI2010_BoardType ()
 : BoardType()
{
  // set board characteristics
  m_isMultiplexed = false;
  m_maxSamplingRate = AD2010_MAX_SRATE;
  m_minSamplingRate = AD2010_MIN_SRATE;
  m_maxChannelsNb  = AD2010_NUM_CHANNELS;
}

// ======================================================================
// ADSAI2010_BoardType::~ADSAI2010_BoardType
// ======================================================================
ADSAI2010_BoardType::~ADSAI2010_BoardType ()
{
}

} // namespace aicontroller


