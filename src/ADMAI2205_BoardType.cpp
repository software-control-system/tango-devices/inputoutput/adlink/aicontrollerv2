//=============================================================================
// ADMAI2205_BoardType.cpp
//=============================================================================
// abstraction.......ADLINK MAI 2205 Board type
// class.............ADMAI2205_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ADMAI2205_BoardType.h"

namespace aicontroller
{

// ======================================================================
// ADMAI2205_BoardType::ADMAI2205_BoardType
// ======================================================================
ADMAI2205_BoardType::ADMAI2205_BoardType ()
 : BoardType()
{ 
  // set board characteristics
  m_isMultiplexed = true;
  m_maxSamplingRate = AD2205_MAX_SRATE;
  m_minSamplingRate = AD2205_MIN_SRATE;
  m_maxChannelsNb  = AD2205_NUM_CHANNELS;
}

// ======================================================================
// ADMAI2205_BoardType::~ADMAI2205_BoardType
// ======================================================================
ADMAI2205_BoardType::~ADMAI2205_BoardType ()
{
}


} // namespace aicontroller


