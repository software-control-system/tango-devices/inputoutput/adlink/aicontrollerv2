//=============================================================================
// AcqMode.h
//=============================================================================
// abstraction.......Acquisition mode abstraction - implements ASL::ContinuousAI
// class.............AcqMode
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _ACQ_MODE_H_
#define _ACQ_MODE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <asl/ContinuousAIConfig.h>
#include <asl/ContinuousAI.h>
#include <yat4tango/ExceptionHelper.h>
#include <yat4tango/DynamicAttributeManager.h>
#include "AcquisitionTypes.h"
#include "NexusManager.h"
#include "BoardType.h"
#include "ConfigurationParser.h"

#if !defined __TANGO_EVENTS__
#include <yat4tango/MonitoredDeviceTask.h>
#endif

namespace aicontroller
{

//-----------------------------------------------------------------------------
//- TYPES & CONSTs

//- Nexus pre & post fixes for item names
const std::string kNEXUS_PREFIX_AV_ITEM = kATTR_NAME_TAG_AVERAGE;
const std::string kNEXUS_PREFIX_PEAK_ITEM = kATTR_NAME_TAG_PEAK;
const std::string kNEXUS_PREFIX_RMS_ITEM = kATTR_NAME_TAG_RMS;
const std::string kNEXUS_POSTFIX_RAW_ITEM = "_RAW";
const std::string kNEXUS_PREFIX_DATA_HIST_ITEM = kATTR_NAME_TAG_DATA_HIST;
const std::string kNEXUS_PREFIX_USER_DATA_ITEM = kATTR_NAME_TAG_USER_DATA;
const std::string kNEXUS_PREFIX_TIMESTAMP_ITEM = kATTR_NAME_TAG_TIMESTAMP;
const std::string kNEXUS_PREFIX_DELTATIME_ITEM = kATTR_NAME_TAG_RELATIVE_DATE;

#ifdef __TANGO_EVENTS__
//- User data device proxy list
typedef std::map<Tango::DeviceProxy *, yat::int32> UserDataDevices_t;
typedef std::pair<Tango::DeviceProxy *, yat::int32> UserDataDevices_pair_t;
typedef UserDataDevices_t::iterator UserDataDevices_it_t;
#else
//- DeviceProxies_t: list of user data <device name, attribute> to connect to
//- map key is a combination of gain/offset1/offset2 string & channel number
const std::string kPROXY_USER_DATA_GAIN   = "udGAIN_";
const std::string kPROXY_USER_DATA_OFFSET1   = "udOFFSET1_";
const std::string kPROXY_USER_DATA_OFFSET2   = "udOFFSET2_";

//- User data monitored device list
typedef std::map<std::string, yat4tango::MonitoredDeviceTask *> UserDataDevices_t;
typedef std::pair<std::string, yat4tango::MonitoredDeviceTask *> UserDataDevices_pair_t;
typedef UserDataDevices_t::iterator UserDataDevices_it_t;
#endif
//-----------------------------------------------------------------------------

#ifdef __TANGO_EVENTS__
class AcqMode;
//-----------------------------------------------------------------------------
//- Tango events callback classes for user data gain, offset1 & offset2
//- for channels 0 to 4
//- TODO : define from 0 to 4 channels call back (! possibly up to 64 channels for 2204 boards!)

class UserDataGainEventCallBack : public Tango::CallBack
{
  public:
    UserDataGainEventCallBack(AcqMode * acq_mode);

    virtual ~UserDataGainEventCallBack() {}

    void push_event(Tango::EventData*);

  protected:
    AcqMode * m_acqMode;
};

class UserDataOffset1EventCallBack : public Tango::CallBack
{
  public:
    UserDataOffset1EventCallBack(AcqMode * acq_mode);

    virtual ~UserDataOffset1EventCallBack() {}

    void push_event(Tango::EventData*);

  protected:
    AcqMode * m_acqMode;
};

class UserDataOffset2EventCallBack : public Tango::CallBack
{
  public:
    UserDataOffset2EventCallBack(AcqMode * acq_mode);

    virtual ~UserDataOffset2EventCallBack() {}

    void push_event(Tango::EventData*);

  protected:
    AcqMode * m_acqMode;
};
//-----------------------------------------------------------------------------
#endif


// ============================================================================
// class: AcqMode
// This class provides common functions and default behavior.
// ============================================================================
class AcqMode : public asl::ContinuousAI, public Tango::LogAdapter
{
public:

  //- constructor
  AcqMode(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~AcqMode();
  
  //---------------------------------------------------------------------------
  //--- asl::ContinuousAI inherited methods
  //---------------------------------------------------------------------------
  /**
   * Data processing user hook.
   * It is the user responsability to delete the passed AIBuffer.
   */
  virtual void handle_input (asl::AIRawData* raw_data);  

  /**
   * DAQ exception user hook.
   */
  virtual void handle_error (const asl::DAQException& ex);
	
  /**
   * Data lost user hook.
   * This default implementation does nothing.
   */
  virtual void handle_data_lost ();  

  /**
   * DAQ end user hook.
   * If enabled, the history buffer is frozen before calling this hook.
	 * This default implementation does nothing.
   */
  virtual void handle_daq_end (ContinuousDAQ::DaqEndReason why);  

  /**
   * Timeout user hook.
   * This default implementation does nothing.
   */
  virtual void handle_timeout (void);


  //---------------------------------------------------------------------------
  //--- Local methods
  //---------------------------------------------------------------------------

  //- Initializes DAQ board
  void initDaqBoard (unsigned short board_id, BoardType* board_impl)
    throw (Tango::DevFailed);

  //- Sets new acquisition configuration
  void configureAcquisition (RawAcquitisionConfig raw_acq_config, 
                             AcqBufferConfiguration acq_buffer_config,
                             AcquisitionParameters current_acq_param)
    throw (Tango::DevFailed);

  //- Removes dynamic interface (useful in case of configuration update)
  void cleanInterface()
    throw (Tango::DevFailed);

  //- Starts new acquisition
  void startAcquisition ()
    throw (Tango::DevFailed);

  //- Stops current acquisition
  virtual void stopAcquisition ()
    throw (Tango::DevFailed);

  //- Aborts current acquisition
  virtual void abortAcquisition ()
    throw (Tango::DevFailed);

  //- Sets new current sampling rate (in Hz)
  void setSamplingRate (double rate)
    throw (Tango::DevFailed);

  //- Sets new integration time (in ms)
  void setIntegrationTime (double it)
    throw (Tango::DevFailed);

  //- Sets new nexus file path
  void setNexusFilePath (std::string path);

  //- Sets new nexus file generation flag
  void setNexusFileGeneration (bool enable);

  //- Sets new number of acquisition per nexus file
  void setNexusNbPerFile(yat::uint16 nb);

  //- Gets current Nexus file base name
  std::string getNxFileBaseName();

  //- Gets current list of items (Nexus id) recorded in Nexus files
  std::vector<std::string> getNxDataItems();

  //- Sets Nexus spool name (for flyscan needs)
  void setNexusSpool(std::string spool)
  {
    this->m_nxSpoolName = spool;
  }

  //- Sets nexus dataset with format: dataset type@dataset name 
  //- (dataset type is optional, default = The specified dataset name replaces the channel
  //- label for all dataset types)
  void updateNxDataset(yat::uint16 chan_nb, std::string dataset)
    throw (Tango::DevFailed);

  //- Gets nexus information for all defined channels 
  //- Returns a map< channel id, infos string> with info string containing:
  //-  NAME:<channel label>  
  //-  ENABLED:true // always true in SAI case (if defined, enabled)
  //-  DATASET_NAME:<name of the dataset>
  //-  DATASET_ENABLED:<true / false>
  //-  DATASET_ATTR:<name of the dataset enabled TANGO attribute>
  std::map<yat::uint16, std::string> getChannelNxInfo()
    throw (Tango::DevFailed);

  //- Sets new statistics history depth
  void setStatisticsHistoryDepth (yat::uint32 depth)
    throw (Tango::DevFailed);

  //- Sets new data history depth (in multiple of integration time)
  void setDataHistoryDepth (yat::uint16 depth)
    throw (Tango::DevFailed);

  //- Sets new timestamp histories depth
  void setTimestampHistoriesDepth(yat::uint32 depth)
    throw (Tango::DevFailed);

  //- Gets current ReTrig counter
  virtual yat::uint32 getReTrigCnt ()
  {
    //- default behavior: do not use retrig counter but inifinite acquisition
    return kINFINITE_ACQ;
  }

  //- Gets current auto tuning value
  bool getAutoTuning ()
  {
    return this->m_dataTrt.autoTuning;
  }

  //- Gets current buffer depth
  yat::uint32 getBufferDepth()
  {
    return (size_t)this->m_daqConfig.get_buffer_depth();
  }

  //- Gets current number of active channels
  size_t getActiveChanNb()
  {
    return (size_t)this->m_daqConfig.num_active_channels();
  }

  //- Gets current overrun strategy
  E_OverrunStrategy_t getOvrStrategy()
  {
    return this->m_acqDefinition.triggerConfig.overrunStrategy;
  }

  //- Gets current ovverun counter
  yat::uint32 getOverrunCounter()
  {
    return this->m_acqData.overrunCounter;
  }

  //- Gets current acquisition data
  AcquisitionData getData ()
  {
    yat::AutoMutex<> guard(this->m_dataLock);
    return this->m_acqData;
  }

  //- Gets DAQ board state
  Tango::DevState getAcqState();

  //- Gets Nexus storage state
  Tango::DevState getNexusState()
    throw (Tango::DevFailed);

  //- Gets handle input frequency
  double getHandleInputFreq()
  {
    return this->m_acqData.dataNotifFrequency;
  }

  //- Gets acquisition errors
  bool getAcqError(std::string & error_msg)
  {
    error_msg = this->m_errorMsg;
    return this->m_errorOccurred;
  }

  //- Sets user data gain for channel index
  void setUserDataGain(yat::uint16 chan_idx, double gain, bool isRunning)
    throw (Tango::DevFailed);

  //- Gets user data gain for channel index
  double getUserDataGain(yat::uint16 chan_idx)
    throw (Tango::DevFailed);

  //- Sets user data offset1 for channel index
  void setUserDataOffset1(yat::uint16 chan_idx, double offset, bool isRunning)
    throw (Tango::DevFailed);

  //- Gets user data offset1 for channel index
  double getUserDataOffset1(yat::uint16 chan_idx)
    throw (Tango::DevFailed);

  //- Sets user data offset2 for channel index
  void setUserDataOffset2(yat::uint16 chan_idx, double offset, bool isRunning)
    throw (Tango::DevFailed);

  //- Gets user data offset2 for channel index
  double getUserDataOffset2(yat::uint16 chan_idx)
    throw (Tango::DevFailed);


  //---------------------------------------------------------------------------
  //- Callback functions:
  //---------------------------------------------------------------------------
  //- read callback function for raw data
  void read_callback_raw_data(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for scaled data
  void read_callback_scaled_data(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for average data
  void read_callback_average_data(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for average history
  void read_callback_average_history(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for rms value
  void read_callback_rms(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for peak-to-peak value
  void read_callback_peak(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for data history
  void read_callback_data_history(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for user data
  void read_callback_user_data(yat4tango::DynamicAttributeReadCallbackData& cbd);
  
  //- read callback function for user data gain
  void read_callback_user_data_gain(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for user data gain
  void write_callback_user_data_gain(yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- read callback function for user data offset1
  void read_callback_user_data_offset1(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for user data offset1
  void write_callback_user_data_offset1(yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- read callback function for user data offset2
  void read_callback_user_data_offset2(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for user data offset2
  void write_callback_user_data_offset2(yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- read callback function for scaled average data
  void read_callback_scaled_average_data(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for buffer timestamps
  void read_callback_buffer_timestamps(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for relative buffer datation
  void read_callback_relative_buffer_datation(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for timestamp histories depth
  void read_callback_timestamp_histo_depth(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for timestamp histories depth
  void write_callback_timestamp_histo_depth(yat4tango::DynamicAttributeWriteCallbackData& cbd);

  
protected:
  
  //---------------------------------------------------------------------------
  //- Data mapping:
  //---------------------------------------------------------------------------

  //- Extracts acquisition definition from raw config
  //- Sets m_acqDefinition
  void extractAcquisitionDefinition(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Extracts general data treatment definition from raw config
  //- Sets m_dataTrt
  void extractDataTrtDefinition(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Maps AI configuration towards asl::ContinuousAIConfig class
  //- Sets m_daqConfig from m_acqDefinition
  void continuousAIconfigMapper()
    throw (Tango::DevFailed);

  //- Converts range from string to asl::ContinuousAIConfig type
  adl::Range convertRange(std::string range_str)
    throw (Tango::DevFailed);

  //- Extracts specific acquisition definition from raw config according
  //- to acquisition mode
  //- Sets m_acqDefinition
  virtual void extractSpecificAcqDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed) = 0;

  //- Extracts specific data treatment definition from raw config
  virtual void extractSpecificDataTrtDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Sets specific configuration depending on acquisition mode
  virtual void specificAIconfigMapper_i()
      throw (Tango::DevFailed) = 0;


  //---------------------------------------------------------------------------
  //- Data buffers management:
  //---------------------------------------------------------------------------

  //- Initializes data buffers
  void initDataBuffers()
    throw (Tango::DevFailed);

  //- Deletes data buffers
  void deleteDataBuffers();

  //- Allocates data buffer memory for raw & scaled data
  //- size = specified depth
  void setDataBufferMem(unsigned long depth);

  //- De-allocates data buffer memory for raw & scaled data
  void unsetDataBufferMem();

  //- Allocates memory for statistics history buffer
  //- size = specified depth
  void setStatHistoBufferMem(unsigned long depth);

  //- Allocates memory for data history buffer
  //- size = specified depth
  void setDataHistoBufferMem(unsigned long depth);

  //- Allocates memory for timestamp history buffers
  //- size = specified depth
  void setTimestampHistoBufferMem(unsigned long depth);

  //---------------------------------------------------------------------------
  //- Specific data treatments:
  //- The default behavior is implemented in AcqMode class.
  //- A specific treatment must be implemented in the acquisition mode class.
  //---------------------------------------------------------------------------
  
  //- Data processing user hook.
  //- No default behaviour.
  virtual void handle_input_i(asl::AIRawData* raw_data)
    throw (Tango::DevFailed) = 0;

  //- DAQ exception user hook.
  virtual void handle_error_i()
    throw (Tango::DevFailed)
  {
    // default behavior: nothing more to do
  }
	
  //- Data lost user hook.
  virtual void handle_data_lost_i()
    throw (Tango::DevFailed)
  {
    // default behavior: nothing more to do
  }

  //- DAQ end user hook.
  virtual void handle_daq_end_i()
    throw (Tango::DevFailed)
  {
    // default behavior: nothing more to do
  }

  //- Timeout user hook.
  virtual void handle_timeout_i()
    throw (Tango::DevFailed)
  {
    // default behavior: nothing more to do
  }

  //- Initializes specific attributes
  virtual void initSpecificAttributes_i()
    throw (Tango::DevFailed);


  //---------------------------------------------------------------------------
  //- Utilities:
  //---------------------------------------------------------------------------

	//- Converts daq exception to a tango exception.
	//- \param de The daq exception.
	Tango::DevFailed daqToTangoException (const asl::DAQException& de);

	//- Computes RMS value.
	//- \param Varray List of values.
  //- \param Vmoz Average value of the list of values.
  double rmsCalculation (yat::Buffer<double> Varray, double Vmoy);

	//- Template class to store a current value in a Device property.
	//- \param value Value to store.
  //- \param property_name Name to use.
  template <class T>
  void store_value_as_property(T value, std::string property_name)
    throw (Tango::DevFailed)
  {
	  Tango::DbDatum current_value(property_name);
    current_value << value;
    Tango::DbData db_data;
    db_data.push_back(current_value);

    try
    {
      this->m_hostDev->get_db_device()->put_property(db_data);
    }
    catch(Tango::DevFailed &df)
    {     
      ERROR_STREAM << df << endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("SOFTWARE_FAILURE"),
                        _CPTC("Error while storing properties in database"),
                        _CPTC("AcqMode::store_value_as_property"));
    }
  }

  //- Associates a timestamp to the received trigger,
  // in string format: Www Mmm dd hh:mm:ss yyyy.
  void stampTrigger_c();

  //- Associates a timestamp to the received buffer,
  // in seconds since EPOCH.
  void stampBuffer_s();

  //- Computes data history for specified channel id:
  //- � get ordered history from ASL
  //- � update data structure for specified channel
  void computeDataHistory(unsigned int chan_id)
    throw (Tango::DevFailed);

  //- Computes whole data history
  //- � get ordered history from ASL
  //- � update data structure
  void computeWholeDataHistory()
    throw (Tango::DevFailed);

  //- Returns data history validity
  virtual bool getDataHistoryValidity()
  {
    // default value
    return true;
  }

  //- Suspends or restarts user data proxies polling
  //- polling: true => resume, false => suspend
  void pollUserDataProxy(bool polling)
    throw (Tango::DevFailed);

  //- Analyzes the specified data mask and sets the boolean mask
  //- in data treatment structure.
  void setBoolDataMask(std::string mask)
    throw (Tango::DevFailed);

  //- Extracts time slots from mask string.
  TimeSlotMask_t parseTimeSlotMask(std::string mask)
    throw (Tango::DevFailed);

  //---------------------------------------------------------------------------
  //- Nexus management:
  //---------------------------------------------------------------------------

  //- Computes the Nexus item list according to acquisition configuration
  nxItemList_t getNxItemList()
    throw (Tango::DevFailed);

  //- Sets the Nexus data types flags & tells if list is empty or not
  bool initNexusTypes()
    throw (Tango::DevFailed);

  //- Stores dataset names for each dataset type and each defined channel
  //- map< channel id, map< dataset type, dataset name> >
  //- with: channel id: from 0 to N
  //-       dataset type: RAW, SCALED, AVERAGE, RMS, PEAK, USER, DATA_HIST
  std::map< yat::uint16, std::map<std::string, std::string> > m_datasetNames;
  

  //---------------------------------------------------------------------------
  //- Dynamic attributes management:
  //---------------------------------------------------------------------------

  //- Creates dynamic attributes according to current acquisition configuration
  //- Calls define_specific_attributes_i()
  virtual std::vector<yat4tango::DynamicAttributeInfo> define_acquisition_attributes();

  //- Creates specific dynamic attributes according to acquisition mode
  virtual void define_specific_attributes_i(std::vector<yat4tango::DynamicAttributeInfo>& dynAttrList)
  {
    // default behaviour: nothing more to add
  }

  //- Gets channel id from its label
  //- Throws exception if label not found
  yat::uint16 get_channel_id_from_label(std::string label)
    throw (Tango::DevFailed);

  //- Gets channel index from its label
  //- Throws exception if label not found
  yat::uint16 get_channel_index_from_label(std::string label)
    throw (Tango::DevFailed);


  //- read callback function for data counter
  void read_callback_data_counter(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for error counter
  void read_callback_error_counter(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for overrun counter
  void read_callback_overrun_counter(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for timeout counter
  void read_callback_timeout_counter(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for handle input call frequency
  void read_callback_handle_input_freq(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for statistics history buffer depth
  void read_callback_stat_history_buffer_depth(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for statistics history buffer depth
  void write_callback_stat_history_buffer_depth(yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- read callback function for historized data buffer depth
  void read_callback_historized_data_buffer_depth(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for historized data buffer depth
  void write_callback_historized_data_buffer_depth(yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- read callback function for enable/disable dataset
  void read_callback_enable_dataset(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for enable/disable dataset
  void write_callback_enable_dataset(yat4tango::DynamicAttributeWriteCallbackData& cbd);
  

  //---------------------------------------------------------------------------
  //- user data management:
  //---------------------------------------------------------------------------

  //- Tells if user data proxy(ies) is(are) needed
  bool needUserDataProxy();

#ifdef __TANGO_EVENTS__
  //- Defines user data proxies for each parameter (gain, offset1 & offset2).
  //- Subscribes to Tango events.
  void defineUserDataEvents()
    throw (Tango::DevFailed);

  //- List of Device proxies & event ids
  UserDataDevices_t m_evtProxies;
#else

  //- Defines user data proxies for each parameter (gain, offset1 & offset2).
  //- Calls initUserDataDeviceProxy().
  void defineUserDataProxies()
    throw (Tango::DevFailed);

  //- Creates & initializes a user data monitored device task.
  //- proxy_type: type of proxy
  //- proxy: proxy definition
  void initUserDataDeviceProxy(std::string proxy_type, UserDataProxy& proxy)
    throw (Tango::DevFailed);

  //- List of user data monitored device tasks
  UserDataDevices_t m_udDeviceTaskList;

  size_t m_userDataProxyPollingPeriod;
#endif

  //---------------------------------------------------------------------------
  //- Local members:
  //---------------------------------------------------------------------------

  //- Current general acquisition data (updated by handle_xxx() functions)
  AcquisitionData m_acqData;

  //- Current specific acquisition data
  SpecificAcquisitionData m_dynAcqData;

  //- Current acquisition parameters
  //- Warning: frequency = current sampling rate (updated by setSamplingRate() function)
  AcquisitionParameters m_acquisitionParam;

  //- Initial acquisition definition (local format)
  //- Warning: frequency = initial sampling rate (not updated after daq config)
  AcquisitionDefinition m_acqDefinition;

  //- Initial acquisition definition (ASL format)
  //- to be used after mapping (continuousAIconfigMapper() call)
  //- Warning: frequency = initial sampling rate (not updated after daq config)
  asl::ContinuousAIConfig m_daqConfig;

  //- Data treatment definition
  DataTreatmentDefinition m_dataTrt;

  //- Initial acquisition buffer configuration
  AcqBufferConfiguration m_bufferConfig;

  //- Nexus manager
  NexusManager * m_nexusManager;

  //- Host device (for logging)
  Tango::DeviceImpl * m_hostDev;

  //- Board type implementation (for board specific parameters)
  BoardType * m_boardImpl;

  //- dynamic attribute manager
  yat4tango::DynamicAttributeManager * m_dynAttrManager;

  //- Board id
  yat::uint16 m_boardId;

  //- Pointer to reusable scaled data buffer
  asl::AIScaledData * m_scaledData;

  //- Pointer to reusable user data buffer
  asl::AIScaledData * m_userData;

  //- Daq end event received flag
  bool m_daqEndEvtReceived;

  //- daq-stop marker flag
  bool m_stopDoneFromHandleInput;

  //- Nexus storage started flag
  bool m_nxStorageStarted;

  //- Nexus spool name
  std::string m_nxSpoolName;

  //- Data types to store in Nexus files flags
  bool m_nxStoreRAW;
  bool m_nxStoreSCALED;
  bool m_nxStoreAVERAGE;
  bool m_nxStoreRMS;
  bool m_nxStorePEAK;
  bool m_nxStoreDATA_HIST;
  bool m_nxStoreUSER_DATA;
  bool m_nxStoreTIMESTAMP;

  //- Handle input metrics
  yat::Timer m_handleInputTimer;
  size_t m_handleInputCpt;

  //- data mutex protection
  yat::Mutex m_dataLock;

  //- error handling
  std::string m_errorMsg;
  bool m_errorOccurred;

  //- init flag
  bool m_modeInitializing;
  
  //- first timestamp
  double m_firstTimestamp;
};

} // namespace aicontroller

#endif // _ACQ_MODE_H_
