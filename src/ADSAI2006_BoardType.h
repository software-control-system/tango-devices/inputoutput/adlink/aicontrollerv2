//=============================================================================
// ADSAI2006_BoardType.h
//=============================================================================
// abstraction.......ADLINK SAI 2006 Board type
// class.............ADSAI2006_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _ADSAI2006_BOARD_H_
#define _ADSAI2006_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BoardType.h"

namespace aicontroller
{


// ============================================================================
// class: ADSAI2006_BoardType
// ============================================================================
class ADSAI2006_BoardType : public BoardType
{
public:
  //- constructor
  ADSAI2006_BoardType ();

  //- destructor
  virtual ~ADSAI2006_BoardType ();
  
  //- board model name as string 
  virtual std::string board_model_name () const
  {
    return kBOARD_TYPE_ADSAI2006_STR;
  }

  //- board model as reference 
  virtual unsigned short board_model_id () const 
  {
    return adl::DAQ2006;
  }
};

} // namespace aicontroller

#endif // _ADSAI2006_BOARD_H_
