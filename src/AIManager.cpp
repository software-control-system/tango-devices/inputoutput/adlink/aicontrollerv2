//=============================================================================
// AIManager.cpp
//=============================================================================
// abstraction.......Continuous acquisition manager implementation
// class.............AIManager
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "AIManager.h"
#include "BoardTypeFactory.h"
#include "ConfigurationParser.h"
#include <yat/utils/XString.h>
#include <yat/threading/Thread.h>

namespace aicontroller
{

//-----------------------------------------------------------------------------
//- Check nexus manager macro:
#define CHECK_ACQ_REF \
    if (! this->m_currentAcquisition) \
    { \
      THROW_DEVFAILED(_CPTC("INTERNAL_ERROR"), \
                      _CPTC("request aborted - the current acquisition mode isn't properly initialized"), \
                      _CPTC("AIManager::check_acq_ref")); \
    }
//-----------------------------------------------------------------------------
// Consts for Device task:
//-----------------------------------------------------------------------------
#define kDEFAULT_CMD_TMO 2000
#define kDEFAULT_PERIODIC_MSG_PERIOD 250
      
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
#define kUPDATE_CONFIG_MSG      (yat::FIRST_USER_MSG + 1000)
#define kABORT_ACQ_NX_MSG       (yat::FIRST_USER_MSG + 1001) 

// ============================================================================
// AIManager::Config::Config ()
// ============================================================================ 
AIManager::Config::Config ()
  : ai_config()
{
  this->host_device = 0;
}

// ============================================================================
// AIManager::Config::Config
// ============================================================================
AIManager::Config::Config (const AIManager::Config & src)
{
  *this = src;
}

// ============================================================================
// AIManager::Config::operator=
// ============================================================================
const AIManager::Config & AIManager::Config::operator= (const AIManager::Config & src)
{
  if (&src == this)
    return *this;
    
  this->host_device = src.host_device;    
  this->ai_config = src.ai_config;

  return *this;
}

// ============================================================================
// AIManager::Config::dump
// ============================================================================
void AIManager::Config::dump () 
{                    
  this->ai_config.dump();
}

// ======================================================================
// AIManager::AIManager
// ======================================================================
AIManager::AIManager(const AIManager::Config& cfg)
: yat4tango::DeviceTask(cfg.host_device),
   m_hostDev(cfg.host_device),
   m_aiConfiguration(cfg.ai_config),
   m_currentConfigPos(-1),
   m_currentAcquisition(NULL),
   m_board(NULL),
   m_initInProgress(true),
   m_initInError(false),
   m_configChangeInProgress(false),
   m_prevRetrigCnt(0)
{
  //---------------------------------------------------------------------
  //- configure internal message handling 
  this->set_timeout_msg_period (0xFFFF);
  this->enable_timeout_msg (false);
  this->enable_periodic_msg (false);

  // get driver memory size and store it in local member for future use
  m_drvMem = this->getDriverMemorySize(); 

  m_nexus_file_generation_managed_by_config = false;
}

// ======================================================================
// AIManager::~AIManager
// ======================================================================
AIManager::~AIManager ()
{
  try
  {
    //- delete current acquisition reference
    if (this->m_currentAcquisition)
    {
      delete this->m_currentAcquisition;
      this->m_currentAcquisition = NULL;
    }
  }
  catch (...)
  {
    //- ignore any error
  }

  //- delete the board implementation
  if (this->m_board)
  {
    delete this->m_board;
    this->m_board = NULL;
  }
}

// ============================================================================
// AIManager::process_message
// ============================================================================
void AIManager::process_message (yat::Message& msg)
  throw (Tango::DevFailed) 
{
  //- handle msg
  switch (msg.type())
  {
    //- THREAD_INIT--
    case yat::TASK_INIT:
      {
        //- initalization
        this->init_i();
      } 
      break;
    //- TASK_EXIT----
    case yat::TASK_EXIT:
      {
        //- cleanup stage
        this->fini_i();
      } 
      break;  
    //- TASK_PERIODIC
    case yat::TASK_PERIODIC:
      {
        //- periodic activity
        this->periodic_job_i();
      } 
      break;
    //- UPDATE CONFIG---------
    case kUPDATE_CONFIG_MSG:
      {
        this->m_configChangeInProgress = true;
        unsigned short id = msg.get_data<unsigned short>();
        this->updateDaq_i(id);
      }
      break;
    //- ABORT ACQ ON NX PB--------- 
    case kABORT_ACQ_NX_MSG:
      {
        this->stop();
      }
      break;
    //- UNHANDLED MSG
    default:
      WARN_STREAM << "AIManager::process_message::unhandled msg type received" << std::endl;
      break;
  }
}

// ======================================================================
// AIManager::init_i
// ======================================================================
void AIManager::init_i ()
    throw (Tango::DevFailed)
{
  // init done in initDaq() function
}

// ======================================================================
// AIManager::fini_i
// ======================================================================
void AIManager::fini_i ()
    throw (Tango::DevFailed)
{
  //- delete the board implementation
  if (this->m_board)
  {
    delete this->m_board;
    this->m_board = NULL;
  }
}

// ============================================================================
// AIManager::periodic_job_i                      [triggered by PERIODIC msg]
// ============================================================================
void AIManager::periodic_job_i ()
  throw (Tango::DevFailed)
{
  if (m_currentAcquisition)
  {
    // In autotuning mode & in standby state, re-evaluate sampling frequency
    if (this->m_currentAcquisition->getAutoTuning() &&
        (this->m_currentAcquisition->getAcqState() == Tango::STANDBY))
    {
      this->updateAutoSamplingRate();
    }
  }
}

// ======================================================================
// AIManager::quitTask
// ======================================================================
void AIManager::quitTask ()
    throw (Tango::DevFailed)
{
  // Disable periodic messages
  this->enable_periodic_msg (false);

  try
  {
    if (this->m_currentAcquisition)
    {
      //- first abort acquisition if running
      if (Tango::RUNNING == this->m_currentAcquisition->getAcqState())
      {
        this->abort();
      }
		
      //- delete current acquisition reference
      delete this->m_currentAcquisition;
      this->m_currentAcquisition = NULL;
    }
  }
  catch (...)
  {
    //- ignore any error
  }

  // exit the task
  this->exit();
}

// ======================================================================
// AIManager::initDaq
// ======================================================================
void AIManager::initDaq(yat::uint16 config_id)
  throw (Tango::DevFailed)
{
  INFO_STREAM << "AIManager::initDaq - initialize config: " << config_id << std::endl;
  this->m_initInError = false;

  // local configuration parser
  ConfigurationParser config_parser(m_hostDev);

  try
  {
    //- delete current DAQ board if any
    if (this->m_board)
    {
      delete this->m_board;
      this->m_board = NULL;
    }

    //- instanciate new DAQ board
    this->m_board = BoardTypeFactory::instanciate (this->m_aiConfiguration.daqBoard.type);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::initDaq caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to instanciate DAQ board (caught DevFailed)!"),
                      _CPTC("AIManager::initDaq"));
  }
  catch (...)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::initDaq caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to instanciate DAQ board (caught [...])!"),
                    _CPTC("AIManager::initDaq"));
  }

  if (!this->m_board)
  {
    this->m_initInError = true;
    ERROR_STREAM << "DAQ board not properly initialized!" << std::endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                    _CPTC("Failed to initialize DAQ board"),
                    _CPTC("AIManager::initDaq"));
  }
  
  //- reset local members
  this->m_currentConfigPos = -1;

  //- check if specified config id exists
  //- search for config_id in m_aiConfig => set m_currentConfigPos
  for (size_t idx = 0; idx < this->m_aiConfiguration.acqDefinitions.size(); idx++)
  {
    if (this->m_aiConfiguration.acqDefinitions[idx].configId == config_id)
    {
      this->m_currentConfigPos = idx;
    }
  }

  if (this->m_currentConfigPos == -1)
  {
    //- config not defined => fatal error
    this->m_initInError = true;
    ERROR_STREAM << "Configuration id not defined!" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Bad configuration id - check Device properties"),
                    _CPTC("AIManager::initDaq"));
  }

  try
  {
    //- delete previous Acquisition Mode reference if any
    if (this->m_currentAcquisition)
    {
      delete this->m_currentAcquisition;
      this->m_currentAcquisition = NULL;
    }

    //- get trigger mode from acquisition configuration
    E_TriggerMode_t trigMode = config_parser.extractTriggerMode(
          this->m_aiConfiguration.acqDefinitions[this->m_currentConfigPos].configKeyList);

    //- instanciate new Acquisition Mode
    this->m_currentAcquisition = AcqModeFactory::instanciate (trigMode, m_hostDev);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::initDaq caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to instanciate acquisition mode (caught DevFailed)!"),
                      _CPTC("AIManager::initDaq"));
  }
  catch (...)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::initDaq caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to instanciate acquisition mode (caught [...])!"),
                    _CPTC("AIManager::initDaq"));
  }

  try
  {
    //- initialize DAQ board
    this->m_currentAcquisition->initDaqBoard(
           this->m_aiConfiguration.daqBoard.id, 
           this->m_board);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::initDaq caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to initialize DAQ board (caught DevFailed)!"),
                      _CPTC("AIManager::initDaq"));
  }
  catch (...)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::initDaq caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to initialize DAQ board (caught [...])!"),
                    _CPTC("AIManager::initDaq"));
  }

  //- initialize the acquisition parameters with initial configuration
  this->m_currentAcqParam.configId = 
    this->m_aiConfiguration.acqDefinitions[this->m_currentConfigPos].configId;

  this->m_currentAcqParam.configName = config_parser.extractConfigName(
    this->m_aiConfiguration.acqDefinitions[this->m_currentConfigPos].configKeyList);

  this->m_currentAcqParam.frequency = config_parser.extractSamplingInfo(
    this->m_aiConfiguration.acqDefinitions[this->m_currentConfigPos].configKeyList).rate;

  //- some parameters have default initial values (not set by configuration)
  this->m_currentAcqParam.integrationTime = kINITIAL_VALUE_INTEGRATION_TIME;
  this->m_currentAcqParam.nexusFileGeneration = kINITIAL_VALUE_NX_GENERATION;
  this->m_currentAcqParam.nexusTargetPath = kINITIAL_VALUE_NX_PATH;

  this->m_currentAcqParam.samplesNumber = (yat::uint32)
    (this->m_currentAcqParam.integrationTime * this->m_currentAcqParam.frequency * kMILLISECONDS_TO_SECONDS);

  // Extraction of the nexus file generation for each configuration.
  m_nexus_file_generation_by_config.clear();
  try
  {
    for(int i=0 ; i<m_aiConfiguration.acqDefinitions.size() ; i++)
    {
      const RawAcquitisionConfig& raw_config = m_aiConfiguration.acqDefinitions[i];
      bool nexus_file_generation_for_config = config_parser.extractNexusFileGeneration(raw_config.configKeyList);
      m_nexus_file_generation_by_config.push_back(nexus_file_generation_for_config);
    }
    if (m_nexus_file_generation_managed_by_config)
    {
      this->m_currentAcqParam.nexusFileGeneration = m_nexus_file_generation_by_config[this->m_currentConfigPos];
    }
  }
  catch (Tango::DevFailed & df)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::initDaq caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to extract the nexus file generation (caught DevFailed)!"),
                      _CPTC("AIManager::initDaq"));
  }

  try
  {
    //- configure acquisition
    this->m_currentAcquisition->configureAcquisition(
                                   this->m_aiConfiguration.acqDefinitions[this->m_currentConfigPos], 
                                   this->m_aiConfiguration.acqBufferConfig, 
                                   this->m_currentAcqParam);
  
    //- set Nexus spool (for flyscan needs)
    this->m_currentAcquisition->setNexusSpool(this->m_aiConfiguration.flyscanSpool);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::initDaq caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to configure acquisition (caught DevFailed)!"),
                      _CPTC("AIManager::initDaq"));
  }
  catch (...)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::initDaq caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to configure acquisition (caught [...])!"),
                    _CPTC("AIManager::initDaq"));
  }

  // Enable periodic msg in autotuning mode, to update sampling rate with retrig number
  // changed by AcqMode_XXX write callback
  if (this->m_currentAcquisition->getAutoTuning())
  {
    this->set_periodic_msg_period(kDEFAULT_PERIODIC_MSG_PERIOD); //ms
    this->enable_periodic_msg (true);
  }

  this->m_initInProgress = false;
}

// ======================================================================
// AIManager::updateDaq
// ======================================================================
void AIManager::updateDaq (yat::uint16 config_id, size_t sync_tmo_msecs)
  throw (Tango::DevFailed)
{
  // Disable periodic messages
  this->enable_periodic_msg (false);

  // first clean Device interface before letting the task do the rest
  // of the update (must clean the Device interface while the function owns
  // the "tango monitor" lock)
  this->m_currentAcquisition->cleanInterface();

  bool have_to_wait = sync_tmo_msecs ? true : false;
  
  yat::Message * msg = yat::Message::allocate(kUPDATE_CONFIG_MSG, DEFAULT_MSG_PRIORITY, have_to_wait);
  
  msg->attach_data(config_id);
  
  if (have_to_wait)
    this->wait_msg_handled(msg, sync_tmo_msecs);
  else
    this->post(msg);  
}

// ======================================================================
// AIManager::updateDaq_i
// ======================================================================
void AIManager::updateDaq_i(yat::uint16 config_id)
  throw (Tango::DevFailed)
{
  INFO_STREAM << "AIManager::updateDaq_i - initialize config: " << config_id << std::endl;
  this->m_initInError = false;

  // local configuration parser
  ConfigurationParser config_parser(m_hostDev);

  try
  {
    //- delete current DAQ board if any
    if (this->m_board)
    {
      delete this->m_board;
      this->m_board = NULL;
    }

    //- instanciate new DAQ board
    this->m_board = BoardTypeFactory::instanciate (this->m_aiConfiguration.daqBoard.type);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::updateDaq_i caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to instanciate DAQ board (caught DevFailed)!"),
                      _CPTC("AIManager::updateDaq_i"));
  }
  catch (...)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::updateDaq_i caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to instanciate DAQ board (caught [...])!"),
                    _CPTC("AIManager::updateDaq_i"));
  }

  if (!this->m_board)
  {
    this->m_initInError = true;
    ERROR_STREAM << "DAQ board not properly initialized!" << std::endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                    _CPTC("Failed to initialize DAQ board"),
                    _CPTC("AIManager::updateDaq_i"));
  }
  
  //- reset local members
  this->m_currentConfigPos = -1;

  //- check if specified config id exists
  //- search for config_id in m_aiConfig => set m_currentConfigPos
  for (size_t idx = 0; idx < this->m_aiConfiguration.acqDefinitions.size(); idx++)
  {
    if (this->m_aiConfiguration.acqDefinitions[idx].configId == config_id)
    {
      this->m_currentConfigPos = idx;
    }
  }

  if (this->m_currentConfigPos == -1)
  {
    //- config not defined => fatal error
    this->m_initInError = true;
    ERROR_STREAM << "Configuration id not defined!" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Bad configuration id - check Device properties"),
                    _CPTC("AIManager::updateDaq_i"));
  }

  try
  {
    //- delete previous Acquisition Mode reference if any
    if (this->m_currentAcquisition)
    {
      delete this->m_currentAcquisition;
      this->m_currentAcquisition = NULL;
    }

    //- get trigger mode from acquisition configuration
    E_TriggerMode_t trigMode = config_parser.extractTriggerMode(
          this->m_aiConfiguration.acqDefinitions[this->m_currentConfigPos].configKeyList);

    //- instanciate new Acquisition Mode
    this->m_currentAcquisition = AcqModeFactory::instanciate (trigMode, m_hostDev);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::updateDaq_i caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to instanciate acquisition mode (caught DevFailed)!"),
                      _CPTC("AIManager::updateDaq_i"));
  }
  catch (...)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::updateDaq_i caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to instanciate acquisition mode (caught [...])!"),
                    _CPTC("AIManager::updateDaq_i"));
  }

  try
  {
    //- initialize DAQ board
    this->m_currentAcquisition->initDaqBoard(
           this->m_aiConfiguration.daqBoard.id, 
           this->m_board);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::updateDaq_i caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to initialize DAQ board (caught DevFailed)!"),
                      _CPTC("AIManager::updateDaq_i"));
  }
  catch (...)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::updateDaq_i caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to initialize DAQ board (caught [...])!"),
                    _CPTC("AIManager::updateDaq_i"));
  }

  //- initialize the acquisition parameters with initial configuration
  this->m_currentAcqParam.configId = 
    this->m_aiConfiguration.acqDefinitions[this->m_currentConfigPos].configId;

  this->m_currentAcqParam.configName = config_parser.extractConfigName(
    this->m_aiConfiguration.acqDefinitions[this->m_currentConfigPos].configKeyList);
  if (m_nexus_file_generation_managed_by_config)
  {
    // If the nexus file generation is managed by configuration, the status is retrieved
    this->m_currentAcqParam.nexusFileGeneration = m_nexus_file_generation_by_config[this->m_currentConfigPos];
  }

  try
  {
    //- configure acquisition
    this->m_currentAcquisition->configureAcquisition(
                                   this->m_aiConfiguration.acqDefinitions[this->m_currentConfigPos], 
                                   this->m_aiConfiguration.acqBufferConfig, 
                                   this->m_currentAcqParam);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::updateDaq_i caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to configure acquisition (caught DevFailed)!"),
                      _CPTC("AIManager::updateDaq_i"));
  }
  catch (...)
  {
    this->m_initInError = true;
    ERROR_STREAM << "AIManager::updateDaq_i caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to configure acquisition (caught [...])!"),
                    _CPTC("AIManager::updateDaq_i"));
  }

  // reapply current frequency
  this->m_currentAcquisition->setSamplingRate(this->m_currentAcqParam.frequency);

  // Enable periodic msg in autotuning mode, to update sampling rate with retrig number
  // changed by AcqMode_XXX write callback
  if (this->m_currentAcquisition->getAutoTuning())
  {
    this->enable_periodic_msg (true);
  }

  this->m_configChangeInProgress = false;
}

// ======================================================================
// AIManager::start
// ======================================================================
void AIManager::start()
  throw (Tango::DevFailed)
{
  INFO_STREAM << "AIManager::start - start acquisition..." << std::endl;

  CHECK_ACQ_REF;

  // In autotuning mode, re-evaluate sampling frequency (in case not done before by periodic_job)
  if (this->m_currentAcquisition->getAutoTuning())
  {
    this->updateAutoSamplingRate();
  }

  // check driver memory before starting acquisition
  try
  {
    //- get computed retrig counter & check driver memory
    yat::uint32 cnt = this->m_currentAcquisition->getReTrigCnt();
    this->checkDriverMemory(cnt);
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AIManager::start caught DevFailed while checking memory: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Memory check failed (caught DevFailed)!"),
                      _CPTC("AIManager::start"));
  }
  catch (...)
  {
    ERROR_STREAM << "AIManager::start caught [...] while checking memory" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Memory check failed (caught [...])!"),
                    _CPTC("AIManager::start"));
  }

  try
  {
    this->checkSamplesNumber();
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AIManager::start caught DevFailed while checking samples number: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Samples number check failed (caught DevFailed)!"),
                      _CPTC("AIManager::start"));
  }
  catch (...)
  {
    ERROR_STREAM << "AIManager::start caught [...] while checking samples number" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Samples number check failed (caught [...])!"),
                    _CPTC("AIManager::start"));
  }

  try
  {
    this->m_currentAcquisition->startAcquisition();
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AIManager::start caught DevFailed while starting acquisition: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to start acquisition (caught DevFailed)!"),
                      _CPTC("AIManager::start"));
  }
  catch (...)
  {
    ERROR_STREAM << "AIManager::start caught [...] while starting acquisition" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to start acquisition (caught [...])!"),
                    _CPTC("AIManager::start"));
  }
}

// ======================================================================
// AIManager::stop
// ======================================================================
void AIManager::stop()
  throw (Tango::DevFailed)
{
  INFO_STREAM << "AIManager::stop - stop acquisition..." << std::endl;

  CHECK_ACQ_REF;

  try
  {
    this->m_currentAcquisition->stopAcquisition();
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AIManager::stop caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to stop acquisition (caught DevFailed)!"),
                      _CPTC("AIManager::stop"));
  }
  catch (...)
  {
    ERROR_STREAM << "AIManager::stop caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to stop acquisition (caught [...])!"),
                    _CPTC("AIManager::stop"));
  }
}

// ======================================================================
// AIManager::abort
// ======================================================================
void AIManager::abort()
  throw (Tango::DevFailed)
{
  INFO_STREAM << "AIManager::abort - abort acquisition..." << std::endl;

  CHECK_ACQ_REF;

  try
  {
    this->m_currentAcquisition->abortAcquisition();
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AIManager::abort caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to abort acquisition (caught DevFailed)!"),
                      _CPTC("AIManager::abort"));
  }
  catch (...)
  {
    ERROR_STREAM << "AIManager::abort caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to abort acquisition (caught [...])!"),
                    _CPTC("AIManager::abort"));
  }
}

// ======================================================================
// AIManager::getDriverMemorySize
// ======================================================================
yat::uint32 AIManager::getDriverMemorySize()
{
  //- check from driver that memory can be allocated (in KB)
  U32 driverMemSizeMax;
  D2K_AI_InitialMemoryAllocated (this->m_aiConfiguration.daqBoard.id, &driverMemSizeMax);
  INFO_STREAM << "AIManager::getDriverMemorySize -> configured driver memory size value = " << driverMemSizeMax << " KBytes." << std::endl;

  driverMemSizeMax = this->m_aiConfiguration.acqBufferConfig.maxMemorySize;
  INFO_STREAM << "AIManager::getDriverMemorySize -> Using driver memory size property value = " << driverMemSizeMax << " KBytes." << std::endl;

  //- convert driver memory size in bytes
  driverMemSizeMax *= kKILOBYTES_TO_BYTES;
  INFO_STREAM << "AIManager::getDriverMemorySize -> max driver memory size = " << driverMemSizeMax << " Bytes." << std::endl;

  return ((yat::uint32)driverMemSizeMax);
}

// ======================================================================
// AIManager::checkDriverMemory
// ======================================================================
void AIManager::checkDriverMemory(yat::uint32 retrig_cnt)
  throw (Tango::DevFailed)
{
  //- compute number of trigs
  yat::uint32 num_of_trig;
  if (retrig_cnt == kINFINITE_ACQ)
  {
    num_of_trig = 1;
  }
  else
  {
    num_of_trig = retrig_cnt;
  }

  //- get number of active channels from current Acquisition Mode
  size_t num_of_active_channels = this->m_currentAcquisition->getActiveChanNb();

  //- compute buffer size
  size_t total_size = (size_t)
    (this->m_currentAcqParam.frequency * this->m_currentAcqParam.integrationTime * kMILLISECONDS_TO_SECONDS *
     num_of_active_channels * num_of_trig * sizeof(yat_int16_t));
  INFO_STREAM << "AIManager::checkDriverMemory -> actual config size = " << total_size << " Bytes." << std::endl;

  //- check buffer size doesnt exceed driver max memory
  if (total_size > this->m_drvMem)
  {
    FATAL_STREAM << "AIManager::checkDriverMemory -> Max driver memory = " << m_drvMem << " bytes, " << std::endl;
    FATAL_STREAM << "\t this config requires = " << total_size << " bytes." << std::endl;
    
    double maxFreq = this->m_drvMem / (this->m_currentAcqParam.integrationTime * kMILLISECONDS_TO_SECONDS *
     num_of_active_channels * num_of_trig * sizeof(yat_int16_t));
    
    FATAL_STREAM << "Max frequency for this config is: " << maxFreq << " Hz." << std::endl;
    THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                    _CPTC("The new buffer depth is above the maximum. Check FATAL log!"),
                    _CPTC("AIManager::checkDriverMemory"));
  }

  if (total_size <= 0)
  {
    FATAL_STREAM << "Buffer depth value cannot be equal or less than 0." << std::endl;
    THROW_DEVFAILED(_CPTC("BAD_PARAMETER"),
                    _CPTC("Buffer depth value cannot be equal or less than 0."),
                    _CPTC("AIManager::checkDriverMemory"));
  }
}

// ======================================================================
// AIManager::computeMaxFreq
// ======================================================================
double AIManager::computeMaxFreq(yat::uint32 retrig_cnt)
{
  // get max possible frequency from board
  double maxBoardFreq = this->m_board->getMaxFrequency();

  //- compute number of trigs
  yat::uint32 num_of_trig;
  if (retrig_cnt == kINFINITE_ACQ)
  {
    num_of_trig = 1;
  }
  else
  {
    num_of_trig = retrig_cnt;
  }

  //- get number of active channels from current Acquisition Mode
  size_t num_of_active_channels = this->m_currentAcquisition->getActiveChanNb();

  //- compute max frequency
  double maxFreq = this->m_drvMem / (this->m_currentAcqParam.integrationTime * kMILLISECONDS_TO_SECONDS *
     num_of_active_channels * num_of_trig * sizeof(yat_int16_t));
  if (maxFreq > maxBoardFreq)
  {
    maxFreq = maxBoardFreq;
  }

  return maxFreq;
}

// ======================================================================
// AIManager::checkSamplesNumber
// ======================================================================
void AIManager::checkSamplesNumber()
  throw (Tango::DevFailed)
{
  //- check if samples number is not null
  if ( this->m_currentAcqParam.samplesNumber == 0 )
  {
    FATAL_STREAM << "Samples number cannot be null." << std::endl;
    THROW_DEVFAILED(_CPTC("BAD_PARAMETER"),
                    _CPTC("samples number value cannot be null."),
                    _CPTC("AIManager::checkSamplesNumber"));
  }
}

// ======================================================================
// AIManager::setSamplingRate
// ======================================================================
void AIManager::setSamplingRate(double rate)
  throw (Tango::DevFailed)
{
SYSTEMTIME start, write, temp1, temp2, temp3;
GetSystemTime(&start); 
DEBUG_STREAM << "AIManager::setSamplingRate() entering... "<< endl;

  yat::AutoMutex<> guard(this->m_freqLock);
  
  //- save current frequency value
  double currentVal = this->m_currentAcqParam.frequency;
  this->m_currentAcqParam.frequency = rate;

  CHECK_ACQ_REF;

  //- stop acquisition if already started
  bool restart_acquisition = false;
  if (this->m_currentAcquisition->getAcqState() == Tango::RUNNING)
  {
    DEBUG_STREAM << "AIManager::setSamplingRate: stop acquisition before changing frequency... "<< endl;
    try
    {
      this->m_currentAcquisition->stopAcquisition();
      restart_acquisition = true;
	  
      // sleep a while for history finalization
      yat::Thread::sleep(500);
    }
    catch (Tango::DevFailed & df)
    {
      this->m_currentAcqParam.frequency = currentVal;
      ERROR_STREAM << "AIManager::setSamplingRate caught DevFailed: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("COMMAND_FAILED"),
                        _CPTC("Failed to stop acquisition (caught DevFailed)!"),
                        _CPTC("AIManager::setSamplingRate"));
    }
    catch (...)
    {
      this->m_currentAcqParam.frequency = currentVal;
      ERROR_STREAM << "AIManager::setSamplingRate caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Failed to stop acquisition (caught [...])!"),
                      _CPTC("AIManager::setSamplingRate"));
    }	
  }

GetSystemTime(&temp1);
DEBUG_STREAM << "INSTRU - setSamplingRate - stopping current acquisition (if needed) took " << 1e3 * (temp1.wSecond - start.wSecond) + (temp1.wMilliseconds - start.wMilliseconds) << " ms." << std::endl; 

  //- set new sampling rate
  try
  {
    this->m_currentAcquisition->setSamplingRate(rate);
    
    //- set new samples number value
    this->m_currentAcqParam.samplesNumber = m_currentAcquisition->getBufferDepth();
  }
  catch (Tango::DevFailed & df)
  {
    this->m_currentAcqParam.frequency = currentVal;
    ERROR_STREAM << "AIManager::setSamplingRate caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to set new sampling rate (caught DevFailed)!"),
                      _CPTC("AIManager::setSamplingRate"));
  }
  catch (...)
  {
    this->m_currentAcqParam.frequency = currentVal;
    ERROR_STREAM << "AIManager::setSamplingRate caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to set new sampling rate (caught [...])!"),
                    _CPTC("AIManager::setSamplingRate"));
  }


GetSystemTime(&temp2);
DEBUG_STREAM << "INSTRU - setSamplingRate - set new sampling rate took " << 1e3 * (temp2.wSecond - temp1.wSecond) + (temp2.wMilliseconds - temp1.wMilliseconds) << " ms." << std::endl; 
  
  //- restart acquisition if previously stopped
  if (restart_acquisition)
  {
    try
    {
      this->checkSamplesNumber();
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << "AIManager::setSamplingRate caught DevFailed while checking samples number: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("COMMAND_FAILED"),
                        _CPTC("Samples number check failed (caught DevFailed)!"),
                        _CPTC("AIManager::setSamplingRate"));
    }
    catch (...)
    {
      ERROR_STREAM << "AIManager::setSamplingRate caught [...] while checking samples number" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Samples number check failed (caught [...])!"),
                      _CPTC("AIManager::setSamplingRate"));
    }

    try
    {
      // wait for nexus finalization before restarting acquisition
      if (this->m_currentAcqParam.nexusFileGeneration)
      {
        while(1)
        {
          Tango::DevState nx_state = this->m_currentAcquisition->getNexusState();
          if (nx_state != Tango::RUNNING)
          {
            break;
          }
          yat::Thread::sleep(50);
        }
      }

      this->m_currentAcquisition->startAcquisition();
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << "AIManager::setSamplingRate caught DevFailed: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("COMMAND_FAILED"),
                        _CPTC("Failed to start acquisition (caught DevFailed)!"),
                        _CPTC("AIManager::setSamplingRate"));
    }
    catch (...)
    {
      ERROR_STREAM << "AIManager::setSamplingRate caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Failed to start acquisition (caught [...])!"),
                      _CPTC("AIManager::setSamplingRate"));
    }
  }

GetSystemTime(&temp3);
DEBUG_STREAM << "INSTRU - setSamplingRate - restarting acquisition (if needed) took " << 1e3 * (temp3.wSecond - temp2.wSecond) + (temp3.wMilliseconds - temp2.wMilliseconds) << " ms." << std::endl; 
  
GetSystemTime(&write);
DEBUG_STREAM << "INSTRU - setSamplingRate() function took " << 1e3 * (write.wSecond - start.wSecond) + (write.wMilliseconds - start.wMilliseconds) << " ms." << std::endl; 
}

// ======================================================================
// AIManager::setIntegrationTime
// ======================================================================
void AIManager::setIntegrationTime(double it)
  throw (Tango::DevFailed)
{
  //- save current integration time value
  double currentVal = this->m_currentAcqParam.integrationTime;
  this->m_currentAcqParam.integrationTime = it;

  CHECK_ACQ_REF;

  //- stop acquisition if already started
  bool restart_acquisition = false;
  if (this->m_currentAcquisition->getAcqState() == Tango::RUNNING)
  {
    try
    {
      this->m_currentAcquisition->stopAcquisition();
      restart_acquisition = true;

      // sleep a while for history finalization
      yat::Thread::sleep(500);
    }
    catch (Tango::DevFailed & df)
    {
      this->m_currentAcqParam.integrationTime = currentVal;
      ERROR_STREAM << "AIManager::setIntegrationTime caught DevFailed: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("COMMAND_FAILED"),
                        _CPTC("Failed to stop acquisition (caught DevFailed)!"),
                        _CPTC("AIManager::setIntegrationTime"));
    }
    catch (...)
    {
      this->m_currentAcqParam.integrationTime = currentVal;
      ERROR_STREAM << "AIManager::setIntegrationTime caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Failed to stop acquisition (caught [...])!"),
                      _CPTC("AIManager::setIntegrationTime"));
    }
  }

  //- set new integration time
  try
  {
    this->m_currentAcquisition->setIntegrationTime(it);

    //- set new samples number value
    this->m_currentAcqParam.samplesNumber = m_currentAcquisition->getBufferDepth();
  }
  catch (Tango::DevFailed & df)
  {
    this->m_currentAcqParam.integrationTime = currentVal;
    ERROR_STREAM << "AIManager::setIntegrationTime caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to set new integration time (caught DevFailed)!"),
                      _CPTC("AIManager::setIntegrationTime"));
  }
  catch (...)
  {
    this->m_currentAcqParam.integrationTime = currentVal;
    ERROR_STREAM << "AIManager::setIntegrationTime caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to set new integration time (caught [...])!"),
                    _CPTC("AIManager::setIntegrationTime"));
  }

  //- if auto tuning enabled, set max possible frequency value
  if (this->m_currentAcquisition->getAutoTuning())
  {
    //- get computed retrig counter
    yat::uint32 cnt = this->m_currentAcquisition->getReTrigCnt();
    double maxFreq = this->computeMaxFreq(cnt);
    this->setSamplingRate(maxFreq);
  }

  //- restart acquisition if previously stopped
  if (restart_acquisition)
  {
    try
    {
      this->checkSamplesNumber();
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << "AIManager::setIntegrationTime caught DevFailed while checking samples number: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("COMMAND_FAILED"),
                        _CPTC("Samples number check failed (caught DevFailed)!"),
                        _CPTC("AIManager::setIntegrationTime"));
    }
    catch (...)
    {
      ERROR_STREAM << "AIManager::setIntegrationTime caught [...] while checking samples number" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Samples number check failed (caught [...])!"),
                      _CPTC("AIManager::setIntegrationTime"));
    }

    try
    {
      // wait for nexus finalization before restarting acquisition
      if (this->m_currentAcqParam.nexusFileGeneration)
      {
        while(1)
        {
          Tango::DevState nx_state = this->m_currentAcquisition->getNexusState();
          if (nx_state != Tango::RUNNING)
          {
            break;
          }
          yat::Thread::sleep(50);
        }
      }

      this->m_currentAcquisition->startAcquisition();
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << "AIManager::setIntegrationTime caught DevFailed: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("COMMAND_FAILED"),
                        _CPTC("Failed to start acquisition (caught DevFailed)!"),
                        _CPTC("AIManager::setIntegrationTime"));
    }
    catch (...)
    {
      ERROR_STREAM << "AIManager::setIntegrationTime caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Failed to start acquisition (caught [...])!"),
                      _CPTC("AIManager::setIntegrationTime"));
    }
  }
}


// ======================================================================
// AIManager::setNexusFilePath
// ======================================================================
void AIManager::setNexusFilePath(std::string path)
  throw (Tango::DevFailed)
{
  CHECK_ACQ_REF;

  if (!path.empty())
  {
    this->m_currentAcquisition->setNexusFilePath(path);
    this->m_currentAcqParam.nexusTargetPath = path;
  }
  else
  {
    ERROR_STREAM << "Bad path name: " << path << std::endl;
    THROW_DEVFAILED(_CPTC("COMMAND_FAILED"),
                    _CPTC("Failed to set new Nexus dile path!"),
                    _CPTC("AIManager::setNexusFilePath"));
  }
}

// ======================================================================
// AIManager::setNexusFileGeneration
// ======================================================================
void AIManager::setNexusFileGeneration(bool enable)
  throw (Tango::DevFailed)
{
  CHECK_ACQ_REF;

  this->m_currentAcquisition->setNexusFileGeneration(enable) ;
  this->m_currentAcqParam.nexusFileGeneration = enable;
  if (m_nexus_file_generation_managed_by_config)
  {
    m_nexus_file_generation_by_config[this->m_currentConfigPos] = enable;
  }
}

// ======================================================================
// AIManager::setNexusFileGenerationManagedByConfig
// ======================================================================
void AIManager::setNexusFileGenerationManagedByConfig (bool enable)
{
  m_nexus_file_generation_managed_by_config = enable;
}

// ======================================================================
// AIManager::setNexusNbPerFile
// ======================================================================
void AIManager::setNexusNbPerFile(yat::uint16 nb)
  throw (Tango::DevFailed)
{
  CHECK_ACQ_REF;

  this->m_currentAcquisition->setNexusNbPerFile(nb) ;
  this->m_currentAcqParam.nexusNbPerFile = nb;
}

// ======================================================================
// AIManager::getAcqStateAndStatus
// ======================================================================
Tango::DevState AIManager::getAcqStateAndStatus(std::string & acq_status)
  throw (Tango::DevFailed)
{
  Tango::DevState mng_state;
  acq_status = "";

  //- check if init failed
  if (this->m_initInError)
  {
     acq_status = "Initialization failed. See logs for details.";
     return Tango::FAULT;
  }

  //- check if init in progress
  if (this->m_initInProgress)
  {
    acq_status = "Initialization in progress...";
    return Tango::INIT;
  }

  //- check if config change in progress
  if (this->m_configChangeInProgress)
  {
    acq_status = "Configuration change in progress...";
    return Tango::MOVING;
  }

  //- if manager error, set state to ALARM 
  std::string acq_error = "";
  if (this->m_currentAcquisition->getAcqError(acq_error))
  {
    mng_state = Tango::ALARM;
    acq_status += acq_error;
  }
  else
  {
    //- get DAQ board state
    mng_state = this->m_currentAcquisition->getAcqState();
    switch (mng_state)
    {
      case Tango::STANDBY:
        acq_status += "The acquisition is stopped.";
        break;
      case Tango::RUNNING:
        acq_status += "The acquisition is running.";
        break;
      case Tango::FAULT:
        acq_status += "The acquisition is in FAULT state.";
        break;
      case Tango::UNKNOWN:
      default:
        acq_status += "The acquisition is in an unknown state.";
        break;
    }
  }

  //- Get Nexus storage state: 
  //   if acquisition stopped and Nexus still running, change manager state
  //   if Nexus in fault, stop acquisition
  try
  {
    Tango::DevState nx_state = this->m_currentAcquisition->getNexusState();

    if (nx_state == Tango::FAULT) 
    {
      // error
      acq_status += "\nNexus file generation in error!";
      mng_state = Tango::FAULT;
      
      // stop acquisition
      yat::Message * msg = yat::Message::allocate(kABORT_ACQ_NX_MSG, DEFAULT_MSG_PRIORITY, false);
      this->post(msg);  
    }
    else
    {
      if (mng_state == Tango::STANDBY)
      {
        if (nx_state == Tango::RUNNING)
        {
          mng_state = Tango::RUNNING;
          acq_status += "\nNexus file generation is running.";
        }
        else
        {
          acq_status += "\nNexus file generation is stopped.";
        }
      }
    }
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AIManager::getAcqState caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to get Nexus state (caught DevFailed)!"),
                      _CPTC("AIManager::getAcqState"));
  }

  //- if OverrunStrategy = ABORT & overruns => set state to ALARM
  if (this->m_currentConfigPos != -1)
  {
    // get overrun strategy from current acquisition mode config
    E_OverrunStrategy_t overrun_strategy = this->m_currentAcquisition->getOvrStrategy();
    if ((overrun_strategy == OVRN_ABORT) &&
        (this->m_currentAcquisition->getOverrunCounter()))
    {
      mng_state = Tango::ALARM;
      acq_status += "WARN : Acquisition stopped on OVERRUN.";
    }
  }

  return mng_state;
}

// ======================================================================
// AIManager::getNexusDataStreams
// ======================================================================
std::vector<std::string> AIManager::getNexusDataStreams () const
{
  std::vector<std::string> nxDataStreams;
  
  // if no nexus recording, return empty list
  if (this->m_currentAcqParam.nexusFileGeneration)
  {
    // get nexus data item list from manager
    std::vector<std::string> itemList = this->m_currentAcquisition->getNxDataItems();
    if ( ! itemList.size() )
      return nxDataStreams;

    // data stream = <nexus basename>:<coma separated nexus data items>@<flyscan spool>
    yat::OSStream oss; 
    oss << this->m_currentAcquisition->getNxFileBaseName()
        << ":"; 

    for (size_t idx = 0; idx < itemList.size(); idx++)
    {
      if ( idx )
        oss << ",";
      oss << itemList.at(idx);
    }

    oss << "@"
        << this->m_aiConfiguration.flyscanSpool;

    nxDataStreams.push_back(oss.str());
  }

  return nxDataStreams;
}

// ======================================================================
// AIManager::updateAutoSamplingRate
// ======================================================================
void AIManager::updateAutoSamplingRate()
{
  if (m_currentAcquisition)
  {
    //- get computed retrig counter
    yat::uint32 cnt = this->m_currentAcquisition->getReTrigCnt();

    if (cnt != m_prevRetrigCnt)
    {
      DEBUG_STREAM << "AIManager::updateAutoSamplingRate(): re-evaluating sampling rate with retrig nb = " << cnt << std::endl;
      m_prevRetrigCnt = cnt;

      double maxFreq = this->computeMaxFreq(cnt);
      this->setSamplingRate(maxFreq);
    }
  }
}

// ======================================================================
// AIManager::changeNxDataset
// ======================================================================
void AIManager::changeNxDataset(yat::uint16 chan_nb, std::string dataset)
  throw (Tango::DevFailed)
{
  CHECK_ACQ_REF;

  this->m_currentAcquisition->updateNxDataset(chan_nb, dataset);
}

// ======================================================================
// AIManager::getChannelInfo
// ======================================================================
std::map<yat::uint16, std::string> AIManager::getChannelInfo()
{
  CHECK_ACQ_REF;

  std::map<yat::uint16, std::string> infos;

  infos = this->m_currentAcquisition->getChannelNxInfo();

  return infos;
}

} // namespace aicontroller


