//=============================================================================
// ADMAI2204_BoardType.cpp
//=============================================================================
// abstraction.......ADLINK MAI 2204 Board type
// class.............ADMAI2204_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ADMAI2204_BoardType.h"

namespace aicontroller
{

// ======================================================================
// ADMAI2204_BoardType::ADMAI2204_BoardType
// ======================================================================
ADMAI2204_BoardType::ADMAI2204_BoardType ()
 : BoardType()
{
  // set board characteristics
  m_isMultiplexed = true;

  //- TODO : default values (waiting for ASL modifications)
  /*
  m_maxSamplingRate = AD2204_MAX_SRATE;
  m_minSamplingRate = AD2204_MIN_SRATE;*/
  m_maxSamplingRate = AD2205_MAX_SRATE;
  m_minSamplingRate = AD2205_MIN_SRATE;

  m_maxChannelsNb  = AD2204_NUM_CHANNELS;
}

// ======================================================================
// ADMAI2204_BoardType::~ADMAI2204_BoardType
// ======================================================================
ADMAI2204_BoardType::~ADMAI2204_BoardType ()
{
}


} // namespace aicontroller


