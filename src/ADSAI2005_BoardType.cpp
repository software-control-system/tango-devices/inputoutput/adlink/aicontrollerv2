//=============================================================================
// ADSAI2005_BoardType.cpp
//=============================================================================
// abstraction.......ADLINK SAI 2005 Board type
// class.............ADSAI2005_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ADSAI2005_BoardType.h"

namespace aicontroller
{

// ======================================================================
// ADSAI2005_BoardType::ADSAI2005_BoardType
// ======================================================================
ADSAI2005_BoardType::ADSAI2005_BoardType ()
 : BoardType()
{
  // set board characteristics
  m_isMultiplexed = false;
  m_maxSamplingRate = AD2005_MAX_SRATE;
  m_minSamplingRate = AD2005_MIN_SRATE;
  m_maxChannelsNb  = AD2005_NUM_CHANNELS;
}

// ======================================================================
// ADSAI2005_BoardType::~ADSAI2005_BoardType
// ======================================================================
ADSAI2005_BoardType::~ADSAI2005_BoardType ()
{
}


} // namespace aicontroller


