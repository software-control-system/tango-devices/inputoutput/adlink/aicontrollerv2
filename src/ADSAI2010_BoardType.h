//=============================================================================
// ADSAI2010_BoardType.h
//=============================================================================
// abstraction.......ADLINK SAI 2010 Board type
// class.............ADSAI2010_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _ADSAI2010_BOARD_H_
#define _ADSAI2010_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BoardType.h"

namespace aicontroller
{


// ============================================================================
// class: ADSAI2010_BoardType
// ============================================================================
class ADSAI2010_BoardType : public BoardType
{
public:
  //- constructor
  ADSAI2010_BoardType ();

  //- destructor
  virtual ~ADSAI2010_BoardType ();
  
  //- board model name as string 
  virtual std::string board_model_name () const
  {
    return kBOARD_TYPE_ADSAI2010_STR;
  }

  //- board model as reference 
  virtual unsigned short board_model_id () const 
  {
    return adl::DAQ2010;
  }

};

} // namespace aicontroller

#endif // _ADSAI2010_BOARD_H_
