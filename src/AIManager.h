//=============================================================================
// AIManager.h
//=============================================================================
// abstraction.......Continuous acquisition manager abstraction
// class.............AIManager
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _AI_MANAGER_H_
#define _AI_MANAGER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/ExceptionHelper.h>
#include <yat4tango/LogHelper.h>
#include "AcqModeFactory.h"

#include <yat4tango/DeviceTask.h>

namespace aicontroller
{

//-----------------------------------------------------------------------------
//- DEFAULT INITIAL VALUES CONSTs
//-
const double kINITIAL_VALUE_INTEGRATION_TIME    = 100.0; // in ms
const std::string kINITIAL_VALUE_NX_PATH        = ".//";
const bool kINITIAL_VALUE_NX_GENERATION         = false;

//-----------------------------------------------------------------------------

// ============================================================================
// class: AIManager
// ============================================================================
class AIManager : public yat4tango::DeviceTask
{
public:

  //- create a dedicated type for SAI configuration
  //-------------------------------------------------------
  typedef struct Config
  {
    //- members --------------------
    Tango::DeviceImpl * host_device;
    AIConfiguration ai_config;

    //- ctor -----------------------
    Config ();
    //- copy ctor ------------------
    Config (const Config& src);
    //- operator= ------------------
    const Config & operator= (const Config& src);
    //- dump -----------------------
    void dump ();
  } Config;

  //- constructor
  AIManager(const AIManager::Config& cfg);

  //- destructor
  virtual ~AIManager();
  
  //- task quit (call exit() function)
  void quitTask()
    throw (Tango::DevFailed);

  //- Initializes Acquisition Board
  void initDaq(yat::uint16 config_id)
    throw (Tango::DevFailed);

  //- Re-nitializes acquisition board consecutivelly to a configuration change
  //- <sync_tmo_msecs == 0> means asynchronous exec - otherwise wait for the 
  //- cmd to be done by ADLINK driver with a timeout of <syn_tmo_msecs>
  void updateDaq (yat::uint16 config_id, size_t sync_tmo_msecs = 0)
    throw (Tango::DevFailed);

  //- Starts acquisition
  void start()
    throw (Tango::DevFailed);

  //- Stops acquisition
  void stop()
    throw (Tango::DevFailed);

  //- Aborts acquisition
  void abort()
    throw (Tango::DevFailed);

  //- Sets new current sampling rate (in Hz)
  void setSamplingRate (double rate)
    throw (Tango::DevFailed);

  //- Sets new integration time (in ms)
  void setIntegrationTime (double it)
    throw (Tango::DevFailed);

  //- Sets new nexus file path
  void setNexusFilePath (std::string path)
    throw (Tango::DevFailed);

  //- Sets new nexus file generation flag
  void setNexusFileGeneration (bool enable)
    throw (Tango::DevFailed);

  //- Sets new nexus file generation managed by configuration flag
  void setNexusFileGenerationManagedByConfig (bool enable);

  //- Sets new number of acquisition per nexus file
  void setNexusNbPerFile (yat::uint16 nb)
    throw (Tango::DevFailed);

  //- Gets nexus data stream list
  std::vector<std::string> getNexusDataStreams () const;

  //- Sets nexus dataset with format: dataset type@dataset name 
  //- (dataset type is optional, default = specified dataset name replaces
  //- channel label for all dataset types)
  void changeNxDataset(yat::uint16 chan_nb, std::string dataset)
    throw (Tango::DevFailed);

  //- Gets current acquisition parameters
  AcquisitionParameters getParam()
  {
    return this->m_currentAcqParam;
  }

  //- Gets current acquisition state and status
  Tango::DevState getAcqStateAndStatus(std::string & acq_status)
    throw (Tango::DevFailed);

  //- Has current acquisition auto tuning on?
  bool isAutoTuningEnabled()
  {
    return (this->m_currentAcquisition->getAutoTuning());
  }

  // Get all channel dataset information as a map:<channel id, info string>
  /* NAME:{channel label}
     ENABLED:{channel enabled true / false}
     DATASET_NAME:{name of the dataset}
     DATASET_ENABLED:{true / false}
     DATASET_ATTR:{name of the 'dataset enabled' TANGO attribute}
  */
  std::map<yat::uint16, std::string> getChannelInfo();

protected:
  
  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  virtual void process_message (yat::Message& msg)
    throw (Tango::DevFailed);

  //- Re-nitializes Acquisition Board (configuration change)
  void updateDaq_i(yat::uint16 config_id)
    throw (Tango::DevFailed);

  //- initialization
  virtual void init_i ()
    throw (Tango::DevFailed);
  
  //- cleanup
  virtual void fini_i ()
    throw (Tango::DevFailed);

  //- cleanup
  virtual void periodic_job_i ()
    throw (Tango::DevFailed);


	//- Checks driver memory according to ReTrigCnt
  void checkDriverMemory(yat::uint32 retrig_cnt)
    throw (Tango::DevFailed);

	//- Checks samples number
  void checkSamplesNumber()
    throw (Tango::DevFailed);

  //- Update sampling rate with retrig number
  void updateAutoSamplingRate();

  //- Gets driver memory size in Bytes
  yat::uint32 getDriverMemorySize();

  //- Computes max frequency according to driver memory size, board type
  //- and retrig number
  double computeMaxFreq(yat::uint32 retrig_cnt);

  //- Host device (for logging)
  Tango::DeviceImpl * m_hostDev;

  //- The acquisition data structure.
  //- Contains all acquisition configurations defined in properties.
  AIConfiguration m_aiConfiguration;

  //- The current acquisition index in config list
  short m_currentConfigPos;

  //- The current acquisition reference.
  AcqMode * m_currentAcquisition;

  //- The current acquisition parameters.
  AcquisitionParameters m_currentAcqParam;

  //- The status of nexus file generation when it's managed by configuration.
  std::vector<bool> m_nexus_file_generation_by_config;

  //- Management by configuration of the nexus file generation
  bool m_nexus_file_generation_managed_by_config;

  //- The board implementation
  BoardType * m_board;

  //- Init flags
  bool m_initInProgress;
  bool m_initInError;

  //- Config change flag
  bool m_configChangeInProgress;

  //- driver memory size
  yat::uint32 m_drvMem;

  //- memorized retrig counter
  yat::uint32 m_prevRetrigCnt;
  
  //- autotuned frequency mutex protection
  yat::Mutex m_freqLock;
};

} // namespace aicontroller

#endif // _AI_MANAGER_H_
