//=============================================================================
// ConfigurationParser.h
//=============================================================================
// abstraction.......Acquisition configuration parser
// class.............ConfigurationParser
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _CONFIG_PARSER_H_
#define _CONFIG_PARSER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "TypesAndConsts.h"
#include <yat4tango/ExceptionHelper.h>
#include <yat4tango/LogHelper.h>

namespace aicontroller
{

//-----------------------------------------------------------------------------
//- Consts and key words for configuration parsing
//-----------------------------------------------------------------------------
static const std::string kKEY_VALUE_SEPARATOR ("::");
static const std::string kVALUE_SEPARATOR_CL (":");
static const std::string kVALUE_SEPARATOR_COMA (",");
static const std::string kCONFIG_NAME ("Config");

static const std::string kKEY_NAME ("NAME");

static const std::string kKEY_CHANNEL_LABEL ("CHAN_LABEL");
static const std::string kKEY_CHANNEL_NUM ("CHAN_NUMBER");
static const std::string kKEY_CHANNEL_RANGE ("CHAN_RANGE");
static const std::string kKEY_CHANNEL_GRD_REF ("CHAN_GRD_REF");
static const std::string kKEY_CHANNEL_DESC ("CHAN_DESCRIPTION");
static const std::string kKEY_CHAN_USER_DATA_GAIN ("CHAN_USER_DATA_GAIN");
static const std::string kKEY_CHAN_USER_DATA_OFFSET1 ("CHAN_USER_DATA_OFFSET1");
static const std::string kKEY_CHAN_USER_DATA_OFFSET2 ("CHAN_USER_DATA_OFFSET2");
static const std::string kKEY_CHAN_USER_DATA ("CHAN_USER_DATA");

static const std::string kKEY_CHAN_USER_DATA_ATTR ("ATTR");
static const std::string kKEY_CHAN_USER_DATA_PROXY ("PROXY");

static const std::string kKEY_CHANNEL_GRD_REF_SGL ("SINGLE");
static const std::string kKEY_CHANNEL_GRD_REF_DIF ("DIFF");

static const std::string kKEY_DATA_SCALED ("SCALED_DATA");
static const std::string kKEY_DATA_STATISTICS ("STATISTICS");
static const std::string kKEY_DATA_HISTORY ("HISTORIZED_DATA");
static const std::string kKEY_BUFFER_TIMESTAMP ("BUFFER_TIMESTAMP");
static const std::string kKEY_DATA_MASK ("DATA_MASK");

static const std::string kKEY_DATA_NX_DATA_TYPE ("NX_DATA_TYPE");
static const std::string kKEY_DATA_NX_FILE_NAME ("NX_FILE_NAME");

static const std::string kKEY_DATA_NX_DATA_TYPE_RAW ("RAW");
static const std::string kKEY_DATA_NX_DATA_TYPE_SCALED ("SCALED");
static const std::string kKEY_DATA_NX_DATA_TYPE_AV ("AVERAGE");
static const std::string kKEY_DATA_NX_DATA_TYPE_RMS ("RMS");
static const std::string kKEY_DATA_NX_DATA_TYPE_PEAK ("PEAK");
static const std::string kKEY_DATA_NX_DATA_TYPE_USER ("USER");
static const std::string kKEY_DATA_NX_DATA_TYPE_DATA_HIST ("DATA_HIST");
static const std::string kKEY_DATA_NX_DATA_TYPE_TIMESTAMP ("TIMESTAMP");
static const std::string kDATASET_TYPE_SEP = "@";

static const std::string kKEY_TRG_OVR_STRATEGY ("OVERRUN_STRATEGY");
static const std::string kKEY_TRG_SAMPLING_SRC ("SAMPLING_SOURCE");
static const std::string kKEY_TRG_TIMEOUT ("TIMEOUT");
static const std::string kKEY_TRG_MODE ("TRIGGER_MODE");
static const std::string kKEY_TRG_EXT_TYPE ("TRIGGER_EXT_TYPE");
static const std::string kKEY_TRG_EXT_AT_SRC ("TRIGGER_EXT_ATRIG_SRC");
static const std::string kKEY_TRG_EXT_EDGE ("TRIGGER_EXT_EDGE");
static const std::string kKEY_TRG_EXT_POST_DELAY ("TRIGGER_EXT_POST_DELAY");
static const std::string kKEY_TRG_EXT_MDL_POST_TRG_NB ("TRIGGER_EXT_MIDDLE_POST_TRIG_SAMPLES");
static const std::string kKEY_TRG_EXT_POST_FINITE_MODE ("TRIGGER_EXT_POST_FINITE_MODE");

static const std::string kKEY_TIMEBASE_TYPE("TIMEBASE_TYPE");
static const std::string kKEY_TIMEBASE_TYPE_INT("INTERNAL");
static const std::string kKEY_TIMEBASE_TYPE_EXT("EXTERNAL");
static const std::string kKEY_TIMEBASE_TYPE_SSI("SSI");

static const std::string kKEY_TRG_OVR_STGY_NOTIF ("NOTIFY");
static const std::string kKEY_TRG_OVR_STGY_ABT ("ABORT");
static const std::string kKEY_TRG_OVR_STGY_TRASH ("TRASH");
static const std::string kKEY_TRG_OVR_STGY_RESTART ("RESTART");
static const std::string kKEY_TRG_OVR_STGY_IGN ("IGNORE");

static const std::string kKEY_TRG_SPL_SRC_INT ("INTERNAL");
static const std::string kKEY_TRG_SPL_SRC_EXT ("EXTERNAL");

static const std::string kKEY_TRG_MODE_INT ("INTERNAL");
static const std::string kKEY_TRG_MODE_POST ("POST");
static const std::string kKEY_TRG_MODE_PRE ("PRE");
static const std::string kKEY_TRG_MODE_MDL ("MIDDLE");
static const std::string kKEY_TRG_MODE_POST_DL ("POST_DELAYED");
static const std::string kKEY_TRG_MODE_INT_POSTMORTEM ("INTERNAL_POSTMORTEM");

static const std::string kKEY_TRG_EXT_ATRIG ("ATRIG");
static const std::string kKEY_TRG_EXT_DTRIG ("DTRIG");
static const std::string kKEY_TRG_EXT_AT_SRC_EXT ("EXT");
static const std::string kKEY_TRG_EXT_AT_SRC_COND_B ("BELOW");
static const std::string kKEY_TRG_EXT_AT_SRC_COND_A ("ABOVE");
static const std::string kKEY_TRG_EXT_AT_SRC_COND_R ("REGION");
static const std::string kKEY_TRG_EXT_AT_SRC_COND_H ("HYSTERESIS");

static const std::string kKEY_TRG_EXT_EDGE_F ("FALLING");
static const std::string kKEY_TRG_EXT_EDGE_R ("RISING");

static const std::string kKEY_TRG_EXT_DL_UNIT_C ("CLOCK_TICKS");
static const std::string kKEY_TRG_EXT_DL_UNIT_S ("SAMPLES");

static const std::string kKEY_TRG_EXT_POST_FINITE_SAFE ("SAFE");
static const std::string kKEY_TRG_EXT_POST_FINITE_BEST_EFFORT ("BEST_EFFORT");

static const std::string kKEY_AUTO_TUNING ("AUTO_TUNING");

static const std::string kKEY_NEXUS_FILE_GENERATION ("NEXUS_FILE_GENERATION");

//-----------------------------------------------------------------------------


// ============================================================================
// class: ConfigurationParser
// This class provides parsing and extracting methods to get data from a ConfigX
// property which contains the acquisition definition.
// ============================================================================
class ConfigurationParser : public Tango::LogAdapter
{
public:

  //- constructor
  ConfigurationParser(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~ConfigurationParser();
  
  //- Parses a "Config<i>" property and fills a map containing the list of 
  //- <key,value> defining the acquisition.
  //- For channel definition, the method adds the number of the channel
  //- (in definition order) to make a unique key.
  AcquisitionKeys_t parseConfigProperty (std::vector<std::string> config_property)
    throw (Tango::DevFailed);

  //- Extracts config name from acquisition definition.
  std::string extractConfigName (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts active channels from acquisition definition.
  Channels_t extractActiveChannels (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts trigger mode from acquisition definition.
  E_TriggerMode_t extractTriggerMode (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts overrun strategy from acquisition definition.
  E_OverrunStrategy_t extractOverrunStrategy (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts data timeout from acquisition definition.
  yat::uint32 extractDataTimeout (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts sampling info from acquisition definition.
  SamplingInfo extractSamplingInfo (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts timebase info from acquisition definition.
  TimebaseInfo extractTimebaseInfo(AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts external trigger type from acquisition definition.
  E_ExtTrigType_t extractExtTrigType (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts external trigger edge from acquisition definition.
  E_ExtAtrigEdge_t extractExtTrigEdge (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts external analog trigger info from acquisition definition.
  ExtAnalogTrigInfo extractExtATrigInfo (AcquisitionKeys_t acq_config) 
    throw (Tango::DevFailed);

  //- Extracts external post delay unit & value for POST_DELAYED or MIDDLE modes
  //- from acquisition definition.
  PostDelay extractPostDelay (AcquisitionKeys_t acq_config, E_TriggerMode_t mode)
    throw (Tango::DevFailed);

  //- Extracts finite mode for POST or POST_DELAYED modes from acquisition definition.
  E_PostTrgFiniteMode_t extractPostFiniteMode (AcquisitionKeys_t acq_config) 
    throw (Tango::DevFailed);

  //- Extracts user defined property (by key) from acquisition definition.
  std::string extractUserDefinedProp (AcquisitionKeys_t acq_config, std::string key)
    throw (Tango::DevFailed);

  //- Extracts data scaled property from acquisition definition.
  bool extractDataScaled (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts data historized property from acquisition definition.
  bool extractDataHistorized (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts buffer timestamped property from acquisition definition.
  bool extractBufferTimestamped (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts statistics property from acquisition definition.
  bool extractStatisticsComputed (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts Nexus data types to store from acquisition definition.
  NxDataTypes_t extractNexusDataTypes (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts Nexus base name for Nexus files.
  std::string ConfigurationParser::extractNexusFileName (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts mask computed property from acquisition definition.
  bool extractMaskComputed (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts auto tuning property from acquisition definition.
  bool extractAutoTuning (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

  //- Extracts nexus file generation property from acquisition definition.
  bool extractNexusFileGeneration (AcquisitionKeys_t acq_config)
    throw (Tango::DevFailed);

private:
	//- Splits a line using the specifed separator and returns the 
  //- result in a string vector.
  //- Checks expected minimum & maximum number of tokens if not null.
	std::vector<std::string> splitLine(std::string line, std::string separator, 
                                     yat::uint16 min_token = 0, yat::uint16 max_token = 0)
    throw (Tango::DevFailed);

	//- Gets {key,value} from line using the specified separator and tells if
  //- the key word equals the specified key
  bool testKey(std::string line, std::string separator, std::string key)
    throw (Tango::DevFailed);

	//- Tells if token is "true" or "false".
  //- Sends exception if neither one of those strings.
  //- Not case sensitive.
  bool getBoolean(std::string token)
    throw (Tango::DevFailed);

};

} // namespace aicontroller

#endif // _CONFIG_PARSER_H_
