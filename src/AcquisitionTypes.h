//=============================================================================
// AcquisitionTypes.h
//=============================================================================
// abstraction.......Acquisition types for AIControllerV2 Device
// class.............AcquisitionParameters, AcquisitionData & DataTreatmentDefinition
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _ACQUISITION_TYPES_H_
#define _ACQUISITION_TYPES_H_

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include <map>
#include "TypesAndConsts.h"
#include <yat/memory/DataBuffer.h>

namespace aicontroller
{

// ============================================================================
// DATA TYPEs
// ============================================================================

//- Channel id type
typedef yat::uint16 ChannelId_t;

//- ChannelsAverageData_t: list of average data values for active channels
//- One average value per active channel
typedef std::map<ChannelId_t, double> ChannelsAverageData_t;
typedef ChannelsAverageData_t::iterator ChannelsAverageData_it_t;

//- ChannelsRms_t: list of RMS results for active channels
//- One rms value per active channel
typedef std::map<ChannelId_t, double> ChannelsRms_t;
typedef ChannelsRms_t::iterator ChannelsRms_it_t;

//- ChannelsPeak_t: list of Peak-to-Peak results for active channels
//- One Peak-to-Peak value per active channel
typedef std::map<ChannelId_t, double> ChannelsPeak_t;
typedef ChannelsPeak_t::iterator ChannelsPeak_it_t;

//- ChannelsRawData_t: list of raw data for active channels
//- One raw data buffer per active channel
//- Data buffer size = acquisition buffer depth
typedef yat::Buffer<yat::uint16> RawData_t; // raw data buffer
typedef std::map<ChannelId_t, RawData_t> ChannelsRawData_t;
typedef ChannelsRawData_t::iterator ChannelsRawData_it_t;

//- ChannelsScaledData_t: list of scaled data for active channels
//- One scaled data buffer per active channel
//- Data buffer size = acquisition buffer depth
typedef yat::Buffer<double> ScaledData_t;
typedef std::map<ChannelId_t, ScaledData_t> ChannelsScaledData_t;
typedef ChannelsScaledData_t::iterator ChannelsScaledData_it_t;

//- ChannelsAverageHistorizedData_t: list of average data history for active channels
//- One average data history buffer per active channel
//- Data buffer size = average history depth
typedef yat::CircularBuffer<double> AverageHistorizedData_t;
typedef yat::Buffer<double> AverageOrderedHistorizedData_t;
typedef std::map<ChannelId_t, AverageHistorizedData_t *> ChannelsAverageHistorizedData_t;
typedef ChannelsAverageHistorizedData_t::iterator ChannelsAverageHistorizedData_it_t;

//- ChannelsScaledHistorizedData_t: list of scaled data history for active channels
//- One scaled data history buffer per active channel
//- Data buffer size = data history depth
typedef double * ScaledHistorizedData_t;
typedef std::map<ChannelId_t, ScaledHistorizedData_t> ChannelsScaledHistorizedData_t;
typedef ChannelsScaledHistorizedData_t::iterator ChannelsScaledHistorizedData_it_t;

//- ChannelsUserData_t: list of user data for active channels
//- One user data buffer per active channel
//- Data buffer size = acquisition buffer depth
typedef yat::Buffer<double> UserData_t;
typedef std::map<ChannelId_t, UserData_t> ChannelsUserData_t;
typedef ChannelsUserData_t::iterator ChannelsUserData_it_t;

//- BufferTimestamps_t: list of absolute buffer timestamps 
//- in number of seconds since EPOCH
//- History size = timestamp histories depth
typedef yat::CircularBuffer<double> BufferTimestamps_t;

//- RelativeBuffTimes_t: list of relative buffer timestamps 
//- in number of seconds since previous buffer
//- History size = timestamp histories depth
typedef yat::CircularBuffer<double> RelativeBuffTimes_t;

//- Data mask types
typedef std::vector<bool> DataMask_t;

typedef struct TimeSlot
{
  //- members ------------
  // 1st time in ms
  double t1;

  // 2nd time in ms
  double t2;

  //- default constructor --
  TimeSlot ()
    : t1(0),
      t2(1)
  {
  }

  //- destructor -----------
  ~TimeSlot ()
  {
  }

  //- copy constructor -----
  TimeSlot (const TimeSlot& src)
  {
    *this = src;
  }

  //- operator= --------------
  const TimeSlot & operator= (const TimeSlot& src)
  {
      if (this == & src) 
        return *this;

      this->t1 = src.t1;
      this->t2 = src.t2;

      return *this;
  }

  //- dump -------------------
  void dump () const
  {
    std::cout << "TimeSlot::t1........." 
              << this->t1
              << std::endl; 
    std::cout << "TimeSlot::t2........." 
              << this->t2
              << std::endl; 
  }
} TimeSlot;

typedef std::vector<TimeSlot> TimeSlotMask_t;

//- DatasetFlags_t: dataset flag (enable/disable nexus storage) for active channels
//- One flag per active channel
typedef std::map<ChannelId_t, bool> DatasetFlags_t;
typedef DatasetFlags_t::const_iterator DatasetFlags_it_t;

// ============================================================================
// Parameters of the defined acquisition (frequency, integration time, ...)
// ============================================================================
typedef struct AcquisitionParameters
{
  //- members --------------------
  // Configuration identifier
  yat::uint16 configId;

  // Configuration name
  std::string configName;

  // Samples number
  yat::uint32 samplesNumber;

  // Sampling rate in Hz
  double frequency;

  // Integration time in ms
  double integrationTime;

  // Enable or disable Nexus file generation
  bool nexusFileGeneration;

  // Nexus target path
  std::string nexusTargetPath;

  // Number of acquisitions pushed in a single Nexus file
  yat::uint16 nexusNbPerFile;  

  // Enable or disable dataset storage in nexus file
  DatasetFlags_t datasetFlags;

  //- default constructor -----------------------
  AcquisitionParameters ()
    : configId(1),
      configName(""),
      samplesNumber(0),
      frequency(100.0),
      integrationTime(100.0),
      nexusFileGeneration(false),
      nexusTargetPath(""),
      nexusNbPerFile(100)
  {
    datasetFlags.clear();
  }

  //- destructor -----------------------
  ~AcquisitionParameters ()
  {
    datasetFlags.clear();
  }

  //- copy constructor ------------------
  AcquisitionParameters (const AcquisitionParameters& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const AcquisitionParameters & operator= (const AcquisitionParameters& src)
  {
      if (this == & src) 
        return *this;

      this->configId = src.configId;
      this->configName = src.configName;
      this->samplesNumber = src.samplesNumber;
      this->frequency = src.frequency;
      this->integrationTime = src.integrationTime;
      this->nexusFileGeneration = src.nexusFileGeneration;
      this->nexusTargetPath = src.nexusTargetPath;
      this->nexusNbPerFile = src.nexusNbPerFile;
      this->datasetFlags = src.datasetFlags;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "AcquisitionParameters::configId........." 
              << this->configId
              << std::endl; 
    std::cout << "AcquisitionParameters::configName........." 
              << this->configName
              << std::endl; 
    std::cout << "AcquisitionParameters::samplesNumber........." 
              << this->samplesNumber
              << std::endl; 
    std::cout << "AcquisitionParameters::frequency........." 
              << this->frequency
              << std::endl; 
    std::cout << "AcquisitionParameters::integrationTime........." 
              << this->integrationTime
              << std::endl; 
    std::cout << "AcquisitionParameters::nexusFileGeneration........." 
              << this->nexusFileGeneration
              << std::endl; 
    std::cout << "AcquisitionParameters::nexusTargetPath........." 
              << this->nexusTargetPath
              << std::endl; 
   	std::cout << "AcquisitionParameters::nexusNbPerFile........." 
              << this->nexusNbPerFile
              << std::endl;

    for (DatasetFlags_it_t it = datasetFlags.begin() ; it != datasetFlags.end(); ++it)
    {
      std::cout << "AcquisitionParameters::datasetFlag chan: " 
              << it->first << " = "
              << it->second
              << std::endl;
    }
  }

} AcquisitionParameters;


// ============================================================================
// General data of the current acquisition (generic results of the acquisition)
// ============================================================================
typedef struct AcquisitionData
{
  //- members --------------------
  // Data counter
  yat::uint32 dataCounter;

  // Error counter
  yat::uint32 errorCounter;

  // Overrun counter
  yat::uint32 overrunCounter;

  // Timeout counter
  yat::uint32 timeoutCounter;

  // Data notification frequency in Hz
  double dataNotifFrequency;

  // Trigger timestamp in string format
  // Used to stamp DAQ END trigger (or user end)
  std::string triggerTimestamp_c;

  // Buffer timestamp in seconds (ms precision)
  // Used to stamp each buffer reception (triggered or not)
  double bufferTimestamp_s;

  // Buffer relative datation in seconds (ms precision)
  // Used to compute duration between current buffer and previous one
  double relBuffTimestamp_s;

  // External trigger flag
  bool extTriggerFlag;

  // Last valid index in scaled data history
  yat::uint32 historyLastIndex;

  // List of raw data buffer (one buffer per active channel)
  ChannelsRawData_t rawData;

  //- default constructor -----------------------
  AcquisitionData ()
    : dataCounter(0),
      errorCounter(0),
      overrunCounter(0),
      timeoutCounter(0),
      dataNotifFrequency(0.0),
      triggerTimestamp_c("00/00/00 00:00:00"),
      bufferTimestamp_s(0.0),
      relBuffTimestamp_s(0.0),
      extTriggerFlag(false),
      historyLastIndex(0)
  {
  }

  //- destructor -----------------------
  ~AcquisitionData ()
  {
    rawData.clear();
  }

  //- copy constructor ------------------
  AcquisitionData (const AcquisitionData& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const AcquisitionData & operator= (const AcquisitionData& src)
  {
      if (this == & src) 
        return *this;

      this->dataCounter = src.dataCounter;
      this->errorCounter = src.errorCounter;
      this->overrunCounter = src.overrunCounter;
      this->timeoutCounter = src.timeoutCounter;
      this->dataNotifFrequency = src.dataNotifFrequency;
      this->triggerTimestamp_c = src.triggerTimestamp_c;
      this->bufferTimestamp_s = src.bufferTimestamp_s;
      this->relBuffTimestamp_s = src.relBuffTimestamp_s;
      this->extTriggerFlag = src.extTriggerFlag;
      this->historyLastIndex = src.historyLastIndex;
      this->rawData = src.rawData;

      return *this;
  }

} AcquisitionData;

// ============================================================================
// Specific data of the current acquisition (specific results of the acquisition)
// Filled according to data treatment and acquisition mode
// ============================================================================
typedef struct SpecificAcquisitionData
{
  //- members --------------------
  // List of scaled data buffer (one buffer per active channel)
  ChannelsScaledData_t scaledData;

  // List of average data values (one value per active channel)
  ChannelsAverageData_t averageData;

  // List of average data history (one history per active channel)
  ChannelsAverageHistorizedData_t averageDataHistory;

  // List of scaled average data values (one value per active channel)
  // Note that this field is available in case of scaled + stats + user data enabled
  // and it is filled with scaled data average values.
  // In this case, the averageData field contains the user data average values.
  ChannelsAverageData_t scaledAvData; 

  // List of scaled data history (one history per active channel)
  ChannelsScaledHistorizedData_t dataHistory;

  // List of RMS computation (one value per active channel)
  ChannelsRms_t rms;

  // List of Peak-To-Peak computation (one value per active channel)
  ChannelsPeak_t peakToPeak;

  // List of user data buffer (one buffer per active channel)
  ChannelsUserData_t userData;
  
  // List of buffer timestamps
  // Note that this field is available in case of timestamp enabled
  // and it is filled with absolute buffer timestamp values
  // in number of seconds since EPOCH.
  BufferTimestamps_t * buffTimestamps;

  // List of relative buffer datation
  // Note that this field is available in case of timestamp enabled
  // and it is filled with relative buffer timestamp values
  // in number of seconds since previous buffer.
  RelativeBuffTimes_t * relativeBuffTimes;

  //- default constructor -----------------------
  SpecificAcquisitionData ():
    buffTimestamps(NULL),
    relativeBuffTimes(NULL)
  {
  }

  //- destructor -----------------------
  ~SpecificAcquisitionData ()
  {
    scaledData.clear();
    averageData.clear();
    dataHistory.clear();
    scaledAvData.clear();
    rms.clear();
    peakToPeak.clear();
    averageDataHistory.clear();
    userData.clear();
    if (buffTimestamps)
    {
      buffTimestamps->clear();
      delete buffTimestamps;
      buffTimestamps = NULL;
    }
    if (relativeBuffTimes)
    {
      relativeBuffTimes->clear();
      delete relativeBuffTimes;
      relativeBuffTimes = NULL;
    }

  }

  //- copy constructor ------------------
  SpecificAcquisitionData (const SpecificAcquisitionData& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const SpecificAcquisitionData & operator= (const SpecificAcquisitionData& src)
  {
      if (this == & src) 
        return *this;

      this->scaledData = src.scaledData;
      this->averageData = src.averageData;
      this->averageDataHistory = src.averageDataHistory;
      this->scaledAvData = src.scaledAvData;
      this->dataHistory = src.dataHistory;
      this->rms = src.rms;
      this->peakToPeak = src.peakToPeak;
      this->userData = src.userData;
      this->buffTimestamps = src.buffTimestamps;
      this->relativeBuffTimes = src.relativeBuffTimes;

      return *this;
  }

} SpecificAcquisitionData;


// ============================================================================
// Configuration of data computing (scaling, history, rms, ...)
// ============================================================================
typedef struct DataTreatmentDefinition
{
  //- members --------------------
  // Is data scaled?
  bool isDataScaled;

  // Is data historized?
  bool isDataHistorized;

  // Length of data history buffer multiple of integration time
  yat::uint16 dataHistoryBufferDepth;

  // Are statistics computed?
  bool hasStatistics;

  // Length of statistics history buffer in number of samples
  yat::uint32 statHistoryBufferDepth;

  // Types of data to store in Nexus files
  NxDataTypes_t nxTypes;

  // Base name for Nexus files
  std::string nxFileName;

  // Is data mask computed?
  bool hasMask;

  // Boolean data mask (for asl config)
  DataMask_t boolDataMask;

  // time slots mask (user interface)
  std::string timeSlotMask;

  // is auto tuning enabled?
  bool autoTuning;

  // is timestamp enabled?
  bool timestamped;

  // Length of timestamp histories in number of samples
  yat::uint32 timestampHistoriesDepth;

  //- default constructor -----------------------
  DataTreatmentDefinition ()
    : isDataScaled(false),
      isDataHistorized(false),
      dataHistoryBufferDepth(2),
      hasStatistics(false),
      statHistoryBufferDepth(1000),
      nxFileName(""),
      hasMask(false),
      timeSlotMask(""),
      autoTuning(false),
      timestamped(false),
      timestampHistoriesDepth(1000)
  {
  }

  //- destructor -----------------------
  ~DataTreatmentDefinition ()
  {
    this->nxTypes.clear();
    this->boolDataMask.clear();
  }

  //- copy constructor ------------------
  DataTreatmentDefinition (const DataTreatmentDefinition& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const DataTreatmentDefinition & operator= (const DataTreatmentDefinition& src)
  {
      if (this == & src) 
        return *this;

      this->isDataScaled = src.isDataScaled;
      this->isDataHistorized = src.isDataHistorized;
      this->dataHistoryBufferDepth = src.dataHistoryBufferDepth;
      this->hasStatistics = src.hasStatistics;
      this->statHistoryBufferDepth = src.statHistoryBufferDepth;
      this->nxTypes = src.nxTypes;
      this->nxFileName = src.nxFileName;
      this->hasMask = src.hasMask;
      this->boolDataMask = src.boolDataMask;
      this->timeSlotMask = src.timeSlotMask;
      this->autoTuning = src.autoTuning;
      this->timestamped = src.timestamped;
      this->timestampHistoriesDepth = src.timestampHistoriesDepth;

      return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "DataTreatmentDefinition::isDataScaled........." 
              << this->isDataScaled
              << std::endl; 
    std::cout << "DataTreatmentDefinition::isDataHistorized........." 
              << this->isDataHistorized
              << std::endl; 
    std::cout << "DataTreatmentDefinition::dataHistoryBufferDepth........." 
              << this->dataHistoryBufferDepth
              << std::endl; 
    std::cout << "DataTreatmentDefinition::hasStatistics........." 
              << this->hasStatistics
              << std::endl; 
    std::cout << "DataTreatmentDefinition::statHistoryBufferDepth........." 
              << this->statHistoryBufferDepth
              << std::endl; 
    std::cout << "DataTreatmentDefinition::nxTypes......... "; 

    for (size_t c = 0; c < nxTypes.size(); c++)
      std::cout << nxTypes[c] << " ";
    std::cout << std::endl;

    std::cout << "DataTreatmentDefinition::nxFileName......"
              << this->nxFileName
              << std::endl;

    std::cout << "DataTreatmentDefinition::hasMask........." 
              << this->hasMask
              << std::endl;

    std::cout << "DataTreatmentDefinition::boolDataMask......... "; 
    for (size_t c = 0; c < boolDataMask.size(); c++)
      std::cout << boolDataMask[c] << " ";
    std::cout << std::endl;

    std::cout << "DataTreatmentDefinition::timeSlotMask........." 
              << this->timeSlotMask
              << std::endl;
    std::cout << "DataTreatmentDefinition::autoTuning........." 
              << this->autoTuning
              << std::endl; 
    std::cout << "DataTreatmentDefinition::timestamped........." 
              << this->timestamped
              << std::endl; 
    std::cout << "DataTreatmentDefinition::timestampHistoriesDepth........." 
              << this->timestampHistoriesDepth
              << std::endl; 
    
  }

} DataTreatmentDefinition;


} //- namespace aicontroller

#endif //- _ACQUISITION_TYPES_H_

