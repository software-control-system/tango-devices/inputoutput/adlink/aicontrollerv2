//=============================================================================
// MIDDLE_AcqMode.h
//=============================================================================
// abstraction.......MIDDLE acquisition mode abstraction
// class.............MIDDLE_AcqMode
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _MIDDLE_ACQ_MODE_H_
#define _MIDDLE_ACQ_MODE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "AcqMode.h"

namespace aicontroller
{

// ============================================================================
// class: MIDDLE_AcqMode
// This class provides specific behavior for MIDDLE trigger mode.
// ============================================================================
class MIDDLE_AcqMode : public AcqMode
{
public:

  //- constructor
  MIDDLE_AcqMode(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~MIDDLE_AcqMode();
  
  //- read callback function for data mask
  void read_callback_data_mask(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- write callback function for data mask
  void write_callback_data_mask(yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- read callback function for trigger timestamp
  void read_callback_trigger_timestamp(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for external trigger flag
  void read_callback_ext_trigger_flag(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- read callback function for history last index
  void read_callback_history_last_index(yat4tango::DynamicAttributeReadCallbackData& cbd);

  //- Sets new data mask (time slot format)
  virtual void setDataMask (std::string mask)
    throw (Tango::DevFailed);

protected:
  
  //- Extracts specific acquisition definition from raw config according
  //- to acquisition mode
  //- Sets m_acqDefinition
  virtual void extractSpecificAcqDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Extracts specific data treatment definition from raw config
  virtual void extractSpecificDataTrtDefinition_i(RawAcquitisionConfig raw_acq_config)
    throw (Tango::DevFailed);

  //- Sets specific configuration depending on acquisition mode
  virtual void specificAIconfigMapper_i()
    throw (Tango::DevFailed);

  //- Creates specific dynamic attributes according to acquisition mode
  virtual void define_specific_attributes_i(std::vector<yat4tango::DynamicAttributeInfo>& dynAttrList);

  //- Data processing user hook.
  virtual void handle_input_i(asl::AIRawData* raw_data)
    throw (Tango::DevFailed);

  //- Timeout user hook.
  void handle_timeout_i()
    throw (Tango::DevFailed);

  //- DAQ end user hook.
  void handle_daq_end_i()
    throw (Tango::DevFailed);

  //- Initializes specific attributes
  virtual void initSpecificAttributes_i()
    throw (Tango::DevFailed);

  //- Returns data history validity
  virtual bool getDataHistoryValidity();

private:
  // data history validity
  bool m_dataHistoryValidity;
};

} // namespace aicontroller

#endif // _MIDDLE_ACQ_MODE_H_
