//=============================================================================
// ConfigurationParser.cpp
//=============================================================================
// abstraction.......Acquisition configuration parser
// class.............ConfigurationParser
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ConfigurationParser.h"
#include <yat/utils/StringTokenizer.h>
#include <yat/utils/XString.h>
#include <yat/utils/String.h>
#include <yat/Exception.h>

namespace aicontroller
{

// ======================================================================
// ConfigurationParser::ConfigurationParser
// ======================================================================
ConfigurationParser::ConfigurationParser(Tango::DeviceImpl * host_device)
: Tango::LogAdapter(host_device)
{
}

// ======================================================================
// ConfigurationParser::~ConfigurationParser
// ======================================================================
ConfigurationParser::~ConfigurationParser ()
{
}

// ======================================================================
// ConfigurationParser::parseConfigProperty
// ======================================================================
AcquisitionKeys_t ConfigurationParser::parseConfigProperty (std::vector<std::string> config_property)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "ConfigurationParser::parseConfigProperty ==>" << std::endl;

  // This function parses a "Config<i>" property and fills a map with the <KEY, value>
  // list defining the acquisition.

  AcquisitionKeys_t acqKeyList;
  size_t chanIdx = 0;

  // Parse config vector and extract KEY::value fields
  for (std::vector<std::string>::iterator it = config_property.begin() ; it != config_property.end(); ++it)
  {
    std::vector<std::string> tokenList = this->splitLine(*it, kKEY_VALUE_SEPARATOR);

    if (tokenList.size() != 2)
    {
      // empty list => fatal error
      yat::OSStream oss;
      oss << "Bad configuration syntax: " << *it << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::parseConfigProperty"));
    }

    // special treatment for channel configuration (because the keys are not
    // unique): add number to the key
    if ((tokenList[0].compare(kKEY_CHANNEL_LABEL) == 0) ||
        (tokenList[0].compare(kKEY_CHANNEL_NUM) == 0) ||
        (tokenList[0].compare(kKEY_CHANNEL_RANGE) == 0) ||
        (tokenList[0].compare(kKEY_CHANNEL_GRD_REF) == 0) ||
        (tokenList[0].compare(kKEY_CHANNEL_DESC) == 0) || 
        (tokenList[0].compare(kKEY_CHAN_USER_DATA_GAIN) == 0) ||
        (tokenList[0].compare(kKEY_CHAN_USER_DATA_OFFSET1) == 0) ||
        (tokenList[0].compare(kKEY_CHAN_USER_DATA_OFFSET2) == 0) ||
        (tokenList[0].compare(kKEY_CHAN_USER_DATA) == 0))
    {
      if (tokenList[0].compare(kKEY_CHANNEL_LABEL) == 0)
      {
        // start new active channel definition
        chanIdx++;
      }

      tokenList[0] += yat::XString<size_t>::to_string(chanIdx);
    }

    // insert <key,value> in map
    acqKeyList.insert(AcquisitionKey_pair_t(tokenList[0], tokenList[1]));
    DEBUG_STREAM << "ConfigurationParser::parseConfigProperty - add <" << tokenList[0] 
    << "," << tokenList[1] << "> field" << std::endl;
  }

  return acqKeyList;
}


// ======================================================================
// ConfigurationParser::splitLine
// ======================================================================
std::vector<std::string> ConfigurationParser::splitLine(std::string line, std::string separator,
                                                        yat::uint16 min_token, yat::uint16 max_tokens)
  throw (Tango::DevFailed)
{
	//- result : the split line 
  std::vector<std::string> tokenList;

  yat::StringTokenizer st(line, separator);

	while (st.has_more_tokens()) // while there is a remaining token
	{
    std::string token_str = st.next_token();
		tokenList.push_back(token_str);
	}

  // checks minimum expected number of tokens if not set to a null value
  if ((min_token != 0) &&
      (tokenList.size() < min_token) )
  {
    // expected more tokens
    yat::OSStream oss;
    oss << "Bad value: " << line << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::splitLine")); 
  }

  // checks maximum expected number of tokens if not set to a null value
  if ((max_tokens != 0) &&
      (tokenList.size() > max_tokens) )
  {
    // expected less tokens
    yat::OSStream oss;
    oss << "Bad value: " << line << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::splitLine")); 
  }

	return tokenList;
}

// ======================================================================
// ConfigurationParser::testKey
// ======================================================================
bool ConfigurationParser::testKey(std::string line, 
                                  std::string separator, 
                                  std::string key)
  throw (Tango::DevFailed)
{
  bool isWrightKey = false;
  std::vector<std::string> tokenList = this->splitLine(line, separator);

  if (tokenList.size() != 2)
  {
    // empty list => fatal error
    yat::OSStream oss;
    oss << "Bad configuration syntax: " << line << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::getKeyValue"));
  }

  if (0 == tokenList[0].compare(key))
  {
    // Wright key word => return true
    isWrightKey = true;
  }

  return isWrightKey;
}

// ======================================================================
// ConfigurationParser::getBoolean
// ======================================================================
bool ConfigurationParser::getBoolean(std::string token)
  throw (Tango::DevFailed)
{
  bool result;
  yat::String ytoken(token);
  
  if (ytoken.is_equal_no_case("true"))
  {
    result = true;
  }
  else if (ytoken.is_equal_no_case("false"))
  {
    result = false;
  }
  else
  {
    // bad boolean value ==> fatal error
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    _CPTC("Bad boolean value"), 
                    _CPTC("ConfigurationParser::parseConfigProperty")); 
  }

  return result;
}


// ======================================================================
// ConfigurationParser::extractTriggerMode
// ======================================================================
E_TriggerMode_t ConfigurationParser::extractTriggerMode (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  E_TriggerMode_t mode = TRG_UNDEFINED;

  AcquisitionKeys_it_t it = acq_config.find(kKEY_TRG_MODE);
  
  if (it == acq_config.end())
  {
    // key not found => error
    yat::OSStream oss;
    oss << "Trigger mode not found in configuration - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractTriggerMode"));
  }

  std::string tokenValue = it->second;

  if (0 == tokenValue.compare(kKEY_TRG_MODE_INT))
  {
    mode = TRG_INTERNAL;
  }
  else if (0 == tokenValue.compare(kKEY_TRG_MODE_POST))
  {
    mode = TRG_POST;
  }
  else if (0 == tokenValue.compare(kKEY_TRG_MODE_PRE))
  {
    mode = TRG_PRE;
  }
  else if (0 == tokenValue.compare(kKEY_TRG_MODE_MDL))
  {
    mode = TRG_MIDDLE;
  }
  else if (0 == tokenValue.compare(kKEY_TRG_MODE_POST_DL))
  {
    mode = TRG_POST_DELAYED;
  }
  else if (0 == tokenValue.compare(kKEY_TRG_MODE_INT_POSTMORTEM))
  {
    mode = TRG_INT_POSTMORTEM;
  }
  else
  {
    // bad value ==> fatal error
    yat::OSStream oss;
    oss << "Bad trigger mode value: " << tokenValue << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractTriggerMode"));
  }

  DEBUG_STREAM << "Trigger mode: " << mode << std::endl;

  return mode;
}

// ======================================================================
// ConfigurationParser::extractConfigName
// ======================================================================
std::string ConfigurationParser::extractConfigName (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  std::string name = "";

  AcquisitionKeys_it_t it = acq_config.find(kKEY_NAME);
  
  if (it == acq_config.end())
  {
    // key not found => error
    yat::OSStream oss;
    oss << "Config name not found in configuration - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractConfigName"));
  }

  name = it->second; 
  DEBUG_STREAM << "Config name: " << name << std::endl;

  return name;
}

// ======================================================================
// ConfigurationParser::extractActiveChannels
// ======================================================================
Channels_t ConfigurationParser::extractActiveChannels (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  Channels_t channelList;

  //- channel configuration:
	//- 0) label
	//- 1) channel id
	//- 2) channel range
	//- 3) ground ref
	//- 4) attr description [OPTIONAL]

  // user data:
	//- 5) gain [OPTIONAL]
	//- 6) offset1 [OPTIONAL]
	//- 7) offset2 [OPTIONAL]
	//- 8) unit [OPTIONAL]


  //- Channel KEYs are post-fixed with number from 1 to N
  yat::uint16 chanIdx = 1;
  AcquisitionKeys_it_t it;
  std::string chanKey = "";
  bool endOfParsing = false;

  do
  {
    Channel ac;

    // get label
    chanKey = kKEY_CHANNEL_LABEL + yat::XString<size_t>::to_string(chanIdx);
    it = acq_config.find(chanKey);

    if (it == acq_config.end())
    {
      // key not found => get out of parsing
      endOfParsing = true;
    }
    else
    {
      ac.label = it->second;
    

      // get id
      chanKey = kKEY_CHANNEL_NUM + yat::XString<size_t>::to_string(chanIdx);
      it = acq_config.find(chanKey);

      if (it == acq_config.end())
      {
        // key not found => error
        yat::OSStream oss;
        oss << "Channel number for channel: " << ac.label << "not found in configuration - check device property";
        ERROR_STREAM << oss.str() << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        oss.str().c_str(), 
                        _CPTC("ConfigurationParser::extractActiveChannels"));
      }
   
      try
      {
        unsigned short id = yat::XString<unsigned short>::to_num(it->second);
        ac.number = (yat::uint16)id;
      }
      catch (const yat::Exception& ye)
      {
        // bad integer value ==> fatal error
        yat::OSStream oss;
        oss << "Bad channel id: " << it->second << "for channel: " << ac.label << " - check device property";
        ERROR_STREAM << oss.str() << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        oss.str().c_str(), 
                        _CPTC("ConfigurationParser::extractActiveChannels")); 
      }


      // get range
      chanKey = kKEY_CHANNEL_RANGE + yat::XString<size_t>::to_string(chanIdx);
      it = acq_config.find(chanKey);

      if (it == acq_config.end())
      {
        // key not found => error
        yat::OSStream oss;
        oss << "Channel range for channel: " << ac.label << "not found in configuration - check device property";
        ERROR_STREAM << oss.str() << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        oss.str().c_str(), 
                        _CPTC("ConfigurationParser::extractActiveChannels"));
      }
      
      ac.range = it->second;


      // get ground ref
      chanKey = kKEY_CHANNEL_GRD_REF + yat::XString<size_t>::to_string(chanIdx);
      it = acq_config.find(chanKey);

      if (it == acq_config.end())
      {
        // key not found => error
        yat::OSStream oss;
        oss << "Channel ground ref for channel: " << ac.label << " not found in configuration - check device property";
        ERROR_STREAM << oss.str() << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        oss.str().c_str(), 
                        _CPTC("ConfigurationParser::extractActiveChannels"));
      }
      
      std::string tokenValue = it->second;

      if (0 == tokenValue.compare(kKEY_CHANNEL_GRD_REF_SGL))
      {
        ac.groundRef = GRD_REF_SINGLE;
      }
      else if (0 == tokenValue.compare(kKEY_CHANNEL_GRD_REF_DIF))
      {
        ac.groundRef = GRD_REF_DIFF;
      }
      else
      {
        // bad value ==> fatal error
        yat::OSStream oss;
        oss << "Bad ground ref value: " << tokenValue << " for channel: " << ac.label << " - check device property";
        ERROR_STREAM << oss.str() << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        oss.str().c_str(), 
                        _CPTC("ConfigurationParser::extractActiveChannels"));       
      }


      // get description (optional)
      chanKey = kKEY_CHANNEL_DESC + yat::XString<size_t>::to_string(chanIdx);
      it = acq_config.find(chanKey);

      if (it != acq_config.end())
      {
        ac.description = it->second;
      }
      else
      {
        // description not set
        ac.description = "";
      }

      // get user data computed (optional)
      chanKey = kKEY_CHAN_USER_DATA + yat::XString<size_t>::to_string(chanIdx);
      it = acq_config.find(chanKey);

      if (it != acq_config.end())
      {
        std::vector<std::string> valueList = splitLine(it->second, kVALUE_SEPARATOR_CL, 2, 2);

        // get user data computed
        try
        {
          ac.userDataDesc.hasUserData = this->getBoolean(valueList[0]);
        }
        catch(Tango::DevFailed & df)
        {
          // bad boolean value ==> fatal error
          yat::OSStream oss;
          oss << "Bad value for user data computed property: " << valueList[0] << " - check device property";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("ConfigurationParser::extractActiveChannels")); 
        }

        // get user data unit
        ac.userDataDesc.unit = valueList[1];
      }
      else
      {
        // user data not set, use default value
        ac.userDataDesc.unit = " ";
        ac.userDataDesc.hasUserData = false;
      }


      // get user data gain (optional)
      chanKey = kKEY_CHAN_USER_DATA_GAIN + yat::XString<size_t>::to_string(chanIdx);
      it = acq_config.find(chanKey);

      if (it != acq_config.end())
      {
        std::vector<std::string> gainList = splitLine(it->second, kVALUE_SEPARATOR_CL, 1, 3);

        if (0 == gainList[0].compare(kKEY_CHAN_USER_DATA_ATTR))
        {
          ac.userDataDesc.gainType = USER_DATA_ATTR;
        }
        else if (0 == gainList[0].compare(kKEY_CHAN_USER_DATA_PROXY))
        {
          ac.userDataDesc.gainType = USER_DATA_PROXY;

          // get Device name & attribute
          if (gainList.size() != 3)
          {
            // Device name and/or attribute not defined => Fatal error
            yat::OSStream oss;
            oss << "Device name and/or attribute not defined for user data gain - check device property";
            ERROR_STREAM << oss.str() << std::endl;
            THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                            oss.str().c_str(), 
                            _CPTC("ConfigurationParser::extractActiveChannels"));
          }
          else
          {
            ac.userDataDesc.gainProxy.deviceName = gainList[1];
            ac.userDataDesc.gainProxy.attributeName = gainList[2];
          }
        }
        else
        {
          // Bad type for user data gain => fatal error
          yat::OSStream oss;
          oss << "Bad type for user data gain: " << gainList[0] << " - check device property";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("ConfigurationParser::extractActiveChannels"));
        }
      }
      else
      {
        // gain not set, use default value
        ac.userDataDesc.gain = 1.0;
        ac.userDataDesc.gainType = USER_DATA_ATTR;
      }

      // get user data offset1 (optional)
      chanKey = kKEY_CHAN_USER_DATA_OFFSET1 + yat::XString<size_t>::to_string(chanIdx);
      it = acq_config.find(chanKey);

      if (it != acq_config.end())
      {
        std::vector<std::string> off1List = splitLine(it->second, kVALUE_SEPARATOR_CL, 1, 3);

        if (0 == off1List[0].compare(kKEY_CHAN_USER_DATA_ATTR))
        {
          ac.userDataDesc.offset1Type = USER_DATA_ATTR;
        }
        else if (0 == off1List[0].compare(kKEY_CHAN_USER_DATA_PROXY))
        {
          ac.userDataDesc.offset1Type = USER_DATA_PROXY;

          // get Device name & attribute
          if (off1List.size() != 3)
          {
            // Device name and/or attribute not defined => Fatal error
            yat::OSStream oss;
            oss << "Device name and/or attribute not defined for user data offset1 - check device property";
            ERROR_STREAM << oss.str() << std::endl;
            THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                            oss.str().c_str(), 
                            _CPTC("ConfigurationParser::extractActiveChannels"));
          }
          else
          {
            ac.userDataDesc.offset1Proxy.deviceName = off1List[1];
            ac.userDataDesc.offset1Proxy.attributeName = off1List[2];
          }
        }
        else
        {
          // Bad type for user data gain => fatal error
          yat::OSStream oss;
          oss << "Bad type for user data offset1: " << off1List[0] << " - check device property";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("ConfigurationParser::extractActiveChannels"));
        }
      }
      else
      {
        // offset1 not set, use default value
        ac.userDataDesc.offset1 = 0.0;
        ac.userDataDesc.offset1Type = USER_DATA_ATTR;
      }

      // get user data offset2 (optional)
      chanKey = kKEY_CHAN_USER_DATA_OFFSET2 + yat::XString<size_t>::to_string(chanIdx);
      it = acq_config.find(chanKey);

      if (it != acq_config.end())
      {
        std::vector<std::string> off2List = splitLine(it->second, kVALUE_SEPARATOR_CL, 1, 3);

        if (0 == off2List[0].compare(kKEY_CHAN_USER_DATA_ATTR))
        {
          ac.userDataDesc.offset2Type = USER_DATA_ATTR;
        }
        else if (0 == off2List[0].compare(kKEY_CHAN_USER_DATA_PROXY))
        {
          ac.userDataDesc.offset2Type = USER_DATA_PROXY;

          // get Device name & attribute
          if (off2List.size() != 3)
          {
            // Device name and/or attribute not defined => Fatal error
            yat::OSStream oss;
            oss << "Device name and/or attribute not defined for user data offset2 - check device property";
            ERROR_STREAM << oss.str() << std::endl;
            THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                            oss.str().c_str(), 
                            _CPTC("ConfigurationParser::extractActiveChannels"));
          }
          else
          {
            ac.userDataDesc.offset2Proxy.deviceName = off2List[1];
            ac.userDataDesc.offset2Proxy.attributeName = off2List[2];
          }
        }
        else
        {
          // Bad type for user data gain => fatal error
          yat::OSStream oss;
          oss << "Bad type for user data offset2: " << off2List[0] << " - check device property";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("ConfigurationParser::extractActiveChannels"));
        }
      }
      else
      {
        // offset2 not set, use default value
        ac.userDataDesc.offset2 = 0.0;
        ac.userDataDesc.offset2Type = USER_DATA_ATTR;
      }

      ac.dump();
      channelList.push_back(ac);

      chanIdx++;
    }
  } while (endOfParsing == false);


  DEBUG_STREAM << "Number of active channels: " << channelList.size() << std::endl;

  return channelList;
}

// ======================================================================
// ConfigurationParser::extractOverrunStrategy
// ======================================================================
E_OverrunStrategy_t ConfigurationParser::extractOverrunStrategy (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  E_OverrunStrategy_t overrun;

  // get overrun strategy (optional)
  AcquisitionKeys_it_t it = acq_config.find(kKEY_TRG_OVR_STRATEGY);
  
  if (it == acq_config.end())
  {
    // key not found, default value = NOTIFY
    overrun = OVRN_NOTIFY;
  }
  else
  {
    std::string tokenValue = it->second;

    if (0 == tokenValue.compare(kKEY_TRG_OVR_STGY_NOTIF))
    {
      overrun = OVRN_NOTIFY;
    }
    else if (0 == tokenValue.compare(kKEY_TRG_OVR_STGY_ABT))
    {
      overrun = OVRN_ABORT;
    }
    else if (0 == tokenValue.compare(kKEY_TRG_OVR_STGY_TRASH))
    {
      overrun = OVRN_TRASH;
    }
    else if (0 == tokenValue.compare(kKEY_TRG_OVR_STGY_RESTART))
    {
      overrun = OVRN_RESTART;
    }
    else if (0 == tokenValue.compare(kKEY_TRG_OVR_STGY_IGN))
    {
      overrun = OVRN_IGNORE;
    }
    else
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad overrun strategy value: " << tokenValue << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractOverrunStrategy")); 
    }
  }

  DEBUG_STREAM << "Overrun strategy: " << overrun << std::endl;

  return overrun;
}

// ======================================================================
// ConfigurationParser::extractDataTimeout
// ======================================================================
yat::uint32 ConfigurationParser::extractDataTimeout (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  yat::uint32 timeout = 0;

  // get timeout 
  AcquisitionKeys_it_t it = acq_config.find(kKEY_TRG_TIMEOUT);
  
  if (it == acq_config.end())
  {
    // key not found => error
    yat::OSStream oss;
    oss << "Data timeout not found in configuration - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractDataTimeout"));    
  }
  else
  {
    try
    {
      unsigned int tmo = yat::XString<unsigned int>::to_num(it->second);
      timeout = (yat::uint32)tmo;
    }
    catch (const yat::Exception& ye)
    {
      // bad int value ==> fatal error
      yat::OSStream oss;
      oss << "Bad timeout value: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractDataTimeout")); 
    }
  }

  DEBUG_STREAM << "Timeout: " << timeout << std::endl;

  return timeout;
}

// ======================================================================
// ConfigurationParser::extractSamplingInfo
// ======================================================================
SamplingInfo ConfigurationParser::extractSamplingInfo (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  SamplingInfo info;

  // get sampling source & rate (optional)
  AcquisitionKeys_it_t it = acq_config.find(kKEY_TRG_SAMPLING_SRC);
  
  if (it == acq_config.end())
  {
    // key not found => default value = INTERNAL:100000
    info.source = SRC_INTERNAL;
    info.rate = kDEFAULT_SAMPLING_RATE;
  }
  else
  {
    std::vector<std::string> valueList = splitLine(it->second, kVALUE_SEPARATOR_CL, 1, 2);

    // get type of source
    if (0 == valueList[0].compare(kKEY_TRG_SPL_SRC_INT))
    {
      info.source = SRC_INTERNAL;
    }
    else if (0 == valueList[0].compare(kKEY_TRG_SPL_SRC_EXT))
    {
      info.source = SRC_EXTERNAL;
    }
    else
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad sampling source value: " << valueList[0] << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractSamplingInfo"));
    }
    DEBUG_STREAM << "Sampling source: " << info.source << std::endl;

    // get sampling rate (only if INTERNAL)
    if (info.source == SRC_INTERNAL)
    {
      if (valueList.size() != 2)
      {
        // set default rate value
        info.rate = kDEFAULT_SAMPLING_RATE;
      }
      else
      {
        try
        {
          double rate = yat::XString<double>::to_num(valueList[1]);
          info.rate = rate;
        }
        catch (const yat::Exception& ye)
        {
          // bad double value ==> fatal error
          yat::OSStream oss;
          oss << "Bad sampling rate value: " << valueList[1] << " - check device property";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("ConfigurationParser::extractSamplingInfo")); 
        }
      }
      DEBUG_STREAM << "Sampling rate: " << info.rate << std::endl;
    }
  }

  return info;
}

// ======================================================================
// ConfigurationParser::extractTimebaseInfo
// ======================================================================
TimebaseInfo ConfigurationParser::extractTimebaseInfo(AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
   TimebaseInfo info;

  // get timebase type & value (optional)
  AcquisitionKeys_it_t it = acq_config.find(kKEY_TIMEBASE_TYPE);

  if (it == acq_config.end())
  {
    // key not found => default value = INTERNAL:40000000
    info.type = TB_INTERNAL;
    info.clock_freq = kDEFAULT_TIMEBASE_FREQ;
  }
  else
  {
    std::vector<std::string> valueList = splitLine(it->second, kVALUE_SEPARATOR_CL, 1, 2);

    // get type of timebase
    if (0 == valueList[0].compare(kKEY_TIMEBASE_TYPE_INT))
    {
      info.type = TB_INTERNAL;
    }
    else if (0 == valueList[0].compare(kKEY_TIMEBASE_TYPE_EXT))
    {
      info.type = TB_EXTERNAL;
    }
    else if (0 == valueList[0].compare(kKEY_TIMEBASE_TYPE_SSI))
    {
      info.type = TB_SSI;
    }
    else
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad timebase type: " << valueList[0] << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
          oss.str().c_str(),
          _CPTC("ConfigurationParser::extractTimebaseInfo"));
    }

    // get timebase value (only if external, if other type: use default)
    if (info.type == TB_EXTERNAL)
    {
      if (valueList.size() != 2)
      {
        // set default rate value
        info.clock_freq = kDEFAULT_TIMEBASE_FREQ;
      }
      else
      {
        try
        {
          double rate = yat::XString<double>::to_num(valueList[1]);
          info.clock_freq = rate;
        }
        catch (const yat::Exception& ye)
        {
          // bad double value ==> fatal error
          yat::OSStream oss;
          oss << "Bad timebase value: " << valueList[1] << " - check device property";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                oss.str().c_str(),
                _CPTC("ConfigurationParser::extractTimebaseInfo"));
        }
      }
    }
  }

  DEBUG_STREAM << "Timebase type: " << info.type << " - timebase value: " << info.clock_freq << std::endl;

  return info;
}

// ======================================================================
// ConfigurationParser::extractExtTrigType
// ======================================================================
E_ExtTrigType_t ConfigurationParser::extractExtTrigType (AcquisitionKeys_t acq_config)
{
  E_ExtTrigType_t type;

  // get external trigger type
  AcquisitionKeys_it_t it = acq_config.find(kKEY_TRG_EXT_TYPE);
  
  if (it == acq_config.end())
  {
    // key not found => error
    yat::OSStream oss;
    oss << "External trigger type not found - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractExtTrigType"));
  }
  else
  {
    std::string tokenValue = it->second;

    if (0 == tokenValue.compare(kKEY_TRG_EXT_ATRIG))
    {
      type = EXT_TRG_ANALOG;
    }
    else if (0 == tokenValue.compare(kKEY_TRG_EXT_DTRIG))
    {
      type = EXT_TRG_DIGITAL;
    }
    else
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad external trigger value: " << tokenValue << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractExtTrigType"));
    }
  } 

  DEBUG_STREAM << "External trigger type: " << type << std::endl;

  return type;
}


// ======================================================================
// ConfigurationParser::extractExtTrigEdge
// ======================================================================
E_ExtAtrigEdge_t ConfigurationParser::extractExtTrigEdge (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  E_ExtAtrigEdge_t edge;

  // get analog trigger edge
  AcquisitionKeys_it_t it = acq_config.find(kKEY_TRG_EXT_EDGE);
  
  if (it == acq_config.end())
  {
    // key not found => error
    yat::OSStream oss;
    oss << "External analog trigger edge not found - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractExtTrigEdge"));
  }
  else
  {
    std::string tokenValue = it->second;

    if (0 == tokenValue.compare(kKEY_TRG_EXT_EDGE_F))
    {
      edge = EXT_ATRIG_EDGE_FALLING;
    }
    else if (0 == tokenValue.compare(kKEY_TRG_EXT_EDGE_R))
    {
      edge = EXT_ATRIG_EDGE_RISING;
    }
    else
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad external trigger edge value: " << tokenValue << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractExtTrigEdge"));
    }
  }
  
  DEBUG_STREAM << "External trigger edge: " << edge << std::endl;

  return edge;
}


// ======================================================================
// ConfigurationParser::extractPostFiniteMode
// ======================================================================
E_PostTrgFiniteMode_t ConfigurationParser::extractPostFiniteMode (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  E_PostTrgFiniteMode_t mode;

  // get finite mode
  AcquisitionKeys_it_t it = acq_config.find(kKEY_TRG_EXT_POST_FINITE_MODE);
  
  if (it == acq_config.end())
  {
    // key not found => default value
    mode = TRG_POST_FINITE_MODE_BEST_EFFORT;
  }
  else
  {
    std::string tokenValue = it->second;

    if (0 == tokenValue.compare(kKEY_TRG_EXT_POST_FINITE_SAFE))
    {
      mode = TRG_POST_FINITE_MODE_SAFE;
    }
    else if (0 == tokenValue.compare(kKEY_TRG_EXT_POST_FINITE_BEST_EFFORT))
    {
      mode = TRG_POST_FINITE_MODE_BEST_EFFORT;
    }
    else
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad post or post_delayed finite mode value: " << tokenValue << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractPostFiniteMode"));
    }
  }
  
  DEBUG_STREAM << "External trigger finite mode: " << mode << std::endl;

  return mode;
}


// ======================================================================
// ConfigurationParser::extractExtATrigInfo
// ======================================================================
ExtAnalogTrigInfo ConfigurationParser::extractExtATrigInfo (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  ExtAnalogTrigInfo atrigInfo;

  // get analog source info
  AcquisitionKeys_it_t it = acq_config.find(kKEY_TRG_EXT_AT_SRC);
  
  if (it == acq_config.end())
  {
    // key not found => exception
    yat::OSStream oss;
    oss << "External analog trigger source infos not found - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractExtATrigInfo"));
  }
  else
  {
    // get analog trig source and conditions 
    std::vector<std::string> valueList = splitLine(it->second, kVALUE_SEPARATOR_CL, 3, 4);

    // get analog trigger source
    if (0 == valueList[0].compare(kKEY_TRG_EXT_AT_SRC_EXT))
    {
      atrigInfo.src = EXT_ATRG_SRC_EXT;
    }
    else if (0 == valueList[0].compare("0"))
    {
      atrigInfo.src = EXT_ATRG_CHAN0;
    }
    else if (0 == valueList[0].compare("1"))
    {
      atrigInfo.src = EXT_ATRG_CHAN1;
    }
    else if (0 == valueList[0].compare("2"))
    {
      atrigInfo.src = EXT_ATRG_CHAN2;
    }
    else if (0 == valueList[0].compare("3"))
    {
      atrigInfo.src = EXT_ATRG_CHAN3;
    }
    else
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value: " << valueList[0] << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractExtATrigInfo"));
    }

    // get condition type
    if (0 == valueList[1].compare(kKEY_TRG_EXT_AT_SRC_COND_B))
    {
      atrigInfo.conditionType = EXT_ATRG_COND_BELOW;
    }
    else if (0 == valueList[1].compare(kKEY_TRG_EXT_AT_SRC_COND_A))
    {
      atrigInfo.conditionType = EXT_ATRG_COND_ABOVE;
    }
    else if (0 == valueList[1].compare(kKEY_TRG_EXT_AT_SRC_COND_R))
    {
      atrigInfo.conditionType = EXT_ATRG_COND_REGION;
    }
    else if (0 == valueList[1].compare(kKEY_TRG_EXT_AT_SRC_COND_H))
    {
      atrigInfo.conditionType = EXT_ATRG_COND_HYST;
    }
    else
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value: " << valueList[1] << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractExtATrigInfo"));
    }

    // get 1st level condition
    try
    {
      double val = yat::XString<double>::to_num(valueList[2]);
      if (atrigInfo.conditionType == EXT_ATRG_COND_ABOVE)
      {
        // if ABOVE, the specified value is a "high" level
        atrigInfo.highLevelCond = val;
      }
      else
      {
        // in other cases, the specified value is a "low" level
        atrigInfo.highLevelCond = val;
      }
    }
    catch (const yat::Exception& ye)
    {
      // bad double value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value: " << valueList[2] << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractExtATrigInfo")); 
    }

    // get 2nd level condition (for REGION and HYSTERESIS conditions)
    bool need2ndCond = ((atrigInfo.conditionType == EXT_ATRG_COND_REGION) ||
                        (atrigInfo.conditionType == EXT_ATRG_COND_HYST));
    
    if ((need2ndCond) && !(valueList.size() == 4))
    {
      // 2nd condition needed but not defined ==> fatal error
      yat::OSStream oss;
      oss << "Bad value: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractExtATrigInfo")); 
    }
    else if (!(need2ndCond) && (valueList.size() == 4))
    {
      // value not taken into account
      WARN_STREAM << "Parameter " << valueList[3] 
        << " defined but not taken into account for this condition type" << std::endl;
    }
    else if (!(need2ndCond) && (valueList.size() == 3))
    {
      // nothing to read, it's ok
    }
    else
    {
      // 2nd condition needed and something to read
      try
      {
        double val = yat::XString<double>::to_num(valueList[3]);
        atrigInfo.highLevelCond = val;
      }
      catch (const yat::Exception& ye)
      {
        // bad double value ==> fatal error
        yat::OSStream oss;
        oss << "Bad value: " << valueList[3] << " - check device property";
        ERROR_STREAM << oss.str() << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        oss.str().c_str(), 
                        _CPTC("ConfigurationParser::extractExtATrigInfo")); 
      }
    }
  }

  return atrigInfo;
}

// ======================================================================
// ConfigurationParser::extractPostDelay
// ======================================================================
PostDelay ConfigurationParser::extractPostDelay (AcquisitionKeys_t acq_config, E_TriggerMode_t mode)
  throw (Tango::DevFailed)
{
  PostDelay delay;
  AcquisitionKeys_it_t it;

  switch (mode)
  {
  case TRG_MIDDLE:
    // get post delay for MIDDLE mode
    it = acq_config.find(kKEY_TRG_EXT_MDL_POST_TRG_NB);
    break;
  case TRG_POST_DELAYED:
    // get post delay for POST_DELAYED mode
    it = acq_config.find(kKEY_TRG_EXT_POST_DELAY);
    break;
  default:
    //- no delay for other modes => exception
    yat::OSStream oss;
    oss << "Delay not supported for this mode";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractPostDelay"));
    break;
  }
  
  if (it == acq_config.end())
  {
    // key not found => exception
    yat::OSStream oss;
    oss << "Post delay not found for mode: " << mode << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractPostDelay"));
  }
  else
  {
    // get delay unit & value
    std::vector<std::string> valueList = splitLine(it->second, kVALUE_SEPARATOR_CL, 2, 2);

    // get delay unit
    if (0 == valueList[0].compare(kKEY_TRG_EXT_DL_UNIT_C))
    {
      delay.unit = TRG_DELAY_UNIT_TICKS;
    }
    else if (0 == valueList[0].compare(kKEY_TRG_EXT_DL_UNIT_S))
    {
      delay.unit = TRG_DELAY_UNIT_SAMPLES;
    }
    else
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad post delay unit: " << valueList[0] << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractPostDelay"));
    }

    // get delay value
    try
    {
      double val = yat::XString<double>::to_num(valueList[1]);
      delay.value = val;
    }
    catch (const yat::Exception& ye)
    {
      // bad double value ==> fatal error
      yat::OSStream oss;
      oss << "Bad post delay value: " << valueList[1] << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractPostDelay")); 
    }
  }

  return delay;
}


// ======================================================================
// ConfigurationParser::extractUserDefinedProp
// ======================================================================
std::string ConfigurationParser::extractUserDefinedProp (AcquisitionKeys_t acq_config, std::string key)
  throw (Tango::DevFailed)
{
  std::string userProperty = "";

  // get user key
  AcquisitionKeys_it_t it = acq_config.find(key);
  
  if (it == acq_config.end())
  {
    // key not found => exception
    yat::OSStream oss;
    oss << "Key: " << key << " not found - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractUserDefinedProp"));
  }
  else
  {
    userProperty = it->second;
  }

  return userProperty;
}

// ======================================================================
// ConfigurationParser::extractDataScaled
// ======================================================================
bool ConfigurationParser::extractDataScaled (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  bool dataScaled = false;

  // get data scaled property
  AcquisitionKeys_it_t it = acq_config.find(kKEY_DATA_SCALED);
  
  if (it == acq_config.end())
  {
    // key not found => exception
    yat::OSStream oss;
    oss << "Data scaled property not found - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("ConfigurationParser::extractDataScaled"));
  }
  else
  {
    try
    {
      dataScaled = this->getBoolean(it->second);
    }
    catch(Tango::DevFailed & df)
    {
      // bad boolean value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value for data scaled property: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractDataScaled")); 
    }
  }

  return dataScaled;
}

// ======================================================================
// ConfigurationParser::extractDataHistorized
// ======================================================================
bool ConfigurationParser::extractDataHistorized (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  bool dataHistorized;

  // get data historized property
  AcquisitionKeys_it_t it = acq_config.find(kKEY_DATA_HISTORY);
  
  if (it == acq_config.end())
  {
    // if not defined, default historized value = false
    dataHistorized = false;
  }
  else
  {
    try
    {
      dataHistorized = this->getBoolean(it->second);
    }
    catch(Tango::DevFailed & df)
    {
      // bad boolean value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value for data historized property: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractDataHistorized")); 
    }
  }

  return dataHistorized;
}

// ======================================================================
// ConfigurationParser::extractBufferTimestamped
// ======================================================================
bool ConfigurationParser::extractBufferTimestamped (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  bool bufferTimestamped;

  // get buffer timestamped property
  AcquisitionKeys_it_t it = acq_config.find(kKEY_BUFFER_TIMESTAMP);
  
  if (it == acq_config.end())
  {
    // if not defined, default value = false
    bufferTimestamped = false;
  }
  else
  {
    try
    {
      bufferTimestamped = this->getBoolean(it->second);
    }
    catch(Tango::DevFailed & df)
    {
      // bad boolean value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value for buffer timestamped property: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractBufferTimestamped")); 
    }
  }

  return bufferTimestamped;
}

// ======================================================================
// ConfigurationParser::extractStatisticsComputed
// ======================================================================
bool ConfigurationParser::extractStatisticsComputed (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  bool statComputed;

  // get stat computed property
  AcquisitionKeys_it_t it = acq_config.find(kKEY_DATA_STATISTICS);

  if (it == acq_config.end())
  {
    // if not defined, default value = false
    statComputed = false;
  }
  else
  {
    try
    {
      statComputed = this->getBoolean(it->second);
    }
    catch(Tango::DevFailed & df)
    {
      // bad boolean value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value for statistics computed property: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractStatisticsComputed")); 
    }
  }

  return statComputed;
}

// ======================================================================
// ConfigurationParser::extractNexusDataTypes
// ======================================================================
NxDataTypes_t ConfigurationParser::extractNexusDataTypes (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  NxDataTypes_t nxTypeList;

  // get data types to store in nexus file 
  AcquisitionKeys_it_t it = acq_config.find(kKEY_DATA_NX_DATA_TYPE);

  if (it == acq_config.end())
  {
    // if not defined, default types = none
    nxTypeList.clear();
  }
  else
  {
    std::vector<std::string> typeList = splitLine(it->second, kVALUE_SEPARATOR_COMA);

    for (size_t ct = 0; ct < typeList.size(); ct++)
    {
      if (0 == typeList[ct].compare(kKEY_DATA_NX_DATA_TYPE_RAW))
      {
        nxTypeList.push_back(NX_RAW_DATA);
      }
      else if (0 == typeList[ct].compare(kKEY_DATA_NX_DATA_TYPE_SCALED))
      {
        nxTypeList.push_back(NX_SCALED_DATA);
      }
      else if (0 == typeList[ct].compare(kKEY_DATA_NX_DATA_TYPE_AV))
      {
        nxTypeList.push_back(NX_AVERAGE_DATA);
      }
      else if (0 == typeList[ct].compare(kKEY_DATA_NX_DATA_TYPE_RMS))
      {
        nxTypeList.push_back(NX_RMS);
      }
      else if (0 == typeList[ct].compare(kKEY_DATA_NX_DATA_TYPE_PEAK))
      {
        nxTypeList.push_back(NX_PEAK);
      }
      else if (0 == typeList[ct].compare(kKEY_DATA_NX_DATA_TYPE_USER))
      {
        nxTypeList.push_back(NX_USER_DATA);
      }
      else if (0 == typeList[ct].compare(kKEY_DATA_NX_DATA_TYPE_DATA_HIST))
      {
        nxTypeList.push_back(NX_DATA_HIST);
      }
      else if (0 == typeList[ct].compare(kKEY_DATA_NX_DATA_TYPE_TIMESTAMP))
      {
        nxTypeList.push_back(NX_TIMESTAMP);
      }
      else
      {
        // bad value ==> fatal error
        yat::OSStream oss;
        oss << "Bad Nexus data type value: " << typeList[ct] << " - check device property";
        ERROR_STREAM << oss.str() << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        oss.str().c_str(), 
                        _CPTC("ConfigurationParser::extractNexusDataTypes")); 
      }    
    }
  }

  return nxTypeList;
}

// ======================================================================
// ConfigurationParser::extractNexusFileName
// ======================================================================
std::string ConfigurationParser::extractNexusFileName (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  std::string nxFileName;

  // get base name for Nexus files 
  AcquisitionKeys_it_t it = acq_config.find(kKEY_DATA_NX_FILE_NAME);

  if (it == acq_config.end())
  {
    // if not defined, default name will be set at start time
    nxFileName = "";
  }
  else
  {
    nxFileName = it->second;
  }

  return nxFileName;
}

// ======================================================================
// ConfigurationParser::extractMaskComputed
// ======================================================================
bool ConfigurationParser::extractMaskComputed (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  bool maskComputed;

  // get mask computed property
  AcquisitionKeys_it_t it = acq_config.find(kKEY_DATA_MASK);

  if (it == acq_config.end())
  {
    // if not defined, default value = false
    maskComputed = false;
  }
  else
  {
    try
    {
      maskComputed = this->getBoolean(it->second);
    }
    catch(Tango::DevFailed & df)
    {
      // bad boolean value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value for mask computed property: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractMaskComputed")); 
    }
  }

  return maskComputed;
}

// ======================================================================
// ConfigurationParser::extractAutoTuning
// ======================================================================
bool ConfigurationParser::extractAutoTuning (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  bool autoTuning = false;

  // get data scaled property
  AcquisitionKeys_it_t it = acq_config.find(kKEY_AUTO_TUNING);
  
  if (it == acq_config.end())
  {
    // if not defined, default value = false
    autoTuning = false;
  }
  else
  {
    try
    {
      autoTuning = this->getBoolean(it->second);
    }
    catch(Tango::DevFailed & df)
    {
      // bad boolean value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value for auto tuning property: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractAutoTuning")); 
    }
  }

  return autoTuning;
}

// ======================================================================
// ConfigurationParser::extractNexusFileGeneration
// ======================================================================
bool ConfigurationParser::extractNexusFileGeneration (AcquisitionKeys_t acq_config)
  throw (Tango::DevFailed)
{
  bool nexusFileGeneration = false;

  // get data scaled property
  AcquisitionKeys_it_t it = acq_config.find(kKEY_NEXUS_FILE_GENERATION);
  
  if (it == acq_config.end())
  {
    // if not defined, default value = false
    nexusFileGeneration = false;
  }
  else
  {
    try
    {
      nexusFileGeneration = this->getBoolean(it->second);
    }
    catch(Tango::DevFailed & df)
    {
      // bad boolean value ==> fatal error
      yat::OSStream oss;
      oss << "Bad value for nexus file generation property: " << it->second << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                      oss.str().c_str(), 
                      _CPTC("ConfigurationParser::extractNexusFileGeneration")); 
    }
  }

  return nexusFileGeneration;
}

} // namespace aicontroller
