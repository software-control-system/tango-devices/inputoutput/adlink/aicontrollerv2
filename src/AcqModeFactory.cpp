//=============================================================================
// AcqModeFactory.cpp
//=============================================================================
// abstraction.......Acquisition mode factory for AIControllerV2 Device
// class.............AcqModeFactory
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "AcqModeFactory.h"
#include "INTERNAL_AcqMode.h"
#include "POST_AcqMode.h"
#include "PRE_AcqMode.h"
#include "MIDDLE_AcqMode.h"
#include "POST_DELAYED_AcqMode.h"
#include "INTERNAL_POSTMORTEM_AcqMode.h"

namespace aicontroller
{

// ======================================================================
// AcqModeFactory::instanciate
// ======================================================================
AcqMode * AcqModeFactory::instanciate (E_TriggerMode_t trigger_mode, Tango::DeviceImpl * host_device)
 throw (Tango::DevFailed)
{
  AcqMode * acq_mode = 0;

  switch (trigger_mode)
  {
    case TRG_INTERNAL:
      acq_mode = new INTERNAL_AcqMode(host_device);
      break;
    case TRG_POST:
      acq_mode = new POST_AcqMode(host_device);
      break;
    case TRG_PRE:
      acq_mode = new PRE_AcqMode(host_device);
      break;
    case TRG_MIDDLE:
      acq_mode = new MIDDLE_AcqMode(host_device);
      break;
    case TRG_POST_DELAYED:
      acq_mode = new POST_DELAYED_AcqMode(host_device);
      break;
    case TRG_INT_POSTMORTEM:
      acq_mode = new INTERNAL_POSTMORTEM_AcqMode(host_device);
      break;

    default:
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("invalid trigger mode specified [check device property]"),
                      _CPTC("AcqModeFactory::instanciate"));
      break;
  }
 
  return acq_mode;
}

} // namespace aicontroller


