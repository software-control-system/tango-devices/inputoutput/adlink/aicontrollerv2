//=============================================================================
// BoardType.cpp
//=============================================================================
// abstraction.......Board type implementation
// class.............BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BoardType.h"

namespace aicontroller
{

// ======================================================================
// BoardType::BoardType
// ======================================================================
BoardType::BoardType ()
{
}

// ======================================================================
// BoardType::~BoardType
// ======================================================================
BoardType::~BoardType ()
{
}

// ======================================================================
// BoardType::checkParameters
// ======================================================================
void BoardType::checkParameters(acqParamCheck acq_param) const
  throw (Tango::DevFailed)
{
  // check if sampling rate maps with board min & max sampling rates
  if ((acq_param.samplingRate > m_maxSamplingRate) || 
      (acq_param.samplingRate < m_minSamplingRate))
  {
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("sampling frequency value is OUT OF RANGE [check device property]"),
                    _CPTC("BoardType::checkParameters"));
  }

  // check if number of active channel maps with board max number of channels
  if (acq_param.activeChanNb > m_maxChannelsNb)
  {
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("too much active channels declared [check device property]"),
                    _CPTC("BoardType::checkParameters"));
  }

  //- check sampling rate & number of active channels (ONLY if using a multiplexed board)
  if (this->m_isMultiplexed)
  {
    if ( (acq_param.samplingRate * acq_param.activeChanNb) > this->m_maxSamplingRate )
    {
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Frequency is above the maximum supported value: Freq * nbActiveChannels should be less than MaxFreq. [check device property]"),
                      _CPTC("BoardType::checkParameters"));

    }
  }

}

} // namespace aicontroller


