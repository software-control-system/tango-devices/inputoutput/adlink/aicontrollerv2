//=============================================================================
// ADSAI2005_BoardType.h
//=============================================================================
// abstraction.......ADLINK SAI 2005 Board type
// class.............ADSAI2005_BoardType
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _ADSAI2005_BOARD_H_
#define _ADSAI2005_BOARD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BoardType.h"

namespace aicontroller
{


// ============================================================================
// class: ADSAI2005_BoardType
// ============================================================================
class ADSAI2005_BoardType : public BoardType
{
public:
  //- constructor
  ADSAI2005_BoardType ();

  //- destructor
  virtual ~ADSAI2005_BoardType ();
  
  //- board model name as string 
  virtual std::string board_model_name () const
  {
    return kBOARD_TYPE_ADSAI2005_STR;
  }

  //- board model as reference 
  virtual unsigned short board_model_id () const 
  {
    return adl::DAQ2005;
  }

};

} // namespace aicontroller

#endif // _ADSAI2005_BOARD_H_
