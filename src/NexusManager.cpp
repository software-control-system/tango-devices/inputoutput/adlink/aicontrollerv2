//=============================================================================
// NexusManager.cpp
//=============================================================================
// abstraction.......Nexus storage manager implementation
// class.............NexusManager
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/time/Timer.h>
#include "NexusManager.h"

namespace aicontroller
{

// ======================================================================
// NexusManager::NexusManager
// ======================================================================
NexusManager::NexusManager (Tango::DeviceImpl * host_device)
: Tango::LogAdapter(host_device),
  m_pAcqWriter(NULL),
  m_finalizeDone(true),
  m_finiteStorage(false),
  m_storageError(false)
{
#if defined (USE_NX_DS_FINALIZER)
  DEBUG_STREAM << "starting the underlying NexusDataStreamerFinalizer..." << std::endl;
  this->m_NexusDataStreamerFinalizer.start();
  DEBUG_STREAM << "NexusDataStreamerFinalizer successfully started" << std::endl;
#endif
}

// ======================================================================
// NexusManager::~NexusManager
// ======================================================================
NexusManager::~NexusManager ()
{
  try
  {
#if defined (USE_NX_DS_FINALIZER)
    //- this is not mandatory, anyway...
	DEBUG_STREAM << "stopping the underlying NexusDataStreamerFinalizer..." << std::endl;
	this->m_NexusDataStreamerFinalizer.stop();
	DEBUG_STREAM << "NexusDataStreamerFinalizer successfully stopped" << std::endl;
#endif

    // delete data buffer if previously allocated
    if (this->m_pAcqWriter)
    {
      delete this->m_pAcqWriter;
      this->m_pAcqWriter = NULL;
    }
  }
  catch (nxcpp::NexusException &n)
  {
    ERROR_STREAM << "Nexus4TangoException caught: "
      << n.to_string()
      << std::endl;
  }
  catch(...)
  {
    ERROR_STREAM << "NexusManager::~NexusManager caught [...]!" << std::endl;
  }
}

// ======================================================================
// NexusManager::initNexusAcquisition
// ======================================================================
void NexusManager::initNexusAcquisition(std::string nexus_file_path, 
                                        std::string nexus_file_name,                            
                                        yat::uint32 acquisition_size,
                                        yat::uint16 nx_number_per_file,
                                        nxItemList_t item_list)
  throw (Tango::DevFailed)
{
  try
  {
    //---- delete data buffer if previously allocated
    if (this->m_pAcqWriter )
    {
       delete this->m_pAcqWriter;
       this->m_pAcqWriter = NULL;
    }
  
    //---- create data buffer for new acquisition
    if (!this->m_pAcqWriter)
    {      
      //- finite nx generation file
      if (acquisition_size != 0)
			{
        this->m_finiteStorage = true;

        //- check that acquisition_size >= m_nxNumberPerFile
        if (acquisition_size >= nx_number_per_file)
        {
          this->m_pAcqWriter = new nxcpp::DataStreamer(nexus_file_name, acquisition_size, nx_number_per_file);
        }
        else
        {
          ERROR_STREAM << "Number of acquisition must be greater than number of nx storage per file" << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          _CPTC("Nexus file configuration error - see logs for details"), 
                          _CPTC("NexusManager::initNexusAcquisition"));
        }
      }
      else //- infinite nx generation file
      {
        this->m_finiteStorage = false;
        this->m_pAcqWriter = new nxcpp::DataStreamer(nexus_file_name, nx_number_per_file);			  
      }
    }
    else
    {
      // de-allocation pb ==> exception
      ERROR_STREAM << "Memory problem during nexus buffer de-allocation" << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                      _CPTC("Memory problem during nexus buffer de-allocation"), 
                      _CPTC("NexusManager::initNexusAcquisition"));
    }
  }
  catch(nxcpp::NexusException &n)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition -> caught Nexus4TangoException: "
      << n.to_string()
      << std::endl;
    
    if (m_pAcqWriter)
      m_pAcqWriter->Abort();
    ERROR_STREAM << "Nexus storage ABORTED." << std::endl;

    Tango::DevFailed df = this->nexusToTangoException(n);
    throw df;
  }
  catch(Tango::DevFailed & df)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("DEVICE_ERROR"),
                      _CPTC("Failed to prepare Nexus storage (caught DevFailed)!"),
                      _CPTC("NexusManager::initNexusAcquisition"));
  }
  catch(...)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                    _CPTC("Failed to prepare Nexus storage (caught [...])!"), 
                    _CPTC("NexusManager::initNexusAcquisition"));
  }

  DEBUG_STREAM << "Store Nexus files in: " << nexus_file_path << std::endl;


  //---- initialize nexus acquisition
  m_itemList = item_list;

  try
  {
    if (this->m_pAcqWriter)
    {
      this->m_pAcqWriter->Initialize(nexus_file_path, "");

      // parse the list of items to store in nexus file
      for (size_t idx = 0; idx < m_itemList.size(); idx++)
      {
        std::string itemName = m_itemList.at(idx).name;
        E_NxDataStorageType_t storageType = m_itemList.at(idx).storageType;
        int dim1 = m_itemList.at(idx).dim1;
        int dim2 = m_itemList.at(idx).dim2;
        
        switch (storageType)
        {
          case NX_DATA_TYPE_0D:
            this->m_pAcqWriter->AddDataItem0D(itemName);
            break;
          case NX_DATA_TYPE_1D:
            this->m_pAcqWriter->AddDataItem1D(itemName, dim1);
            break;
          case NX_DATA_TYPE_2D:
            this->m_pAcqWriter->AddDataItem2D(itemName, dim1, dim2);
            break;
          default:
            // not supported type => fatal error
            ERROR_STREAM << "Bad data type for nexus storage" << std::endl;
            THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                            _CPTC("Bad data type for nexus storage"), 
                            _CPTC("NexusManager::initNexusAcquisition"));
            break;
        }
      }
    }
    else
    {
      // allocation pb ==> exception
      ERROR_STREAM << "Memory problem during nexus buffer allocation" << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                      _CPTC("Memory problem during nexus buffer allocation"), 
                      _CPTC("NexusManager::initNexusAcquisition"));
    }
  }
  catch(nxcpp::NexusException &n4te)
  {    
    ERROR_STREAM << "NexusManager::initNexusAcquisition -> caught NEXUS Exception: "
      << n4te.to_string()
      << std::endl;

    Tango::DevFailed df = this->nexusToTangoException(n4te);
    throw df;
  }
  catch(Tango::DevFailed & df)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition caught DevFailed : " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("DEVICE_ERROR"),
                      _CPTC("Failed to initialize Nexus storage (caught DevFailed)!"),
                      _CPTC("NexusManager::initNexusAcquisition"));
  }
  catch(...)
  {
    ERROR_STREAM << "NexusManager::initNexusAcquisition caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                    _CPTC("Failed to initialize Nexus storage (caught [...])!"), 
                    _CPTC("NexusManager::initNexusAcquisition"));
  }	

  // set exception handler
  this->m_pAcqWriter->SetExceptionHandler(this);
  
  // set flags
  m_finalizeDone = false;
  m_storageError = false;
}

// ======================================================================
// NexusManager::finalizeNxGeneration
// ======================================================================
void NexusManager::finalizeNexusGeneration()
  throw (Tango::DevFailed)
{
  //- Ends nexus generation file.
  //- This method can be called from two ways
  //- in the same time, and in some cases can
  //- cause a CRASH !!!

  if (this->m_pAcqWriter)
  {
    if (!this->m_finalizeDone)
    {
      yat::AutoMutex<> guard(this->m_finalizeNxLock);

      try
      {
        //- if infinite Nx generation, call Stop before Finalize !!!
        if (!this->m_finiteStorage)
          this->m_pAcqWriter->Stop();

#if defined(USE_NX_DS_FINALIZER)
		DEBUG_STREAM << "passing DataStreamer to the NexusDataStreamerFinalizer" << std::endl;
		nxcpp::NexusDataStreamerFinalizer::Entry *e = new nxcpp::NexusDataStreamerFinalizer::Entry();
		e->data_streamer = this->m_pAcqWriter;
		this->m_pAcqWriter = NULL;
		this->m_NexusDataStreamerFinalizer.push(e);
#else
        //- in all cases call Finalize
		yat::Timer t;
        this->m_pAcqWriter->Finalize();  
		DEBUG_STREAM << "Finalizing nexus stream tooks "
					 << t.elapsed_msec()
					 << " msec"
					 << std::endl;
#endif
        //- now Nx file generation is done !
        this->m_finalizeDone = true;
      }
      catch(nxcpp::NexusException &n)
      {
        ERROR_STREAM << "NexusManager::finalizeNxGeneration -> Nexus4TangoException caught: "
          << n.to_string()
          << std::endl;
        
        if (m_pAcqWriter)
          m_pAcqWriter->Abort();
        
        ERROR_STREAM << "Nexus storage ABORTED." << std::endl;
        Tango::DevFailed df = this->nexusToTangoException(n);
        throw df;
      }
      catch(yat::Exception &n)
      {
        ERROR_STREAM << "NexusManager::finalizeNxGeneration -> yat::Exception caught: "
          << n.to_string()
          << std::endl;

        if (m_pAcqWriter)
          m_pAcqWriter->Abort();

        ERROR_STREAM << "Nexus storage ABORTED." << std::endl;
      }
      catch(...)
      {
        ERROR_STREAM << "NexusManager::finalizeNxGeneration -> caught [...] Exception" << std::endl;
        this->manageNexusAbort();
        ERROR_STREAM << "Nexus stotage ABORTED." << std::endl;
        THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                        _CPTC("Failed to finilize Nexus storage (caught [...])!"), 
                        _CPTC("NexusManager::finalizeNxGeneration"));
      }
    }
  }
}

// ======================================================================
// NexusManager::manageNexusAbort
// ======================================================================
void NexusManager::manageNexusAbort()
  throw (Tango::DevFailed)
{
  if (this->m_pAcqWriter)
  {
    ERROR_STREAM << "NexusManager::manageNexusAbort <- ..." << std::endl;
    
    this->m_pAcqWriter->Abort();
    
    this->m_pAcqWriter->Finalize();

    delete this->m_pAcqWriter;    
    this->m_pAcqWriter = 0;
	this->m_finalizeDone = true;

    ERROR_STREAM << "NexusManager::manageNexusAbort ->" << std::endl;
  }
}

// ======================================================================
// NexusManager::nexusToTangoException
// ======================================================================
Tango::DevFailed NexusManager::nexusToTangoException(const nxcpp::NexusException &nxte)
{
  Tango::DevErrorList error_list(nxte.errors.size());
  error_list.length(nxte.errors.size());
	
  for (unsigned int i = 0; i < nxte.errors.size(); i++)
  {
    error_list[i].reason = CORBA::string_dup(nxte.errors[i].reason.c_str());
    ERROR_STREAM << "NexusManager::nexusToTangoException -> REASON = " << error_list[i].reason << std::endl;
		
    error_list[i].desc = CORBA::string_dup(nxte.errors[i].desc.c_str());
    ERROR_STREAM << "NexusManager::nexusToTangoException -> DESC = " << error_list[i].desc << std::endl;
		
    error_list[i].origin = CORBA::string_dup(nxte.errors[i].origin.c_str());
    ERROR_STREAM << "NexusManager::nexusToTangoException -> ORIGIN = " << error_list[i].origin << std::endl;

    error_list[i].severity = Tango::ERR;
  }

  return Tango::DevFailed(error_list);
}


} // namespace aicontroller


