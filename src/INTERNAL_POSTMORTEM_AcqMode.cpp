//=============================================================================
// INTERNAL_POSTMORTEM_AcqMode.cpp
//=============================================================================
// abstraction.......INTERNAL POSTMORTEM acquisition mode implementation
// class.............INTERNAL_POSTMORTEM_AcqMode
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "INTERNAL_POSTMORTEM_AcqMode.h"

namespace aicontroller
{
//-----------------------------------------------------------------------------
//- Check nexus manager macro:
#define CHECK_NX_MANAGER \
    if (! this->m_nexusManager) \
    { \
      this->m_errorMsg = "Internal error."; \
      this->m_errorOccurred = true; \
      THROW_DEVFAILED(_CPTC("INTERNAL_ERROR"), \
                      _CPTC("request aborted - the Nexus manager isn't properly initialized"), \
                      _CPTC("INTERNAL_POSTMORTEM_AcqMode::check_nx_manager")); \
    }
//-----------------------------------------------------------------------------

// ======================================================================
// INTERNAL_POSTMORTEM_AcqMode::INTERNAL_POSTMORTEM_AcqMode
// ======================================================================
INTERNAL_POSTMORTEM_AcqMode::INTERNAL_POSTMORTEM_AcqMode (Tango::DeviceImpl * host_device)
:AcqMode(host_device)
{
  this->m_dataHistoryValidity = false;
}

// ======================================================================
// INTERNAL_POSTMORTEM_AcqMode::~INTERNAL_POSTMORTEM_AcqMode
// ======================================================================
INTERNAL_POSTMORTEM_AcqMode::~INTERNAL_POSTMORTEM_AcqMode ()
{
}

// ======================================================================
// INTERNAL_POSTMORTEM_AcqMode::extractSpecificAcqDefinition_i
// ======================================================================
void INTERNAL_POSTMORTEM_AcqMode::extractSpecificAcqDefinition_i(RawAcquitisionConfig raw_acq_config)
  throw (Tango::DevFailed)
{
  //- User data is disabled for all defined channels
  //- (not possible in POSTMORTEM acquisition mode, for optimization reasons)
  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.hasUserData)
    {
      WARN_STREAM << "User data not available in POSTMORTEM trigger mode." << std::endl;
      this->m_acqDefinition.activeChannels[chan].userDataDesc.hasUserData = false;
    }
  }
}

// ======================================================================
// INTERNAL_POSTMORTEM_AcqMode::extractSpecificDataTrtDefinition_i
// ======================================================================
void INTERNAL_POSTMORTEM_AcqMode::extractSpecificDataTrtDefinition_i(RawAcquitisionConfig raw_acq_config)
  throw (Tango::DevFailed) 
{
  //- Statistics is disabled
  //- (not possible in POSTMORTEM acquisition mode, for optimization reasons)
  if (this->m_dataTrt.hasStatistics)
  {
    WARN_STREAM << "Statistics not available in POSTMORTEM trigger mode." << std::endl;
    this->m_dataTrt.hasStatistics = false;
  }
}

// ======================================================================
// INTERNAL_POSTMORTEM_AcqMode::specificAIconfigMapper_i
// ======================================================================
void INTERNAL_POSTMORTEM_AcqMode::specificAIconfigMapper_i()
  throw (Tango::DevFailed)
{
  //- INTERNAL trigger mode specific configuration:

  //- no retrigg mode:
  this->m_daqConfig.disable_retrigger();

  //- last data buffer recovery:
  this->m_daqConfig.enable_last_buffer_recovery();
} 

// ======================================================================
// INTERNAL_POSTMORTEM_AcqMode::handle_daq_end_i
// ======================================================================
void INTERNAL_POSTMORTEM_AcqMode::handle_daq_end_i()
  throw (Tango::DevFailed)
{
  //- get data history information (if required & if any)
  if ((this->m_dataTrt.isDataScaled) &&
      (this->m_dataTrt.isDataHistorized) &&
      (this->m_acqData.dataCounter != 0))
  {
    //- get data history for all channels & update data structure

    this->m_dataHistoryValidity = true;

    DEBUG_STREAM << "INTERNAL_POSTMORTEM_AcqMode::handle_daq_end_i - start" << std::endl;
	
    const asl::ActiveAIChannels& ac = this->m_daqConfig.get_active_channels();
    unsigned int chan_id = 0; // active channel id (for data storage)

    DEBUG_STREAM << "INTERNAL_POSTMORTEM_AcqMode::handle_daq_end_i - compute history for all channels" << std::endl;
    this->computeWholeDataHistory();

    //- store the DATA HISTORY spectrum in Nexus File (if required)
    if (this->m_nxStorageStarted && 
        this->m_nxStoreDATA_HIST)
    {
      CHECK_NX_MANAGER;

      //- loop on active channels
      for (unsigned int chan_idx = 0;  chan_idx < ac.size();  chan_idx++) 
      {
        chan_id = ac[chan_idx].id;

        // check if dataset enabled
        if (this->m_acquisitionParam.datasetFlags[chan_id])
        {
          try
          {
            this->m_nexusManager->pushNexusData(m_datasetNames[chan_id][kKEY_DATA_NX_DATA_TYPE_DATA_HIST],
              this->m_dynAcqData.dataHistory[chan_id]);
          }
          catch (Tango::DevFailed & df)
          {
            this->m_errorMsg = "Nexus storage error.";
            this->m_errorOccurred = true;

            ERROR_STREAM << "INTERNAL_POSTMORTEM_AcqMode::handle_daq_end_i -> pushNexusData caught DevFailed: " << df << std::endl;
            this->m_nexusManager->manageNexusAbort();
            return;
          }
          catch (...)
          {
            this->m_errorMsg = "Nexus storage error.";
            this->m_errorOccurred = true;

            ERROR_STREAM << "INTERNAL_POSTMORTEM_AcqMode::handle_daq_end_i -> pushNexusData caught [...] Exception" << std::endl;
            this->m_nexusManager->manageNexusAbort();
            return;
          }
        }
      } // end of loop on active channels
    }

    //- get data history last valid index
    //- Note that ASL provides the last valid index of the entire history buffer.
    this->m_acqData.historyLastIndex = this->last_sample_index_in_history_buffer(); 

    // must divide by the number of active channels
    unsigned int nbac = ac.size();

    if (nbac == 0)
    {
      ERROR_STREAM << "Number of active channels is null!" << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                      _CPTC("Error while trying to get history last valid index - see logs for details"), 
                      _CPTC("INTERNAL_POSTMORTEM_AcqMode::handle_daq_end_i"));
    }

    this->m_acqData.historyLastIndex /= nbac;
  }
}

// ======================================================================
// INTERNAL_POSTMORTEM_AcqMode::handle_input_i
// ======================================================================
void INTERNAL_POSTMORTEM_AcqMode::handle_input_i(asl::AIRawData* raw_data)
  throw (Tango::DevFailed)
{
  yat::AutoMutex<> guard(this->m_dataLock);

  this->m_dataHistoryValidity = false;

  //- Default behavior = INFINITE trigger mode

  //- In infinite trigger mode, 1 trigger => 1 handle_input() received => 1 data buffer

  //- Do nothing because raw & scaled data not available in this mode
  //- for optimization reasons

  //- increase data counter
  this->m_acqData.dataCounter++;
}

// ======================================================================
// INTERNAL_POSTMORTEM_AcqMode::define_specific_attributes_i
// ======================================================================
void INTERNAL_POSTMORTEM_AcqMode::define_specific_attributes_i(std::vector<yat4tango::DynamicAttributeInfo>& dynAttrList)
{
  //----->>>>>>>>>>  Add dynamic attributes for each active channel  <<<<<<<<<<-----
  //- Add only history related attributes for optimization reasons
  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    std::string chanLabel = this->m_acqDefinition.activeChannels[chan].label;
    std::string chanDescription = this->m_acqDefinition.activeChannels[chan].description;

    if (this->m_dataTrt.isDataScaled)
    {
      if (this->m_dataTrt.isDataHistorized)
      {
        //----------------------
        //- scaled data history:
        //----------------------
        yat4tango::DynamicAttributeInfo dai;

        //- attribute definition:
        dai.dev = this->m_hostDev;

        dai.tai.name = aicontroller::kATTR_NAME_TAG_DATA_HIST + chanLabel;
        dai.tai.label = dai.tai.name; 
        dai.tai.data_format = Tango::SPECTRUM;
        dai.tai.data_type = Tango::DEV_DOUBLE;
        dai.tai.disp_level = Tango::OPERATOR;
        dai.tai.writable = Tango::READ;
        dai.tai.description = chanDescription;
		dai.tai.max_dim_x = LONG_MAX;

        //- attribute properties:
        dai.tai.unit = "V";
        dai.tai.standard_unit = "V";
        dai.tai.display_unit = "V";
        dai.tai.format = "%6.2e";

        dai.cdb = true;

        //- read callback
        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<INTERNAL_POSTMORTEM_AcqMode&>(*this), 
                                                                       &AcqMode::read_callback_data_history);

        //- add attribute description in list
        dynAttrList.push_back(dai);
      }
    }
  } // end of active channels parsing


  // Add data history last index (if history enabled)
  if (this->m_dataTrt.isDataHistorized)
  {
    yat4tango::DynamicAttributeInfo dai1;

    //- attribute definition:
    dai1.dev = this->m_hostDev;

    dai1.tai.name = "dataHistoryLastIndex";
    dai1.tai.label = "data history last valid index"; 
    dai1.tai.data_format = Tango::SCALAR;
    dai1.tai.data_type = Tango::DEV_ULONG;
    dai1.tai.disp_level = Tango::EXPERT;
    dai1.tai.writable = Tango::READ;
    dai1.tai.description = "Index of the last acquired value in the data history buffer.";

    //- attribute properties:
    dai1.tai.unit = " ";
    dai1.tai.standard_unit = " ";
    dai1.tai.display_unit = " ";
    dai1.tai.format = "%5d";

    dai1.cdb = true;

    //- read callback
    dai1.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<INTERNAL_POSTMORTEM_AcqMode&>(*this), 
                                                                    &INTERNAL_POSTMORTEM_AcqMode::read_callback_history_last_index);

    //- add attribute description in list
    dynAttrList.push_back(dai1);
  }
}

//+------------------------------------------------------------------
//
//  method:  INTERNAL_POSTMORTEM_AcqMode::read_callback_history_last_index
//
//  description:    read callback function for history last index.
//
//+------------------------------------------------------------------
void INTERNAL_POSTMORTEM_AcqMode::read_callback_history_last_index(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  // set attribute value
	static Tango::DevULong __history_index__;
  __history_index__ = static_cast<Tango::DevULong>(INTERNAL_POSTMORTEM_AcqMode::m_acqData.historyLastIndex);
  cbd.tga->set_value(&__history_index__);
}

// ======================================================================
// INTERNAL_POSTMORTEM_AcqMode::getDataHistoryValidity
// ======================================================================
bool INTERNAL_POSTMORTEM_AcqMode::getDataHistoryValidity()
{
  return this->m_dataHistoryValidity;
}


} // namespace aicontroller


