//=============================================================================
// AcqMode.cpp
//=============================================================================
// abstraction.......Acquisition mode implementation
// class.............AcqMode
// original author...S. MINOLLI - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "AcqMode.h"
#include <yat/utils/XString.h>
#include <time.h>
#include <yat/utils/StringTokenizer.h>
#include <yat/utils/String.h>
#include <yat/time/Time.h>

namespace aicontroller
{

//-----------------------------------------------------------------------------
//- Check nexus manager macro:
#define CHECK_NX_MANAGER \
    if (! this->m_nexusManager) \
    { \
      this->m_errorMsg = "Internal error."; \
      this->m_errorOccurred = true; \
      THROW_DEVFAILED(_CPTC("INTERNAL_ERROR"), \
                      _CPTC("request aborted - the Nexus manager isn't properly initialized"), \
                      _CPTC("AcqMode::check_nx_manager")); \
    }

//- Macro for user data parameters type mapping:
#ifdef __TANGO_EVENTS__
#define EVENT_ATTR_TYPE(TYPEID, YAT_TYPE)     \
  case TYPEID:                                  \
    {                                           \
      YAT_TYPE value;                           \
      *(myevent->attr_value) >> value;          \
      double_value = static_cast<double>(value);       \
    }                                                 \
    break;
#endif
//-----------------------------------------------------------------------------

// ======================================================================
// AcqMode::AcqMode
// ======================================================================
AcqMode::AcqMode (Tango::DeviceImpl * host_device)
:Tango::LogAdapter(host_device),
 m_nexusManager(NULL),
 m_hostDev(host_device),
 m_boardImpl(NULL),
 m_dynAttrManager(NULL),
 m_boardId(0),
 m_scaledData(NULL),
 m_userData(NULL),
 m_daqEndEvtReceived(false),
 m_stopDoneFromHandleInput(false),
 m_nxStorageStarted(false),
 m_nxSpoolName(""),
 m_nxStoreRAW(false),
 m_nxStoreSCALED(false),
 m_nxStoreAVERAGE(false),
 m_nxStoreRMS(false),
 m_nxStorePEAK(false),
 m_nxStoreDATA_HIST(false),
 m_nxStoreUSER_DATA(false),
 m_nxStoreTIMESTAMP(false),
 m_handleInputCpt(0),
 m_errorMsg(""),
 m_errorOccurred(false),
 m_modeInitializing(false),
 m_firstTimestamp(0.0)
{
}

// ======================================================================
// AcqMode::~AcqMode
// ======================================================================
AcqMode::~AcqMode ()
{
#ifdef __TANGO_EVENTS__
  // unsubscribe to Tango events
  UserDataDevices_it_t it;
  for (it = this->m_evtProxies.begin();
       it != this->m_evtProxies.end();
       ++it)
  {
    if (it->first)
    {
      (it->first)->unsubscribe_event(it->second);
      delete it->first;
    }
  }
#else
  // stop polling & delete user data monitored device
  UserDataDevices_it_t it;
  for (it = this->m_udDeviceTaskList.begin();
       it != this->m_udDeviceTaskList.end();
       ++it)
  {
    if (it->second)
    {
      (it->second)->quit();
    }
  }
#endif

  //- delete scaled data if any:
  if (this->m_scaledData)
  {
    delete this->m_scaledData;
    this->m_scaledData = NULL;
  }

  //- delete user data if any:
  if (this->m_userData)
  {
    delete this->m_userData;
    this->m_userData = NULL;
  }

  // delete nexus manager if exists
  if (this->m_nexusManager)
  {
    delete this->m_nexusManager;
    this->m_nexusManager = NULL;
  }

  // remove all dynamic attributes
  try
  {
    if (this->m_dynAttrManager)
    {
      this->m_dynAttrManager->remove_attributes();
    }
  }
  catch (...)
  {
    //- ignore any error
  }

  // remove dynamic attribute manager
  if (this->m_dynAttrManager)
  {
    delete m_dynAttrManager;
    m_dynAttrManager = NULL;
  }

  // release data buffers
  this->deleteDataBuffers();

  // clear Nexus dataset names
  m_datasetNames.clear();
}

// ======================================================================
// AcqMode::initDaqBoard
// ======================================================================
void AcqMode::initDaqBoard (unsigned short board_id, BoardType* board_impl)
  throw (Tango::DevFailed)
{
  // set init flag
  this->m_modeInitializing = true;

  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  if (!board_impl)
  {
    this->m_errorMsg = "Board implementation not properly initialized.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "Board implementation not properly initialized" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to initialize DAQ board"),
                    _CPTC("AcqMode::initDaqBoard"));
  }

  // set local members
  m_boardImpl = board_impl;
  m_boardId = board_id;

  //- initialize DAQ board
  try
  {
    asl::ContinuousAI::init(m_boardImpl->board_model_id(), board_id);
  }
  catch (asl::DAQException& daqe)
  {
    Tango::DevFailed df = this->daqToTangoException(daqe);
    this->m_errorMsg = "DAQ initialization failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::initDaqBoard DevFAILED: \n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to initialize DAQ board (caught DevFailed)!"),
                      _CPTC("AcqMode::initDaqBoard"));
  }
  catch (...)
  {
    this->m_errorMsg = "DAQ initialization failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::initDaqBoard caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to initialize DAQ board (caught [...])!"),
                    _CPTC("AcqMode::initDaqBoard"));
  }
}

// ======================================================================
// AcqMode::configureAcquisition
// ======================================================================
void AcqMode::configureAcquisition(RawAcquitisionConfig raw_acq_config, 
                                   AcqBufferConfiguration acq_buffer_config,
                                   AcquisitionParameters current_acq_param)
    throw (Tango::DevFailed)
{
  // initialize local members
  this->m_bufferConfig = acq_buffer_config;
  this->m_acquisitionParam = current_acq_param;

  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  try
  {
    // fill acquisition definition structure from raw config 
    // (set this->m_acqDefinition)
    this->extractAcquisitionDefinition(raw_acq_config);

    // fill data treatment structure from raw config
    // (set this->m_dataTrt)
    this->extractDataTrtDefinition(raw_acq_config);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_errorMsg = "Configuration error.";
    this->m_errorOccurred = true;

    ERROR_STREAM << "AcqMode::configureAcquisition -> caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to configure acquisition (caught DevFailed)!"),
                      _CPTC("AcqMode::configureAcquisition"));
  }
  catch (...)
  {
    this->m_errorMsg = "Configuration error.";
    this->m_errorOccurred = true;

    ERROR_STREAM << "AcqMode::configureAcquisition -> caught [...] Exception" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to configure acquisition (caught [...])!"),
                    _CPTC("AcqMode::configureAcquisition"));
  }

  // create nexus manager if any data to store
  if (this->initNexusTypes())
  {
    m_nexusManager = new aicontroller::NexusManager(m_hostDev);

    if (!m_nexusManager)
    {
      this->m_errorMsg = "Internal error. Problem occured during Nexus manager allocation.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "Problem occured during Nexus manager allocation" << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                      _CPTC("Problem during Nexus manager allocation"), 
                      _CPTC("AcqMode::configureAcquisition"));
    }
  }
  else
  {
    WARN_STREAM << "Nexus storage not available: no data type set in Device property" << std::endl;
  }

  // map AI configuration to ContinuousAIConfig
  this->continuousAIconfigMapper();
  

  //- Instanciate dynamic attribute manager
  this->m_dynAttrManager = new yat4tango::DynamicAttributeManager(this->m_hostDev);

  if (!this->m_dynAttrManager)
  {
    this->m_errorMsg = "Acquisition configuration error.";
    this->m_errorOccurred = true;

    ERROR_STREAM << "AcqMode::configureAcquisition --> error while creating dynamic attribute manager" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to create dynamic attribute manager"),
                    _CPTC("AcqMode::configureAcquisition"));
  }

  //- Create dynamic attributes according to acquisition configuration
  try
  {
    std::vector<yat4tango::DynamicAttributeInfo> attrDescList;

    attrDescList = this->define_acquisition_attributes();
    this->m_dynAttrManager->add_attributes(attrDescList);

    DEBUG_STREAM << "AcqMode::configureAcquisition -- dynamic attributes added" << std::endl;

    // initialize specific attributes (including memorized attributes)
    this->initSpecificAttributes_i();

    // initialize data buffers according to number of active channels
    this->initDataBuffers();
  }
  catch(Tango::DevFailed& df)
  {
    this->m_errorMsg = "Acquisition configuration error.";
    this->m_errorOccurred = true;

    ERROR_STREAM << "AcqMode::configureAcquisition -> error while creating dynamic attributes - caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to create dynamic attributes (caught DevFailed)!"),
                      _CPTC("AcqMode::configureAcquisition"));
  }
  catch(...)
  {
    this->m_errorMsg = "Acquisition configuration error.";
    this->m_errorOccurred = true;

    ERROR_STREAM << "AcqMode::configureAcquisition --> error while creating dynamic attributes - caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to create dynamic attributes (caught [...])!"),
                    _CPTC("AcqMode::configureAcquisition"));
  }

  //- Initialize user data device proxies (if needed)
  if (this->needUserDataProxy())
  {
    try
    {
#ifdef __TANGO_EVENTS__
      //- Subscribe Tango events
      this->defineUserDataEvents();
#else
      //- Create monitored devices
      this->defineUserDataProxies();
#endif
    }
    catch(Tango::DevFailed& df)
    {
      this->m_errorMsg = "Acquisition configuration error.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "AcqMode::configureAcquisition -> error while initializing user data proxies - caught DevFailed: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("CONFIGURATION_ERROR"),
                        _CPTC("Failed to initialize user data proxies (caught DevFailed)!"),
                        _CPTC("AcqMode::configureAcquisition"));
    }
    catch(...)
    {
      this->m_errorMsg = "Acquisition configuration error.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "AcqMode::configureAcquisition --> error while initializing user data proxies - caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to initializing user data proxies (caught [...])!"),
                      _CPTC("AcqMode::configureAcquisition"));
    }
  }

  // call ContinuousAI::configure()
  try
  {
    asl::ContinuousAI::configure(this->m_daqConfig);
    DEBUG_STREAM << "AcqMode::configureAcquisition -- AI board configured" << std::endl;
  }
  catch (asl::DAQException& daqe)
  {
    this->m_errorMsg = "DAQ configuration failed.";
    this->m_errorOccurred = true;
    Tango::DevFailed df = this->daqToTangoException(daqe);
    ERROR_STREAM << "AcqMode::configureAcquisition DevFAILED :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to configure DAQ board (caught DevFailed)!"),
                      _CPTC("AcqMode::configureAcquisition"));
  }
  catch (...)
  {
    this->m_errorMsg = "DAQ configuration failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::configureAcquisition caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to configure DAQ board (caught [...])!"),
                    _CPTC("AcqMode::configureAcquisition"));
  }

  // set init flag
  this->m_modeInitializing = false;
}

// ======================================================================
// AcqMode::startAcquisition
// ======================================================================
void AcqMode::startAcquisition()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "AcqMode::startAcquisition -->" << std::endl;

  {
    yat::AutoMutex<> guard(this->m_dataLock);

    // reinit error management
    this->m_errorMsg = "";
    this->m_errorOccurred = false;

    //- reset counters
    this->m_acqData.dataCounter = 0;
    this->m_acqData.errorCounter = 0;
    this->m_acqData.overrunCounter = 0;
    this->m_acqData.timeoutCounter = 0;

    this->m_daqEndEvtReceived = false;
    this->m_stopDoneFromHandleInput = false;
    this->m_acqData.extTriggerFlag = false;
    this->m_acqData.historyLastIndex = 0;

    this->m_acqData.bufferTimestamp_s = 0.0;
    this->m_acqData.relBuffTimestamp_s = 0.0;
	this->m_firstTimestamp = 0.0;
  }

  //- reset acquisition buffers:
  try
  {
    // release data buffers
    this->deleteDataBuffers();

    this->initDataBuffers();

    //- delete scaled data if any:
    if (this->m_scaledData)
    {
      delete this->m_scaledData;
      this->m_scaledData = NULL;
    }

    //- delete user data if any:
    if (this->m_userData)
    {
      delete this->m_userData;
      this->m_userData = NULL;
    }
  }
  catch(...)
  {
    this->m_errorMsg = "Internal error. Failed to reset acquisition buffers.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::startAcquisition caught [...]"<< std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to reset acquisition buffers (caught [...])!"),
                    _CPTC("AcqMode::startAcquisition"));
  }
     
  {
    yat::AutoMutex<> guard(this->m_dataLock);

    //- initialize Nexus acquisition if nexus storage enabled
    //- and if data types to store
    if (this->m_nexusManager && this->m_acquisitionParam.nexusFileGeneration)
    {
      try
      {
        //- configure nexus storage:

        // Base name for Nexus files
        std::string nexusFileName;

        if (this->m_dataTrt.nxFileName.empty())
        {
          //- default nexus file name: built with the Device name & the board number to have an unique Nx file name
          std::string devName = this->m_hostDev->get_name();
          //- replace "/" by "_" in Device name
          std::string::size_type idx;
          while ( (idx = devName.find("/")) != std::string::npos )
          {
            devName.replace(idx, 1, "_");
          }

          // default value
          nexusFileName = devName + "_" + yat::XString<size_t>::to_string(this->m_boardImpl->board_model_id()) + "_board_" + yat::XString<size_t>::to_string(this->m_boardId);
        }
        else
        {
          nexusFileName = this->m_dataTrt.nxFileName;
        }

        nxItemList_t itemList = this->getNxItemList();

        this->m_nexusManager->initNexusAcquisition(
                 this->m_acquisitionParam.nexusTargetPath, 
                 nexusFileName,
                 this->getReTrigCnt(),
                 this->m_acquisitionParam.nexusNbPerFile,
                 itemList);

        this->m_nxStorageStarted = true; 
      }
      catch (Tango::DevFailed & df)
      {
        this->m_errorMsg = "Nexus initialization failed.";
        this->m_errorOccurred = true;
        ERROR_STREAM << "AcqMode::startAcquisition caught DevFailed : " << df << std::endl;
        RETHROW_DEVFAILED(df,
                          _CPTC("COMMAND_FAILED"),
                          _CPTC("Failed to initialize nexus acquisition (caught DevFailed)!"),
                          _CPTC("AcqMode::startAcquisition"));
      }
      catch (...)
      {
        this->m_errorMsg = "Nexus initialization failed.";
        this->m_errorOccurred = true;
        ERROR_STREAM << "AcqMode::startAcquisition caught [...]" << std::endl;
        THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                        _CPTC("Failed to initialize nexus acquisition (caught [...])!"),
                        _CPTC("AcqMode::startAcquisition"));
      }
    }
  }  

  //- set gain & offsets values for each channel
  if (this->m_dataTrt.isDataScaled)
  {
    for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
    {
      if (this->m_acqDefinition.activeChannels[chan].userDataDesc.hasUserData)
      {
#if !defined __TANGO_EVENTS__
        //- update gain & offsets values (if required)
        this->getUserDataGain(chan);
        this->getUserDataOffset1(chan);
        this->getUserDataOffset2(chan);
#endif

        // check gain value != 0
        if (this->m_acqDefinition.activeChannels[chan].userDataDesc.gain == 0.0)
        {
          this->m_errorMsg = "Start acquisition failed: user data gain is null!";
          this->m_errorOccurred = true;

          // stop nexus (if previously launched)
          if (this->m_nxStorageStarted)
            this->m_nexusManager->manageNexusAbort();

          // consider that gain is null => error
          ERROR_STREAM << "Start acquisition failed: user data gain fon chan: " << chan << " is null!" << endl;
          THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                          _CPTC("Start acquisition failed: user data gain is null!"),
                          _CPTC("AcqMode::startAcquisition"));    
        }

        //- set user data gain, offset1 & offset2 for each channel
        this->m_daqConfig.set_user_data_parameters(
                this->m_acqDefinition.activeChannels[chan].number,
                this->m_acqDefinition.activeChannels[chan].userDataDesc.gain,
                this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1,
                this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2);

        //- enable user data
        this->m_daqConfig.enable_user_data(this->m_acqDefinition.activeChannels[chan].number);
      }
      else
      {
        //- disable user data
        this->m_daqConfig.disable_user_data(this->m_acqDefinition.activeChannels[chan].number);
      }
    }
  }
  else
  {
    //- disable user data for all channels
    this->m_daqConfig.disable_user_data();
  }


  //- set data mask (if required)
  if ((this->m_dataTrt.isDataScaled) && 
      (this->m_dataTrt.hasMask))
  {
    //- check data mask length with data buffer length
    if (this->m_daqConfig.get_buffer_depth() != this->m_dataTrt.boolDataMask.size())
    {
      this->m_errorMsg = "Start acquisition failed: data mask length differs from data buffer length. Check data mask length!";
      this->m_errorOccurred = true;

      // stop nexus (if previously launched)
      if (this->m_nxStorageStarted)
        this->m_nexusManager->manageNexusAbort();

      ERROR_STREAM << "Start acquisition failed: data mask length differs from data buffer length. Check data mask length!" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Start acquisition failed: data mask length differs from data buffer length."),
                      _CPTC("AcqMode::startAcquisition"));
    }

    //- enable data mask
    this->m_daqConfig.enable_data_mask();

    //- set data mask in ContinuousAIConfig
    this->m_daqConfig.set_data_mask(this->m_dataTrt.boolDataMask);
  }
  else
  {
    //- disable data mask
    this->m_daqConfig.disable_data_mask();
  }


  // call ContinuousAI::configure()
  try
  {
    asl::ContinuousAI::configure(this->m_daqConfig);
  }
  catch (asl::DAQException& daqe)
  {
    this->m_errorMsg = "Start acquisition failed: configure acquisition caught exception.";
    this->m_errorOccurred = true;

    // stop nexus (if previously launched)
    if (this->m_nxStorageStarted)
      this->m_nexusManager->manageNexusAbort();

    Tango::DevFailed df = this->daqToTangoException(daqe);
    ERROR_STREAM << "AcqMode::startAcquisition DevFAILED :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to configure DAQ board (caught DevFailed)!"),
                      _CPTC("AcqMode::startAcquisition"));
  }
  catch (...)
  {
    this->m_errorMsg = "Start acquisition failed: configure acquisition caught exception";
    this->m_errorOccurred = true;
	
    // stop nexus (if previously launched)
    if (this->m_nxStorageStarted)
      this->m_nexusManager->manageNexusAbort();
	
    ERROR_STREAM << "AcqMode::startAcquisition caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to configure DAQ board (caught [...])!"),
                    _CPTC("AcqMode::startAcquisition"));
  }


  //- start acquisition
  try
  {
    //- call mother class start method (may throw asl::DAQException)
    this->asl::ContinuousAI::start();

    //- reset handle input counters
    this->m_handleInputCpt = 0;
    this->m_handleInputTimer.restart();
    this->m_acqData.dataNotifFrequency = 0.0;

    INFO_STREAM << "AcqMode::startAcquisition : DONE" << std::endl;
  }
  catch (asl::DAQException& daqe)
  {
    this->m_errorMsg = "Start acquisition failed.";
    this->m_errorOccurred = true;
    Tango::DevFailed df = this->daqToTangoException(daqe);

    // stop nexus (if previously launched)
    if (this->m_nxStorageStarted)
      this->m_nexusManager->manageNexusAbort();
	
    ERROR_STREAM << "AcqMode::start DAQException :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to start the acquisition (caught DAQ exception)!"),
                      _CPTC("AcqMode::startAcquisition"));
  }
  catch (Tango::DevFailed & df)
  {
    this->m_errorMsg = "Start acquisition failed.";
    this->m_errorOccurred = true;

    // stop nexus (if previously launched)
    if (this->m_nxStorageStarted)
      this->m_nexusManager->manageNexusAbort();
	
    ERROR_STREAM << "AcqMode::startAcquisition caught DevFailed : " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to start the acquisition (caught DevFailed)!"),
                      _CPTC("AcqMode::startAcquisition"));
  }
  catch (...)
  {
    this->m_errorMsg = "Start acquisition failed.";
    this->m_errorOccurred = true;

    // stop nexus (if previously launched)
    if (this->m_nxStorageStarted)
      this->m_nexusManager->manageNexusAbort();
	
    ERROR_STREAM << "AcqMode::startAcquisition caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to start the acquisition (caught [...])!"),
                    _CPTC("AcqMode::startAcquisition"));
  }
}

// ======================================================================
// AcqMode::stopAcquisition
// ======================================================================
void AcqMode::stopAcquisition()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "AcqMode::stopAcquisition -->" << std::endl;

  try
  {
    //- stop DAQ
    this->asl::ContinuousAI::stop();

    //- nexus storage finalization done in handle_daq_end
    
    // freeze average history if any
    if (this->m_dataTrt.isDataScaled && this->m_dataTrt.hasStatistics)
    {
      ChannelsAverageHistorizedData_it_t it;
      for (it = this->m_dynAcqData.averageDataHistory.begin(); 
           it != this->m_dynAcqData.averageDataHistory.end();
           ++it)
      {
        it->second->freeze();
      }
    }

    // freeze timestamp histories if any
    if (this->m_dataTrt.timestamped)
    {
      this->m_dynAcqData.buffTimestamps->freeze();
      this->m_dynAcqData.relativeBuffTimes->freeze();
    }

    INFO_STREAM << "AcqMode::stopAcquisition : DONE." << std::endl;
  }
  catch (asl::DAQException& daqe)
  {
    this->m_errorMsg = "Stop acquisition failed.";
    this->m_errorOccurred = true;
    Tango::DevFailed df = this->daqToTangoException(daqe);
    ERROR_STREAM << "AcqMode::stopAcquisition DAQException :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to stop the current acquisition (caught DAQ exception)!"),
                      _CPTC("AcqMode::stopAcquisition"));
  }
  catch(Tango::DevFailed& df)
  {
    this->m_errorMsg = "Stop acquisition failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::stopAcquisition caught DevFailed :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to stop the current acquisition (caught DevFailed)!"),
                      _CPTC("AcqMode::stopAcquisition"));
  }
  catch(...)
  {
    this->m_errorMsg = "Stop acquisition failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::stopAcquisition caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to stop the current acquisition (caught [...])!"),
                    _CPTC("AcqMode::stopAcquisition"));
  }
}

// ======================================================================
// AcqMode::abortAcquisition
// ======================================================================
void AcqMode::abortAcquisition()
  throw (Tango::DevFailed)
{
  try
  {
    //- nexus storage finalization done in handle_daq_end

    //- stop DAQ
    this->asl::ContinuousAI::abort();
  }
  catch (asl::DAQException& daqe)
  {
    this->m_errorMsg = "Abort acquisition failed.";
    this->m_errorOccurred = true;
    Tango::DevFailed df = this->daqToTangoException(daqe);
    ERROR_STREAM << "AcqMode::abortAcquisition DAQException :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to abort the current acquisition (caught DAQ exception)!"),
                      _CPTC("AcqMode::abortAcquisition"));
  }
  catch(Tango::DevFailed& df)
  {
    this->m_errorMsg = "Abort acquisition failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::abortAcquisition caught DevFailed :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("COMMAND_FAILED"),
                      _CPTC("Failed to abort the current acquisition (caught DevFailed)!"),
                      _CPTC("AcqMode::abortAcquisition"));
  }
  catch(...)
  {
    this->m_errorMsg = "Abort acquisition failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::abortAcquisition caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to abort the current acquisition (caught [...])!"),
                    _CPTC("AcqMode::abortAcquisition"));
  }
}

// ======================================================================
// AcqMode::setSamplingRate
// ======================================================================
void AcqMode::setSamplingRate(double rate)
  throw (Tango::DevFailed)
{
SYSTEMTIME start, write, temp1, temp2, temp3, temp4;
GetSystemTime(&start); 
DEBUG_STREAM << "AcqMode::setSamplingRate() entering... "<< endl;

  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  // set new sampling rate in DAQ configuration

  // check if sampling rate is in board range
  BoardType::acqParamCheck boardParam;
  boardParam.activeChanNb = (yat::uint16) this->m_daqConfig.num_active_channels();
  boardParam.samplingRate = rate;

  try
  {
    this->m_boardImpl->checkParameters(boardParam);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_errorMsg = "Set sampling rate failed.";
    this->m_errorOccurred = true;
    // Rate out of range => fatal error
    ERROR_STREAM << "AcqMode::setSamplingRate DevFAILED :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                    _CPTC("CONFIGURATION_ERROR"), 
                    _CPTC("Configuration out of range for this board type"), 
                    _CPTC("AcqMode::setSamplingRate")); 
  }

  // set new buffer size = integration time * sampling rate
  unsigned long depth = (unsigned long) (this->m_acquisitionParam.integrationTime * kMILLISECONDS_TO_SECONDS * rate);

  // check that depth is not null before setting new sampling rate value
  if (depth == 0)
  {
    this->m_errorMsg = "Set sampling rate failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::setSamplingRate - new buffer depth is null!" << std::endl;
    THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"),
      _CPTC("Bad sampling rate value: new buffer depth is null!"),
      _CPTC("AcqMode::setSamplingRate"));
  }

GetSystemTime(&temp1);
DEBUG_STREAM << "INSTRU - AcqMode::setSamplingRate - computing new buffer depth took " << 1e3 * (temp1.wSecond - start.wSecond) + (temp1.wMilliseconds - start.wMilliseconds) << " ms." << std::endl; 
  
  this->m_daqConfig.set_sampling_rate(rate);
  this->m_daqConfig.set_buffer_depth(depth);

  // re-allocate memory for raw & scaled data buffers
  this->unsetDataBufferMem();
  this->setDataBufferMem(this->m_daqConfig.get_buffer_depth());

GetSystemTime(&temp2);
DEBUG_STREAM << "INSTRU - AcqMode::setSamplingRate - configuring local buffers took " << 1e3 * (temp2.wSecond - temp1.wSecond) + (temp2.wMilliseconds - temp1.wMilliseconds) << " ms." << std::endl; 
  
  // call ContinuousAI::configure()
  try
  {
    asl::ContinuousAI::configure(this->m_daqConfig);
  }
  catch (asl::DAQException& daqe)
  {
    this->m_errorMsg = "Set sampling rate failed.";
    this->m_errorOccurred = true;
    Tango::DevFailed df = this->daqToTangoException(daqe);
    ERROR_STREAM << "AcqMode::setSamplingRate DevFAILED :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to configure DAQ board (caught DevFailed)!"),
                      _CPTC("AcqMode::setSamplingRate"));
  }
  catch (...)
  {
    this->m_errorMsg = "Set sampling rate failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::setSamplingRate caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to configure DAQ board (caught [...])!"),
                    _CPTC("AcqMode::setSamplingRate"));
  }

GetSystemTime(&temp3);
DEBUG_STREAM << "INSTRU - AcqMode::setSamplingRate - configuring ASL buffers took " << 1e3 * (temp3.wSecond - temp2.wSecond) + (temp3.wMilliseconds - temp2.wMilliseconds) << " ms." << std::endl; 
  
  // set my local current acquisition data
  this->m_acquisitionParam.frequency = rate;

  // force update of data history length with new sampling rate value
  try
  {
    this->setDataHistoryDepth(this->m_dataTrt.dataHistoryBufferDepth);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_errorMsg = "Update of data history length failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::setSamplingRate DevFAILED :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                    _CPTC("CONFIGURATION_ERROR"), 
                    _CPTC("Update of data history length failed"), 
                    _CPTC("AcqMode::setSamplingRate")); 
  }

  // reapply data mask (if enabled) 'cause 
  // data buffer depth could have changed
  if (this->m_dataTrt.hasMask)
  {
DEBUG_STREAM << "AcqMode::setSamplingRate() : configure data mask " << std::endl;
    this->setBoolDataMask(this->m_dataTrt.timeSlotMask);
  }
  
GetSystemTime(&temp4);
DEBUG_STREAM << "INSTRU - AcqMode::setSamplingRate - setting data history & mask took " << 1e3 * (temp4.wSecond - temp3.wSecond) + (temp4.wMilliseconds - temp3.wMilliseconds) << " ms." << std::endl; 

GetSystemTime(&write);
DEBUG_STREAM << "INSTRU - AcqMode::setSamplingRate() function took " << 1e3 * (write.wSecond - start.wSecond) + (write.wMilliseconds - start.wMilliseconds) << " ms." << std::endl;  
}

// ======================================================================
// AcqMode::setDataBufferMem
// ======================================================================
void AcqMode::setDataBufferMem(unsigned long depth)
{
  RawData_t chRawData;
  ScaledData_t chScaledData;
  UserData_t chUserData;

  yat::AutoMutex<> guard(this->m_dataLock);

	//- Allocate buffers memory for raw, scaled & user data, for each active channel.
  //- Allocated memory depends on acquisition buffer size.

  const asl::ActiveAIChannels& ac = this->m_daqConfig.get_active_channels();
  unsigned int nbChan = 0;

  for (unsigned int idx = 0;  idx < ac.size();  idx++) 
  {
    nbChan = ac[idx].id;

    // raw data
    chRawData.capacity(depth);
		chRawData.force_length(depth);
		chRawData.fill(0);

    this->m_acqData.rawData.insert( pair<ChannelId_t, RawData_t>((ChannelId_t)nbChan, chRawData) );

    // scaled data (if necessary)
    if (this->m_dataTrt.isDataScaled)
    {
      chScaledData.capacity(depth);
      chScaledData.force_length(depth);
      chScaledData.fill(yat::IEEE_NAN);
      this->m_dynAcqData.scaledData.insert( pair<ChannelId_t, ScaledData_t>((ChannelId_t)nbChan, chScaledData) );
	  
      // user data (if necessary)
      if (ac[idx].user_data_enabled)
      {
        chUserData.capacity(depth);
        chUserData.force_length(depth);
        chUserData.fill(yat::IEEE_NAN);
        this->m_dynAcqData.userData.insert( pair<ChannelId_t, UserData_t>((ChannelId_t)nbChan, chUserData) );
      }	  
    }	
  }
}

//============================================================
// AcqMode::unsetDataBufferMem
//============================================================
void AcqMode::unsetDataBufferMem()
{
  yat::AutoMutex<> guard(this->m_dataLock);

  //- de-allocate buffers memory
  //- raw data:
  this->m_acqData.rawData.clear();

  //- scaled data (if allocated):
  if (this->m_dataTrt.isDataScaled)
  {
    this->m_dynAcqData.scaledData.clear();
	
    //- user data:
    this->m_dynAcqData.userData.clear();
  }
}

// ======================================================================
// AcqMode::initDataBuffers
// ======================================================================
void AcqMode::initDataBuffers()
  throw (Tango::DevFailed)
{
  //- initialize maps for raw & scaled data
  //- size = initial data buffer depth
  this->setDataBufferMem(this->m_daqConfig.get_buffer_depth());

  //- initialize maps for average, rms & peak values, for each active channel
  if (this->m_dataTrt.isDataScaled) // if not scaled, nothing to do
	{
    {
      yat::AutoMutex<> guard(this->m_dataLock);

      const asl::ActiveAIChannels& ac = this->m_daqConfig.get_active_channels();
      unsigned int nbChan = 0;

      for (unsigned int idx = 0;  idx < ac.size();  idx++) 
      {
        nbChan = ac[idx].id;

        if (this->m_dataTrt.hasStatistics)
        {
          this->m_dynAcqData.averageData.insert( pair<ChannelId_t, double>((ChannelId_t)nbChan, yat::IEEE_NAN) );
          this->m_dynAcqData.rms.insert( pair<ChannelId_t, double>((ChannelId_t)nbChan, yat::IEEE_NAN) );
          this->m_dynAcqData.peakToPeak.insert( pair<ChannelId_t, double>((ChannelId_t)nbChan, yat::IEEE_NAN) );

          if (ac[idx].user_data_enabled)
          {
            this->m_dynAcqData.scaledAvData.insert( pair<ChannelId_t, double>((ChannelId_t)nbChan, yat::IEEE_NAN) );
          }
        }
      }
    }

    //- initialize map for statistics history buffer
    //- size = initial statistics history depth (in number of samples)
    // Check if value not null
    if (this->m_dataTrt.statHistoryBufferDepth == 0)
    {
      ERROR_STREAM << "Init data buffers failed. Data history size should at least be 1!" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to init stats data buffers!"),
                      _CPTC("AcqMode::initDataBuffers"));
    }

    this->setStatHistoBufferMem(this->m_dataTrt.statHistoryBufferDepth);

    if (this->m_dataTrt.isDataHistorized)  // if data not historized, nothing to do
	  {
      // test if history depth above 2 (to avoid null value & respect ASL constraint)
      if (this->m_dataTrt.dataHistoryBufferDepth < 2)
      {
        this->m_errorMsg = "Init data buffers failed. Data history size should at least be 2!";
        this->m_errorOccurred = true;

        ERROR_STREAM << "Init data buffers failed. Data history size should at least be 2!" << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                        _CPTC("Failed to init data history buffers!"),
                        _CPTC("AcqMode::initDataBuffers"));
      }

      //- initialize map for data history buffer
      //- size = initial data history depth * it (in ms) * frequency
      unsigned long depth1 = (unsigned long) (this->m_dataTrt.dataHistoryBufferDepth * this->getBufferDepth());
      this->setDataHistoBufferMem(depth1);
    }
  }

  //- initialize buffers for timestamp histories
  //- size = initial timestamp histories depth (in number of samples)
  if (this->m_dataTrt.timestamped)
  {
    // Check if value not null
    if (this->m_dataTrt.timestampHistoriesDepth == 0)
    {
      ERROR_STREAM << "Init data buffers failed. Timestamp histories size should at least be 1!" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to init timestamp buffers!"),
                      _CPTC("AcqMode::initDataBuffers"));
    }

    this->setTimestampHistoBufferMem(this->m_dataTrt.timestampHistoriesDepth);
  }
}

// ======================================================================
// AcqMode::setStatHistoBufferMem
// ======================================================================
void AcqMode::setStatHistoBufferMem(unsigned long depth)
{    
  yat::AutoMutex<> guard(this->m_dataLock);

  if (this->m_dataTrt.isDataScaled && this->m_dataTrt.hasStatistics) // if not scaled & has non stats, nothing to do
  {
    AverageHistorizedData_t * chHistData = NULL;

    // clean old history
    ChannelsAverageHistorizedData_it_t it;
    for (it = this->m_dynAcqData.averageDataHistory.begin(); 
         it != this->m_dynAcqData.averageDataHistory.end();
         ++it)
    {
      delete it->second;
    }
    this->m_dynAcqData.averageDataHistory.clear();

    const asl::ActiveAIChannels& ac = this->m_daqConfig.get_active_channels();
    unsigned int nbChan = 0;

    for (unsigned int idx = 0;  idx < ac.size();  idx++) 
    {
      nbChan = ac[idx].id;

      chHistData = new AverageHistorizedData_t(depth);
		  chHistData->fill(yat::IEEE_NAN);
      this->m_dynAcqData.averageDataHistory.insert( pair<ChannelId_t, AverageHistorizedData_t*>((ChannelId_t)nbChan, chHistData) );
	  }
  }
}

// ======================================================================
// AcqMode::setDataHistoBufferMem
// ======================================================================
void AcqMode::setDataHistoBufferMem(unsigned long depth)
{
  ScaledHistorizedData_t chHistData;

  yat::AutoMutex<> guard(this->m_dataLock);

  if ((this->m_dataTrt.isDataScaled) &&
      (this->m_dataTrt.isDataHistorized) &&
      (depth != 0))  // if data not scaled or not historized or depth is null, nothing to do
	{
    const asl::ActiveAIChannels& ac = this->m_daqConfig.get_active_channels();
    unsigned int nbChan = 0;

    for (unsigned int idx = 0;  idx < ac.size();  idx++) 
    {
      nbChan = ac[idx].id;
      chHistData = new double[depth];
      for (size_t idx = 0; idx < depth; idx++)
        chHistData[idx] = yat::IEEE_NAN;

      this->m_dynAcqData.dataHistory.insert( pair<ChannelId_t, ScaledHistorizedData_t>((ChannelId_t)nbChan, chHistData) );
    }
  }
}

//============================================================
// AcqMode::deleteDataBuffers
//============================================================
void AcqMode::deleteDataBuffers()
{
  //- release raw & scaled data buffers:
	this->unsetDataBufferMem();

  {
    yat::AutoMutex<> guard(this->m_dataLock);

    //- clear statistics data: 
    this->m_dynAcqData.averageData.clear();
    this->m_dynAcqData.peakToPeak.clear();
    this->m_dynAcqData.rms.clear();
    this->m_dynAcqData.scaledAvData.clear();

    //- clear timestamp buffers
    if (m_dynAcqData.buffTimestamps)
    {
      m_dynAcqData.buffTimestamps->clear();
      delete m_dynAcqData.buffTimestamps;
      m_dynAcqData.buffTimestamps = NULL;
    }
    if (m_dynAcqData.relativeBuffTimes)
    {
      m_dynAcqData.relativeBuffTimes->clear();
      delete m_dynAcqData.relativeBuffTimes;
      m_dynAcqData.relativeBuffTimes = NULL;
    }

    //- clear average history data
    ChannelsAverageHistorizedData_it_t it;
    for (it = this->m_dynAcqData.averageDataHistory.begin(); 
         it != this->m_dynAcqData.averageDataHistory.end();
         ++it)
    {
      delete it->second;
    }
    this->m_dynAcqData.averageDataHistory.clear();

    //- clear data history
    if (this->m_dataTrt.isDataHistorized)
    {
      ChannelsScaledHistorizedData_it_t itt;
      for (itt = this->m_dynAcqData.dataHistory.begin(); 
           itt != this->m_dynAcqData.dataHistory.end();
           ++itt)
      {
        delete [] itt->second;
      }
      this->m_dynAcqData.dataHistory.clear();
    }
  }
}

// ======================================================================
// AcqMode::setIntegrationTime
// ======================================================================
void AcqMode::setIntegrationTime(double it)
  throw (Tango::DevFailed)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  // set new buffer size in DAQ configuration

  // new buffer size = new integration time * sampling rate
  unsigned long depth = (unsigned long) (it * kMILLISECONDS_TO_SECONDS * this->m_daqConfig.get_sampling_rate());

  // check that depth is not null before setting new integration time value
  if (depth == 0)
  {
    this->m_errorMsg = "Set integration time failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::setIntegrationTime - new buffer depth is null!" << std::endl;
    THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"),
      _CPTC("Bad integration time value: new buffer depth is null!"),
      _CPTC("AcqMode::setIntegrationTime"));
  }

	this->m_daqConfig.set_buffer_depth(depth);

  // re-allocate memory for buffers
  this->unsetDataBufferMem();
  this->setDataBufferMem(this->m_daqConfig.get_buffer_depth());

  // call ContinuousAI::configure()
  try
  {
    asl::ContinuousAI::configure(this->m_daqConfig);
  }
  catch (asl::DAQException& daqe)
  {
    this->m_errorMsg = "Set integration time failed.";
    this->m_errorOccurred = true;
    Tango::DevFailed df = this->daqToTangoException(daqe);
    ERROR_STREAM << "AcqMode::setSamplingRate DevFAILED :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to configure DAQ board (caught DevFailed)!"),
                      _CPTC("AcqMode::setIntegrationTime"));
  }
  catch (...)
  {
    this->m_errorMsg = "Set integration time failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::setIntegrationTime caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to configure DAQ board (caught [...])!"),
                    _CPTC("AcqMode::setIntegrationTime"));
  }

  // set my local current acquisition data
  this->m_acquisitionParam.integrationTime = it;

  // force update of data history length with new integration time value
  try
  {
    this->setDataHistoryDepth(this->m_dataTrt.dataHistoryBufferDepth);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_errorMsg = "Update of data history length failed.";
    this->m_errorOccurred = true;
    ERROR_STREAM << "AcqMode::setIntegrationTime DevFAILED :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                    _CPTC("CONFIGURATION_ERROR"), 
                    _CPTC("Update of data history length failed"), 
                    _CPTC("AcqMode::setIntegrationTime")); 
  }

  // reapply data mask (if enabled) 'cause 
  // data buffer depth could have changed
  if (this->m_dataTrt.hasMask)
  {
    this->setBoolDataMask(this->m_dataTrt.timeSlotMask);
  }
}

// ======================================================================
// AcqMode::setStatisticsHistoryDepth
// ======================================================================
void AcqMode::setStatisticsHistoryDepth(yat::uint32 depth)
  throw (Tango::DevFailed)
{
  yat::AutoMutex<> guard(this->m_dataLock);

  // re-allocate memory for statistics history buffer

  if (this->m_dataTrt.isDataScaled) // if not scaled, nothing to do
  {
    this->m_dataTrt.statHistoryBufferDepth = depth;

    //- set buffer size (in number of samples)
    this->setStatHistoBufferMem(this->m_dataTrt.statHistoryBufferDepth);
  }
}

// ======================================================================
// AcqMode::setDataHistoryDepth
// ======================================================================
void AcqMode::setDataHistoryDepth(yat::uint16 depth)
  throw (Tango::DevFailed)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  // re-allocate memory for data history buffer

  if ((this->m_dataTrt.isDataScaled) &&
      (this->m_dataTrt.isDataHistorized))  // if data not scaled or not historized, nothing to do
	{
    yat::AutoMutex<> guard(this->m_dataLock);

    //- clear scaled history data
    ChannelsScaledHistorizedData_it_t itt;
    for (itt = this->m_dynAcqData.dataHistory.begin(); 
         itt != this->m_dynAcqData.dataHistory.end();
         ++itt)
    {
      delete [] itt->second;
    }
    this->m_dynAcqData.dataHistory.clear();

    //- size = new data history depth * integration time (in ms) * frequency
    unsigned long buffSize = (unsigned long)(depth * this->getBufferDepth());

    // test if size is not null
    if (0 == buffSize)
    {
      this->m_errorMsg = "Set data history depth failed. Size is null!";
      this->m_errorOccurred = true;

      ERROR_STREAM << "Set data history depth failed. Size is null!" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to configure data history! Size is null!"),
                      _CPTC("AcqMode::setDataHistoryDepth"));
    }

    this->setDataHistoBufferMem(buffSize);
    
    // resize the history with new depth (in ms)
    this->m_daqConfig.set_history_length((unsigned long)(depth * this->m_acquisitionParam.integrationTime));

    // call ContinuousAI::configure()
    try
    {
      asl::ContinuousAI::configure(this->m_daqConfig);
    }
    catch (asl::DAQException& daqe)
    {
      this->m_errorMsg = "Set scaled history depth failed.";
      this->m_errorOccurred = true;

      Tango::DevFailed df = this->daqToTangoException(daqe);
      ERROR_STREAM << "AcqMode::setDataHistoryDepth DevFAILED :\n" << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("CONFIGURATION_ERROR"),
                        _CPTC("Failed to configure DAQ board (caught DevFailed)!"),
                        _CPTC("AcqMode::setDataHistoryDepth"));
    }
    catch (...)
    {
      this->m_errorMsg = "Set scaled history depth failed.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "AcqMode::setDataHistoryDepth caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to configure DAQ board (caught [...])!"),
                      _CPTC("AcqMode::setDataHistoryDepth"));
    }

    this->m_dataTrt.dataHistoryBufferDepth = depth;
  }
}

// ======================================================================
// AcqMode::setNexusFilePath
// ======================================================================
void AcqMode::setNexusFilePath(std::string path)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  //- set local acquisition parameters
  this->m_acquisitionParam.nexusTargetPath = path;
}

// ======================================================================
// AcqMode::setNexusFileGeneration
// ======================================================================
void AcqMode::setNexusFileGeneration(bool enable)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  //- set local acquisition parameters
  this->m_acquisitionParam.nexusFileGeneration = enable;
}

// ======================================================================
// AcqMode::setNexusNbPerFile
// ======================================================================
void AcqMode::setNexusNbPerFile(yat::uint16 nb)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  //- set local acquisition parameters
  this->m_acquisitionParam.nexusNbPerFile = nb;
}

// ======================================================================
// AcqMode::continuousAIconfigMapper
// ======================================================================
void AcqMode::continuousAIconfigMapper()
  throw (Tango::DevFailed)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  //- m_acqDefinition to m_daqConfig mapping
  
  //---------------------------------------------------------------------------
  //- Set common parameters:
  //---------------------------------------------------------------------------

  //- sampling source:
  switch (this->m_acqDefinition.triggerConfig.samplingInfo.source)
  {
    case SRC_INTERNAL:
      this->m_daqConfig.set_conversion_source(adl::ai_internal_timer);
      break;
    case SRC_EXTERNAL:
      this->m_daqConfig.set_conversion_source(adl::ai_external_afio);
      break;
    default:
      this->m_daqConfig.set_conversion_source(adl::ai_internal_timer);
      break;
  }

  //- timebase:
  switch (this->m_acqDefinition.triggerConfig.timebaseInfo.type)
  {
    case TB_INTERNAL:
      this->m_daqConfig.set_timebase_type(adl::int_time_base);
      break;
    case TB_EXTERNAL:
      this->m_daqConfig.set_timebase_type(adl::ext_time_base);
      this->m_daqConfig.set_ext_timebase_val(this->m_acqDefinition.triggerConfig.timebaseInfo.clock_freq);
      break;
    case TB_SSI:
      this->m_daqConfig.set_timebase_type(adl::ssi_time_base);
      break;
    default:
      this->m_daqConfig.set_timebase_type(adl::int_time_base);
      break;
  }

  //- data lost strategy:
  switch (this->m_acqDefinition.triggerConfig.overrunStrategy)
  {
    case OVRN_NOTIFY:
      this->m_daqConfig.set_data_lost_strategy(adl::notify);
      break;
    case OVRN_ABORT:
      this->m_daqConfig.set_data_lost_strategy(adl::abort);
      break;
    case OVRN_TRASH:
      this->m_daqConfig.set_data_lost_strategy(adl::trash);
      break;
    case OVRN_RESTART:
      this->m_daqConfig.set_data_lost_strategy(adl::restart);
      break;
    case OVRN_IGNORE:
      this->m_daqConfig.set_data_lost_strategy(adl::ignore);
      break;
    default:
      this->m_daqConfig.set_data_lost_strategy(adl::notify);
      break;
  }

  //- data timeout:
  this->m_daqConfig.set_timeout(this->m_acqDefinition.triggerConfig.dataTimeout);

  //- active channels:
  std::vector<Channel>::iterator it;

  for (it = this->m_acqDefinition.activeChannels.begin();
       it != this->m_acqDefinition.activeChannels.end();
       ++it)
  {
    asl::ActiveAIChannel ac;
    
    ac.id = (*it).number;
    ac.range = this->convertRange((*it).range); 

    if (this->m_boardImpl->isBoardMultiplexed())
    {
      switch ((*it).groundRef)
      {
        case GRD_REF_SINGLE:
          ac.grd_ref = adl::ref_single_ended;
          break;
        case GRD_REF_DIFF:
          ac.grd_ref = adl::differential;
          break;
        default:
          this->m_errorMsg = "DAQ configuration failed.";
          this->m_errorOccurred = true;

          ERROR_STREAM << "DAQ configuration error - GRD REF must be defined for this board type" << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          _CPTC("DAQ configuration error - see logs for details"), 
                          _CPTC("AcqMode::continuousAIconfigMapper"));
          break;
      }    
    }

    //- set user data gain, offset1 & offset2
    ac.user_data_enabled = (*it).userDataDesc.hasUserData;
    ac.user_data_gain = (*it).userDataDesc.gain;
    ac.user_data_offset1 = (*it).userDataDesc.offset1;
    ac.user_data_offset2 = (*it).userDataDesc.offset2;

    this->m_daqConfig.add_active_channel(ac);
  }

  //- sampling rate:
  //- check if sampling rate & number of active channels are in board range
  BoardType::acqParamCheck boardParam;
  boardParam.activeChanNb = (yat::uint16) this->m_daqConfig.num_active_channels();
  boardParam.samplingRate = this->m_acqDefinition.triggerConfig.samplingInfo.rate;
  try
  {
    this->m_boardImpl->checkParameters(boardParam);
  }
  catch (Tango::DevFailed & df)
  {
    this->m_errorMsg = "DAQ configuration failed.";
    this->m_errorOccurred = true;

    // Rate and/or active channels out of range => fatal error
    ERROR_STREAM << "AcqMode::continuousAIconfigMapper DevFAILED :\n" << df << std::endl;
    RETHROW_DEVFAILED(df,
                    _CPTC("CONFIGURATION_ERROR"), 
                    _CPTC("Configuration out of range for this board type"), 
                    _CPTC("AcqMode::continuousAIconfigMapper")); 
  }
  this->m_daqConfig.set_sampling_rate(this->m_acqDefinition.triggerConfig.samplingInfo.rate);


  //- initial acquisition buffer size:
  //- buffer size = integration time * initial sampling rate
  unsigned long depth = (unsigned long) (this->m_acquisitionParam.integrationTime * kMILLISECONDS_TO_SECONDS 
    * this->m_daqConfig.get_sampling_rate());
	this->m_daqConfig.set_buffer_depth(depth);

  //- trigger mode:
  switch (this->m_acqDefinition.triggerConfig.triggerMode)
  {
  case TRG_INTERNAL:
    //- no "internal" mode defined in ASL...
    this->m_daqConfig.set_trigger_source(adl::internal_software);
    break;
  case TRG_POST:
    this->m_daqConfig.set_trigger_mode(adl::ai_post);
    break;
  case TRG_PRE:
    this->m_daqConfig.set_trigger_mode(adl::ai_pre);
    break;
  case TRG_MIDDLE:
    this->m_daqConfig.set_trigger_mode(adl::ai_middle);
    break;
  case TRG_POST_DELAYED:
    this->m_daqConfig.set_trigger_mode(adl::ai_delay);
    break;
  case TRG_INT_POSTMORTEM:
    //- Internal postmortem mode is an "internal" asl mode
    this->m_daqConfig.set_trigger_source(adl::internal_software);
    break;
  default:
    this->m_errorMsg = "DAQ configuration failed.";
    this->m_errorOccurred = true;

    ERROR_STREAM << "DAQ configuration error - TRIGGER mode must be defined" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    _CPTC("DAQ configuration error - see logs for details"), 
                    _CPTC("AcqMode::continuousAIconfigMapper"));
    break;
  }
  
  //- data history (if enabled)
  if (this->m_dataTrt.isDataHistorized)
  {
    this->m_daqConfig.enable_history((unsigned long)(this->m_dataTrt.dataHistoryBufferDepth * 
      this->m_acquisitionParam.integrationTime));
  }
  else
  {
    this->m_daqConfig.disable_history();
  }

  //- set specific configuration (depending on trigger mode)
  this->specificAIconfigMapper_i();

  //- dump configuration
  this->m_daqConfig.dump();
}

// ======================================================================
// AcqMode::convertRange
// ======================================================================
adl::Range AcqMode::convertRange(std::string range_str)
  throw (Tango::DevFailed)
{
  // reinit error management
  this->m_errorMsg = "";
  this->m_errorOccurred = false;

  adl::Range adl_range;

	if (range_str.find("BP") != std::string::npos )
	{
    if ( range_str == "BP_10" )
      adl_range = adl::bp_10;
    if ( range_str == "BP_5" )
      adl_range = adl::bp_5;
    if ( range_str == "BP_2_5" )
      adl_range = adl::bp_2_5;
    if ( range_str == "BP_1_25" )
      adl_range = adl::bp_1_25;
    if ( range_str == "BP_0_625" )
      adl_range = adl::bp_0_625;
    if ( range_str == "BP_0_3125" )
      adl_range = adl::bp_0_3125;
    if ( range_str == "BP_0_5" )
      adl_range = adl::bp_0_5;
    if ( range_str == "BP_0_05" )
      adl_range = adl::bp_0_05;
    if ( range_str == "BP_0_005" )
      adl_range = adl::bp_0_005;
    if ( range_str == "BP_1" )
      adl_range = adl::bp_1;
    if ( range_str == "BP_0_1" )
      adl_range = adl::bp_0_1;
    if ( range_str == "BP_0_01" )
      adl_range = adl::bp_0_01;
    if ( range_str == "BP_0_001" )
      adl_range = adl::bp_0_001;
    if ( range_str == "BP_2" )
      adl_range = adl::bp_2;
    if ( range_str == "BP_0_25" )
      adl_range = adl::bp_0_25;
    if ( range_str == "BP_0_2" )
      adl_range = adl::bp_0_2;
	}
	else if (range_str.find("UP") != std::string::npos)
	{
    if ( range_str == "UP_10" )
      adl_range = adl::up_10;
    if ( range_str == "UP_5" )
      adl_range = adl::up_5;
    if ( range_str == "UP_2_5" )
      adl_range = adl::up_2_5;
    if ( range_str == "UP_1_25" )
      adl_range = adl::up_1_25;
    if ( range_str == "UP_1" )
      adl_range = adl::up_1;
    if ( range_str == "UP_0_1" )
      adl_range = adl::up_0_1;
    if ( range_str == "UP_0_01" )
      adl_range = adl::up_0_01;
    if ( range_str == "UP_0_001" )
      adl_range = adl::up_0_001;
    if ( range_str == "UP_4" )
      adl_range = adl::up_4;
    if ( range_str == "UP_2" )
      adl_range = adl::up_2;
    if ( range_str == "UP_0_5" )
      adl_range = adl::up_0_5;
    if ( range_str == "UP_0_4" )
      adl_range = adl::up_0_4;
	}
	else
	{
    this->m_errorMsg = "DAQ configuration failed.";
    this->m_errorOccurred = true;

    // bad range ==> fatal error
    ERROR_STREAM << "DAQ configuration error - range:" << range_str << " not supported by this DAQ board" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                    _CPTC("DAQ configuration error - see logs for details"), 
                    _CPTC("AcqMode::convertRange"));
	}
  
  return adl_range;
}

// ======================================================================
// AcqMode::daqToTangoException
// ======================================================================
Tango::DevFailed AcqMode::daqToTangoException(const asl::DAQException& de)
{
	Tango::DevErrorList error_list(de.errors.size());
	error_list.length(de.errors.size());
	
	for (unsigned int i = 0; i < de.errors.size(); i++)
	{
		error_list[i].reason = CORBA::string_dup(de.errors[i].reason.c_str());
    ERROR_STREAM << "AcqMode::daqToTangoException -> REASON = " << error_list[i].reason << std::endl;
		error_list[i].desc = CORBA::string_dup(de.errors[i].desc.c_str());
    ERROR_STREAM << "AcqMode::daqToTangoException -> DESC = " << error_list[i].desc << std::endl;
		error_list[i].origin = CORBA::string_dup(de.errors[i].origin.c_str());
    ERROR_STREAM << "AcqMode::daqToTangoException -> ORIGIN = " << error_list[i].origin << std::endl;
		switch(de.errors[i].severity)
		{
		case asl::WARN:
			error_list[i].severity = Tango::WARN;
			break;
		case asl::PANIC:
			error_list[i].severity = Tango::PANIC;
			break;
		case asl::ERR:
		default:
			error_list[i].severity = Tango::ERR;
			break;
		}
	}

	return Tango::DevFailed(error_list);
}

// ======================================================================
// AcqMode::handle_input
// ======================================================================
void AcqMode::handle_input(asl::AIRawData* raw_data)
{
  //DEBUG_STREAM << "AcqMode::handle_input : STARTED!" << std::endl;

  //- update handle input call frequency
  this->m_acqData.dataNotifFrequency = 
    ( 1000. * (this->m_handleInputCpt++) ) / this->m_handleInputTimer.elapsed_msec();

  //- data scaling (if required)
  if (this->m_dataTrt.isDataScaled) 
  {
    //- At acquistion start, the m_scaledData pointer is null 
    //- => ASL allocates the data buffer.
    //- While acquisition is running, the same data buffer is reused.
    scale_data(raw_data, this->m_scaledData);

    //- apply user data (if user data enabled at least for one channel)
    if (this->m_daqConfig.user_data_enabled())
    {
      //- convert to user data
      //- At acquistion start, the m_userData pointer is null 
      //- => ASL allocates the data buffer.
      //- While acquisition is running, the same data buffer is reused.
      convert_to_user_data (this->m_scaledData, this->m_userData);

      //- apply data mask on user data (if required)
      if (this->m_dataTrt.hasMask)
      {
        //- We reuse the received data buffer (m_userData pointer allocated
        //- by ASL at 1st use).
        apply_user_data_mask(this->m_userData);
      }
      
#if !defined __TANGO_EVENTS__
      //- update gain & offsets values if changes

      for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
      {     
        if (this->m_acqDefinition.activeChannels[chan].userDataDesc.hasUserData)
        {
          // save current values
          double currGain = this->m_acqDefinition.activeChannels[chan].userDataDesc.gain;
          double currOffset1 = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1;
          double currOffset2 = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2;
        
          // get distant values
          this->getUserDataGain(chan);
          this->getUserDataOffset1(chan);
          this->getUserDataOffset2(chan);

          // check if new gain is not null
          if (this->m_acqDefinition.activeChannels[chan].userDataDesc.gain == 0.0)
          {
            this->m_errorMsg = "Acquisition failed: user data gain became null!";
            this->m_errorOccurred = true;

            // consider that gain is null => error
            ERROR_STREAM << "Acquisition failed: user data gain became null!" << endl;
            THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                            _CPTC("Acquisition failed: user data gain became null!"),
                            _CPTC("AcqMode::handle_input"));    
          }

          // update ASL if at least one change
          if ( (currGain != this->m_acqDefinition.activeChannels[chan].userDataDesc.gain) ||
               (currOffset1 != this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1) ||
               (currOffset2 != this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2) )
          {
              
            set_user_data_parameters(
              this->m_acqDefinition.activeChannels[chan].number,
              this->m_acqDefinition.activeChannels[chan].userDataDesc.gain, 
              this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1,
              this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2);
          }
        }
      }
#endif
    }
    else
    {
      //- apply data mask on scaled data (if required)
      if (this->m_dataTrt.hasMask)
      {
        //- We reuse the received data buffer (m_scaledData pointer allocated
        //- by ASL at 1st use).
        apply_user_data_mask(this->m_scaledData);
      }
    }
  }

  //- Note that:
  // � the data history is computed on user request or in DAQ end event 
  //   handler in order to avoid CPU & RAM overload.
  // � The ASL layer applies user data and/or data mask for data history.

  //- call specific treatment according to trigger mode
  this->handle_input_i(raw_data);

  //- delete data buffers (allocated in the ASL part)
  delete raw_data;
  
  //DEBUG_STREAM << "AcqMode::handle_input : FINISHED!" << std::endl;
}

// ======================================================================
// AcqMode::handle_error
// ======================================================================
void AcqMode::handle_error(const asl::DAQException& ex)
{
  {
    yat::AutoMutex<> guard(this->m_dataLock);
    //- update counter
    this->m_acqData.errorCounter++;
  }

  //- trace error occurrence
  Tango::DevFailed df = this->daqToTangoException(ex);
  ERROR_STREAM << "error occured during acquisition -> check logs for more info." << std::endl;
  ERROR_STREAM << df << std::endl;

  //- call specific behavior
  this->handle_error_i();
}

// ======================================================================
// AcqMode::handle_data_lost
// ======================================================================
void AcqMode::handle_data_lost()
{
  {
    yat::AutoMutex<> guard(this->m_dataLock);
    //- update counter
    this->m_acqData.overrunCounter++;
  }

  //- trace overrun occurrence
  WARN_STREAM << "**** DATA LOST - COUNTER : " << this->m_acqData.overrunCounter << std::endl;

  //- call specific behavior
  this->handle_data_lost_i();
}

// ======================================================================
// AcqMode::handle_daq_end
// ======================================================================
void AcqMode::handle_daq_end(ContinuousDAQ::DaqEndReason why)
{
  INFO_STREAM << "------------- AcqMode::handle_daq_end received -------------" << std::endl;
	
  switch (why)
  {
    case ContinuousDAQ::DAQEND_ON_USER_REQUEST:
      INFO_STREAM << "\treason: user request\n" << std::endl;
      //- call timestamp function
      this->stampTrigger_c();
    break;
    case ContinuousDAQ::DAQEND_ON_EXTERNAL_TRIGGER:
      INFO_STREAM << "\treason: digital external trigger raised (pre-trigger or middle mode)\n" << std::endl;
      INFO_STREAM << "\treason: history buffer frozen\n" << std::endl;
      //- call timestamp function
      this->stampTrigger_c();
      //- raise ext trigger flag
      this->m_acqData.extTriggerFlag = true;
    break;
    case ContinuousDAQ::DAQEND_ON_FINITE_RETRIGGER_SEQUENCE:
      INFO_STREAM << "\treason: end of finite retrigger sequence\n" << std::endl;
    break;
    case ContinuousDAQ::DAQEND_ON_ERROR:
      ERROR_STREAM << "\treason: daq error\n" << std::endl;
    break;		
    case ContinuousDAQ::DAQEND_ON_CALIB_REQUEST:
      ERROR_STREAM << "\treason: board calibration on the way.\n" << std::endl;
    break;		
    case ContinuousDAQ::DAQEND_ON_OVERRUN:
      INFO_STREAM << "\treason: daq buffer overrun\n" << std::endl;
      INFO_STREAM << "\treason: daq has been <aborted> as required by the specified overrun strategy\n" << std::endl;
    break;
    case ContinuousDAQ::DAQEND_ON_UNKNOWN_EVT:
    default:
      FATAL_STREAM << "\treason: unknown event received." << std::endl;
    break;
  }
	
  { //- enter critical section
    yat::AutoMutex<> guard(this->m_dataLock);
    //- set daq end received flag
    this->m_daqEndEvtReceived = true;
  }

  //- call specific behavior
  this->handle_daq_end_i();

  //- stop nexus recording (if previously started). 
  if (this->m_nxStorageStarted)
  {
    CHECK_NX_MANAGER;

    try
    {
      INFO_STREAM << "Finalize NEXUS generation..." << std::endl;
      this->m_nexusManager->finalizeNexusGeneration();

      this->m_nxStorageStarted = false;
    }
    catch (Tango::DevFailed & df)
	  {
      this->m_errorMsg = "Nexus storage end error.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "AcqMode::handle_daq_end caught DevFailed : " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("DEVICE_ERROR"),
                        _CPTC("Failed to stop Nexus storage (caught DevFailed)!"),
                        _CPTC("AcqMode::handle_daq_end"));
	  }    
  }

  INFO_STREAM << " \t\t****	AcqMode::handle_daq_end ends SUCCESSFULLY" << std::endl;
}

// ======================================================================
// AcqMode::handle_timeout
// ======================================================================
void AcqMode::handle_timeout(void)
{
  {
    yat::AutoMutex<> guard(this->m_dataLock);
    //- update counter
    this->m_acqData.timeoutCounter++;
  }

  //- trace timeout occurrence
	WARN_STREAM << "::: AcqMode::handle_timeout :::" << std::endl; 
	WARN_STREAM << "\ttimeout notification #" <<  this->m_acqData.timeoutCounter << std::endl;
	WARN_STREAM << "\treason: the DAQ internal processing task did not receive data since " 
							<< this->configuration().get_timeout()
							<< " msec" 
							<< std::endl; 
	WARN_STREAM << "\treason: this is the timeout specified in the DAQ configuration." << std::endl; 
	
  //- call specific behavior
  this->handle_timeout_i();
}

// ======================================================================
// AcqMode::getAcqState
// ======================================================================
Tango::DevState AcqMode::getAcqState()
{
  Tango::DevState daq_state;

  switch (asl::ContinuousAI::state())
  {
    case asl::ContinuousAI::STANDBY:
      daq_state = Tango::STANDBY;
      break;
    case asl::ContinuousAI::RUNNING:
    case asl::ContinuousAI::ABORTING:
    case asl::ContinuousAI::CALIBRATING_HW:
      daq_state = Tango::RUNNING;
      break;
    case asl::ContinuousAI::FAULT:
      daq_state = Tango::FAULT;
      break;
    case asl::ContinuousAI::UNKNOWN:
    default:
      daq_state = Tango::UNKNOWN;
      break;
  }

  return daq_state;
}

// ======================================================================
// AcqMode::getNexusState
// ======================================================================
Tango::DevState AcqMode::getNexusState()
  throw (Tango::DevFailed)
{
  Tango::DevState nx_state = Tango::UNKNOWN;

  //- if Nexus storage started
  if (this->m_nxStorageStarted)
  {
    CHECK_NX_MANAGER;

    // get nexus state
    nx_state = this->m_nexusManager->getNexusStorageState();
  }
  else
  {
    // return STANDBY
    nx_state = Tango::STANDBY;
  }

  return nx_state;
}

// ======================================================================
// AcqMode::getNxItemList
// ======================================================================
nxItemList_t AcqMode::getNxItemList()
  throw (Tango::DevFailed)
{
  nxItemList_t items;

  //- according to the data configuration, sets the item list for Nexus storage
  //- Warning: should be the same names as used in handle_input_i() functions!

  //- parse the list of active channels
  for (Channels_it_t it = this->m_acqDefinition.activeChannels.begin();
       it != this->m_acqDefinition.activeChannels.end();
       ++it)
  {
    // check if dataset for channel is enabled
    yat::uint16 chan_id = get_channel_id_from_label((*it).label);
    if (this->m_acquisitionParam.datasetFlags[chan_id])
    {
      if (this->m_nxStoreAVERAGE)
      {
        nxItem item;
        item.name = m_datasetNames[chan_id][kKEY_DATA_NX_DATA_TYPE_AV];
        item.storageType = NX_DATA_TYPE_0D;
        item.dim1 = 0;
        item.dim2 = 0;
        items.push_back(item);
      }

      if (this->m_nxStoreRMS)
      {
        nxItem item;
        item.name = m_datasetNames[chan_id][kKEY_DATA_NX_DATA_TYPE_RMS];
        item.storageType = NX_DATA_TYPE_0D;
        item.dim1 = 0;
        item.dim2 = 0;
        items.push_back(item);
      }

      if (this->m_nxStorePEAK)
      {
        nxItem item;
        item.name = m_datasetNames[chan_id][kKEY_DATA_NX_DATA_TYPE_PEAK];
        item.storageType = NX_DATA_TYPE_0D;
        item.dim1 = 0;
        item.dim2 = 0;
        items.push_back(item);
      }

      if (this->m_nxStoreRAW)
      {
        nxItem item;
        item.name = m_datasetNames[chan_id][kKEY_DATA_NX_DATA_TYPE_RAW];
        item.storageType = NX_DATA_TYPE_1D;
        item.dim1 = this->m_daqConfig.get_buffer_depth();
        item.dim2 = 0;
        items.push_back(item);
      }

      if (this->m_nxStoreSCALED)
      {
        nxItem item;
        item.name = m_datasetNames[chan_id][kKEY_DATA_NX_DATA_TYPE_SCALED];
        item.storageType = NX_DATA_TYPE_1D;
        item.dim1 = this->m_daqConfig.get_buffer_depth();
        item.dim2 = 0;
        items.push_back(item);
      }

      if (this->m_nxStoreDATA_HIST)
      {
        nxItem item;
        item.name = m_datasetNames[chan_id][kKEY_DATA_NX_DATA_TYPE_DATA_HIST];
        item.storageType = NX_DATA_TYPE_1D;
        item.dim1 = this->m_daqConfig.get_history_length();
        item.dim2 = 0;
        items.push_back(item);
      }
  	
      if (this->m_nxStoreUSER_DATA)
      {
        nxItem item;
        item.name = m_datasetNames[chan_id][kKEY_DATA_NX_DATA_TYPE_USER];
        item.storageType = NX_DATA_TYPE_1D;
        item.dim1 = this->m_daqConfig.get_buffer_depth();
        item.dim2 = 0;
        items.push_back(item);
      }
    }
  }

  if (this->m_nxStoreTIMESTAMP)
  {
    nxItem item;
    item.name = kNEXUS_PREFIX_TIMESTAMP_ITEM;
    item.storageType = NX_DATA_TYPE_0D;
    item.dim1 = 0;
    item.dim2 = 0;
    items.push_back(item);

    item.name = kNEXUS_PREFIX_DELTATIME_ITEM;
    item.storageType = NX_DATA_TYPE_0D;
    item.dim1 = 0;
    item.dim2 = 0;
    items.push_back(item);
  }

  return items;
}

// ======================================================================
// AcqMode::initNexusTypes
// ======================================================================
bool AcqMode::initNexusTypes()
  throw (Tango::DevFailed)
{
  bool isAnyDataTypesToStore = false;

  isAnyDataTypesToStore = ! this->m_dataTrt.nxTypes.empty();

  //- parse the list of data type to store
  for (NxDataTypes_it_t it = this->m_dataTrt.nxTypes.begin();
    it != this->m_dataTrt.nxTypes.end();
    ++it)
  {
    switch ((*it))
    {
      case NX_RAW_DATA:
        this->m_nxStoreRAW = true;
        break;
      case NX_SCALED_DATA:
        this->m_nxStoreSCALED = true;
        break;
      case NX_AVERAGE_DATA:
        this->m_nxStoreAVERAGE = true;
        break;
      case NX_RMS:
        this->m_nxStoreRMS = true;
        break;
      case NX_PEAK:
        this->m_nxStorePEAK = true;
        break;
      case NX_DATA_HIST:
        this->m_nxStoreDATA_HIST = true;
        break;
      case NX_USER_DATA:
        this->m_nxStoreUSER_DATA = true;
        break;	
      case NX_TIMESTAMP:
        // check timestamp enabled before
        if (this->m_dataTrt.timestamped)
          this->m_nxStoreTIMESTAMP = true;
        break;	

      default:
        this->m_errorMsg = "Nexus configuration error.";
        this->m_errorOccurred = true;

        ERROR_STREAM << "Nexus configuration error - unknown data type for Nexus storage" << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                        _CPTC("Nexus configuration error - see logs for details"), 
                        _CPTC("AcqMode::initNexusTypes"));
        break;
    }
  }

  // Initialize Nexus dataset map with default values for each defined channel
  // and each possible dataset type (if needed)
  m_datasetNames.clear();
  std::vector<Channel>::iterator itc;
  std::string datasetName = "";

  if (isAnyDataTypesToStore)
  {
    for (itc = this->m_acqDefinition.activeChannels.begin();
         itc != this->m_acqDefinition.activeChannels.end();
         ++itc)
    {
      /// channel number
      yat::uint16 id = (*itc).number;

      // dataset names:
      // AVERAGE:
      datasetName = kNEXUS_PREFIX_AV_ITEM + (*itc).label;
      m_datasetNames[id][kKEY_DATA_NX_DATA_TYPE_AV] = datasetName;
      // RMS:
      datasetName = kNEXUS_PREFIX_RMS_ITEM + (*itc).label;
      m_datasetNames[id][kKEY_DATA_NX_DATA_TYPE_RMS] = datasetName;
      // PEAK:
      datasetName = kNEXUS_PREFIX_PEAK_ITEM + (*itc).label;
      m_datasetNames[id][kKEY_DATA_NX_DATA_TYPE_PEAK] = datasetName;
      // RAW:
      datasetName = (*itc).label + kNEXUS_POSTFIX_RAW_ITEM;
      m_datasetNames[id][kKEY_DATA_NX_DATA_TYPE_RAW] = datasetName;
      // SCALED:
      datasetName = (*itc).label;
      m_datasetNames[id][kKEY_DATA_NX_DATA_TYPE_SCALED] = datasetName;
      // DATA HISTORY:
      datasetName = kNEXUS_PREFIX_DATA_HIST_ITEM + (*itc).label;
      m_datasetNames[id][kKEY_DATA_NX_DATA_TYPE_DATA_HIST] = datasetName;
      // USER DATA:
      datasetName = kNEXUS_PREFIX_USER_DATA_ITEM + (*itc).label;
      m_datasetNames[id][kKEY_DATA_NX_DATA_TYPE_USER] = datasetName;
    }
  }

  return isAnyDataTypesToStore;
}

// ======================================================================
// AcqMode::rmsCalculation
// ======================================================================
double AcqMode::rmsCalculation (yat::Buffer<double> Varray, double Vmoy)
{
  double varTemp = 0.0;
  double stdv = 0.0;

	for (size_t idx = 0; idx < Varray.length(); idx++)
	{   
		varTemp += pow((Varray[idx] - Vmoy),2);
	}   

  if (Varray.length() != 0)
  {
	  stdv = sqrt(varTemp / Varray.length());
  }
  else
  {
    stdv = 0.0;
    WARN_STREAM << "AcqMode::rmsCalculation -- size of array is NULL! RMS set to zero." << std::endl;
  }

	return stdv;
}

// ======================================================================
// AcqMode::extractAcquisitionDefinition
// ======================================================================
void AcqMode::extractAcquisitionDefinition(RawAcquitisionConfig raw_acq_config)
  throw (Tango::DevFailed) 
{
  ConfigurationParser config_parser(this->m_hostDev);

  // get general characteristics of the acquisition from raw definition
  // i.e. acquisition definition
  try
  {
    this->m_acqDefinition.configId = raw_acq_config.configId;
    this->m_acqDefinition.configName = 
      config_parser.extractConfigName(raw_acq_config.configKeyList);
    this->m_acqDefinition.activeChannels = 
      config_parser.extractActiveChannels(raw_acq_config.configKeyList);
    this->m_acqDefinition.triggerConfig.dataTimeout = 
      config_parser.extractDataTimeout(raw_acq_config.configKeyList);
    this->m_acqDefinition.triggerConfig.overrunStrategy =
      config_parser.extractOverrunStrategy(raw_acq_config.configKeyList);
    this->m_acqDefinition.triggerConfig.samplingInfo =
      config_parser.extractSamplingInfo(raw_acq_config.configKeyList);
    this->m_acqDefinition.triggerConfig.triggerMode = 
      config_parser.extractTriggerMode(raw_acq_config.configKeyList);
    this->m_acqDefinition.triggerConfig.timebaseInfo =
        config_parser.extractTimebaseInfo(raw_acq_config.configKeyList);

    // get specific definition according to acquisition mode
    this->extractSpecificAcqDefinition_i(raw_acq_config);
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AcqMode::extractAcquisitionDefinition -> caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to analyse acquisition configuration (caught DevFailed)!"),
                      _CPTC("AcqMode::extractAcquisitionDefinition"));
  }
  catch (...)
  {
    ERROR_STREAM << "AcqMode::extractAcquisitionDefinition -> caught [...] Exception" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to analyse acquisition configuration (caught [...])!"),
                    _CPTC("AcqMode::extractAcquisitionDefinition"));
  }
}

// ======================================================================
// AcqMode::extractDataTrtDefinition
// ======================================================================
void AcqMode::extractDataTrtDefinition(RawAcquitisionConfig raw_acq_config)
  throw (Tango::DevFailed) 
{
  ConfigurationParser config_parser(this->m_hostDev);

  // get general treatment of the acquisition results from raw definition
  try
  {
    this->m_dataTrt.isDataScaled = 
      config_parser.extractDataScaled(raw_acq_config.configKeyList);

    if (this->m_dataTrt.isDataScaled)
    {
      this->m_dataTrt.hasStatistics = 
        config_parser.extractStatisticsComputed(raw_acq_config.configKeyList);
      this->m_dataTrt.isDataHistorized = 
        config_parser.extractDataHistorized(raw_acq_config.configKeyList);
    }

    this->m_dataTrt.nxTypes = 
      config_parser.extractNexusDataTypes(raw_acq_config.configKeyList);
    
    this->m_dataTrt.nxFileName = 
      config_parser.extractNexusFileName(raw_acq_config.configKeyList);

    // add auto tuning extraction
    this->m_dataTrt.autoTuning =
      config_parser.extractAutoTuning(raw_acq_config.configKeyList);

    // get specific data treatment according to acquisition mode
    this->extractSpecificDataTrtDefinition_i(raw_acq_config);
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AcqMode::extractDataTrtDefinition -> caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to analyse data treatment definition (caught DevFailed)!"),
                      _CPTC("AcqMode::extractDataTrtDefinition"));
  }
  catch (...)
  {
    ERROR_STREAM << "AcqMode::extractDataTrtDefinition -> caught [...] Exception" << std::endl;
    THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                    _CPTC("Failed to analyse data treatment definition (caught [...])!"),
                    _CPTC("AcqMode::extractDataTrtDefinition"));
  }
}

// ======================================================================
// AcqMode::extractSpecificDataTrtDefinition_i
// ======================================================================
void AcqMode::extractSpecificDataTrtDefinition_i(RawAcquitisionConfig raw_acq_config)
  throw (Tango::DevFailed) 
{
  // nothing more to do
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::define_acquisition_attributes
//
//  description:  defines the acquition's dynamic attributes according
//  to current configuration definition.
//
// @return  Dynamic attribute description list
//
//+------------------------------------------------------------------
std::vector<yat4tango::DynamicAttributeInfo> AcqMode::define_acquisition_attributes()
{
  std::vector<yat4tango::DynamicAttributeInfo> dynAttrList;

  //----->>>>>>>>>>  Add dynamic attributes for general acquisition results  <<<<<<<<<<-----

  //---------------
  //- dataCounter:
  //---------------
  yat4tango::DynamicAttributeInfo dai00;

  //- attribute definition:
  dai00.dev = this->m_hostDev;

  dai00.tai.name = "dataCounter";
  dai00.tai.label = "data counter"; 
  dai00.tai.data_format = Tango::SCALAR;
  dai00.tai.data_type = Tango::DEV_ULONG;
  dai00.tai.disp_level = Tango::EXPERT;
  dai00.tai.writable = Tango::READ;
  dai00.tai.description = "Number of data buffer received.";

  //- attribute properties:
  dai00.tai.unit = " ";
  dai00.tai.standard_unit = " ";
  dai00.tai.display_unit = " ";
  dai00.tai.format = "%7d";

  dai00.cdb = true;

  //- read callback
  dai00.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                  &AcqMode::read_callback_data_counter);

  //- add attribute description in list
  dynAttrList.push_back(dai00);


  //---------------
  //- errorCounter:
  //---------------
  yat4tango::DynamicAttributeInfo dai01;

  //- attribute definition:
  dai01.dev = this->m_hostDev;

  dai01.tai.name = "errorCounter";
  dai01.tai.label = "error counter"; 
  dai01.tai.data_format = Tango::SCALAR;
  dai01.tai.data_type = Tango::DEV_ULONG;
  dai01.tai.disp_level = Tango::EXPERT;
  dai01.tai.writable = Tango::READ;
  dai01.tai.description = "Number of error notifications received.";

  //- attribute properties:
  dai01.tai.unit = " ";
  dai01.tai.standard_unit = " ";
  dai01.tai.display_unit = " ";
  dai01.tai.format = "%7d";

  dai01.cdb = true;

  //- read callback
  dai01.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                  &AcqMode::read_callback_error_counter);

  //- add attribute description in list
  dynAttrList.push_back(dai01);


  //------------------
  //- overrunCounter:
  //------------------
  yat4tango::DynamicAttributeInfo dai02;

  //- attribute definition:
  dai02.dev = this->m_hostDev;

  dai02.tai.name = "overrunCounter";
  dai02.tai.label = "overrun counter"; 
  dai02.tai.data_format = Tango::SCALAR;
  dai02.tai.data_type = Tango::DEV_ULONG;
  dai02.tai.disp_level = Tango::EXPERT;
  dai02.tai.writable = Tango::READ;
  dai02.tai.description = "Number of overrun notifications received.";

  //- attribute properties:
  dai02.tai.unit = " ";
  dai02.tai.standard_unit = " ";
  dai02.tai.display_unit = " ";
  dai02.tai.format = "%7d";

  dai02.cdb = true;

  //- read callback
  dai02.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                  &AcqMode::read_callback_overrun_counter);

  //- add attribute description in list
  dynAttrList.push_back(dai02);


  //------------------
  //- timeoutCounter:
  //------------------
  yat4tango::DynamicAttributeInfo dai03;

  //- attribute definition:
  dai03.dev = this->m_hostDev;

  dai03.tai.name = "timeoutCounter";
  dai03.tai.label = "timeout counter"; 
  dai03.tai.data_format = Tango::SCALAR;
  dai03.tai.data_type = Tango::DEV_ULONG;
  dai03.tai.disp_level = Tango::OPERATOR;
  dai03.tai.writable = Tango::READ;
  dai03.tai.description = "Number of timeout notifications received.";

  //- attribute properties:
  dai03.tai.unit = " ";
  dai03.tai.standard_unit = " ";
  dai03.tai.display_unit = " ";
  dai03.tai.format = "%7d";

  dai03.cdb = true;

  //- read callback
  dai03.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                  &AcqMode::read_callback_timeout_counter);

  //- add attribute description in list
  dynAttrList.push_back(dai03);

  //-----------------------
  //- handleInputFrequency:
  //-----------------------
  yat4tango::DynamicAttributeInfo dai04;

  //- attribute definition:
  dai04.dev = this->m_hostDev;

  dai04.tai.name = "handleInputCallFrequency";
  dai04.tai.label = "handle input call frequency"; 
  dai04.tai.data_format = Tango::SCALAR;
  dai04.tai.data_type = Tango::DEV_DOUBLE;
  dai04.tai.disp_level = Tango::EXPERT;
  dai04.tai.writable = Tango::READ;
  dai04.tai.description = "Handle input call frequency in Hz.";

  //- attribute properties:
  dai04.tai.unit = "Hz";
  dai04.tai.standard_unit = "Hz";
  dai04.tai.display_unit = "Hz";
  dai04.tai.format = "%6.3f";

  dai04.cdb = true;

  //- read callback
  dai04.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                  &AcqMode::read_callback_handle_input_freq);

  //- add attribute description in list
  dynAttrList.push_back(dai04);


  if (this->m_dataTrt.isDataScaled)
  {
    if (this->m_dataTrt.hasStatistics)
    {
      //----------------------------------
      //- statistics history buffer depth:
      //----------------------------------
      yat4tango::DynamicAttributeInfo dai7;

      //- attribute definition:
      dai7.dev = this->m_hostDev;

      dai7.tai.name = "statHistoryBufferDepth";
      dai7.tai.label = "statistics history buffer depth"; 
      dai7.tai.data_format = Tango::SCALAR;
      dai7.tai.data_type = Tango::DEV_ULONG;
      dai7.tai.disp_level = Tango::OPERATOR;
      dai7.tai.writable = Tango::READ_WRITE;
      dai7.tai.description = "Statistics history buffer depth in number of samples. Has an effect only if statistics are enabled.";

      //- attribute properties:
      dai7.tai.unit = " ";
      dai7.tai.standard_unit = " ";
      dai7.tai.display_unit = " ";
      dai7.tai.format = "%6d";

      dai7.cdb = true;

      //- read callback
      dai7.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                      &AcqMode::read_callback_stat_history_buffer_depth);

      //- write callback
      dai7.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                      &AcqMode::write_callback_stat_history_buffer_depth);

      //- add attribute description in list
      dynAttrList.push_back(dai7);
	}

    if (this->m_dataTrt.isDataHistorized)
    {
      //-------------------------------
      //- data history buffer depth:
      //-------------------------------
      yat4tango::DynamicAttributeInfo dai8;

      //- attribute definition:
      dai8.dev = this->m_hostDev;

      dai8.tai.name = "dataHistoryBufferDepth";
      dai8.tai.label = "data history buffer depth"; 
      dai8.tai.data_format = Tango::SCALAR;
      dai8.tai.data_type = Tango::DEV_USHORT;
      dai8.tai.disp_level = Tango::OPERATOR;
      dai8.tai.writable = Tango::READ_WRITE;
      dai8.tai.description = "Data history buffer depth in ms (multiple of the integration time). Has an effect only if data is scaled and historized.";

      //- attribute properties:
      dai8.tai.unit = "x integration time";
      dai8.tai.standard_unit = "ms";
      dai8.tai.display_unit = "x integration time";
      dai8.tai.format = "%6d";

      dai8.cdb = true;

      //- read callback
      dai8.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                      &AcqMode::read_callback_historized_data_buffer_depth);

      //- write callback
      dai8.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                      &AcqMode::write_callback_historized_data_buffer_depth);

      //- add attribute description in list
      dynAttrList.push_back(dai8);
    }
  }
  
  //----->>>>>>>>>>  Add dynamic attributes for Nexus storage  <<<<<<<<<<-----

  // Add a enable/disable flag for each defined channel
  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    std::string chanLabel = this->m_acqDefinition.activeChannels[chan].label;
    yat4tango::DynamicAttributeInfo dai9;

    //- attribute definition:
    dai9.dev = this->m_hostDev;

    dai9.tai.name = aicontroller::kATTR_NAME_TAG_DATASET + chanLabel;
    dai9.tai.label = dai9.tai.name; 
    dai9.tai.data_format = Tango::SCALAR;
    dai9.tai.data_type = Tango::DEV_BOOLEAN;
    dai9.tai.disp_level = Tango::EXPERT;
    dai9.tai.writable = Tango::READ_WRITE;
    dai9.tai.description = "Enable dataset (i.e. Nexus storage) for this channel.";

    //- attribute properties:
    dai9.tai.unit = " ";
    dai9.tai.standard_unit = " ";
    dai9.tai.display_unit = " ";
    dai9.tai.format = "%d";

    dai9.cdb = true;

    //- read callback
    dai9.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                    &AcqMode::read_callback_enable_dataset);

    //- write callback
    dai9.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(const_cast<AcqMode&>(*this), 
                                                                    &AcqMode::write_callback_enable_dataset);

    //- add attribute description in list
    dynAttrList.push_back(dai9);
  }


  //----->>>>>>>>>>  Add specific dynamic attributes for acquisition mode  <<<<<<<<<<-----

  this->define_specific_attributes_i(dynAttrList);

  return dynAttrList;
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_data_counter
//
//  description:  read callback function for data counter.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_data_counter(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // set attribute value
    static Tango::DevULong __data_counter__;
    __data_counter__ = static_cast<Tango::DevULong>(AcqMode::m_acqData.dataCounter);
    cbd.tga->set_value(&__data_counter__); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_error_counter
//
//  description:  read callback function for error counter.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_error_counter(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // set attribute value
    static Tango::DevULong __error_counter__;
    __error_counter__ = static_cast<Tango::DevULong>(AcqMode::m_acqData.errorCounter);
    cbd.tga->set_value(&__error_counter__); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_overrun_counter
//
//  description:  read callback function for error counter.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_overrun_counter(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // set attribute value
    static Tango::DevULong __overrun_counter__;
    __overrun_counter__ = static_cast<Tango::DevULong>(AcqMode::m_acqData.overrunCounter);
    cbd.tga->set_value(&__overrun_counter__); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_timeout_counter
//
//  description:  read callback function for error counter.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_timeout_counter(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // set attribute value
    static Tango::DevULong __timeout_counter__;
    __timeout_counter__ = static_cast<Tango::DevULong>(AcqMode::m_acqData.timeoutCounter);
    cbd.tga->set_value(&__timeout_counter__); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_handle_input_freq
//
//  description:  read callback function for handle input call frequency.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_handle_input_freq(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // set attribute value
    static Tango::DevDouble __freq__;
    __freq__ = static_cast<Tango::DevDouble>(AcqMode::getHandleInputFreq());
    cbd.tga->set_value(&__freq__); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_stat_history_buffer_depth
//
//  description:  read callback function for statistics history buffer depth.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_stat_history_buffer_depth(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // set attribute value
	  static Tango::DevULong __av_histo_depth__;
    __av_histo_depth__ = static_cast<Tango::DevULong>(AcqMode::m_dataTrt.statHistoryBufferDepth);
    cbd.tga->set_value(&__av_histo_depth__);
  }
}

//+----------------------------------------------------------------------------
//
// method : 		AcqMode::write_callback_stat_history_buffer_depth
// 
// description : 	write callback function for statistics history buffer depth.
//
//-----------------------------------------------------------------------------
void AcqMode::write_callback_stat_history_buffer_depth(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = AcqMode::getAcqState();
  if ((Tango::RUNNING == state) || 
      (Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("AcqMode::write_callback_stat_history_buffer_depth"));
  }

  Tango::DevULong histo_depth;
  cbd.tga->get_write_value(histo_depth);

  // Check if value not null
  if (histo_depth == 0)
  {
    THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                    _CPTC("request aborted - attribute cannot be null!"),
                    _CPTC("AcqMode::write_callback_stat_history_buffer_depth"));
  }

  this->setStatisticsHistoryDepth(histo_depth);
  this->store_value_as_property(histo_depth, "__StatHistoryBufferDepth");
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_historized_data_buffer_depth
//
//  description:  read callback function for historized data buffer depth.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_historized_data_buffer_depth(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // set attribute value
	  static Tango::DevUShort __histo_depth__;
    __histo_depth__ = static_cast<Tango::DevUShort>(this->m_dataTrt.dataHistoryBufferDepth);

    cbd.tga->set_value(&__histo_depth__);
  }
}

//+----------------------------------------------------------------------------
//
// method : 		AcqMode::write_callback_historized_data_buffer_depth
// 
// description : 	write callback function for historized data buffer depth.
//
//-----------------------------------------------------------------------------
void AcqMode::write_callback_historized_data_buffer_depth(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = AcqMode::getAcqState();
  if ((Tango::RUNNING == state) || 
      (Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("AcqMode::write_callback_historized_data_buffer_depth"));
  }

  Tango::DevUShort histo_depth;
  cbd.tga->get_write_value(histo_depth);

  // test if history depth above 2 (to avoid null value & respect ASL constraint)
  if (histo_depth < 2)
  {
    THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                    _CPTC("request aborted - minimum value is 2!"),
                    _CPTC("AcqMode::write_callback_historized_data_buffer_depth"));
  }

  AcqMode::setDataHistoryDepth(histo_depth);
  this->store_value_as_property(histo_depth, "__DataHistoryBufferDepth");
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_raw_data
//
//  description:  read callback function for raw data.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_raw_data(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    size_t pos = attrName.find(kATTR_NAME_TAG_RAW);
    std::string label = attrName.substr(0, pos);

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    {
      yat::AutoMutex<> guard(this->m_dataLock);

      // set attribute value
      RawData_t & data = this->m_acqData.rawData[chan_id];
      cbd.tga->set_value(data.base(), data.length()); 
    }
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_scaled_data
//
//  description:  read callback function for scaled data.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_scaled_data(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName;

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    {
      yat::AutoMutex<> guard(this->m_dataLock);

      // set attribute value
      ScaledData_t & data = this->m_dynAcqData.scaledData[chan_id];
      cbd.tga->set_value(data.base(), data.length()); 
    }
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_average_data
//
//  description:  read callback function for average data.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_average_data(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_TAG_AVERAGE.size(), std::string::npos);

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    // set attribute value
    cbd.tga->set_value(&(m_dynAcqData.averageData[chan_id])); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_average_history
//
//  description:  read callback function for average history.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_average_history(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_TAG_AV_HIST.size(), std::string::npos);

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    {
      yat::AutoMutex<> guard(this->m_dataLock);

      // set attribute value
      const AverageOrderedHistorizedData_t & data = AcqMode::m_dynAcqData.averageDataHistory[chan_id]->ordered_data();
      cbd.tga->set_value(data.base(), data.length()); 
    }
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_rms
//
//  description:  read callback function for rms value.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_rms(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_TAG_RMS.size(), std::string::npos);

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    // set attribute value
    cbd.tga->set_value(&(m_dynAcqData.rms[chan_id])); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_peak
//
//  description:    read callback function for peak to peak value
//
//+------------------------------------------------------------------
void AcqMode::read_callback_peak(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_TAG_PEAK.size(), std::string::npos);

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    // set attribute value
    cbd.tga->set_value(&(m_dynAcqData.peakToPeak[chan_id])); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_data_history
//
//  description:    read callback function for data history.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_data_history(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  size_t num_active_chan = AcqMode::m_acqDefinition.activeChannels.size();

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (num_active_chan == 0) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_TAG_DATA_HIST.size(), std::string::npos);

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    //DEBUG_STREAM << "AcqMode::read_callback_data_history for chan id=" << chan_id << std::endl;
	
    if (this->getDataHistoryValidity())
    {
      // compute data history for required channel id
      //- do that only if acquisition is running 'cause if acquisition is stopped, 
      //- the stop function has already computed whole history,
      if (state == Tango::RUNNING)
      {
        AcqMode::computeDataHistory(chan_id);
      }

      // set attribute value
      aicontroller::ScaledHistorizedData_t data = AcqMode::m_dynAcqData.dataHistory[chan_id];    
      size_t buff_depth = (size_t)(AcqMode::m_dataTrt.dataHistoryBufferDepth * this->getBufferDepth());
      cbd.tga->set_value(data, buff_depth);
    }
    else
    {
      cbd.tga->set_quality(Tango::ATTR_INVALID);
    }
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_user_data
//
//  description:    read callback function for user data.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_user_data(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_TAG_USER_DATA.size(), std::string::npos);

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    {
      yat::AutoMutex<> guard(this->m_dataLock);

      // set attribute value
      UserData_t & data = this->m_dynAcqData.userData[chan_id];
      cbd.tga->set_value(data.base(), data.length()); 
    }
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_user_data_gain
//
//  description:    read callback function for user data gain.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_user_data_gain(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_USER_DATA_GAIN.size(), std::string::npos);

    // get channel idx from label
    yat::uint16 chan_idx = get_channel_index_from_label(label);

    // set attribute value
    static Tango::DevDouble __gain__;
    __gain__ = static_cast<Tango::DevDouble>(this->getUserDataGain(chan_idx));
    cbd.tga->set_value(&__gain__); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_scaled_average_data
//
//  description:  read callback function for scaled average data.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_scaled_average_data(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_TAG_AVERAGE_SCD.size(), std::string::npos);

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    // set attribute value
    cbd.tga->set_value(&(m_dynAcqData.scaledAvData[chan_id])); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::write_callback_user_data_gain
//
//  description:    write callback function for user data gain.
//
//+------------------------------------------------------------------
void AcqMode::write_callback_user_data_gain(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = this->getAcqState();
  if ((Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("AcqMode::write_callback_user_data_gain"));
  }

  Tango::DevDouble gain;
  cbd.tga->get_write_value(gain);

  // extract channel label from attribute name
  std::string label = attrName.substr(kATTR_NAME_USER_DATA_GAIN.size(), std::string::npos);

  // get channel idx from label
  yat::uint16 chan_idx = get_channel_index_from_label(label);

  //- check acquisition state
  if (Tango::RUNNING == state)
  {
    AcqMode::setUserDataGain(chan_idx, gain, true);
  }
  else
  {
    AcqMode::setUserDataGain(chan_idx, gain, false);
  }

  std::string name = "__UserDataGain" + label;
  this->store_value_as_property(gain, name);
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_user_data_offset1
//
//  description:    read callback function for user data offset1.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_user_data_offset1(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_USER_DATA_OFS1.size(), std::string::npos);

    // get channel idx from label
    yat::uint16 chan_idx = get_channel_index_from_label(label);

    // set attribute value
    static Tango::DevDouble __ofs1__;
    __ofs1__ = static_cast<Tango::DevDouble>(this->getUserDataOffset1(chan_idx));
    cbd.tga->set_value(&__ofs1__);
  }
}


//+------------------------------------------------------------------
//
//  method:  AcqMode::write_callback_user_data_offset1
//
//  description:    write callback function for user data offset1.
//
//+------------------------------------------------------------------
void AcqMode::write_callback_user_data_offset1(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = this->getAcqState();
  if ((Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("AcqMode::write_callback_user_data_offset1"));
  }

  Tango::DevDouble offset;
  cbd.tga->get_write_value(offset);

  // extract channel label from attribute name
  std::string label = attrName.substr(kATTR_NAME_USER_DATA_OFS1.size(), std::string::npos);

  // get channel idx from label
  yat::uint16 chan_idx = get_channel_index_from_label(label);

  //- check acquisition state
  if (Tango::RUNNING == state)
  {
    AcqMode::setUserDataOffset1(chan_idx, offset, true);
  }
  else
  {
    AcqMode::setUserDataOffset1(chan_idx, offset, false);
  }

  std::string name = "__UserDataOffset1" + label;
  this->store_value_as_property(offset, name);
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_user_data_offset2
//
//  description:    read callback function for user data offset2.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_user_data_offset2(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_USER_DATA_OFS2.size(), std::string::npos);

    // get channel idx from label
    yat::uint16 chan_idx = get_channel_index_from_label(label);

    // set attribute value
    static Tango::DevDouble __ofs2__;
    __ofs2__ = static_cast<Tango::DevDouble>(this->getUserDataOffset2(chan_idx));
    cbd.tga->set_value(&__ofs2__);
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::write_callback_user_data_offset2
//
//  description:    write callback function for user data offset2.
//
//+------------------------------------------------------------------
void AcqMode::write_callback_user_data_offset2(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = this->getAcqState();
  if ((Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("AcqMode::write_callback_user_data_offset2"));
  }
  Tango::DevDouble offset;
  cbd.tga->get_write_value(offset);

  // extract channel label from attribute name
  std::string label = attrName.substr(kATTR_NAME_USER_DATA_OFS2.size(), std::string::npos);

  // get channel idx from label
  yat::uint16 chan_idx = get_channel_index_from_label(label);

  //- check acquisition state
  if (Tango::RUNNING == state)
  {
    AcqMode::setUserDataOffset2(chan_idx, offset, true);
  }
  else
  {
    AcqMode::setUserDataOffset2(chan_idx, offset, false);
  }

  std::string name = "__UserDataOffset2" + label;
  this->store_value_as_property(offset, name);
}

// ======================================================================
// AcqMode::get_channel_id_from_label
// ======================================================================
yat::uint16 AcqMode::get_channel_id_from_label(std::string label)
{
  yat::uint16 chan_id = 0;
  bool config_found = false;

  for (size_t idx = 0; idx < this->m_acqDefinition.activeChannels.size(); idx++)
  {
    if (this->m_acqDefinition.activeChannels[idx].label == label)
    {
      config_found = true;
      chan_id = this->m_acqDefinition.activeChannels[idx].number;
      break;
    }
  }

  if (!config_found)
  {
    ERROR_STREAM << "Data reading: channel label not found!" << endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                    _CPTC("Data reading: error while getting channel label!"),
                    _CPTC("AcqMode::get_channel_id_from_label"));
  }

  return chan_id;
}

// ======================================================================
// AcqMode::get_channel_index_from_label
// ======================================================================
yat::uint16 AcqMode::get_channel_index_from_label(std::string label)
{
  yat::uint16 index = 0;
  bool config_found = false;

  for (size_t idx = 0; idx < this->m_acqDefinition.activeChannels.size(); idx++)
  {
    if (this->m_acqDefinition.activeChannels[idx].label == label)
    {
      config_found = true;
      index = idx;
      break;
    }
  }

  if (!config_found)
  {
    ERROR_STREAM << "Data reading: channel label not found!" << endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                    _CPTC("Data reading: error while getting channel label!"),
                    _CPTC("AcqMode::get_channel_index_from_label"));
  }

  return index;
}

// ======================================================================
// AcqMode::initSpecificAttributes_i
// ======================================================================
void AcqMode::initSpecificAttributes_i()
    throw (Tango::DevFailed)
{
  //- Set the list of writable attributes memorized as Device property
	Tango::DbData	dev_prop;
  std::string name = "";

  name = "__StatHistoryBufferDepth";
	dev_prop.push_back(Tango::DbDatum(name));

  name = "__DataHistoryBufferDepth";
	dev_prop.push_back(Tango::DbDatum(name));

#if !defined __TANGO_EVENTS__
  // add polling period property
  dev_prop.push_back(Tango::DbDatum("UserDataProxyPollingPeriod"));
#endif

  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    name = "__UserDataGain" + this->m_acqDefinition.activeChannels[chan].label;
	  dev_prop.push_back(Tango::DbDatum(name));

    name = "__UserDataOffset1" + this->m_acqDefinition.activeChannels[chan].label;
	  dev_prop.push_back(Tango::DbDatum(name));

    name = "__UserDataOffset2" + this->m_acqDefinition.activeChannels[chan].label;
	  dev_prop.push_back(Tango::DbDatum(name));
  }

  name = "__TimestampHistoriesDepth";
	dev_prop.push_back(Tango::DbDatum(name));

  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    name = "__EnableDataset" + this->m_acqDefinition.activeChannels[chan].label;
	  dev_prop.push_back(Tango::DbDatum(name));
  }


	//-	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
    this->m_hostDev->get_db_device()->get_property(dev_prop);

  size_t idx = 0;

	//-	Try to extract saved property from database

  // stat history depth
  unsigned long statDepth = 1000;
  if (dev_prop[idx].is_empty()==false)
  {
    dev_prop[idx++]  >>  statDepth;
  }
  else
  {
    idx++;
  }
  this->m_dataTrt.statHistoryBufferDepth = statDepth;

  // data history depth
  unsigned long dataDepth = 2;
  if (dev_prop[idx].is_empty()==false)
  {
    dev_prop[idx++]  >>  dataDepth;
  }
  else
  {
    idx++;
  }
  this->m_dataTrt.dataHistoryBufferDepth = (yat::uint16)dataDepth;

#if !defined __TANGO_EVENTS__
  // user data proxy polling period
  double period = 1000.0; // default value
	if (dev_prop[idx].is_empty()==false)
	{
		dev_prop[idx++]  >>  period;
  }
  else
  {
    idx++;
  }
  this->m_userDataProxyPollingPeriod = (size_t)period;
#endif

  // user data
  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    // user data gain
    double gain = 1.0; // default value
	  if (dev_prop[idx].is_empty()==false)
	  {
      dev_prop[idx++]  >>  gain;
    }
    else
    {
      idx++;
    }
    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.gainType == USER_DATA_ATTR) // set value only in ATTR mode
      this->m_acqDefinition.activeChannels[chan].userDataDesc.gain = gain;

    // user data offset1
    double offset1 = 0.0; // default value
	  if (dev_prop[idx].is_empty()==false)
	  {
		  dev_prop[idx++]  >>  offset1;
    }
    else
    {
      idx++;
    }
    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Type == USER_DATA_ATTR) // set value only in ATTR mode
      this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1 = offset1;

    // user data offset2
    double offset2 = 0.0; // default value
	  if (dev_prop[idx].is_empty()==false)
	  {
		  dev_prop[idx++]  >>  offset2;
    }
    else
    {
      idx++;
    }
    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Type == USER_DATA_ATTR) // set value only in ATTR mode
      this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2 = offset2;
  }

  // timestamp histories depth
  unsigned long tsDepth = 1000;
  if (dev_prop[idx].is_empty()==false)
  {
    dev_prop[idx++]  >>  tsDepth;
  }
  else
  {
    idx++;
  }
  this->m_dataTrt.timestampHistoriesDepth = tsDepth;

  // channels dataset flags
  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    bool dataset = true; // default value: all channels are stored
	  if (dev_prop[idx].is_empty()==false)
	  {
      dev_prop[idx++]  >>  dataset;
    }
    else
    {
      idx++;
    }
    yat::uint16 chan_id = get_channel_id_from_label(this->m_acqDefinition.activeChannels[chan].label);
    this->m_acquisitionParam.datasetFlags[chan_id] = dataset;
  }
}

// ======================================================================
// AcqMode::needUserDataProxy
// ======================================================================
bool AcqMode::needUserDataProxy()
{
  bool isAnyProxy = false;

  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.hasUserData)
    {
      isAnyProxy = isAnyProxy ||
        (this->m_acqDefinition.activeChannels[chan].userDataDesc.gainType == USER_DATA_PROXY) ||
        (this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Type == USER_DATA_PROXY) ||
        (this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Type == USER_DATA_PROXY);
    }
  }

  return isAnyProxy;
}

#if !defined __TANGO_EVENTS__
// ======================================================================
// AcqMode::defineUserDataProxies
// ======================================================================
void AcqMode::defineUserDataProxies()
  throw (Tango::DevFailed)
{
  // Creates the device proxies from acquisition config, for each channel
  
  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    // Is there a gain device proxy ?
    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.gainType == USER_DATA_PROXY)
    {
      UserDataProxy proxy;
      proxy.deviceName = this->m_acqDefinition.activeChannels[chan].userDataDesc.gainProxy.deviceName;
      proxy.attributeName = this->m_acqDefinition.activeChannels[chan].userDataDesc.gainProxy.attributeName;

      if ( (!proxy.deviceName.empty()) &&
           (!proxy.attributeName.empty()) )
      {
        try
        {
          DEBUG_STREAM << "Init user data monitored device for channel " << chan << " gain parameter" << std::endl;
          std::string prxId = kPROXY_USER_DATA_GAIN + yat::XString<size_t>::to_string(chan);
          this->initUserDataDeviceProxy(prxId, proxy);
          this->m_acqDefinition.activeChannels[chan].userDataDesc.gainProxy.attributeType = proxy.attributeType;
        }
        catch (Tango::DevFailed & df)
        {
          ERROR_STREAM << "AcqMode::defineUserDataProxies caught DevFailed: " << df << std::endl;
          RETHROW_DEVFAILED(df,
                            _CPTC("DEVICE_ERROR"),
                            _CPTC("Failed to init monitored device task (caught DevFailed)!"),
                            _CPTC("AcqMode::defineUserDataProxies"));
        }
        catch (...)
        {
          ERROR_STREAM << "AcqMode::defineUserDataProxies caught [...]" << std::endl;
          THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                          _CPTC("Failed to init monitored device task (caught [...])!"),
                          _CPTC("AcqMode::defineUserDataProxies"));
        }
      }
    }

    // Is there a offset1 device proxy ?
    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Type == USER_DATA_PROXY)
    {
      UserDataProxy proxy;
      proxy.deviceName = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Proxy.deviceName;
      proxy.attributeName = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Proxy.attributeName;

      if ( (!proxy.deviceName.empty()) &&
           (!proxy.attributeName.empty()) )
      {
        try
        {
          DEBUG_STREAM << "Init user data monitored device for channel " << chan << " offset1 parameter" << std::endl;
          std::string prxId = kPROXY_USER_DATA_OFFSET1 + yat::XString<size_t>::to_string(chan);
          this->initUserDataDeviceProxy(prxId, proxy);
          this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Proxy.attributeType = proxy.attributeType;
        }
        catch (Tango::DevFailed & df)
        {
          ERROR_STREAM << "AcqMode::defineUserDataProxies caught DevFailed: " << df << std::endl;
          RETHROW_DEVFAILED(df,
                            _CPTC("DEVICE_ERROR"),
                            _CPTC("Failed to init monitored device task (caught DevFailed)!"),
                            _CPTC("AcqMode::defineUserDataProxies"));
        }
        catch (...)
        {
          ERROR_STREAM << "AcqMode::defineUserDataProxies caught [...]" << std::endl;
          THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                          _CPTC("Failed to init monitored device task (caught [...])!"),
                          _CPTC("AcqMode::defineUserDataProxies"));
        }
      }
    }

    // Is there a offset2 device proxy ?
    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Type == USER_DATA_PROXY)
    {
      UserDataProxy proxy;
      proxy.deviceName = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Proxy.deviceName;
      proxy.attributeName = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Proxy.attributeName;

      if ( (!proxy.deviceName.empty()) &&
           (!proxy.attributeName.empty()) )
      {
        try
        {
          DEBUG_STREAM << "Init user data monitored device for channel " << chan << " offset2 parameter" << std::endl;
          std::string prxId = kPROXY_USER_DATA_OFFSET2 + yat::XString<size_t>::to_string(chan);
          this->initUserDataDeviceProxy(prxId, proxy);
          this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Proxy.attributeType = proxy.attributeType;
        }
        catch (Tango::DevFailed & df)
        {
          ERROR_STREAM << "AcqMode::defineUserDataProxies caught DevFailed: " << df << std::endl;
          RETHROW_DEVFAILED(df,
                            _CPTC("DEVICE_ERROR"),
                            _CPTC("Failed to init monitored device task (caught DevFailed)!"),
                            _CPTC("AcqMode::defineUserDataProxies"));
        }
        catch (...)
        {
          ERROR_STREAM << "AcqMode::defineUserDataProxies caught [...]" << std::endl;
          THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                          _CPTC("Failed to init monitored device task (caught [...])!"),
                          _CPTC("AcqMode::defineUserDataProxies"));
        }
      }
    }
  }
}
#endif

// ======================================================================
// AcqMode::setUserDataGain
// ======================================================================
void AcqMode::setUserDataGain(yat::uint16 chan_idx, double gain, bool isRunning)
  throw (Tango::DevFailed)
{
  // check if new gain is not null
  if (gain == 0.0)
  {
    this->m_errorMsg = "Set user data gain failed: gain is null!";
    this->m_errorOccurred = true;

    // consider that gain is null => error
    ERROR_STREAM << "Set user data gain failed: gain is null!" << endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                    _CPTC("Set user data gain failed: gain is null!"),
                    _CPTC("AcqMode::setUserDataGain"));    
  }

  if (isRunning)
  {
    // acquisition running: update asl config
    // get current values and only replace gain
    double currentGain, currentOffset1, currentOffset2;
    yat::uint16 chan_id = this->m_acqDefinition.activeChannels[chan_idx].number;
    this->configuration().get_user_data_parameters(chan_id, currentGain, 
      currentOffset1, currentOffset2);
    set_user_data_parameters(chan_id, gain, currentOffset1, currentOffset2);
    this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.gain = gain;
  }
  else
  {
    // acquisition not running: just update internal structure
    this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.gain = gain;
  }
} 

// ======================================================================
// AcqMode::getUserDataGain
// ======================================================================
double AcqMode::getUserDataGain(yat::uint16 chan_idx)
  throw (Tango::DevFailed)
{
#if !defined __TANGO_EVENTS__
  if (this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.gainType == USER_DATA_PROXY)
  {
//- Macro for type mapping:
#define POLLING_GAIN_ATTR_TYPE(TYPEID, YAT_TYPE)        \
    case TYPEID:                                        \
      {                                                 \
        YAT_TYPE value;                                 \
        this->m_udDeviceTaskList[prxId]->get_monitored_attribute( \
          this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.gainProxy.attributeName).get_value(value); \
        this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.gain = static_cast<double>(value);       \
      }                                                 \
      break;

    // get current gain value from monitored device
    try
    {
      std::string prxId = kPROXY_USER_DATA_GAIN + yat::XString<size_t>::to_string(chan_idx);
      // get polled attribute according to type
      switch (this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.gainProxy.attributeType)
      {
        POLLING_GAIN_ATTR_TYPE(Tango::DEV_SHORT, yat::int16);
        POLLING_GAIN_ATTR_TYPE(Tango::DEV_LONG, yat::int32);
        POLLING_GAIN_ATTR_TYPE(Tango::DEV_DOUBLE, double);
        POLLING_GAIN_ATTR_TYPE(Tango::DEV_FLOAT, float);
        POLLING_GAIN_ATTR_TYPE(Tango::DEV_USHORT, yat::uint16);
        POLLING_GAIN_ATTR_TYPE(Tango::DEV_ULONG, yat::uint32);
        POLLING_GAIN_ATTR_TYPE(Tango::DEV_LONG64, yat::int64);
        POLLING_GAIN_ATTR_TYPE(Tango::DEV_ULONG64, yat::uint64);
        default:
          THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
            _CPTC("Failed to get monitored gain attribute value: not supported type!"),
            _CPTC("AcqMode::getUserDataGain"));
        break;
      }
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << "AcqMode::getUserDataGain caught DevFailed: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("DEVICE_ERROR"),
                        _CPTC("Failed to get monitored gain attribute value (caught DevFailed)!"),
                        _CPTC("AcqMode::getUserDataGain"));
    }
    catch (...)
    {
      ERROR_STREAM << "AcqMode::getUserDataGain caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Failed to get monitored gain attribute value (caught [...])!"),
                      _CPTC("AcqMode::getUserDataGain"));
    }
  }
#endif

  return this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.gain;
} 

// ======================================================================
// AcqMode::setUserDataOffset1
// ======================================================================
void AcqMode::setUserDataOffset1(yat::uint16 chan_idx, double offset, bool isRunning)
  throw (Tango::DevFailed)
{     
  if (isRunning)
  {
    // acquisition running: update asl config
    // get current values and only replace offset1
    double currentGain, currentOffset1, currentOffset2;
    yat::uint16 chan_id = this->m_acqDefinition.activeChannels[chan_idx].number;
    this->configuration().get_user_data_parameters (chan_id, currentGain, 
      currentOffset1, currentOffset2);
    set_user_data_parameters(chan_id, currentGain, offset, currentOffset2);
    this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset1 = offset;
  }
  else
  {
    // acquisition not running: just update internal structure
    this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset1 = offset;
  }
}

// ======================================================================
// AcqMode::getUserDataOffset1
// ======================================================================
double AcqMode::getUserDataOffset1(yat::uint16 chan_idx)
  throw (Tango::DevFailed)
{
#if !defined __TANGO_EVENTS__
  if (this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset1Type == USER_DATA_PROXY)
  {
//- Macro for type mapping:
#define POLLING_OFFSET1_ATTR_TYPE(TYPEID, YAT_TYPE)     \
    case TYPEID:                                        \
      {                                                 \
        YAT_TYPE value;                                 \
        this->m_udDeviceTaskList[prxId]->get_monitored_attribute( \
          this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset1Proxy.attributeName).get_value(value); \
        this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset1 = static_cast<double>(value);       \
      }                                                 \
      break;

    // get current offset1 value from monitored device
    try
    {
      std::string prxId = kPROXY_USER_DATA_OFFSET1 + yat::XString<size_t>::to_string(chan_idx);
      // get polled attribute according to type
      switch (this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset1Proxy.attributeType)
      {
        POLLING_OFFSET1_ATTR_TYPE(Tango::DEV_SHORT, yat::int16);
        POLLING_OFFSET1_ATTR_TYPE(Tango::DEV_LONG, yat::int32);
        POLLING_OFFSET1_ATTR_TYPE(Tango::DEV_DOUBLE, double);
        POLLING_OFFSET1_ATTR_TYPE(Tango::DEV_FLOAT, float);
        POLLING_OFFSET1_ATTR_TYPE(Tango::DEV_USHORT, yat::uint16);
        POLLING_OFFSET1_ATTR_TYPE(Tango::DEV_ULONG, yat::uint32);
        POLLING_OFFSET1_ATTR_TYPE(Tango::DEV_LONG64, yat::int64);
        POLLING_OFFSET1_ATTR_TYPE(Tango::DEV_ULONG64, yat::uint64);
        default:
          THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
            _CPTC("Failed to get monitored offset1 attribute value: not supported type!"),
            _CPTC("AcqMode::getUserDataOffset1"));
        break;
      }
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << "AcqMode::getUserDataOffset1 caught DevFailed: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("DEVICE_ERROR"),
                        _CPTC("Failed to get monitored offset1 attribute value (caught DevFailed)!"),
                        _CPTC("AcqMode::getUserDataOffset1"));
    }
    catch (...)
    {
      ERROR_STREAM << "AcqMode::getUserDataOffset1 caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Failed to get monitored offset1 attribute value (caught [...])!"),
                      _CPTC("AcqMode::getUserDataOffset1"));
    }
  }
#endif

  return this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset1;
} 

// ======================================================================
// AcqMode::setUserDataOffset2
// ======================================================================
void AcqMode::setUserDataOffset2(yat::uint16 chan_idx, double offset, bool isRunning)
  throw (Tango::DevFailed)
{
  if (isRunning)
  {
    // acquisition running: update asl config
    // get current values and only replace offset2
    double currentGain, currentOffset1, currentOffset2;
    yat::uint16 chan_id = this->m_acqDefinition.activeChannels[chan_idx].number;
    this->configuration().get_user_data_parameters (chan_id, currentGain, 
      currentOffset1, currentOffset2);
    set_user_data_parameters(chan_id, currentGain, currentOffset1, offset);
    this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset2 = offset;
  }
  else
  {
    // acquisition not running: just update internal structure
    this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset2 = offset;
  }
}


// ======================================================================
// AcqMode::getUserDataOffset2
// ======================================================================
double AcqMode::getUserDataOffset2(yat::uint16 chan_idx)
  throw (Tango::DevFailed)
{
#if !defined __TANGO_EVENTS__
  if (this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset2Type == USER_DATA_PROXY)
  {
//- Macro for type mapping:
#define POLLING_OFFSET2_ATTR_TYPE(TYPEID, YAT_TYPE)     \
    case TYPEID:                                        \
      {                                                 \
        YAT_TYPE value;                                 \
        this->m_udDeviceTaskList[prxId]->get_monitored_attribute( \
          this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset2Proxy.attributeName).get_value(value); \
        this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset2 = static_cast<double>(value);       \
      }                                                 \
      break;

    // get current offset2 value from monitored device
    try
    {
      std::string prxId = kPROXY_USER_DATA_OFFSET2 + yat::XString<size_t>::to_string(chan_idx);
      // get polled attribute according to type
      switch (this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset2Proxy.attributeType)
      {
        POLLING_OFFSET2_ATTR_TYPE(Tango::DEV_SHORT, yat::int16);
        POLLING_OFFSET2_ATTR_TYPE(Tango::DEV_LONG, yat::int32);
        POLLING_OFFSET2_ATTR_TYPE(Tango::DEV_DOUBLE, double);
        POLLING_OFFSET2_ATTR_TYPE(Tango::DEV_FLOAT, float);
        POLLING_OFFSET2_ATTR_TYPE(Tango::DEV_USHORT, yat::uint16);
        POLLING_OFFSET2_ATTR_TYPE(Tango::DEV_ULONG, yat::uint32);
        POLLING_OFFSET2_ATTR_TYPE(Tango::DEV_LONG64, yat::int64);
        POLLING_OFFSET2_ATTR_TYPE(Tango::DEV_ULONG64, yat::uint64);
        default:
          THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
            _CPTC("Failed to get monitored offset2 attribute value: not supported type!"),
            _CPTC("AcqMode::getUserDataOffset2"));
        break;
      }
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << "AcqMode::getUserDataOffset2 caught DevFailed: " << df << std::endl;
      RETHROW_DEVFAILED(df,
                        _CPTC("DEVICE_ERROR"),
                        _CPTC("Failed to get monitored offset2 attribute value (caught DevFailed)!"),
                        _CPTC("AcqMode::getUserDataOffset2"));
    }
    catch (...)
    {
      ERROR_STREAM << "AcqMode::getUserDataOffset2 caught [...]" << std::endl;
      THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                      _CPTC("Failed to get monitored offset2 attribute value (caught [...])!"),
                      _CPTC("AcqMode::getUserDataOffset2"));
    }
  }
#endif

  return this->m_acqDefinition.activeChannels[chan_idx].userDataDesc.offset2;
} 

#if !defined __TANGO_EVENTS__
// ======================================================================
// AcqMode::initUserDataDeviceProxy
// ======================================================================
void AcqMode::initUserDataDeviceProxy(std::string proxy_type, UserDataProxy& proxy)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "AcqMode::initUserDataDeviceProxy - Create monitored device task on: " 
    << proxy.deviceName << "::" << proxy.attributeName << std::endl;
  
  yat4tango::MonitoredDeviceTask * deviceTask = new yat4tango::MonitoredDeviceTask(proxy.deviceName, this->m_hostDev);

  if (!deviceTask)
  {
    yat::OSStream oss;
    oss << "Failed to create MonitoredDeviceTask for Device" << proxy.deviceName;
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                    oss.str().c_str(), 
                    _CPTC("AcqMode::initUserDataDeviceProxy"));
  }

  try
  {
    deviceTask->add_monitored_attribute(proxy.attributeName);
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AcqMode::initUserDataDeviceProxy caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("DEVICE_ERROR"),
                      _CPTC("Failed to configure monitored device task (caught DevFailed)!"),
                      _CPTC("AcqMode::initUserDataDeviceProxy"));
  }
  catch (...)
  {
    ERROR_STREAM << "AcqMode::initUserDataDeviceProxy caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to configure monitored device task (caught [...])!"),
                    _CPTC("AcqMode::initUserDataDeviceProxy"));
  }

  //- create proxy on Device to get attribute type
  try
  {
    Tango::DeviceProxy * device_proxy = new (std::nothrow) Tango::DeviceProxy(proxy.deviceName);
      
    if (device_proxy == 0)
        throw std::bad_alloc();

    proxy.attributeType = device_proxy->get_attribute_config(proxy.attributeName).data_type;
    delete device_proxy;
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AcqMode::initUserDataDeviceProxy caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("DEVICE_ERROR"),
                      _CPTC("Failed to create proxy on external Device (caught DevFailed)!"),
                      _CPTC("AcqMode::initUserDataDeviceProxy"));
  }
  catch (...)
  {
    ERROR_STREAM << "AcqMode::initUserDataDeviceProxy caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to create proxy on external Device (caught [...])!"),
                    _CPTC("AcqMode::initUserDataDeviceProxy"));
  }

  //- set polling period and start polling device
  try
  {
    deviceTask->set_polling_period(this->m_userDataProxyPollingPeriod);

    DEBUG_STREAM << "Start polling..." << std::endl;
    deviceTask->start();
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "AcqMode::initUserDataDeviceProxy caught DevFailed: " << df << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("DEVICE_ERROR"),
                      _CPTC("Failed to start monitored device task (caught DevFailed)!"),
                      _CPTC("AcqMode::initUserDataDeviceProxy"));
  }
  catch (...)
  {
    ERROR_STREAM << "AcqMode::initUserDataDeviceProxy caught [...]" << std::endl;
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Failed to start monitored device task (caught [...])!"),
                    _CPTC("AcqMode::initUserDataDeviceProxy"));
  }
  
  // add device task in list
  this->m_udDeviceTaskList.insert( UserDataDevices_pair_t(proxy_type, deviceTask) );
}
#endif

// ======================================================================
// AcqMode::stampTrigger_c
// ======================================================================
void AcqMode::stampTrigger_c()
{
  //- Associates a timestamp to the received trigger
  time_t ltime;
  
  //- Get UNIX-style time and display as string.
  time( &ltime );
  std::string triggerTimestamp (ctime( &ltime ));
  this->m_acqData.triggerTimestamp_c = triggerTimestamp;
}

// ======================================================================
// AcqMode::stampBuffer_s
// ======================================================================
void AcqMode::stampBuffer_s()
{
  //- Associates a timestamp to the received buffer
  yat::CurrentTime l_date;

  // get current date
  this->m_acqData.bufferTimestamp_s = l_date.double_unix();
  
  // compute relative datation
  if (this->m_firstTimestamp == 0) // first value
  {
    this->m_firstTimestamp = this->m_acqData.bufferTimestamp_s;
    this->m_acqData.relBuffTimestamp_s = 0;
  }
  else
    this->m_acqData.relBuffTimestamp_s = this->m_acqData.bufferTimestamp_s - this->m_firstTimestamp;

  // store in buffers
  if (this->m_dynAcqData.buffTimestamps)
    this->m_dynAcqData.buffTimestamps->push(this->m_acqData.bufferTimestamp_s);
  if (this->m_dynAcqData.relativeBuffTimes)
    this->m_dynAcqData.relativeBuffTimes->push(this->m_acqData.relBuffTimestamp_s);
}

// ======================================================================
// AcqMode::computeDataHistory
// ======================================================================
void AcqMode::computeDataHistory(unsigned int chan_id)
  throw (Tango::DevFailed)
{
  // compute data history for one channel (if required && if any)

  if ((this->m_dataTrt.isDataScaled) &&
      (this->m_dataTrt.isDataHistorized) &&
      (this->m_acqData.dataCounter != 0))
  {
    //DEBUG_STREAM << "AcqMode::computeDataHistory - chan id=" << chan_id << std::endl;
  
    //- get active channels
    unsigned int nbac = this->m_daqConfig.num_active_channels();

    if (nbac == 0)
    {
      this->m_errorMsg = "Internal error. Number of active channels is null.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "Number of active channels is null!" << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                      _CPTC("Error while trying to get data history - see logs for details"), 
                      _CPTC("AcqMode::computeDataHistory"));
    }
	
    // get channel index from channel list
    unsigned int chan_idx = 0;
    bool config_found = false;
    for (size_t idx = 0; idx < this->m_acqDefinition.activeChannels.size(); idx++)
    {
      if (this->m_acqDefinition.activeChannels[idx].number == chan_id)
      {
        config_found = true;
        chan_idx = idx;
        break;
      }
    }

    if (!config_found)
    {
      ERROR_STREAM << "Data reading: channel id not found!" << endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                      _CPTC("Data reading: error while getting channel id!"),
                      _CPTC("AcqMode::computeDataHistory"));
    }

    //- get data history from ASL
    asl::AIScaledData * scaled_history_buffer = NULL;
    scaled_history_buffer = this->scaled_history();
	
    //DEBUG_STREAM << "AcqMode::computeDataHistory - successfully took scaled history from asl" << std::endl;
	
    //- update data structure
    if (scaled_history_buffer != NULL)
    {
      //- get scaled data history 
      double * history_buffer_addr = scaled_history_buffer->base();
      size_t history_buffer_depth = scaled_history_buffer->depth() / nbac;

      //- get local data associated to current channel
      if (this->m_dynAcqData.dataHistory.count(chan_id) == 0)
      {
        this->m_errorMsg = "Internal error. Bad channel id for data history.";
        this->m_errorOccurred = true;

        // bad id ==> fatal error
        ERROR_STREAM << "Bad channel id for local data history:" << chan_id << std::endl;
        THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                        _CPTC("Error while trying to get data history - see logs for details"), 
                        _CPTC("AcqMode::computeDataHistory"));
      }

      ScaledHistorizedData_t & bHistorized = this->m_dynAcqData.dataHistory[chan_id];
      
      //- history_depth values to store per handle_input() received
      for (size_t buffIdx = 0; buffIdx < history_buffer_depth; buffIdx++)
      {
        bHistorized[buffIdx] = history_buffer_addr[buffIdx * nbac + chan_idx];
      }

      //- delete ASL buffer
      delete scaled_history_buffer;
      DEBUG_STREAM << "AcqMode::computeDataHistory - end of history data treatment for chan=" << chan_id << std::endl;
    }
    else
    {
      this->m_errorMsg = "Internal error. History buffer is empty.";
      this->m_errorOccurred = true;

      // bad id ==> fatal error
      ERROR_STREAM << "Internal error. History buffer from ASL is empty for chan id:" << chan_id << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                      _CPTC("Error while trying to get data history - see logs for details"), 
                      _CPTC("AcqMode::computeDataHistory"));
    }
  }
}

// ======================================================================
// AcqMode::computeWholeDataHistory
// ======================================================================
void AcqMode::computeWholeDataHistory()
  throw (Tango::DevFailed)
{
  // compute data history for all channels (if required)

  if ((this->m_dataTrt.isDataScaled) &&
      (this->m_dataTrt.isDataHistorized) && 
      (this->m_acqData.dataCounter != 0))
  {
    DEBUG_STREAM << "AcqMode::computeWholeDataHistory" << std::endl;
  
    //- get active channels
    const asl::ActiveAIChannels& ac = this->m_daqConfig.get_active_channels();
    unsigned int nbac = this->m_daqConfig.num_active_channels();
    unsigned int chan_id = 0; // active channel id (for data storage)

    if (nbac == 0)
    {
      this->m_errorMsg = "Internal error. Number of active channels is null.";
      this->m_errorOccurred = true;

      ERROR_STREAM << "Number of active channels is null!" << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                      _CPTC("Error while trying to get data history - see logs for details"), 
                      _CPTC("AcqMode::computeWholeDataHistory"));
    }

    //- get data history from ASL
    asl::AIScaledData * scaled_history_buffer = NULL;
    scaled_history_buffer = this->scaled_history();
	
    DEBUG_STREAM << "AcqMode::computeWholeDataHistory - successfully took scaled history from asl" << std::endl;
	
    //- update data structure
    if (scaled_history_buffer != NULL)
    {
      yat::AutoMutex<> guard(this->m_dataLock);

      //- get scaled data history 
      double * history_buffer_addr = scaled_history_buffer->base();
      size_t history_buffer_depth = scaled_history_buffer->depth() / nbac;

      //- get local data associated to each channel
      for (unsigned int chan_idx = 0;  chan_idx < ac.size();  chan_idx++) 
      {
        chan_id = ac[chan_idx].id;

        if (this->m_dynAcqData.dataHistory.count(chan_id) == 0)
        {
          this->m_errorMsg = "Internal error. Bad channel id for data history.";
          this->m_errorOccurred = true;

          // bad id ==> fatal error
          ERROR_STREAM << "Bad channel id for local data history:" << chan_id << std::endl;
          THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                          _CPTC("Error while trying to get data history - see logs for details"), 
                          _CPTC("AcqMode::computeWholeDataHistory"));
        }

        ScaledHistorizedData_t & bHistorized = this->m_dynAcqData.dataHistory[chan_id];
        
        //- history_depth values to store per handle_input() received
        for (size_t buffIdx = 0; buffIdx < history_buffer_depth; buffIdx++)
        {
          bHistorized[buffIdx] = history_buffer_addr[buffIdx * nbac + chan_idx];
        }
      }

      //- delete ASL buffer
      delete scaled_history_buffer;
   
      DEBUG_STREAM << "AcqMode::computeWholeDataHistory - end of history data treatment" << std::endl;
    }
    else
    {
      this->m_errorMsg = "Internal error. History buffer is empty.";
      this->m_errorOccurred = true;

      // bad id ==> fatal error
      ERROR_STREAM << "Internal error. History buffer from ASL is empty." << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
                      _CPTC("Error while trying to get data history - see logs for details"), 
                      _CPTC("AcqMode::computeWholeDataHistory"));
    }
  }
}

// ======================================================================
// AcqMode::setBoolDataMask
// ======================================================================
void AcqMode::setBoolDataMask(std::string mask)
  throw (Tango::DevFailed)
{
  // set bool data mask size to data buffer length and initialize to true:
  this->m_dataTrt.boolDataMask.clear();
  this->m_dataTrt.boolDataMask.resize(this->m_daqConfig.get_buffer_depth(), true);

  //- Transforms the {t1;t2} slots of integration period 
  //- into {n1;n2} index intervals in data buffer

  //- Extract time slots from attribute content
  TimeSlotMask_t tsList;
  tsList = this->parseTimeSlotMask(mask);
  
  for (size_t c = 0; c < tsList.size(); c++)
  {
    //- check that 0 <= t1 < t2
    if ((tsList[c].t1 < 0.0) ||
        (tsList[c].t2 < tsList[c].t1))
    {
      // fatal error
      ERROR_STREAM << "Bad data mask values: check time values!" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to write data mask: bad time values"),
                      _CPTC("AcqMode::setBoolDataMask"));
    }

    // compute {n1;n2} interval
    yat::uint32 n1 = 0;
    yat::uint32 n2 = 1;
    yat::uint32 temp = 0;

    temp = (yat::uint32)floor(tsList[c].t1 * this->m_acquisitionParam.frequency 
      * kMILLISECONDS_TO_SECONDS);
    
    // do nothing if time slot is outside the integration period
    if (temp <= this->m_dataTrt.boolDataMask.size())
    {
      if (temp > 0)
      {
        n1 = temp - 1;
      }

      n2 = (yat::uint32)floor(tsList[c].t2 * this->m_acquisitionParam.frequency
        * kMILLISECONDS_TO_SECONDS);  

      if (n2 > this->m_dataTrt.boolDataMask.size())
      {
        n2 = this->m_dataTrt.boolDataMask.size();
      }

      DEBUG_STREAM << "setBoolDataMask: set data mask interval n1 = " << n1 << " -- n2 = " << n2 << std::endl;
      // set the boolean mask to false in the {n1;n2} interval
      for (size_t idx = n1; idx < n2; idx++)
      {
        this->m_dataTrt.boolDataMask[idx] = false;
      }
    }
  }
}

// ======================================================================
// AcqMode::parseTimeSlotMask
// ======================================================================
TimeSlotMask_t AcqMode::parseTimeSlotMask(std::string mask)
  throw (Tango::DevFailed)
{
  TimeSlotMask_t tsList;
  
  //- In mask, time slots format is: {t1;t2}:{t3;t4}:...
  std::string interval_sep = ":";
  yat::StringTokenizer st(mask, interval_sep);

	//- result : the split line 
  std::vector<std::string> tokenList;

	while (st.has_more_tokens()) // while there is a remaining token
	{
    std::string token_str = st.next_token();
		tokenList.push_back(token_str);
	}

  for (size_t c = 0; c < tokenList.size(); c++)
  {
    double t1;
    double t2;

    yat::String ys(tokenList[c]);
    if (ys.remove_enclosure('{','}'))
    {
      yat::String t1_str;
      if (ys.extract_token(';', &t1_str) != yat::String::SEP_FOUND)
      {
        // fatal error
        ERROR_STREAM << "Bad data mask format: use correct separator between values!" << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                        _CPTC("Failed to extract data mask: bad separator format"),
                        _CPTC("AcqMode::parseTimeSlotMask"));
      }

      try
      {
        t1 = yat::XString<double>::to_num(t1_str);
        t2 = yat::XString<double>::to_num(ys);
      }
      catch (const yat::Exception& ye)
      {
        // bad double value ==> fatal error
        yat::OSStream oss;
        // fatal error
        ERROR_STREAM << "Bad data mask time format!" << std::endl;
        THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                        _CPTC("Failed to extract data mask: bad time format"),
                        _CPTC("AcqMode::parseTimeSlotMask"));
      }

      TimeSlot ts;
      ts.t1 = t1;
      ts.t2 = t2;
      tsList.push_back(ts);
    }
    else
    {
      // fatal error
      ERROR_STREAM << "Bad data mask format: use correct enclosure!" << std::endl;
      THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"),
                      _CPTC("Failed to extract data mask: bad enclosure format"),
                      _CPTC("AcqMode::parseTimeSlotMask"));
    }
  }

  return tsList;
}

// ======================================================================
// AcqMode::pollUserDataProxy
// ======================================================================
void AcqMode::pollUserDataProxy(bool polling)
  throw (Tango::DevFailed)
{
  // parse user data proxy list
  UserDataDevices_it_t it;
  for (it = this->m_udDeviceTaskList.begin();
       it != this->m_udDeviceTaskList.end();
       ++it)
  {
    if (it->second)
    {
      if (polling)
      {
        // restart user data proxies polling
        (it->second)->resume();
      }
      else
      {
        // suspend user data proxies polling
        (it->second)->suspend();
      }
    }
  }
}

// ======================================================================
// AcqMode::cleanInterface
// ======================================================================
void AcqMode::cleanInterface()
  throw (Tango::DevFailed)
{
  // remove all dynamic attributes of the current acquisition mode
  try
  {
    if (this->m_dynAttrManager)
    {
      this->m_dynAttrManager->remove_attributes();
    }
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << "Error while trying to remove dynamic attributes (caught DevFailed)!" << std::endl;
    RETHROW_DEVFAILED(df,
                      _CPTC("DEVICE_ERROR"),
                      _CPTC("Failed to remove dynamic attributes (caught DevFailed)!"),
                      _CPTC("AcqMode::cleanInterface"));
  }
  catch (...)
  {
    ERROR_STREAM << "Error while trying to remove dynamic attributes!" << std::endl;
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                    _CPTC("Failed to remove dynamic attributes"),
                    _CPTC("AcqMode::cleanInterface"));
  }
}

// ======================================================================
// AcqMode::getNxDataItems
// ======================================================================
std::vector<std::string> AcqMode::getNxDataItems()
{
  std::vector<std::string> nxDataItems;
  
  // get Nexus item list
  nxItemList_t itemList = this->getNxItemList();

  // extract Nexus id names and push them in returned list
  for (size_t idx = 0; idx < itemList.size(); idx++)
  {
     std::string itemName = itemList.at(idx).name;
     nxDataItems.push_back(itemName);
  }

  return nxDataItems;
}

// ======================================================================
// AcqMode::updateNxDataset
// ======================================================================
void AcqMode::updateNxDataset(yat::uint16 chan_nb, std::string dataset)
  throw (Tango::DevFailed)
{
  // check channel id
  if (this->m_datasetNames.count(chan_nb) == 0)
  {
    // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Undefined channel id: " << chan_nb;
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"), 
                      oss.str().c_str(), 
                      _CPTC("AcqMode::updateNxDataset")); 
  }

  // extract dataset type & name
  std::string datasetType = "";
  std::string datasetName = dataset; 

  // get dataset type (if specified)
  std::size_t pos = dataset.find(kDATASET_TYPE_SEP); 
  if (pos != std::string::npos)
  {
    // extract dataset type
    datasetType = dataset.substr(0, pos);
    datasetName = dataset.substr(pos + 1);

    if ((datasetType.compare(kKEY_DATA_NX_DATA_TYPE_RAW) != 0) &&
        (datasetType.compare(kKEY_DATA_NX_DATA_TYPE_SCALED) != 0) &&
        (datasetType.compare(kKEY_DATA_NX_DATA_TYPE_AV) != 0) &&
        (datasetType.compare(kKEY_DATA_NX_DATA_TYPE_RMS) != 0) &&
        (datasetType.compare(kKEY_DATA_NX_DATA_TYPE_PEAK) != 0) &&
        (datasetType.compare(kKEY_DATA_NX_DATA_TYPE_USER) != 0) &&
        (datasetType.compare(kKEY_DATA_NX_DATA_TYPE_DATA_HIST) != 0))
    {
      // bad value ==> fatal error
      yat::OSStream oss;
      oss << "Bad Nexus data type value: " << datasetType;
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"), 
                      oss.str().c_str(), 
                      _CPTC("AcqMode::updateNxDataset")); 
    }
	
    // update internal map
    m_datasetNames[chan_nb][datasetType] = datasetName;	
  }
  else
  {
    // default behaviour: change channel label for all dataset types
    // AVERAGE:
    m_datasetNames[chan_nb][kKEY_DATA_NX_DATA_TYPE_AV] = kNEXUS_PREFIX_AV_ITEM + datasetName;
    // RMS:
    m_datasetNames[chan_nb][kKEY_DATA_NX_DATA_TYPE_RMS] = kNEXUS_PREFIX_RMS_ITEM + datasetName;
    // PEAK:
    m_datasetNames[chan_nb][kKEY_DATA_NX_DATA_TYPE_PEAK] = kNEXUS_PREFIX_PEAK_ITEM + datasetName;
    // RAW:
    m_datasetNames[chan_nb][kKEY_DATA_NX_DATA_TYPE_RAW] = datasetName + kNEXUS_POSTFIX_RAW_ITEM;
    // SCALED:
    m_datasetNames[chan_nb][kKEY_DATA_NX_DATA_TYPE_SCALED] = datasetName;
    // DATA HISTORY:
    m_datasetNames[chan_nb][kKEY_DATA_NX_DATA_TYPE_DATA_HIST] = kNEXUS_PREFIX_DATA_HIST_ITEM + datasetName;
    // USER DATA:
    m_datasetNames[chan_nb][kKEY_DATA_NX_DATA_TYPE_USER] = kNEXUS_PREFIX_USER_DATA_ITEM + datasetName;
  }
}

#ifdef __TANGO_EVENTS__
// ======================================================================
// UserDataGainEventCallBack::UserDataGainEventCallBack
// ======================================================================
UserDataGainEventCallBack::UserDataGainEventCallBack(AcqMode * acq_mode)
:Tango::CallBack()
{
  // TODO user data event : define one event per channel for gain, offset1 & 2

  this->m_acqMode = acq_mode;
}

// ======================================================================
// UserDataOffset1EventCallBack::UserDataOffset1EventCallBack
// ======================================================================
UserDataOffset1EventCallBack::UserDataOffset1EventCallBack(AcqMode * acq_mode)
:Tango::CallBack()
{
  // TODO user data event : define one event per channel for gain, offset1 & 2

  this->m_acqMode = acq_mode;
}

// ======================================================================
// UserDataOffset2EventCallBack::UserDataOffset2EventCallBack
// ======================================================================
UserDataOffset2EventCallBack::UserDataOffset2EventCallBack(AcqMode * acq_mode)
:Tango::CallBack()
{
  // TODO user data event : define one event per channel for gain, offset1 & 2

  this->m_acqMode = acq_mode;
}

// ======================================================================
// UserDataGainEventCallBack::push_event
// ======================================================================
void UserDataGainEventCallBack::push_event(Tango::EventData *myevent)
{
  // TODO user data event : define one event per channel for gain, offset1 & 2
  double double_value = yat::IEEE_NAN;
  
  try
  {
    std::cout << "UserDataGainEventCallBack::push_event(): called attribute "
        << myevent->attr_name
        << " event "
        << myevent->event
        << " (err="
        << myevent->err
        << ")" << std::endl;

    if (!myevent->err)
    {
      // get attribute according to type
      switch (this->m_dataTrt.userDefinedParam.gainProxy.attributeType)
      {
        EVENT_ATTR_TYPE(Tango::DEV_SHORT, yat::int16);
        EVENT_ATTR_TYPE(Tango::DEV_LONG, yat::int32);
        EVENT_ATTR_TYPE(Tango::DEV_DOUBLE, double);
        EVENT_ATTR_TYPE(Tango::DEV_FLOAT, float);
        EVENT_ATTR_TYPE(Tango::DEV_USHORT, yat::uint16);
        EVENT_ATTR_TYPE(Tango::DEV_ULONG, yat::uint32);
        EVENT_ATTR_TYPE(Tango::DEV_LONG64, yat::int64);
        EVENT_ATTR_TYPE(Tango::DEV_ULONG64, yat::uint64);
        default:
        break;
      }

      std::cout << "double value = "
                << double_value
                << std::endl;
      
      //- update gain according to state
      if (this->m_acqMode->getAcqState() == Tango::RUNNING)
      {
        this->m_acqMode->setUserDataGain(double_value, true);
      }
      else
      {
        this->m_acqMode->setUserDataGain(double_value, false);
      }
    }
  }
  catch (...)
  {
    std::cout << "UserDataGainEventCallBack::push_event(): could not extract data !\n";
  }
}

// ======================================================================
// UserDataOffset1EventCallBack::push_event
// ======================================================================
void UserDataOffset1EventCallBack::push_event(Tango::EventData *myevent)
{
  // TODO user data event : define one event per channel for gain, offset1 & 2

  double double_value = yat::IEEE_NAN;
  
  try
  {
    std::cout << "UserDataOffset1EventCallBack::push_event(): called attribute "
        << myevent->attr_name
        << " event "
        << myevent->event
        << " (err="
        << myevent->err
        << ")" << std::endl;

    if (!myevent->err)
    {
      // get attribute according to type
      switch (this->m_dataTrt.userDefinedParam.offset1Proxy.attributeType)
      {
        EVENT_ATTR_TYPE(Tango::DEV_SHORT, yat::int16);
        EVENT_ATTR_TYPE(Tango::DEV_LONG, yat::int32);
        EVENT_ATTR_TYPE(Tango::DEV_DOUBLE, double);
        EVENT_ATTR_TYPE(Tango::DEV_FLOAT, float);
        EVENT_ATTR_TYPE(Tango::DEV_USHORT, yat::uint16);
        EVENT_ATTR_TYPE(Tango::DEV_ULONG, yat::uint32);
        EVENT_ATTR_TYPE(Tango::DEV_LONG64, yat::int64);
        EVENT_ATTR_TYPE(Tango::DEV_ULONG64, yat::uint64);
        default:
        break;
      }

      std::cout << "double value "
                << double_value
                << std::endl;
      
      //- update offset1 according to state
      if (this->m_acqMode->getAcqState() == Tango::RUNNING)
      {
        this->m_acqMode->setUserDataOffset1(double_value, true);
      }
      else
      {
        this->m_acqMode->setUserDataOffset1(double_value, false);
      }
    }
  }
  catch (...)
  {
    std::cout << "UserDataOffset1EventCallBack::push_event(): could not extract data !\n";
  }
}

// ======================================================================
// UserDataOffset2EventCallBack::push_event
// ======================================================================
void UserDataOffset2EventCallBack::push_event(Tango::EventData *myevent)
{
  // TODO user data event : define one event per channel for gain, offset1 & 2
  double double_value = yat::IEEE_NAN;
  
  try
  {
    std::cout << "UserDataOffset2EventCallBack::push_event(): called attribute "
        << myevent->attr_name
        << " event "
        << myevent->event
        << " (err="
        << myevent->err
        << ")" << std::endl;

    if (!myevent->err)
    {
      // get attribute according to type
      switch (this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Proxy.attributeType)
      {
        EVENT_ATTR_TYPE(Tango::DEV_SHORT, yat::int16);
        EVENT_ATTR_TYPE(Tango::DEV_LONG, yat::int32);
        EVENT_ATTR_TYPE(Tango::DEV_DOUBLE, double);
        EVENT_ATTR_TYPE(Tango::DEV_FLOAT, float);
        EVENT_ATTR_TYPE(Tango::DEV_USHORT, yat::uint16);
        EVENT_ATTR_TYPE(Tango::DEV_ULONG, yat::uint32);
        EVENT_ATTR_TYPE(Tango::DEV_LONG64, yat::int64);
        EVENT_ATTR_TYPE(Tango::DEV_ULONG64, yat::uint64);
        default:
        break;
      }
    
      std::cout << "double value "
                << double_value
                << std::endl;
    
      //- update offset2 according to state
      if (this->m_acqMode->getAcqState() == Tango::RUNNING)
      {
        this->m_acqMode->setUserDataOffset2(chan, double_value, true);
      }
      else
      {
        this->m_acqMode->setUserDataOffset2(chan, double_value, false);
      }
    }
  }
  catch (...)
  {
    std::cout << "UserDataOffset2EventCallBack::push_event(): could not extract data !\n";
  }
}

// ======================================================================
// AcqMode::defineUserDataEvents
// ======================================================================
void AcqMode::defineUserDataEvents()
  throw (Tango::DevFailed)
{
  // Creates the device proxies from acquisition config
  Tango::DeviceProxy * device_proxy = NULL;
  yat::int32 event_id;
  

  // TODO user data event : define one event per channel for gain, offset1 & 2
  for (size_t chan = 0; chan < this->m_acqDefinition.activeChannels.size(); chan++)
  {
    if (this->m_acqDefinition.activeChannels[chan].userDataDesc.hasUserData)
    {
      // Is there a gain device proxy ?
      if (this->m_acqDefinition.activeChannels[chan].userDataDesc.gainType == USER_DATA_PROXY)
      {
        UserDataProxy proxy;
        proxy.deviceName = this->m_acqDefinition.activeChannels[chan].userDataDesc.gainProxy.deviceName;
        proxy.attributeName = this->m_acqDefinition.activeChannels[chan].userDataDesc.gainProxy.attributeName;

        if ( (!proxy.deviceName.empty()) &&
             (!proxy.attributeName.empty()) )
        {
          try
          {
            DEBUG_STREAM << "Create user data device proxy for gain parameter" << std::endl;
            device_proxy = new Tango::DeviceProxy(proxy.deviceName);

            UserDataGainEventCallBack * change_callback_user_data_gain = new UserDataGainEventCallBack(this);
            std::vector<std::string> filters;

            event_id = device_proxy->subscribe_event(
                                            proxy.attributeName,
                                            Tango::CHANGE_EVENT,
                                            change_callback_user_data_gain,
                                            filters);

            DEBUG_STREAM << "User data gain - event id = " << event_id << endl;
            
            this->m_acqDefinition.activeChannels[chan].userDataDesc.gainProxy.attributeType = 
              device_proxy->get_attribute_config(proxy.attributeName).data_type;
            
            this->m_evtProxies.insert(UserDataDevices_pair_t(device_proxy, event_id));
          }
          catch (Tango::DevFailed & df)
          {
            ERROR_STREAM << "AcqMode::defineUserDataEvents caught DevFailed: " << df << std::endl;
            RETHROW_DEVFAILED(df,
                              _CPTC("DEVICE_ERROR"),
                              _CPTC("Failed to create user data proxy (caught DevFailed)!"),
                              _CPTC("AcqMode::defineUserDataEvents"));
          }
          catch (...)
          {
            ERROR_STREAM << "AcqMode::defineUserDataEvents caught [...]" << std::endl;
            THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                            _CPTC("Failed to create user data proxy (caught [...])!"),
                            _CPTC("AcqMode::defineUserDataEvents"));
          }
        }
      }

      // Is there a offset1 device proxy ?
      if (this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Type == USER_DATA_PROXY)
      {
        UserDataProxy proxy;
        proxy.deviceName = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Proxy.deviceName;
        proxy.attributeName = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Proxy.attributeName;

        if ( (!proxy.deviceName.empty()) &&
             (!proxy.attributeName.empty()) )
        {
          try
          {
            DEBUG_STREAM << "Create user data device proxy for offset1 parameter" << std::endl;
            device_proxy = new Tango::DeviceProxy(proxy.deviceName);

            UserDataOffset1EventCallBack * change_callback_user_data_offset1 = new UserDataOffset1EventCallBack(this);
            std::vector<std::string> filters;

            event_id = device_proxy->subscribe_event(
                                            proxy.attributeName,
                                            Tango::CHANGE_EVENT,
                                            change_callback_user_data_offset1,
                                            filters);

            DEBUG_STREAM << "User data offset1 - event id = " << event_id << endl;

            this->m_acqDefinition.activeChannels[chan].userDataDesc.offset1Proxy.attributeType = 
              device_proxy->get_attribute_config(proxy.attributeName).data_type;

            this->m_evtProxies.insert(UserDataDevices_pair_t(device_proxy, event_id));
          }
          catch (Tango::DevFailed & df)
          {
            ERROR_STREAM << "AcqMode::defineUserDataEvents caught DevFailed: " << df << std::endl;
            RETHROW_DEVFAILED(df,
                              _CPTC("DEVICE_ERROR"),
                              _CPTC("Failed to create user data proxy (caught DevFailed)!"),
                              _CPTC("AcqMode::defineUserDataEvents"));
          }
          catch (...)
          {
            ERROR_STREAM << "AcqMode::defineUserDataEvents caught [...]" << std::endl;
            THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                            _CPTC("Failed to create user data proxy (caught [...])!"),
                            _CPTC("AcqMode::defineUserDataEvents"));
          }
        }
      }

      // Is there a offset2 device proxy ?
      if (this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Type == USER_DATA_PROXY)
      {
        UserDataProxy proxy;
        proxy.deviceName = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Proxy.deviceName;
        proxy.attributeName = this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Proxy.attributeName;

        if ( (!proxy.deviceName.empty()) &&
             (!proxy.attributeName.empty()) )
        {
          try
          {
            DEBUG_STREAM << "Create user data device proxy for offset2 parameter" << std::endl;
            device_proxy = new Tango::DeviceProxy(proxy.deviceName);

            UserDataOffset2EventCallBack * change_callback_user_data_offset2 = new UserDataOffset2EventCallBack(this);
            std::vector<std::string> filters;

            event_id = device_proxy->subscribe_event(
                                            proxy.attributeName,
                                            Tango::CHANGE_EVENT,
                                            change_callback_user_data_offset2,
                                            filters);

            DEBUG_STREAM << "User data offset2 - event id = " << event_id << endl;

            this->m_acqDefinition.activeChannels[chan].userDataDesc.offset2Proxy.attributeType = 
              device_proxy->get_attribute_config(proxy.attributeName).data_type;

            this->m_evtProxies.insert(UserDataDevices_pair_t(device_proxy, event_id));
          }
          catch (Tango::DevFailed & df)
          {
            ERROR_STREAM << "AcqMode::defineUserDataEvents caught DevFailed: " << df << std::endl;
            RETHROW_DEVFAILED(df,
                              _CPTC("DEVICE_ERROR"),
                              _CPTC("Failed to create user data proxy (caught DevFailed)!"),
                              _CPTC("AcqMode::defineUserDataEvents"));
          }
          catch (...)
          {
            ERROR_STREAM << "AcqMode::defineUserDataEvents caught [...]" << std::endl;
            THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                            _CPTC("Failed to create user data proxy (caught [...])!"),
                            _CPTC("AcqMode::defineUserDataEvents"));
          }
        }
      }

    } // end if user data enabled
  } // end for on channels
}
#endif

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_timestamp_histo_depth
//
//  description:  read callback function for timestamp histories depth.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_timestamp_histo_depth(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  if (this->m_modeInitializing)
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // set attribute value
	  static Tango::DevULong __ts_histo_depth__;
    __ts_histo_depth__ = static_cast<Tango::DevULong>(AcqMode::m_dataTrt.timestampHistoriesDepth);
    cbd.tga->set_value(&__ts_histo_depth__);
  }
}

//+----------------------------------------------------------------------------
//
// method : 		AcqMode::write_callback_timestamp_histo_depth
// 
// description : 	write callback function for timestamp histories depth.
//
//-----------------------------------------------------------------------------
void AcqMode::write_callback_timestamp_histo_depth(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = AcqMode::getAcqState();
  if ((Tango::RUNNING == state) || 
      (Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("AcqMode::write_callback_timestamp_histo_depth"));
  }

  Tango::DevULong histo_depth;
  cbd.tga->get_write_value(histo_depth);

  // Check if value not null
  if (histo_depth == 0)
  {
    THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                    _CPTC("request aborted - attribute cannot be null!"),
                    _CPTC("AcqMode::write_callback_timestamp_histo_depth"));
  }

  this->setTimestampHistoriesDepth(histo_depth);
  this->store_value_as_property(histo_depth, "__TimestampHistoriesDepth");
}

// ======================================================================
// AcqMode::setTimestampHistoriesDepth
// ======================================================================
void AcqMode::setTimestampHistoriesDepth(yat::uint32 depth)
  throw (Tango::DevFailed)
{
  yat::AutoMutex<> guard(this->m_dataLock);

  // re-allocate memory for timestamp history buffers

  if (this->m_dataTrt.timestamped) // if no timestamp required, nothing to do
  {
    this->m_dataTrt.timestampHistoriesDepth = depth;

    //- set buffer size (in number of samples)
    this->setTimestampHistoBufferMem(this->m_dataTrt.timestampHistoriesDepth);
  }
}

// ======================================================================
// AcqMode::setTimestampHistoBufferMem
// ======================================================================
void AcqMode::setTimestampHistoBufferMem(unsigned long depth)
{
  yat::AutoMutex<> guard(this->m_dataLock);

  if (this->m_dataTrt.timestamped) // if no timestamp required, nothing to do
  {
    // clean old histories
    if (m_dynAcqData.buffTimestamps)
    {
      m_dynAcqData.buffTimestamps->clear();
      delete m_dynAcqData.buffTimestamps;
      m_dynAcqData.buffTimestamps = NULL;
    }
    if (m_dynAcqData.relativeBuffTimes)
    {
      m_dynAcqData.relativeBuffTimes->clear();
      delete m_dynAcqData.relativeBuffTimes;
      m_dynAcqData.relativeBuffTimes = NULL;
    }

    // create new ones with specified depth
    m_dynAcqData.buffTimestamps = new BufferTimestamps_t(depth);
    m_dynAcqData.buffTimestamps->fill(yat::IEEE_NAN);

    m_dynAcqData.relativeBuffTimes = new BufferTimestamps_t(depth);
    m_dynAcqData.relativeBuffTimes->fill(yat::IEEE_NAN);
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_buffer_timestamps
//
//  description:    read callback function for buffer timestamps.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_buffer_timestamps(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {	
    // set attribute value
    const yat::Buffer<double> & data = AcqMode::m_dynAcqData.buffTimestamps->ordered_data();
    cbd.tga->set_value(data.base(), data.length()); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_relative_buffer_datation
//
//  description:    read callback function for relative buffer datation.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_relative_buffer_datation(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {	
    // set attribute value
    const yat::Buffer<double> & data = AcqMode::m_dynAcqData.relativeBuffTimes->ordered_data();
    cbd.tga->set_value(data.base(), data.length()); 
  }
}

//+------------------------------------------------------------------
//
//  method:  AcqMode::read_callback_enable_dataset
//
//  description:  read callback function for enable/disable dataset.
//
//+------------------------------------------------------------------
void AcqMode::read_callback_enable_dataset(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  //DEBUG_STREAM << "read request for attribute: " 
  //          << attrName
  //          << std::endl;

  Tango::DevState state = this->getAcqState();

  if ((state == Tango::FAULT) ||
      (state == Tango::UNKNOWN) ||
      (this->m_modeInitializing))
  {
    cbd.tga->set_quality(Tango::ATTR_INVALID);
  }
  else
  {
    // extract channel label from attribute name
    std::string label = attrName.substr(kATTR_NAME_TAG_DATASET.size(), std::string::npos);

    // get channel id from label
    yat::uint16 chan_id = get_channel_id_from_label(label);

    // set attribute value
    cbd.tga->set_value(&(m_acquisitionParam.datasetFlags[chan_id])); 
  }
}

//+----------------------------------------------------------------------------
//
// method : 		AcqMode::write_callback_enable_dataset
// 
// description : 	write callback function for enable/disable dataset.
//
//-----------------------------------------------------------------------------
void AcqMode::write_callback_enable_dataset(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
  std::string attrName = cbd.dya->get_name();

  /*
  std::cout << "write request for attribute: " 
            << attrName
            << std::endl;
  */

  //- check acquisition state
  Tango::DevState state = AcqMode::getAcqState();
  if ((Tango::RUNNING == state) || 
      (Tango::MOVING == state) ||
      (this->m_modeInitializing))
  {
    THROW_DEVFAILED(_CPTC("BAD_STATE"),
                    _CPTC("request aborted - attribute not writable in this state"),
                    _CPTC("AcqMode::write_callback_enable_dataset"));
  }

  // extract channel label from attribute name
  std::string label = attrName.substr(kATTR_NAME_TAG_DATASET.size(), std::string::npos);

  // get channel id from label
  yat::uint16 chan_id = get_channel_id_from_label(label);

  Tango::DevBoolean enable;
  cbd.tga->get_write_value(enable);

  m_acquisitionParam.datasetFlags[chan_id] = enable;
  
  std::string name = "__EnableDataset" + label;
  this->store_value_as_property(enable, name);
}

// ======================================================================
// AcqMode::getNxFileBaseName
// ======================================================================
std::string AcqMode::getNxFileBaseName()
{
  std::string nexusFileName = "";

  if (this->m_dataTrt.nxFileName.empty())
  {
    //- default nexus file name: built with the Device name & the board number to have an unique Nx file name
    std::string devName = this->m_hostDev->get_name();
    //- replace "/" by "_" in Device name
    std::string::size_type idx;
    while ( (idx = devName.find("/")) != std::string::npos )
    {
	  devName.replace(idx, 1, "_");
    }

    // default value
    nexusFileName = devName + "_" + yat::XString<size_t>::to_string(this->m_boardImpl->board_model_id()) + "_board_" + yat::XString<size_t>::to_string(this->m_boardId);
  }
  else
  {
    nexusFileName = this->m_dataTrt.nxFileName;
  }

  return nexusFileName;
}
  
// ======================================================================
// AcqMode::getChannelNxInfo
// ======================================================================
std::map<yat::uint16, std::string> AcqMode::getChannelNxInfo()
  throw (Tango::DevFailed)
{
  std::map<yat::uint16, std::string> infos;

  // get number of active channels
  size_t num_of_active_channels = getActiveChanNb();

  // parse list of active channels for current configuration
  for (size_t idx = 0; idx < num_of_active_channels; idx++)
  {
    yat::uint16 chan_nb = this->m_acqDefinition.activeChannels[idx].number;
    std::string chan_label = this->m_acqDefinition.activeChannels[idx].label;
    std::string dataset_name = "";
    bool dataset_enabled = false;
    yat::OSStream oss;
	std::string separator = ";";

    // the string contains:
    /*      
    NAME:<channel label>  
    ENABLED:true // always true in SAI case (if defined, enabled)
    DATASET_NAME:<name of the dataset>
    DATASET_ENABLED:<true / false>
    DATASET_ATTR:<name of the dataset enabled TANGO attribute>
    */

    oss << "NAME:" << chan_label << separator;
    oss << "ENABLED:true" << separator;

    if (this->m_datasetNames[chan_nb].count(kKEY_DATA_NX_DATA_TYPE_SCALED) != 0)
      dataset_name = this->m_datasetNames[chan_nb][kKEY_DATA_NX_DATA_TYPE_SCALED];
    else
      dataset_name = chan_label;

    oss << "DATASET_NAME:" << dataset_name << separator;

    if (this->m_acquisitionParam.datasetFlags.count(chan_nb) != 0)
      dataset_enabled = this->m_acquisitionParam.datasetFlags[chan_nb];

    std::string tof = dataset_enabled ? "true" : "false"; 
    oss << "DATASET_ENABLED:" << tof << separator;

    oss << "DATASET_ATTR:" << kATTR_NAME_TAG_DATASET << chan_label;

    infos.insert( pair<yat::uint16, std::string>(chan_nb, oss.str()) );
  }

  return infos;
}

} // namespace aicontroller


