//=============================================================================
// AcqModeFactory.h
//=============================================================================
// abstraction.......Acquisition mode factory for AIControllerV2 Device
// class.............AcqModeFactory
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _ACQ_MODE_FACTORY_H_
#define _ACQ_MODE_FACTORY_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/ExceptionHelper.h>
#include "AcqMode.h"

namespace aicontroller
{

// ============================================================================
// class: AcqModeFactory
// ============================================================================
class AcqModeFactory
{
public: 
  //- instanciate a specialized Acquisition Mode
  static AcqMode * instanciate (E_TriggerMode_t trigger_mode, Tango::DeviceImpl * host_device)
    throw (Tango::DevFailed);

private:
  AcqModeFactory ();
  ~AcqModeFactory ();
};

} // namespace aicontroller

#endif // _ACQ_MODE_FACTORY_H_
