@startuml AIController class diagram
title
<font color=red>AIController</font>
<b>Diagram Class</b>
Partial diagram of the configuration
01/10/2021
<u>Guillaume Pichon - Synchrotron SOLEIL</u>
end title

class Tango::Device_4Impl
{
}

class yat4tango::DeviceTask
{
    #process_message(yat::Message& msg)=0;
}

class AIController
{
    +virtual void write_configurationId(Tango::WAttribute &attr);
	+virtual void read_configurationName(Tango::Attribute &attr);
	+virtual void read_samplesNumber(Tango::Attribute &attr);
	+virtual void read_frequency(Tango::Attribute &attr);
	+virtual void write_frequency(Tango::WAttribute &attr);
	+virtual void read_integrationTime(Tango::Attribute &attr);
	+virtual void write_integrationTime(Tango::WAttribute &attr);
	+virtual void read_nexusFileGeneration(Tango::Attribute &attr);
	+virtual void write_nexusFileGeneration(Tango::WAttribute &attr);
	+virtual void read_nexusTargetPath(Tango::Attribute &attr);
	+virtual void write_nexusTargetPath(Tango::WAttribute &attr);
	+virtual void read_nexusNbAcqPerFile(Tango::Attribute &attr);
	+virtual void write_nexusNbAcqPerFile(Tango::WAttribute &attr);
}

class AIManager
{
    #AcquisitionParameters m_currentAcqParam;
    #AIConfiguration m_aiConfiguration;
    #AcqMode* m_currentAcquisition;
    #short m_currentConfigPos;

    +void updateDaq(yat::uint16 config_id)
    #void updateDaq_i(yat::uint16 config_id)
}

note left of AIManager::m_currentConfigPos
    The current acquisition index in config vector
    contained in AIManager::m_aiConfiguration
end note
note right of AIManager::updateDaq
    This method post the message to switch from the
    current configuration to another.
end note
note right of AIManager::updateDaq_i
    This method switch the configuration.
    It's called in the task thread.
end note

class AcqModeFactory
{
	static AcqMode* instanciate(...);
}

class AIConfiguration
{
      +std::vector<RawAcquitisionConfig> acqDefinitions;
}

class RawAcquitisionConfig
{
    +yat::uint16 configId;
    +AcquisitionKeys_t configKeyList;
}
note right of RawAcquitisionConfig::configKeyList
    List of <key,value> config parameters.
end note

class AcqMode
{
	+void cleanInterface();
	+void configureAcquisition
}

class AcquisitionParameters
{
    +yat::uint16 configId;
    +std::string configName;
    +yat::uint32 samplesNumber;
    +double frequency;
    +double integrationTime;
    +bool nexusFileGeneration;
    +std::string nexusTargetPath;
    +yat::uint16 nexusNbPerFile;  
    +DatasetFlags_t datasetFlags;
}

Tango::Device_4Impl <|--- AIController
yat4tango::DeviceTask <|--- AIManager
AIController *--- AIManager
AIManager *--- AcquisitionParameters
AIManager *--- AIConfiguration
AIManager *--- AcqMode
AIConfiguration *--- "1..*" RawAcquitisionConfig

@enduml

@startuml AIController sequence diagram
title
<font color=red>AIController</font>
<b>Sequence Class</b>
Partial diagram of the configuration modifications
01/10/2021
<u>Guillaume Pichon - Synchrotron SOLEIL</u>
end title

actor User
group Thread user
	User -> AIController : write_configurationId(short configId)
	AIController -> AIManager : updateDaq(short configId)
	AIManager -> AIManager : post(kUPDATE_CONFIG_MSG)
	AIController <-- AIManager
	User <-- AIController
end

hnote over AIManager:idle

group Thread task AIManager
	AIManager -> AIManager : process_message(kUPDATE_CONFIG_MSG)
	AIManager -> AIManager : updateDaq_i(short config_id)
	AIManager -> AcqModeFactory : instanciate(...)
	AIManager <-- AcqModeFactory : AcqMode*
	note right
		m_currentAcquisition = AcqModeFactory::instanciate(...)
	end note
	AIManager -> AIManager : m_currentAcqParam.configId =\n\tm_aiConfiguration.acqDefinitions[m_currentConfigPos].configId;\nm_currentAcqParam.configName = config_parser.extractConfigName(\n\tm_aiConfiguration.acqDefinitions[m_currentConfigPos].configKeyList);
	AIManager -> AcqMode : configureAcquisition(\n\tm_aiConfiguration.acqDefinitions[m_currentConfigPos],\n\tm_aiConfiguration.acqBufferConfig,\n\t<b>m_currentAcqParam</b>);
	note right
		This is where the value of the 
		nexusFileGeneration attribute is
		send to the new configuration
		along with others values in class
		AcquisitionParameters.
	end note

end

hnote over AIManager:idle

group Thread user
	User -> AIController : write_nexusFileGeneration(bool nexusFileGeneration)
	AIController -> AIManager : setNexusFileGeneration(bool nexusFileGeneration)
	AIManager -> AIManager : m_currentAcquisition->setNexusFileGeneration(enable);\nm_currentAcqParam.nexusFileGeneration = enable;
	AIController <-- AIManager
	User <-- AIController
end
note right
	This is a synchronous call, it must be
	done while the device is not in state :
	\tRUNNING, INIT, MOVING
end note

@enduml
