from conan import ConanFile

class AIControllerV2Recipe(ConanFile):
    name = "aicontrollerv2"
    executable = "ds_AIControllerV2"
    version = "2.9.4"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Sonia Minolli, Nicolas Leclercq"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/inputoutput/adlink/aicontrollerv2.git"
    description = "AIControllerV2 Device to interface ADLink digital and analog input boards through the ASL library."
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("nexuscpp/[>=3 <4]@soleil/stable")
        self.requires("asl/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
